C to All Language(lua)  binding 
================================

一，介绍  
---------
这是把一个c函数声明绑定所有语言的处理工具

该项目现在是实验阶段，测试生成有openGL,SDL2 的lua绑定

二，原理或过程
--------------
利用gcc 选项-fdump-translation-unit 生成中间解析树，再利用lua脚本处理解析树，最后根据树生成语言绑定
note:原本想利用clang生成的ast来解析的，发现太麻烦了，后来想利用生成的中间码(类似lisp)，不过还是麻烦。最后还是选择了'translation-unit'

三，现阶段支持
---------------
函数声明导出
支持: 基本类型支持，指针类型(void*) 支持
未支持:回调函数，结构体(struct)参数，enum导出,struct导出

四，未来计划
------------
### 1.enum导出
### 2.struct导出
### 3.宏变量

五,数据类型转换
---------------
##### string    -> void*
##### userdata  -> *(void**)
##### lightuserdata -> void*
##### integer   -> int
##### number    -> double
##### function  -> error

