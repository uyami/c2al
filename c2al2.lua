function GetFileDir(f)
    f = string.reverse(f);
    local pos = f:find('\\');
    local pos2 = f:find('/');
    if(pos and pos2)then 
        if(pos>pos2)then pos = pos2;end;
    elseif(pos==nil)then
        pos = pos2;
    end
    if(pos==nil)then
        return "./"
    end;
    return string.reverse(f:sub(pos));
end

function ScriptDir()
    local info = debug.getinfo(ScriptDir);
    local dir = info.source;
    dir = dir:sub(2);
    return GetFileDir(dir);
end

function LoadTu(fname)
    local f = io.open(fname,"rb");
    local s = f:read("*a");
    parse(s);
    f:close();
end

package.path = package.path .. ';' .. ScriptDir().."lib/?.lua"


require('base');
require('parser');
require('exportLua');

---------------------------------------------------------------------------------------------------------

outfile = io.stdout;
outfilename = nil;
inputfile = nil;
filter = nil;
enable_struct = false;
enable_enum = true;
load_files = {};
do_files = {};
out_debug = false;
lib_name = nil;
Remove = {};
dumpSource= {};
dumpSourceFlag = nil;
cpp2cfunc = nil;
isDump = function(name,decl) return false; end;

arg = {
[[C:\Users\yami\Documents\hello\qttest.tu]],
'-lang',
'luacpp',
}

local k = 0;
while(k<#arg) do
    k = k + 1;
    local v = arg[k];
    if(v:sub(1,1)~="-")then
        inputfile = v;
    else 
        if(v=="-o" or v:sub(1,2)=="-o")then
            --out file
            local out;
            if(v=="-o")then
                out = arg[k+1];
                k = k + 1;
            else
                out = v:sub(3);
            end
            outfilename = out;
            outfile = io.open(out,"w+");
        elseif(v=="-l")then
            --load lua file
            --设置一些选项接下来导出数据
            --和-d 选项不同
            table.insert(load_files,arg[k+1]);
            k = k + 1;
        elseif(v=='-t')then
            --开启类型导出
            typeExport = true;
        elseif(v=='-tn')then
            typeExport = false;
        elseif(v=="-d")then
            --do lua file
            --执行lua脚本, 不会去生成数据
            --可以调用函数来生成数据
            table.insert(do_files,arg[k+1]);
            k = k + 1;
        elseif(v=="-f")then
            --filter
            filter = arg[k+1];
            k = k + 1;
        elseif(v=="-es")then
            --enable struct out
            enable_struct = true;
        elseif(v=="-de")then
            enable_enum = false;
        elseif(v=="-ed")then
            --enable out debug
            out_debug = true;
        elseif(v=="-name")then
            lib_name = arg[k+1];
            k = k + 1;
        elseif(v=="-dllname")then
            dll_name = arg[k+1];
            k = k + 1;
        elseif(v=="-lang")then
            langdump = arg[k+1];
            k = k + 1;
        elseif(v=="-sed")then
            sortEnumDump = true;
        elseif(v=="-nolink")then
            nolink = true;
        elseif(v=='-remove')then
            local name = arg[k+1];
            Remove[name] = k+1;
            k = k +1;
        elseif(v=='-header')then
            local name = arg[k+1];
            dumpSource[name] = k + 1;
            dumpSourceFlag = true;
            k = k + 1;
        elseif(v=='-cppc')then
            cpp2cfunc = true;
        elseif(v=='-enum2int')then
            enum2int = true;
        elseif(v=='-basetype')then
            --把基本类型别名全部转为基本类型
            basetype = true;
        elseif(v=='-dumpbin')then
            if(dumpbins==nil)then dumpbins={}; end;
            table.insert(dumpbins,arg[k+1]);
            k = k + 1;
        end

    end
end

print("input  :",inputfile);
print("out    :",outfile);
print("filter :",filter);
if(lib_name)then
print("libName:",lib_name);
end
if(langdump)then
print("lang   :",langdump);
end
if(enable_struct)then
print("struct : true");
end

LoadTu(inputfile);

for k,v in ipairs(load_files) do
    dofile(v);
end

if(dumpbins)then
    dumpFuncs = {};
    for k,v in ipairs(dumpbins)do
        for line in io.lines(v)do
            --print(line:sub(27));
            dumpFuncs[line:sub(27)] = 1;
        end
    end
    if(filter==nil)then
        filter = 'dumpbins';
    end
    isDump = function(name)
        --print(name);
        return dumpFuncs[name];
    end
end

if(#do_files>0)then
    for k,v in ipairs(do_files)do
        dofile(v);
    end;
else
    if(langdump)then
        lang = dofile("lang/"..langdump..".lua");
        lang.outfile = outfile;
        --ldump_function(filter,outfile);
        if(lang.exportFunction)then
            --lang.func_out = io.stdout;
            lang.filter = filter;
            lang.exportFunction(lang);
        end
        if(lang.exportType)then
            lang.exportType(lang);
        end
    else
        dumpFunctions2lua(filter,outfile);
    end
end

if(lang and lang.exportEnd)then
    lang.exportEnd(lang);
end


