
local function getEnumName(enum)
    local tree = enum.root;
    if(enum.name)then
        local name = tree[enum.name];
        if(name.code=="type_decl")then
            name = tree[name.name];
        end
        return name.strg; --identifier_node.strg
    end
    return nil;
end

local function getFirstField(enum)
    local tree = enum.root;
    return tree[enum.csts]; --tree_list;
end

local function getNextField(tree_list)
    local tree = tree_list.root;
    if(tree_list.chan)then
        return tree[tree_list.chan];
    end
    return nil;
end

local function getFieldName(tree_list)
    local tree = tree_list.root;
    if(tree_list.purp)then
        return tree[tree_list.purp].strg;
    end
    return nil;
end

local function getFieldValue(tree_list)
    local tree = tree_list.root;
    if(tree_list.valu)then
        local integer_cst = tree[tree_list.valu];
        return tree[tree_list.valu].int;
    end
    print(tree_list.id);
    assert(0);
end

function dumpEnums2lua(f,out)
    local dumpAll = false;
    local len;
    local dump_list = {};
    --local out = io.stdout;
    
    --如果为空，则调用 stderr FILE*
    if(out==nil)then
        out = io.stderr;
    elseif(type(out)=="string")then --如果是字符串 则，打开out文件  FILE*
        out = io.open(out,"w+");
    end
    
    if(f)then
        len = #f;
    else
        dumpAll = true;
        f = "test"
    end
    local t;
    
    out:write([[
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

]]);
    local denums = {};
    local dfields = {};
    --sortEnumDump = 
    for k,v in ipairs(enumeral_type) do
        local enumName = getEnumName(v);
        local dumpAllField = false;
        if(dumpAll==false and enumName and enumName:sub(1,len)==f)then
            dumpAllField = true;
        end
        if(dumpAllField==false and isDump(enumName,v)) then
            dumpAllField = true;
        end
        if(sortDump)then
            dfields = {};
        end
        
        local ff = getFirstField(v);
        if(ff)then
            table.insert(dfields,ff);
            while(true)do
                local n = getFieldName(ff);
                ff = getNextField(ff); --next
                if(ff)then
                    if(dumpAll or dumpAllField or isDump(n,ff))then
                        table.insert(dfields,ff);
                    else
                        local n = getFieldName(ff);
                        if(n and n:sub(1,len)==f)then
                            table.insert(dfields,ff);
                        end
                    end
                else
                    break;
                end
            end
        end

        if(sortEnumDump==nil)then
            -- for kk,vv in ipairs(dfields)do
                -- out:write(string.format('  lua_registerInt(L,"%s",%s);\n',getFieldName(vv),getFieldValue(vv)));
            -- end
        else
            table.sort(dfields,function(a,b)
                local aa = getFieldName(a);
                local bb = getFieldName(b);
                return aa<bb;
            end)
            dfields.name = enumName;
            table.insert(denums,dfields);
        end
    end
    if(sortEnumDump)then
        table.sort(denums,function(a,b)
            local aa = a.name;
            local bb = b.name;
            if(aa==nil)then aa = "";end;
            if(bb==nil)then bb = "";end;
            return aa<bb;
        end);
        for k,v in ipairs(denums)do
            out:write(string.format("  //enum %s\n",v.name));
            for kk,vv in ipairs(v)do
                out:write(string.format('  lua_registerInt(L,"%s",%s);\n',getFieldName(vv),getFieldValue(vv)));
            end
            out:write(string.format("  //enum end %s\n\n",v.name));
        end
    else
        table.sort(dfields,function(a,b)
            local aa = getFieldName(a);
            local bb = getFieldName(b);
            return aa<bb;
        end);
        for kk,vv in ipairs(dfields)do
            out:write(string.format('  lua_registerInt(L,"%s",%s);\n',getFieldName(vv),getFieldValue(vv)));
        end
    end
    
    
    out:write("\n\n");
end

