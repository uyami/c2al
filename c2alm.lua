
if(split==nil)then
    split = function(s,d)
        local t = {};
        local p = "(.-)"..d;
        s = s .. d;
        for match in s:gmatch(p) do
            table.insert(t,match);
        end
        return t;
    end
end

if(string.trim==nil)then
    function string.trim(s)
        return (s:gsub("^%s*(.-)%s*$", "%1"))
    end
end

macros = {};

function getMacro(s)
    local p = '#define ([0-9a-zA-Z_]-[^(]) ([0-9a-zA-Z_"]-[^(])\n';
    for k,v in s:gmatch(p) do
        local vv = {};
        vv.name = k;
        vv.value = v;
        table.insert(macros,vv);
        --print(k,v);
    end
end

function parseM(code)
    getMacro(code);
end

function dumpLuaMacro(n,ofile)
    local dumpAll = false;
    local len;
    local out = io.stdout;
    if(ofile)then
        out = io.open(ofile,"w+");
    end
    if(n)then
        len = #n;
    else
        dumpAll = true;
        n = "test";
    end
    local t = {};
    if(dumpAll==false)then
        for k,v in pairs(macros) do
            if(v.name:sub(1,len)==n)then
                table.insert(t,v);
            end
        end
    else
        t = macros;
    end
    
    table.sort(t,function(a,b)
        return a.name<b.name;
    end 
    );
    
    out:write("return {\n");
    for k,v in ipairs(t) do
        s = string.format("  %s=%s,\n",v.name,v.value);
        out:write(s);
    end
    out:write("};\n\n");
    
end

local file = io.open(arg[1],"r");
local code = file:read("*a");
parseM(code);
dumpLuaMacro("GL",arg[2]);
