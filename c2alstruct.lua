local function getName(record_type)
    local tree = record_type.root;
    if(record_type.unql)then
        local type_decl = tree[record_type.unql];
        if(type_decl.name==nil)then
            return nil;
        end
        return tree[type_decl.name].strg;
    end
    if(record_type.name)then
        return tree[record_type.name].strg;
    end 
end

local function getFirstField(record_type)
    local tree = record_type.root;
    if(record_type.flds)then 
        local field_decl = tree[record_type.flds];
        return field_decl;
    end
end

local function getNextField(field_decl)
    local tree = field_decl.root;
    if(field_decl.chain==nil)then return nil; end;
    field_decl = tree[field_decl.chain];
    return field_decl;
end

local function getFieldBitPos(field_decl)
    local tree = field_decl.root;
    return tree[field_decl.bpos].int;
end

local function getFieldBitSize(field_decl)
    local tree = field_decl.root;
    return tree[field_decl.size].int;
end

local function getFieldType(field_decl)
    local tree = field_decl.root;
    return tree[field_decl.type];
end

local function getFieldTypeName(field_decl)
    local tree = field_decl.root;
    local typ = getFieldType(field_decl);
    local point;
    if(typ.code=="array_type")then
        typ = tree[typ.elts];
    end
    while(typ.name==nil and typ.code=="pointer_type")do
        if(point==nil)then
            point = "";
        else
            point = point .. "*";
        end
        if(tree[typ.ptd].code=="void_type")then
            return "void*" .. point;
        end
        typ = tree[typ.ptd];
    end
    if(point==nil)then point = "";end;
    if(typ.code=="integer_type" or typ.code=="real_type" or typ.code=="pointer_type" or typ.code=="record_type")then
        while(typ.unql~=nil and typ.code~="pointer_type")do
            typ = tree[typ.unql];
        end
        typ = tree[typ.name];
        if(typ.code=="identifier_node")then
            return typ.strg .. point;
        end
    end
    if(typ.name==nil and typ.code=="union_type")then
        return "__union".. point;
    end
    if(typ.name==nil and typ.code=="function_type")then
        return "__callback".. point;
    end
    if(typ.code=="enumeral_type")then
        return "int".. point;
    end
    --print(typ.id);W
    return tree[typ.name].strg.. point;
end

local function getFieldName(field_decl)
    local tree = field_decl.root;
    if(field_decl.name==nil)then return nil end;
    return tree[field_decl.name].strg;
end

local function getStructSize(field_decl)
    local tree = field_decl.root;
    if(field_decl.size)then
        return tree[field_decl.size].int;
    end
    return nil;
end

function dumpStructs2lua(f,out)
    local dumpAll = false;
    local len;
    local dump_list = {};
    if(out==nil)then
        out = io.stderr;
    elseif(type(out)=="string")then --如果是字符串 则，打开out文件  FILE*
        out = io.open(out,"w+");
    end
    
    if(f)then
        len = #f;
    else
        dumpAll = true;
        f = "test"
    end
    local dumpStructs = {};
    for k,v in ipairs(record_type) do
        local name = getName(v);
        if(dumpAll or isDump(name,v) or (name and name:sub(1,len)==f))then
            table.insert(dumpStructs,v);
        end
    end
    for k,v in ipairs(union_type)do
        local name = getName(v);
        if(dumpAll or isDump(name,v) or (name and name:sub(1,len)==f))then
            table.insert(dumpStructs,v);
        end
    end
    table.sort(dumpStructs,function(a,b)
        local aa = getName(a);
        local bb = getName(b);
        return aa<bb;
    end
    );
    
    local s = [[
  lua_getglobal(L,"structs");
  if(lua_isnil(L,-1)){
    lua_newtable(L);
    lua_setglobal(L,"structs");
    lua_getglobal(L,"structs");
  };
  
#ifndef lua_StructBegin
#define lua_setField(name,bpos,bsiz,type,typeName)  \
    lua_createtable(L,4,1);\
    lua_pushinteger(L,bpos);lua_setfield(L,-2,"bpos");\
    lua_pushinteger(L,bsiz);lua_setfield(L,-2,"bsiz");\
    lua_pushstring(L,type);lua_setfield(L,-2,"type");\
    lua_pushstring(L,typeName);lua_setfield(L,-2,"tname");\
    lua_setfield(L,-2,name);

#ifndef lua_StructSize
#define lua_StructSize(siz)   lua_pushinteger(L,siz); lua_setfield(L,-2,"1size");
#endif
#define lua_StructBegin(len)  lua_createtable(L,len+1,4);
#define lua_StructEnd(name)   lua_setfield(L,-2,name);
#endif
  
]];
    if(#dumpStructs>0)then
        out:write(s);
    else
        out:write("//no structs\n");
    end
    
    local dumplist = {};
    for k,v in ipairs(dumpStructs)do
        local name = getName(v)
            if(dumplist[name]==nil)then
            if(out_debug)then
                print("struct ".. name .. "{");
                print(v.id);
            end
            dumplist[name] = v;
            local field = getFirstField(v);
            local fields = {};
            if(field==nil)then
                --没有成员字段
                -- typedef struct xxx xxx;
            end
            while(true)do
                if(field==nil)then
                    break;
                end
                table.insert(fields,field);
                --print("  "..getFieldName(field)..":"..getFieldBitPos(field)..":" .. getFieldBitSize(field));
                field = getNextField(field);
            end
            
            --[[
            
            structs[name] = {
                fieldName = {
                    bpos = bitPos,
                    bsiz = bitSize,
                    type = name,
                },
            };
            
            lua_createtable(L,#fields+1,4);   //__gc
              lua_createtable(L,4,1);        //
                  lua_pushinteger(L,field.bpos);
                  lua_setfield(L,-2,"bpos");
                  lua_pushinteger(L,field.bsiz);
                  lua_setfield(L,-2,"bsiz");
                  lua_pushstring(L,field.type);
                  lua_setfield(L,-2,"type");
              lua_setfield(L,-2,field.name); 
              
#define lua_setField(name,bpos,bsiz,type,typeName)  \
    lua_createtable(L,4,1);\
    lua_pushinteger(L,bpos);lua_setfield(L,-2,"bpos");\
    lua_pushinteger(L,bsiz);lua_setfield(L,-2,"bsiz");\
    lua_pushstring(L,type);lua_setfield(L,-2,"type");\
    lua_pushstring(L,typeName);lua_setField(L,-2,"tname");\
    lua_setfield(L,-2,name);
    
#define lua_StructBegin(len)  lua_createtable(L,len+1,4);
#define lua_StructEnd(name)   lua_setfield(L,-2,name);
    
            ]]
            local fields_out = "";
            for kk,vv in ipairs(fields) do
                print(v.id,vv.id,name);
                fields_out = fields_out .. string.format('    lua_setField("%s",%s,%s,"%s","%s");\n',
                getFieldName(vv),getFieldBitPos(vv),getFieldBitSize(vv),getFieldType(vv).code,getFieldTypeName(vv)
                );
            end
            
            local structout = [[
  lua_StructBegin(%d);
%s  %s
  lua_StructEnd("%s");
  
]];
            local structsize;
            local size = getStructSize(v);
            if(size)then
                structsize = string.format('lua_StructSize(%s);\n',getStructSize(v));
            else
                structsize = "\n";
            end
            structout = string.format(structout,#fields+1,fields_out,structsize,name);
            --print(structout);
            out:write(structout);
            if(out_debug)then
                print("}");
            end
        end
    end
end