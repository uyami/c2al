#define DEFCF(retn,name,...)  static retn (*c##name)(__VA_ARGS__) = 0

#define dlloadsym(dll,name) c#name = dlsym(dll,#name);

#ifdef   _WIN32
 #include <windows.h>
 #define dlopen(n,...) LoadLibrary(n)
 #define dlsym         GetProcAddress
 #define dlclose       FreeLibrary

#else
 #include <dlfcn.h>
#endif


