--[[
C# dump
]]

local cs_keywords = {
["abstract"]=1,["as"]=1,["base"]=1,["bool"]=1,["break"]=1,["byte"]=1,["case"]=1,["catch"]=1,["char"]=1,["checked"]=1,["class"]=1,["const"]=1,["continue"]=1,["decimal"]=1,["default"]=1,["delegate"]=1,["do"]=1,["double"]=1,["else"]=1,["enum"]=1,["event"]=1,["explicit"]=1,["extern"]=1,["false"]=1,["finally"]=1,["fixed"]=1,["float"]=1,["for"]=1,["foreach"]=1,["goto"]=1,["if"]=1,["implicit"]=1,["in"]=1,["int"]=1,["interface"]=1,["internal"]=1,["is"]=1,["lock"]=1,["long"]=1,["namespace"]=1,["new"]=1,["null"]=1,["object"]=1,["operator"]=1,["out"]=1,["override"]=1,["params"]=1,["private"]=1,["protected"]=1,["public"]=1,["readonly"]=1,["ref"]=1,["return"]=1,["sbyte"]=1,["sealed"]=1,["short"]=1,["sizeof"]=1,["stackalloc"]=1,["static"]=1,["string"]=1,["struct"]=1,["switch"]=1,["this"]=1,["throw"]=1,["true"]=1,["try"]=1,["typeof"]=1,["uint"]=1,["ulong"]=1,["unchecked"]=1,["unsafe"]=1,["ushort"]=1,["using"]=1,["using static"]=1,["virtual"]=1,["void"]=1,["volatile"]=1,["while"]=1,
}

local tocs_basetype = {
["unsigned int"]="uint",
["short unsigned int"] = "ushort",
["unsigned char"] = "byte",
["long unsigned int"] = "uint",
["long long unsigned int"] = "ulong",
["signed char"] = "sbyte",
["char"] = "char",
["long long int"] = "long",
["long int"] = "int",
["short int"] = "short",
["unsigned int*"]="uint*",
["short unsigned int*"] = "ushort*",
["unsigned char*"] = "byte*",
["long unsigned int*"] = "uint*",
["long long unsigned int*"] = "ulong*",
["signed char*"] = "sbyte*",
["char*"] = "char*",
["long long int*"] = "long*",
["long int*"] = "int*",
["short int*"] = "short*",
}


local function getName(t)
    local tree = t.root;
    if(t.name==nil)then return ""; end;
    if(tree[t.name].strg)then return tree[t.name].strg;end;
    return "";
end

local function getPrms(t)
    local tree = t.root;
    local ftype = tree[t.type];
    if(ftype.prms==nil)then
        return {};
    end
    local prms = tree[ftype.prms];
    local p = {};
    local next = prms;
    while(true)do
        local typ = tree[next.valu];
        if(next.chan)then
            --local t = getTypeName(typ);  --Name
            local t = typ;
            table.insert(p,t);
        else
            table.insert(p,typ);
            break;
        end
        next = tree[next.chan];
    end
    return p;
end

local function getRetrnType(t)
    local tree = t.root;
    local ftype = tree[t.type];
    if(ftype.retn)then return tree[ftype.retn];end;
end

local function getPrmsName(v)
    local t = {};
    local tree = v.root;
    local args = tree[v.args];
    while(args)do
        table.insert(t,getName(args));
        args = tree[args.chain];
    end
    return t;
end

local function isString(v)
    local tree = v.root;
    if(v.code=="pointer_type")then
        local ptd = tree[v.ptd];
        --const char
        if(ptd.code=="integer_type" and ptd.prec=="8" and ptd.sign=="signed" and ptd.qual=="c")then
            return true;
        end
    end
    return false;
end

local function getFirstField(enum)
    local tree = enum.root;
    return tree[enum.csts]; --tree_list;
end

local function getFieldName(tree_list)
    local tree = tree_list.root;
    if(tree_list.purp)then
        return tree[tree_list.purp].strg;
    end
    return nil;
end

local function getNextField(tree_list)
    local tree = tree_list.root;
    if(tree_list.chan)then
        return tree[tree_list.chan];
    end
    return nil;
end

local function getFieldValue(tree_list)
    local tree = tree_list.root;
    local integer_cst = tree[tree_list.valu];
    
    if(integer_cst.code=="const_decl")then
        --c++
        local const_decl = integer_cst;
        integer_cst = tree[const_decl.cnst];
    end
    if(integer_cst.int)then
        return integer_cst.int;
    end
    assert(0);
end

local function getEnumName(enum)
    local tree = enum.root;
    if(enum.name)then
        local name = tree[enum.name];
        if(name.code=="type_decl")then
            name = tree[name.name];
        end
        return name.strg; --identifier_node.strg
    end
    return nil;
end

local function getStructName(type_decl)
    local tree = type_decl.root;
    if(type_decl.name)then
        local name = tree[type_decl.name];
        return name.strg;
    end
    return nil;
    --print("error struct name:",type_decl.id);
    --assert(0);
end

local function getStructFirstField(type_decl)
    local tree = type_decl.root;
    if(tree[type_decl.type].code=="union_type")then
        local union_type = tree[type_decl.type];
        if(union_type.flds)then
            return tree[union_type.flds];
        end
    elseif(tree[type_decl.type].code=="record_type")then
        local record_type = tree[type_decl.type];
        if(record_type.flds)then
            return tree[record_type.flds];
        end
    end
end

local function getPointTypeName(pointer_type)
    local tree = pointer_type.root;
    if(pointer_type.code=="pointer_type")then
        local typ = tree[pointer_type.ptd];  --record_type
        if(typ.code=="void_type")then
            return "IntPtr";
        end
        if(typ.code=="pointer_type")then
            return "ref IntPtr";
        end
        if(typ.code=="record_type")then
            return getTypeName(typ);
        elseif(typ.code=="integer_type")then 
            return getTypeName(typ) .. "*";
        end
    end
    return nil;
end

local function getTypeName(type_decl)
    local tree = type_decl.root;
    local name;
    if(type_decl.code=="array_type")then
        type_decl = tree[type_decl.domn];
    end
    if(type_decl.code=="enumeral_type")then
        type_decl = tree[type_decl.name];
    end
    if(type_decl.code=="integer_type" or type_decl.code=="real_type")then
        while(type_decl.unql)do
            type_decl = tree[type_decl.unql];
        end
        if(type_decl.name==nil)then
            return "int";
        end
        name = tree[type_decl.name];
    elseif(type_decl.code=="record_type")then
        name = tree[type_decl.name];
    else
        while(type_decl.code~="type_decl")do
            type_decl = tree[type_decl.name];
        end
        name = type_decl;
    end
    if(name.code=="type_decl")then
        name = tree[name.name];
    end
    local strg = name.strg;
    if(tocs_basetype)then
        if(tocs_basetype[strg])then
            print(strg);
            return tocs_basetype[strg];
        end
    end
    return strg;
end

local function getStructFieldValue(field_decl)
    local tree = field_decl.root;
    print(field_decl.id);
    local name = tree[field_decl.name].strg;
    local bpos = tree[field_decl.bpos].int;
    local size = tree[field_decl.size].int;
    local typ  = tree[field_decl.type];
    if(typ.code=="pointer_type")then
        typ = getPointTypeName(typ);
    else 
        typ = getTypeName(typ);
    end
    return name,typ,bpos,size;
end

local function getStructNextField(field_decl)
    local tree = field_decl.root;
    local next = tree[field_decl.chain];
    if(next.code~="field_decl")then
        return nil;
    end
    return next;
end

local function getIdent(name)
    if(cs_keywords[name])then return "_" .. name; end;
    return name
end

function ldump_function(f,out)
    local dumpAll = false;
    local len;
    local dump_list = {};
    if(out==nil)then
        out = io.stderr;
    elseif(type(out)=="string")then --如果是字符串 则，打开out文件  FILE*
        out = io.open(out,"w+");
    end
    if(f)then
        len = #f;
    else
        dumpAll = true;
        f = "test"
    end
    
    local s = [[
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace C
{
    class %s
    {
        const string dll_file = "%s";
]];
    s = string.format(s,lib_name,dll_name);
    out:write(s);
    
    table.sort(enumeral_type,function(a,b)
        local aa,bb;
        aa = getEnumName(a);
        bb = getEnumName(b);
        return aa<bb;
    end
    )
    local tmpfile = io.tmpfile();
    local dumplist = {};
    for k,v in ipairs(enumeral_type) do
        local enumName = getEnumName(v);
        if(dumplist[enumName]==nil)then
            dumplist[enumName] = true;
            local dumpAllField = false;
            
            if(dumpAll==false and enumName and enumName:sub(1,len)==f)then
                dumpAllField = true;
            end
            if(dumpAllField==false and isDump(enumName,v)) then
                dumpAllField = true;
            end
            local dfields = {};
            
            local ff = getFirstField(v);
            if(ff)then
                table.insert(dfields,ff);
                while(true)do
                    local n = getFieldName(ff);
                    ff = getNextField(ff); --next
                    if(ff)then
                        if(dumpAll or dumpAllField or isDump(n,ff))then
                            table.insert(dfields,ff);
                        else
                            local n = getFieldName(ff);
                            if(n and n:sub(1,len)==f)then
                                table.insert(dfields,ff);
                            end
                        end
                    else
                        break;
                    end
                end
            end
            
            
            
            if(enumName:sub(1,1)~=".")then
                out:write(string.format("        [Flags] public enum %s:int\n{        \n",enumName));
            end
            local function hex(v)
                v = tonumber(v);
                if(v<0)then return tostring(v);end;
                return string.format("0x%X",v);
            end
            for kk,vv in ipairs(dfields)do
                local s = [[
                %s = %s,
]]
                tmpfile:write(string.format("        public const int %s = %s;\n",getFieldName(vv),hex(getFieldValue(vv))));
                if(enumName:sub(1,1)~=".")then
                    out:write(string.format(s,getFieldName(vv),hex(getFieldValue(vv))));
                end
            end
            if(enumName:sub(1,1)~=".")then
                out:write("        };\n");
            end
        end
    end
    
    table.sort(type_decl,function(a,b)
        local aa = getStructName(a);
        local bb = getStructName(b);
        if(aa==nil)then aa = "";end;
        if(bb==nil)then bb = ""; end;
        return aa<bb;
    end);
    local dumpd = {};
    for k,v in ipairs(type_decl)do
        local name = getStructName(v);
        if(name and dumpd[name]==nil and dumplist[name]==nil  and ( isDump(name,v) or name:sub(1,len)==f))then
            dumpd[name] = true;
            local field = getStructFirstField(v);
            local s = [[
            public class %s:Struct
            {
]];
            local f = [[
                public unsafe %s %s
                {
                    get{ %s v; this.bRead(out v,%s,%s); return v;}
                    set{ this.bWrite(value,%s,%s);}
                }
]]
            out:write(string.format(s,name));
            while(field)do
                local name,typ,bpos,size = getStructFieldValue(field);
                local ff = f;
                if(tocs_basetype[typ])then
                    typ = tocs_basetype[typ];
                end
                if(typ==nil or typ:sub(1,1)==".")then
                    ff = "/*" .. ff .. "*/\n";
                end
                out:write(string.format(ff,typ,getIdent(name),typ,bpos,size,bpos,size));
                field = getStructNextField(field);
            end
            out:write("            };\n");
        end
    end
    
    tmpfile:seek("set");
    out:write(tmpfile:read("*a"),"\n");
    
    local dumpStructs = {};
    local cs_code = io.tmpfile();
    
    
    
    table.sort(function_decl,function(a,b)
        a = getName(a);
        b = getName(b);
        return a<b;
    end
    );
    
    for k,v in ipairs(function_decl) do
        local dump = false;
        local name = getName(v);
        if(dumpAll or isDump(name,v) or name:sub(1,len)==f)then
            dump = true;
        end
        
        if(dump)then
            local prms = getPrms(v); --获得参数列表
            local prms_str = "";
            local err;
            local parms_name = getPrmsName(v);
            local string_parm;
            local comma = "";
            local cs_check_args = "";
            for i=1,#prms do
                local code = prms[i].code;
                if(code=="void_type")then
                    break;  --结束参数列表
                end
                if(cs_type==nil)then
                    cs_type = {};
                    cs_type["pointer_type"] = function() return "IntPtr"; end;
                    cs_type["integer_type"] = function(v) 
                        if(v.prec=="8")then return "sbyte"; end;
                        if(v.prec=="16")then return "short"; end;
                        if(v.prec=="32")then return "int"; end;
                        if(v.prec=="64")then return "Int64"; end;
                    end
                    cs_type["real_type"] = function(v)
                        if(v.prec=="64")then
                            return "double";
                        elseif(v.prec=="32")then
                            return "float";
                        end
                    end
                    cs_type["void_type"] = function() return "void"; end;
                    cs_type["enumeral_type"] = function() return "int"; end;
                end
                --参数列表
                local parm_name;
                if(parms_name[i]~=nil and parms_name[i]~="")then
                    if(cs_keywords[parms_name[i]])then
                        parm_name = " _" .. parms_name[i];
                    else
                        parm_name = " " .. parms_name[i];
                    end
                else
                    parm_name = " a" .. i;
                end
                
                if(cs_type[code])then
                    if(isString(prms[i]))then
                        if(string_parm==nil)then string_parm = prms_str; end;
                        string_parm = string_parm .. comma .. "string" .. parm_name;
                    elseif(string_parm)then
                        string_parm = string_parm .. comma .. cs_type[code](prms[i])  .. parm_name;
                    end
                    local cs_check_type = getPointTypeName(prms[i]);
                    
                    --
                    if(isString(prms[i]))then
                        cs_check_type = "string"
                    end
                    --
                    
                    local typename = cs_type[code](prms[i]);
                    prms_str = prms_str .. comma .. typename .. parm_name;
                    if(cs_check_type==nil)then
                        cs_check_args = cs_check_args .. comma .. typename  .. parm_name;
                    else
                        cs_check_args = cs_check_args .. comma .. cs_check_type  .. parm_name;
                    end
                else
                    prms_str = prms_str .. "error type:" .. code;
                    err = true;
                end 
                comma = ","
            end
                
            local ret_str;
            local ret_typ = getRetrnType(v);
            if(cs_type[ret_typ.code])then
                ret_str = cs_type[ret_typ.code](ret_typ);
            else
                ret_str = "error type:" .. ret_typ.code;
                err = true;
            end
            local s = [[
            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
            public static extern %s %s(%s);
]];
            if(err)then
                s = "/*" .. s .. "*/\n";
                --cs_check_args = nil;
            end
            if(string_parm)then
                out:write(string.format(s,ret_str,getName(v),string_parm));
            end
            if(cs_check_args)then
                cs_code:write(string.format(s,getPointTypeName(ret_typ),getName(v),cs_check_args));
            end
            s = string.format(s,ret_str,getName(v),prms_str);
            out:write(s);
            
        end
    end
    
    cs_code:seek("set");
    --out:write(cs_code:read("*a"));
    
    out:write([[
        
    }
}
]]);
    
end

