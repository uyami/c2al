
function getExportEnums(f)
    local enums = {};
    local len = 0;
    if(f)then len = #f;end;
    for kk,vv in ipairs(enumeral_type) do
        local v = vv:GetDecl();
        local name = v:GetEnumName();
        local src,line = v:GetFilePosition();
        if(dumpSourceFlag and dumpSource[src])then
            table.insert(enums,v);
        elseif(f==nil)then 
            if(dumpSourceFlag)then
            else
                table.insert(enums,v);
            end
        elseif(name)then
            if(name:sub(1,len)==f)then
                table.insert(enums,v);
            else
                --成员存在名称
                --速度或许会很慢
                local name,value,next = v:GetNextEnumInfo();
                while(true)do
                    if(name:sub(1,len)==f)then
                        table.insert(enums,v);
                        break;
                    end
                    if(next==nil)then
                        break;
                    end
                    name,value,next = next:GetNextEnumInfo();
                end
            end
        end
    end
    return enums;
end

function exportEnum(f,out)
    local enums = {};
    local format = string.format;

    --enum['enumName'] = { {name,value} };
    local enum_decls = getExportEnums(f);
    for k,v in ipairs(enum_decls)do
        local enum = {};
        local enumName = v:GetEnumName();
        local name,value,next = v:GetNextEnumInfo();
        while(true)do
            table.insert(enum,{name = name,value = value});
            if(next==nil)then break;end;
            name,value,next = next:GetNextEnumInfo();
        end
        enums[enumName] = enum;
    end

    local enumNames = {};
    for k,v in pairs(enums)do
        table.insert(enumNames,k);
    end
    table.sort(enumNames,function(a,b)return a<b;end);

    local s = [[

//enums
#ifndef lregisterInt
#ifndef USE_HEADER
#define lregisterInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,#name);
#else
#define lregisterInt(L,name,value) lua_pushinteger(L,name); lua_setglobal(L,#name);
#endif
#endif


]];
    out:write(s);
    local enumNum = 0;
    for k,v in ipairs(enumNames)do
        local enum = enums[v];
        local s = format('  //enum %s',v);
        out:write(s..'\n');
        for kk,vv in pairs(enum)do
            enumNum = enumNum + 1;
            out:write(format('  lregisterInt(L,%s,%s);\n',vv.name,vv.value));
        end
        out:write(s.." end\n\n");
    end
    out:write(format('  //export Lua enum:%d\n\n',enumNum));

end

