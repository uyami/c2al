local head = [[
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>

#define INLINE inline static

#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    luaL_argerror(L,idx,"type error:pointer_type");
  };
  return data;
};
#endif

INLINE void MCopy(void*d,void*s,int siz){
  if(siz==1)
    *(uint8_t*)d = *(uint8_t*)s;
  else if(siz==2)
    *(uint16_t*)d = *(uint16_t*)s;
  else if(siz==4)
    *(uint32_t*)d = *(uint32_t*)s;
  else if(siz==8)
    *(uint64_t*)d = *(uint64_t*)s;
  else
    memcpy(d,s,siz);
}

#include <string.h>
#define TArrayName "TArray"
typedef struct{
  void* p;
  char* typeName;
}TArray;

#define DEFF(t) \
static int larray##t##_get(lua_State*L){ \
  t*p = (t*)lcheck_toptr(L,1);               \
  int idx = luaL_checkinteger(L,2);      \
  lua_pushnumber(L,p[idx]);              \
  return 1;                              \
}                                        \
static int larray##t##_set(lua_State*L){ \
  t*p = (t*)lcheck_toptr(L,1);           \
  int idx = luaL_checkinteger(L,2);      \
  t f = luaL_checknumber(L,3);       \
  p[idx] = f;                           \
  return 0;                             \
}

#define DEFI(t) \
static int larray##t##_get(lua_State*L){ \
  t*p = (t*)lcheck_toptr(L,1);               \
  int idx = luaL_checkinteger(L,2);      \
  lua_pushinteger(L,p[idx]);              \
  return 1;                              \
}                                        \
static int larray##t##_set(lua_State*L){ \
  t*p = (t*)lcheck_toptr(L,1);           \
  int idx = luaL_checkinteger(L,2);      \
  t f = (t)luaL_checkinteger(L,3);       \
  p[idx] = f;                           \
  return 0;                             \
}

DEFF(float)
DEFF(double)
DEFI(int)
DEFI(short)
DEFI(char)

DEFI(int8_t)
DEFI(int16_t)
DEFI(int32_t)
DEFI(int64_t)

DEFI(uint8_t)
DEFI(uint16_t)
DEFI(uint32_t)
DEFI(uint64_t)

static int larray_get(lua_State*L){
  TArray *array;
  int idx = luaL_checkinteger(L,2);
  if(lua_islightuserdata(L,1)){
    //array = lua_touserdata(L,1);
    luaL_argerror(L,1,"error array");
  }
  else if(lua_isuserdata(L,1)){
    array = lua_touserdata(L,1);
  }
  void**p = array->p;
  char *typeName = strdup(array->typeName);
  int len = strlen(typeName);
  if(len>0 && typeName[len]=='*'){
    typeName[len-1] = 0;
    if(luaL_getmetatable(L,typeName)){
      *(void**)lua_newuserdata(L,sizeof(void**)) = p[idx];
      luaL_setmetatable(L,typeName);
    }
    else{
      TArray *darray = lua_newuserdata(L,sizeof(TArray));
      darray->p = p[idx];
      darray->typeName = typeName;
      luaL_setmetatable(L,TArrayName);
    }
  }
  else{
    return 0;
  }
  return 1;
}

static int larray_set(lua_State*L){
  TArray*array = luaL_checkudata(L,1,TArrayName);
  int idx = luaL_checkinteger(L,2);
  void*p = lcheck_toptr(L,3);
  void**dp = array->p;
  dp[idx] = p;
  return 0;
}

static int larray_gc(lua_State*L){
  TArray*array = luaL_checkudata(L,1,TArrayName);
  free(array->typeName);
  return 0;
}

static int larray_tostring(lua_State*L){
  TArray*array = luaL_checkudata(L,1,TArrayName);
  int len = strlen(array->typeName);
  char*s = calloc(len+0x20,1);
  sprintf(s,"%s%p",array->typeName,array->p);
  lua_pushstring(L,s);
  free(s);
  return 1;
}

static int larrayCreateMetatable(lua_State*L){
  #define DEFSMETA(t) if(luaL_newmetatable(L,#t"*")){ \
      lua_pushcfunction(L,larray##t##_get);           \
      lua_setfield(L,-2,"__index");                   \
      lua_pushcfunction(L,larray##t##_set);           \
      lua_setfield(L,-2,"__newindex");                \
      lua_pushstring(L,#t"*");                        \
      lua_setfield(L,-2,"name");                      \
      lua_pop(L,1);                                   \
    }

    DEFSMETA(float)
    DEFSMETA(double)
    DEFSMETA(int)
    DEFSMETA(short)
    DEFSMETA(char)
    DEFSMETA(uint64_t)
    DEFSMETA(uint32_t)
    DEFSMETA(uint16_t)
    DEFSMETA(uint8_t)
    DEFSMETA(int64_t)
    DEFSMETA(int32_t)
    DEFSMETA(int16_t)
    DEFSMETA(int8_t)

  if(luaL_newmetatable(L,TArrayName)){ 
    lua_pushcfunction(L,larray_get);           
    lua_setfield(L,-2,"__index");                   
    lua_pushcfunction(L,larray_set);           
    lua_setfield(L,-2,"__newindex");       
    lua_pushcfunction(L,larray_gc);           
    lua_setfield(L,-2,"__gc"); 
    lua_pushcfunction(L,larray_tostring);           
    lua_setfield(L,-2,"__tostring"); 
  
    lua_pushstring(L,TArrayName);                        
    lua_setfield(L,-2,"name");                      
    lua_pop(L,1);                                   
  }
}

static int clarrayNew(lua_State*L,void*ptr,const char*type){
  TArray *darray = lua_newuserdata(L,sizeof(TArray));
  darray->p = ptr;
  darray->typeName = strdup(type);
  luaL_setmetatable(L,TArrayName);
  return 1;
}

static int larrayNew(lua_State*L){
  void*p = lcheck_toptr(L,1);
  const char*type = luaL_checkstring(L,2);
  return clarrayNew(L,p,type);
}

static int lgetm(lua_State*L){
  const char*s = luaL_checkstring(L,1);
  luaL_getmetatable(L,s);
  return 1;
}

static int lsetm(lua_State*L){
  if(lua_islightuserdata(L,1)){
    *(void**)lua_newuserdata(L,sizeof(void**)) = lua_touserdata(L,1);
  }
  else
    lua_pushvalue(L,1);
  
  if(lua_istable(L,2)){
    lua_pushvalue(L,2);
    lua_setmetatable(L,-2);
    return 1;
  }
  else if(lua_isstring(L,2)){
    luaL_setmetatable(L,lua_tostring(L,2));
    return 1;
  }
  return 0;
}

]];

local Define = [[
  const char*name = "%s";
#define lregister(L,n) lua_register(L,#n,l##n)
  larrayCreateMetatable(L);
  lregister(L,arrayNew);
  lregister(L,setm);
  lregister(L,getm);
]];


function WriteHead(out)
    out:write(head);
end

function WriteDefine(out,name)
    local define = string.format(Define,name);
    out:write(define);
end
