
local cxx = {};
cxx.name = '';
local cxxmeta = {};
function cxxmeta:__index(k)
    local cma = '.';
    if(self.name=='')then 
        cma = '';
        self.lcname = '_';
    end;
    local n = {
        name=self.name..cma..k,
        lcname = string.format( "%s%d%s",self.lcname,#k,k);
    };
    return setmetatable(n, cxxmeta);
end

function cxxmeta:__tostring()
    return self.lcname;
end

function cxxmeta:__call(...)
    print(...);
    local n = self.lcname;
    if(_G[n])then
        local cxxobj = {_G[n](...),__methods = {}};
        setmetatable(cxxobj,{
            __index = function(k)
                --如果缓存方法有该方法,则
                if(self.__methods[k])then return self.__methods[k]; end;
                local cxxcall = {cxxobj[1][k]};
                setmetatable(cxxcall,{
                    __call = function(...)
                        local v,typ = cxxcall[1](...);
                        
                        return v;
                    end
                });
                --写入该方法
                self.__methods[k] = cxxcall;
                return cxxcall;
            end,
        });
    else
        assert(nil,'未找到方法');
    end
end

cxx = setmetatable(cxx,cxxmeta);

