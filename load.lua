function GetFileDir(f)
    f = string.reverse(f);
    local pos = f:find('\\');
    local pos2 = f:find('/');
    if(pos and pos2)then 
        if(pos>pos2)then pos = pos2;end;
    elseif(pos==nil)then
        pos = pos2;
    end
    if(pos==nil)then pos = 1;end;
    return string.reverse(f:sub(pos));
end

function ScriptDir()
    local info = debug.getinfo(ScriptDir);
    local dir = info.source;
    dir = dir:sub(2);
    return GetFileDir(dir);
end

function LoadTu(fname)
    local f = io.open(fname,"rb");
    local s = f:read("*a");
    parse(s);
    f:close();
end

package.path = package.path .. ';' .. ScriptDir().."lib/?.lua"

print(require('base'));
print(require('parser'));

--LoadTu(arg[1]);

