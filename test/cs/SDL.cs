﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace C
{
    class SDL
    {
        const string dll_file = "SDL2.dll";
        public enum SDL_AssertState
        {
            SDL_ASSERTION_RETRY = 0x0,
            SDL_ASSERTION_BREAK = 0x1,
            SDL_ASSERTION_ABORT = 0x2,
            SDL_ASSERTION_IGNORE = 0x3,
            SDL_ASSERTION_ALWAYS_IGNORE = 0x4,
        };
        public enum SDL_AudioStatus
        {
            SDL_AUDIO_STOPPED = 0x0,
            SDL_AUDIO_PLAYING = 0x1,
            SDL_AUDIO_PAUSED = 0x2,
        };
        public enum SDL_BlendMode
        {
            SDL_BLENDMODE_NONE = 0x0,
            SDL_BLENDMODE_BLEND = 0x1,
            SDL_BLENDMODE_ADD = 0x2,
            SDL_BLENDMODE_MOD = 0x4,
        };
        public enum SDL_DUMMY_ENUM
        {
            DUMMY_ENUM_VALUE = 0x0,
        };
        public enum SDL_EventType
        {
            SDL_FIRSTEVENT = 0x0,
            SDL_QUIT = 0x100,
            SDL_APP_TERMINATING = 0x101,
            SDL_APP_LOWMEMORY = 0x102,
            SDL_APP_WILLENTERBACKGROUND = 0x103,
            SDL_APP_DIDENTERBACKGROUND = 0x104,
            SDL_APP_WILLENTERFOREGROUND = 0x105,
            SDL_APP_DIDENTERFOREGROUND = 0x106,
            SDL_WINDOWEVENT = 0x200,
            SDL_SYSWMEVENT = 0x201,
            SDL_KEYDOWN = 0x300,
            SDL_KEYUP = 0x301,
            SDL_TEXTEDITING = 0x302,
            SDL_TEXTINPUT = 0x303,
            SDL_KEYMAPCHANGED = 0x304,
            SDL_MOUSEMOTION = 0x400,
            SDL_MOUSEBUTTONDOWN = 0x401,
            SDL_MOUSEBUTTONUP = 0x402,
            SDL_MOUSEWHEEL = 0x403,
            SDL_JOYAXISMOTION = 0x600,
            SDL_JOYBALLMOTION = 0x601,
            SDL_JOYHATMOTION = 0x602,
            SDL_JOYBUTTONDOWN = 0x603,
            SDL_JOYBUTTONUP = 0x604,
            SDL_JOYDEVICEADDED = 0x605,
            SDL_JOYDEVICEREMOVED = 0x606,
            SDL_CONTROLLERAXISMOTION = 0x650,
            SDL_CONTROLLERBUTTONDOWN = 0x651,
            SDL_CONTROLLERBUTTONUP = 0x652,
            SDL_CONTROLLERDEVICEADDED = 0x653,
            SDL_CONTROLLERDEVICEREMOVED = 0x654,
            SDL_CONTROLLERDEVICEREMAPPED = 0x655,
            SDL_FINGERDOWN = 0x700,
            SDL_FINGERUP = 0x701,
            SDL_FINGERMOTION = 0x702,
            SDL_DOLLARGESTURE = 0x800,
            SDL_DOLLARRECORD = 0x801,
            SDL_MULTIGESTURE = 0x802,
            SDL_CLIPBOARDUPDATE = 0x900,
            SDL_DROPFILE = 0x1000,
            SDL_DROPTEXT = 0x1001,
            SDL_DROPBEGIN = 0x1002,
            SDL_DROPCOMPLETE = 0x1003,
            SDL_AUDIODEVICEADDED = 0x1100,
            SDL_AUDIODEVICEREMOVED = 0x1101,
            SDL_RENDER_TARGETS_RESET = 0x2000,
            SDL_RENDER_DEVICE_RESET = 0x2001,
            SDL_USEREVENT = 0x8000,
            SDL_LASTEVENT = 0xFFFF,
        };
        public enum SDL_GLattr
        {
            SDL_GL_RED_SIZE = 0x0,
            SDL_GL_GREEN_SIZE = 0x1,
            SDL_GL_BLUE_SIZE = 0x2,
            SDL_GL_ALPHA_SIZE = 0x3,
            SDL_GL_BUFFER_SIZE = 0x4,
            SDL_GL_DOUBLEBUFFER = 0x5,
            SDL_GL_DEPTH_SIZE = 0x6,
            SDL_GL_STENCIL_SIZE = 0x7,
            SDL_GL_ACCUM_RED_SIZE = 0x8,
            SDL_GL_ACCUM_GREEN_SIZE = 0x9,
            SDL_GL_ACCUM_BLUE_SIZE = 0xA,
            SDL_GL_ACCUM_ALPHA_SIZE = 0xB,
            SDL_GL_STEREO = 0xC,
            SDL_GL_MULTISAMPLEBUFFERS = 0xD,
            SDL_GL_MULTISAMPLESAMPLES = 0xE,
            SDL_GL_ACCELERATED_VISUAL = 0xF,
            SDL_GL_RETAINED_BACKING = 0x10,
            SDL_GL_CONTEXT_MAJOR_VERSION = 0x11,
            SDL_GL_CONTEXT_MINOR_VERSION = 0x12,
            SDL_GL_CONTEXT_EGL = 0x13,
            SDL_GL_CONTEXT_FLAGS = 0x14,
            SDL_GL_CONTEXT_PROFILE_MASK = 0x15,
            SDL_GL_SHARE_WITH_CURRENT_CONTEXT = 0x16,
            SDL_GL_FRAMEBUFFER_SRGB_CAPABLE = 0x17,
            SDL_GL_CONTEXT_RELEASE_BEHAVIOR = 0x18,
        };
        public enum SDL_GLcontextFlag
        {
            SDL_GL_CONTEXT_DEBUG_FLAG = 0x1,
            SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG = 0x2,
            SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG = 0x4,
            SDL_GL_CONTEXT_RESET_ISOLATION_FLAG = 0x8,
        };
        public enum SDL_GLcontextReleaseFlag
        {
            SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE = 0x0,
            SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x1,
        };
        public enum SDL_GLprofile
        {
            SDL_GL_CONTEXT_PROFILE_CORE = 0x1,
            SDL_GL_CONTEXT_PROFILE_COMPATIBILITY = 0x2,
            SDL_GL_CONTEXT_PROFILE_ES = 0x4,
        };
        public enum SDL_GameControllerAxis
        {
            SDL_CONTROLLER_AXIS_INVALID = -1,
            SDL_CONTROLLER_AXIS_LEFTX = 0x0,
            SDL_CONTROLLER_AXIS_LEFTY = 0x1,
            SDL_CONTROLLER_AXIS_RIGHTX = 0x2,
            SDL_CONTROLLER_AXIS_RIGHTY = 0x3,
            SDL_CONTROLLER_AXIS_TRIGGERLEFT = 0x4,
            SDL_CONTROLLER_AXIS_TRIGGERRIGHT = 0x5,
            SDL_CONTROLLER_AXIS_MAX = 0x6,
        };
        public enum SDL_GameControllerBindType
        {
            SDL_CONTROLLER_BINDTYPE_NONE = 0x0,
            SDL_CONTROLLER_BINDTYPE_BUTTON = 0x1,
            SDL_CONTROLLER_BINDTYPE_AXIS = 0x2,
            SDL_CONTROLLER_BINDTYPE_HAT = 0x3,
        };
        public enum SDL_GameControllerButton
        {
            SDL_CONTROLLER_BUTTON_INVALID = -1,
            SDL_CONTROLLER_BUTTON_A = 0x0,
            SDL_CONTROLLER_BUTTON_B = 0x1,
            SDL_CONTROLLER_BUTTON_X = 0x2,
            SDL_CONTROLLER_BUTTON_Y = 0x3,
            SDL_CONTROLLER_BUTTON_BACK = 0x4,
            SDL_CONTROLLER_BUTTON_GUIDE = 0x5,
            SDL_CONTROLLER_BUTTON_START = 0x6,
            SDL_CONTROLLER_BUTTON_LEFTSTICK = 0x7,
            SDL_CONTROLLER_BUTTON_RIGHTSTICK = 0x8,
            SDL_CONTROLLER_BUTTON_LEFTSHOULDER = 0x9,
            SDL_CONTROLLER_BUTTON_RIGHTSHOULDER = 0xA,
            SDL_CONTROLLER_BUTTON_DPAD_UP = 0xB,
            SDL_CONTROLLER_BUTTON_DPAD_DOWN = 0xC,
            SDL_CONTROLLER_BUTTON_DPAD_LEFT = 0xD,
            SDL_CONTROLLER_BUTTON_DPAD_RIGHT = 0xE,
            SDL_CONTROLLER_BUTTON_MAX = 0xF,
        };
        public enum SDL_HintPriority
        {
            SDL_HINT_DEFAULT = 0x0,
            SDL_HINT_NORMAL = 0x1,
            SDL_HINT_OVERRIDE = 0x2,
        };
        public enum SDL_HitTestResult
        {
            SDL_HITTEST_NORMAL = 0x0,
            SDL_HITTEST_DRAGGABLE = 0x1,
            SDL_HITTEST_RESIZE_TOPLEFT = 0x2,
            SDL_HITTEST_RESIZE_TOP = 0x3,
            SDL_HITTEST_RESIZE_TOPRIGHT = 0x4,
            SDL_HITTEST_RESIZE_RIGHT = 0x5,
            SDL_HITTEST_RESIZE_BOTTOMRIGHT = 0x6,
            SDL_HITTEST_RESIZE_BOTTOM = 0x7,
            SDL_HITTEST_RESIZE_BOTTOMLEFT = 0x8,
            SDL_HITTEST_RESIZE_LEFT = 0x9,
        };
        public enum SDL_JoystickPowerLevel
        {
            SDL_JOYSTICK_POWER_UNKNOWN = -1,
            SDL_JOYSTICK_POWER_EMPTY = 0x0,
            SDL_JOYSTICK_POWER_LOW = 0x1,
            SDL_JOYSTICK_POWER_MEDIUM = 0x2,
            SDL_JOYSTICK_POWER_FULL = 0x3,
            SDL_JOYSTICK_POWER_WIRED = 0x4,
            SDL_JOYSTICK_POWER_MAX = 0x5,
        };
        public enum SDL_Keymod
        {
            KMOD_NONE = 0x0,
            KMOD_LSHIFT = 0x1,
            KMOD_RSHIFT = 0x2,
            KMOD_LCTRL = 0x40,
            KMOD_RCTRL = 0x80,
            KMOD_LALT = 0x100,
            KMOD_RALT = 0x200,
            KMOD_LGUI = 0x400,
            KMOD_RGUI = 0x800,
            KMOD_NUM = 0x1000,
            KMOD_CAPS = 0x2000,
            KMOD_MODE = 0x4000,
            KMOD_RESERVED = 0x8000,
        };
        public enum SDL_LogPriority
        {
            SDL_LOG_PRIORITY_VERBOSE = 0x1,
            SDL_LOG_PRIORITY_DEBUG = 0x2,
            SDL_LOG_PRIORITY_INFO = 0x3,
            SDL_LOG_PRIORITY_WARN = 0x4,
            SDL_LOG_PRIORITY_ERROR = 0x5,
            SDL_LOG_PRIORITY_CRITICAL = 0x6,
            SDL_NUM_LOG_PRIORITIES = 0x7,
        };
        public enum SDL_MessageBoxButtonFlags
        {
            SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT = 0x1,
            SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT = 0x2,
        };
        public enum SDL_MessageBoxColorType
        {
            SDL_MESSAGEBOX_COLOR_BACKGROUND = 0x0,
            SDL_MESSAGEBOX_COLOR_TEXT = 0x1,
            SDL_MESSAGEBOX_COLOR_BUTTON_BORDER = 0x2,
            SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND = 0x3,
            SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED = 0x4,
            SDL_MESSAGEBOX_COLOR_MAX = 0x5,
        };
        public enum SDL_MessageBoxFlags
        {
            SDL_MESSAGEBOX_ERROR = 0x10,
            SDL_MESSAGEBOX_WARNING = 0x20,
            SDL_MESSAGEBOX_INFORMATION = 0x40,
        };
        public enum SDL_MouseWheelDirection
        {
            SDL_MOUSEWHEEL_NORMAL = 0x0,
            SDL_MOUSEWHEEL_FLIPPED = 0x1,
        };
        public enum SDL_PowerState
        {
            SDL_POWERSTATE_UNKNOWN = 0x0,
            SDL_POWERSTATE_ON_BATTERY = 0x1,
            SDL_POWERSTATE_NO_BATTERY = 0x2,
            SDL_POWERSTATE_CHARGING = 0x3,
            SDL_POWERSTATE_CHARGED = 0x4,
        };
        public enum SDL_RendererFlags
        {
            SDL_RENDERER_SOFTWARE = 0x1,
            SDL_RENDERER_ACCELERATED = 0x2,
            SDL_RENDERER_PRESENTVSYNC = 0x4,
            SDL_RENDERER_TARGETTEXTURE = 0x8,
        };
        public enum SDL_RendererFlip
        {
            SDL_FLIP_NONE = 0x0,
            SDL_FLIP_HORIZONTAL = 0x1,
            SDL_FLIP_VERTICAL = 0x2,
        };
        public enum SDL_Scancode
        {
            SDL_SCANCODE_UNKNOWN = 0x0,
            SDL_SCANCODE_A = 0x4,
            SDL_SCANCODE_B = 0x5,
            SDL_SCANCODE_C = 0x6,
            SDL_SCANCODE_D = 0x7,
            SDL_SCANCODE_E = 0x8,
            SDL_SCANCODE_F = 0x9,
            SDL_SCANCODE_G = 0xA,
            SDL_SCANCODE_H = 0xB,
            SDL_SCANCODE_I = 0xC,
            SDL_SCANCODE_J = 0xD,
            SDL_SCANCODE_K = 0xE,
            SDL_SCANCODE_L = 0xF,
            SDL_SCANCODE_M = 0x10,
            SDL_SCANCODE_N = 0x11,
            SDL_SCANCODE_O = 0x12,
            SDL_SCANCODE_P = 0x13,
            SDL_SCANCODE_Q = 0x14,
            SDL_SCANCODE_R = 0x15,
            SDL_SCANCODE_S = 0x16,
            SDL_SCANCODE_T = 0x17,
            SDL_SCANCODE_U = 0x18,
            SDL_SCANCODE_V = 0x19,
            SDL_SCANCODE_W = 0x1A,
            SDL_SCANCODE_X = 0x1B,
            SDL_SCANCODE_Y = 0x1C,
            SDL_SCANCODE_Z = 0x1D,
            SDL_SCANCODE_1 = 0x1E,
            SDL_SCANCODE_2 = 0x1F,
            SDL_SCANCODE_3 = 0x20,
            SDL_SCANCODE_4 = 0x21,
            SDL_SCANCODE_5 = 0x22,
            SDL_SCANCODE_6 = 0x23,
            SDL_SCANCODE_7 = 0x24,
            SDL_SCANCODE_8 = 0x25,
            SDL_SCANCODE_9 = 0x26,
            SDL_SCANCODE_0 = 0x27,
            SDL_SCANCODE_RETURN = 0x28,
            SDL_SCANCODE_ESCAPE = 0x29,
            SDL_SCANCODE_BACKSPACE = 0x2A,
            SDL_SCANCODE_TAB = 0x2B,
            SDL_SCANCODE_SPACE = 0x2C,
            SDL_SCANCODE_MINUS = 0x2D,
            SDL_SCANCODE_EQUALS = 0x2E,
            SDL_SCANCODE_LEFTBRACKET = 0x2F,
            SDL_SCANCODE_RIGHTBRACKET = 0x30,
            SDL_SCANCODE_BACKSLASH = 0x31,
            SDL_SCANCODE_NONUSHASH = 0x32,
            SDL_SCANCODE_SEMICOLON = 0x33,
            SDL_SCANCODE_APOSTROPHE = 0x34,
            SDL_SCANCODE_GRAVE = 0x35,
            SDL_SCANCODE_COMMA = 0x36,
            SDL_SCANCODE_PERIOD = 0x37,
            SDL_SCANCODE_SLASH = 0x38,
            SDL_SCANCODE_CAPSLOCK = 0x39,
            SDL_SCANCODE_F1 = 0x3A,
            SDL_SCANCODE_F2 = 0x3B,
            SDL_SCANCODE_F3 = 0x3C,
            SDL_SCANCODE_F4 = 0x3D,
            SDL_SCANCODE_F5 = 0x3E,
            SDL_SCANCODE_F6 = 0x3F,
            SDL_SCANCODE_F7 = 0x40,
            SDL_SCANCODE_F8 = 0x41,
            SDL_SCANCODE_F9 = 0x42,
            SDL_SCANCODE_F10 = 0x43,
            SDL_SCANCODE_F11 = 0x44,
            SDL_SCANCODE_F12 = 0x45,
            SDL_SCANCODE_PRINTSCREEN = 0x46,
            SDL_SCANCODE_SCROLLLOCK = 0x47,
            SDL_SCANCODE_PAUSE = 0x48,
            SDL_SCANCODE_INSERT = 0x49,
            SDL_SCANCODE_HOME = 0x4A,
            SDL_SCANCODE_PAGEUP = 0x4B,
            SDL_SCANCODE_DELETE = 0x4C,
            SDL_SCANCODE_END = 0x4D,
            SDL_SCANCODE_PAGEDOWN = 0x4E,
            SDL_SCANCODE_RIGHT = 0x4F,
            SDL_SCANCODE_LEFT = 0x50,
            SDL_SCANCODE_DOWN = 0x51,
            SDL_SCANCODE_UP = 0x52,
            SDL_SCANCODE_NUMLOCKCLEAR = 0x53,
            SDL_SCANCODE_KP_DIVIDE = 0x54,
            SDL_SCANCODE_KP_MULTIPLY = 0x55,
            SDL_SCANCODE_KP_MINUS = 0x56,
            SDL_SCANCODE_KP_PLUS = 0x57,
            SDL_SCANCODE_KP_ENTER = 0x58,
            SDL_SCANCODE_KP_1 = 0x59,
            SDL_SCANCODE_KP_2 = 0x5A,
            SDL_SCANCODE_KP_3 = 0x5B,
            SDL_SCANCODE_KP_4 = 0x5C,
            SDL_SCANCODE_KP_5 = 0x5D,
            SDL_SCANCODE_KP_6 = 0x5E,
            SDL_SCANCODE_KP_7 = 0x5F,
            SDL_SCANCODE_KP_8 = 0x60,
            SDL_SCANCODE_KP_9 = 0x61,
            SDL_SCANCODE_KP_0 = 0x62,
            SDL_SCANCODE_KP_PERIOD = 0x63,
            SDL_SCANCODE_NONUSBACKSLASH = 0x64,
            SDL_SCANCODE_APPLICATION = 0x65,
            SDL_SCANCODE_POWER = 0x66,
            SDL_SCANCODE_KP_EQUALS = 0x67,
            SDL_SCANCODE_F13 = 0x68,
            SDL_SCANCODE_F14 = 0x69,
            SDL_SCANCODE_F15 = 0x6A,
            SDL_SCANCODE_F16 = 0x6B,
            SDL_SCANCODE_F17 = 0x6C,
            SDL_SCANCODE_F18 = 0x6D,
            SDL_SCANCODE_F19 = 0x6E,
            SDL_SCANCODE_F20 = 0x6F,
            SDL_SCANCODE_F21 = 0x70,
            SDL_SCANCODE_F22 = 0x71,
            SDL_SCANCODE_F23 = 0x72,
            SDL_SCANCODE_F24 = 0x73,
            SDL_SCANCODE_EXECUTE = 0x74,
            SDL_SCANCODE_HELP = 0x75,
            SDL_SCANCODE_MENU = 0x76,
            SDL_SCANCODE_SELECT = 0x77,
            SDL_SCANCODE_STOP = 0x78,
            SDL_SCANCODE_AGAIN = 0x79,
            SDL_SCANCODE_UNDO = 0x7A,
            SDL_SCANCODE_CUT = 0x7B,
            SDL_SCANCODE_COPY = 0x7C,
            SDL_SCANCODE_PASTE = 0x7D,
            SDL_SCANCODE_FIND = 0x7E,
            SDL_SCANCODE_MUTE = 0x7F,
            SDL_SCANCODE_VOLUMEUP = 0x80,
            SDL_SCANCODE_VOLUMEDOWN = 0x81,
            SDL_SCANCODE_KP_COMMA = 0x85,
            SDL_SCANCODE_KP_EQUALSAS400 = 0x86,
            SDL_SCANCODE_INTERNATIONAL1 = 0x87,
            SDL_SCANCODE_INTERNATIONAL2 = 0x88,
            SDL_SCANCODE_INTERNATIONAL3 = 0x89,
            SDL_SCANCODE_INTERNATIONAL4 = 0x8A,
            SDL_SCANCODE_INTERNATIONAL5 = 0x8B,
            SDL_SCANCODE_INTERNATIONAL6 = 0x8C,
            SDL_SCANCODE_INTERNATIONAL7 = 0x8D,
            SDL_SCANCODE_INTERNATIONAL8 = 0x8E,
            SDL_SCANCODE_INTERNATIONAL9 = 0x8F,
            SDL_SCANCODE_LANG1 = 0x90,
            SDL_SCANCODE_LANG2 = 0x91,
            SDL_SCANCODE_LANG3 = 0x92,
            SDL_SCANCODE_LANG4 = 0x93,
            SDL_SCANCODE_LANG5 = 0x94,
            SDL_SCANCODE_LANG6 = 0x95,
            SDL_SCANCODE_LANG7 = 0x96,
            SDL_SCANCODE_LANG8 = 0x97,
            SDL_SCANCODE_LANG9 = 0x98,
            SDL_SCANCODE_ALTERASE = 0x99,
            SDL_SCANCODE_SYSREQ = 0x9A,
            SDL_SCANCODE_CANCEL = 0x9B,
            SDL_SCANCODE_CLEAR = 0x9C,
            SDL_SCANCODE_PRIOR = 0x9D,
            SDL_SCANCODE_RETURN2 = 0x9E,
            SDL_SCANCODE_SEPARATOR = 0x9F,
            SDL_SCANCODE_OUT = 0xA0,
            SDL_SCANCODE_OPER = 0xA1,
            SDL_SCANCODE_CLEARAGAIN = 0xA2,
            SDL_SCANCODE_CRSEL = 0xA3,
            SDL_SCANCODE_EXSEL = 0xA4,
            SDL_SCANCODE_KP_00 = 0xB0,
            SDL_SCANCODE_KP_000 = 0xB1,
            SDL_SCANCODE_THOUSANDSSEPARATOR = 0xB2,
            SDL_SCANCODE_DECIMALSEPARATOR = 0xB3,
            SDL_SCANCODE_CURRENCYUNIT = 0xB4,
            SDL_SCANCODE_CURRENCYSUBUNIT = 0xB5,
            SDL_SCANCODE_KP_LEFTPAREN = 0xB6,
            SDL_SCANCODE_KP_RIGHTPAREN = 0xB7,
            SDL_SCANCODE_KP_LEFTBRACE = 0xB8,
            SDL_SCANCODE_KP_RIGHTBRACE = 0xB9,
            SDL_SCANCODE_KP_TAB = 0xBA,
            SDL_SCANCODE_KP_BACKSPACE = 0xBB,
            SDL_SCANCODE_KP_A = 0xBC,
            SDL_SCANCODE_KP_B = 0xBD,
            SDL_SCANCODE_KP_C = 0xBE,
            SDL_SCANCODE_KP_D = 0xBF,
            SDL_SCANCODE_KP_E = 0xC0,
            SDL_SCANCODE_KP_F = 0xC1,
            SDL_SCANCODE_KP_XOR = 0xC2,
            SDL_SCANCODE_KP_POWER = 0xC3,
            SDL_SCANCODE_KP_PERCENT = 0xC4,
            SDL_SCANCODE_KP_LESS = 0xC5,
            SDL_SCANCODE_KP_GREATER = 0xC6,
            SDL_SCANCODE_KP_AMPERSAND = 0xC7,
            SDL_SCANCODE_KP_DBLAMPERSAND = 0xC8,
            SDL_SCANCODE_KP_VERTICALBAR = 0xC9,
            SDL_SCANCODE_KP_DBLVERTICALBAR = 0xCA,
            SDL_SCANCODE_KP_COLON = 0xCB,
            SDL_SCANCODE_KP_HASH = 0xCC,
            SDL_SCANCODE_KP_SPACE = 0xCD,
            SDL_SCANCODE_KP_AT = 0xCE,
            SDL_SCANCODE_KP_EXCLAM = 0xCF,
            SDL_SCANCODE_KP_MEMSTORE = 0xD0,
            SDL_SCANCODE_KP_MEMRECALL = 0xD1,
            SDL_SCANCODE_KP_MEMCLEAR = 0xD2,
            SDL_SCANCODE_KP_MEMADD = 0xD3,
            SDL_SCANCODE_KP_MEMSUBTRACT = 0xD4,
            SDL_SCANCODE_KP_MEMMULTIPLY = 0xD5,
            SDL_SCANCODE_KP_MEMDIVIDE = 0xD6,
            SDL_SCANCODE_KP_PLUSMINUS = 0xD7,
            SDL_SCANCODE_KP_CLEAR = 0xD8,
            SDL_SCANCODE_KP_CLEARENTRY = 0xD9,
            SDL_SCANCODE_KP_BINARY = 0xDA,
            SDL_SCANCODE_KP_OCTAL = 0xDB,
            SDL_SCANCODE_KP_DECIMAL = 0xDC,
            SDL_SCANCODE_KP_HEXADECIMAL = 0xDD,
            SDL_SCANCODE_LCTRL = 0xE0,
            SDL_SCANCODE_LSHIFT = 0xE1,
            SDL_SCANCODE_LALT = 0xE2,
            SDL_SCANCODE_LGUI = 0xE3,
            SDL_SCANCODE_RCTRL = 0xE4,
            SDL_SCANCODE_RSHIFT = 0xE5,
            SDL_SCANCODE_RALT = 0xE6,
            SDL_SCANCODE_RGUI = 0xE7,
            SDL_SCANCODE_MODE = 0x101,
            SDL_SCANCODE_AUDIONEXT = 0x102,
            SDL_SCANCODE_AUDIOPREV = 0x103,
            SDL_SCANCODE_AUDIOSTOP = 0x104,
            SDL_SCANCODE_AUDIOPLAY = 0x105,
            SDL_SCANCODE_AUDIOMUTE = 0x106,
            SDL_SCANCODE_MEDIASELECT = 0x107,
            SDL_SCANCODE_WWW = 0x108,
            SDL_SCANCODE_MAIL = 0x109,
            SDL_SCANCODE_CALCULATOR = 0x10A,
            SDL_SCANCODE_COMPUTER = 0x10B,
            SDL_SCANCODE_AC_SEARCH = 0x10C,
            SDL_SCANCODE_AC_HOME = 0x10D,
            SDL_SCANCODE_AC_BACK = 0x10E,
            SDL_SCANCODE_AC_FORWARD = 0x10F,
            SDL_SCANCODE_AC_STOP = 0x110,
            SDL_SCANCODE_AC_REFRESH = 0x111,
            SDL_SCANCODE_AC_BOOKMARKS = 0x112,
            SDL_SCANCODE_BRIGHTNESSDOWN = 0x113,
            SDL_SCANCODE_BRIGHTNESSUP = 0x114,
            SDL_SCANCODE_DISPLAYSWITCH = 0x115,
            SDL_SCANCODE_KBDILLUMTOGGLE = 0x116,
            SDL_SCANCODE_KBDILLUMDOWN = 0x117,
            SDL_SCANCODE_KBDILLUMUP = 0x118,
            SDL_SCANCODE_EJECT = 0x119,
            SDL_SCANCODE_SLEEP = 0x11A,
            SDL_SCANCODE_APP1 = 0x11B,
            SDL_SCANCODE_APP2 = 0x11C,
            SDL_NUM_SCANCODES = 0x200,
        };
        public enum SDL_SystemCursor
        {
            SDL_SYSTEM_CURSOR_ARROW = 0x0,
            SDL_SYSTEM_CURSOR_IBEAM = 0x1,
            SDL_SYSTEM_CURSOR_WAIT = 0x2,
            SDL_SYSTEM_CURSOR_CROSSHAIR = 0x3,
            SDL_SYSTEM_CURSOR_WAITARROW = 0x4,
            SDL_SYSTEM_CURSOR_SIZENWSE = 0x5,
            SDL_SYSTEM_CURSOR_SIZENESW = 0x6,
            SDL_SYSTEM_CURSOR_SIZEWE = 0x7,
            SDL_SYSTEM_CURSOR_SIZENS = 0x8,
            SDL_SYSTEM_CURSOR_SIZEALL = 0x9,
            SDL_SYSTEM_CURSOR_NO = 0xA,
            SDL_SYSTEM_CURSOR_HAND = 0xB,
            SDL_NUM_SYSTEM_CURSORS = 0xC,
        };
        public enum SDL_TextureAccess
        {
            SDL_TEXTUREACCESS_STATIC = 0x0,
            SDL_TEXTUREACCESS_STREAMING = 0x1,
            SDL_TEXTUREACCESS_TARGET = 0x2,
        };
        public enum SDL_TextureModulate
        {
            SDL_TEXTUREMODULATE_NONE = 0x0,
            SDL_TEXTUREMODULATE_COLOR = 0x1,
            SDL_TEXTUREMODULATE_ALPHA = 0x2,
        };
        public enum SDL_ThreadPriority
        {
            SDL_THREAD_PRIORITY_LOW = 0x0,
            SDL_THREAD_PRIORITY_NORMAL = 0x1,
            SDL_THREAD_PRIORITY_HIGH = 0x2,
        };
        public enum SDL_WindowEventID
        {
            SDL_WINDOWEVENT_NONE = 0x0,
            SDL_WINDOWEVENT_SHOWN = 0x1,
            SDL_WINDOWEVENT_HIDDEN = 0x2,
            SDL_WINDOWEVENT_EXPOSED = 0x3,
            SDL_WINDOWEVENT_MOVED = 0x4,
            SDL_WINDOWEVENT_RESIZED = 0x5,
            SDL_WINDOWEVENT_SIZE_CHANGED = 0x6,
            SDL_WINDOWEVENT_MINIMIZED = 0x7,
            SDL_WINDOWEVENT_MAXIMIZED = 0x8,
            SDL_WINDOWEVENT_RESTORED = 0x9,
            SDL_WINDOWEVENT_ENTER = 0xA,
            SDL_WINDOWEVENT_LEAVE = 0xB,
            SDL_WINDOWEVENT_FOCUS_GAINED = 0xC,
            SDL_WINDOWEVENT_FOCUS_LOST = 0xD,
            SDL_WINDOWEVENT_CLOSE = 0xE,
            SDL_WINDOWEVENT_TAKE_FOCUS = 0xF,
            SDL_WINDOWEVENT_HIT_TEST = 0x10,
        };
        public enum SDL_WindowFlags
        {
            SDL_WINDOW_FULLSCREEN = 0x1,
            SDL_WINDOW_OPENGL = 0x2,
            SDL_WINDOW_SHOWN = 0x4,
            SDL_WINDOW_HIDDEN = 0x8,
            SDL_WINDOW_BORDERLESS = 0x10,
            SDL_WINDOW_RESIZABLE = 0x20,
            SDL_WINDOW_MINIMIZED = 0x40,
            SDL_WINDOW_MAXIMIZED = 0x80,
            SDL_WINDOW_INPUT_GRABBED = 0x100,
            SDL_WINDOW_INPUT_FOCUS = 0x200,
            SDL_WINDOW_MOUSE_FOCUS = 0x400,
            SDL_WINDOW_FULLSCREEN_DESKTOP = 0x1001,
            SDL_WINDOW_FOREIGN = 0x800,
            SDL_WINDOW_ALLOW_HIGHDPI = 0x2000,
            SDL_WINDOW_MOUSE_CAPTURE = 0x4000,
            SDL_WINDOW_ALWAYS_ON_TOP = 0x8000,
            SDL_WINDOW_SKIP_TASKBAR = 0x10000,
            SDL_WINDOW_UTILITY = 0x20000,
            SDL_WINDOW_TOOLTIP = 0x40000,
            SDL_WINDOW_POPUP_MENU = 0x80000,
        };
        public enum SDL_bool
        {
            SDL_FALSE = 0x0,
            SDL_TRUE = 0x1,
        };
        public enum SDL_errorcode
        {
            SDL_ENOMEM = 0x0,
            SDL_EFREAD = 0x1,
            SDL_EFWRITE = 0x2,
            SDL_EFSEEK = 0x3,
            SDL_UNSUPPORTED = 0x4,
            SDL_LASTERROR = 0x5,
        };
        public enum SDL_eventaction
        {
            SDL_ADDEVENT = 0x0,
            SDL_PEEKEVENT = 0x1,
            SDL_GETEVENT = 0x2,
        };
        public enum _MM_MANTISSA_NORM_ENUM
        {
            _MM_MANT_NORM_1_2 = 0x0,
        };
        public enum _MM_MANTISSA_SIGN_ENUM
        {
            _MM_MANT_SIGN_src = 0x0,
        };
        public enum _MM_PERM_ENUM
        {
            _MM_PERM_AAAA = 0x0,
        };
        public enum _mm_hint
        {
            _MM_HINT_ET0 = 0x7,
        };
        public const int SDL_PIXELTYPE_UNKNOWN = 0x0;
        public const int SDL_PIXELTYPE_INDEX1 = 0x1;
        public const int SDL_PIXELTYPE_INDEX4 = 0x2;
        public const int SDL_PIXELTYPE_INDEX8 = 0x3;
        public const int SDL_PIXELTYPE_PACKED8 = 0x4;
        public const int SDL_PIXELTYPE_PACKED16 = 0x5;
        public const int SDL_PIXELTYPE_PACKED32 = 0x6;
        public const int SDL_PIXELTYPE_ARRAYU8 = 0x7;
        public const int SDL_PIXELTYPE_ARRAYU16 = 0x8;
        public const int SDL_PIXELTYPE_ARRAYU32 = 0x9;
        public const int SDL_PIXELTYPE_ARRAYF16 = 0xA;
        public const int SDL_PIXELTYPE_ARRAYF32 = 0xB;
        public const int SDL_BITMAPORDER_NONE = 0x0;
        public const int SDL_BITMAPORDER_4321 = 0x1;
        public const int SDL_BITMAPORDER_1234 = 0x2;
        public const int SDL_PACKEDORDER_NONE = 0x0;
        public const int SDL_PACKEDORDER_XRGB = 0x1;
        public const int SDL_PACKEDORDER_RGBX = 0x2;
        public const int SDL_PACKEDORDER_ARGB = 0x3;
        public const int SDL_PACKEDORDER_RGBA = 0x4;
        public const int SDL_PACKEDORDER_XBGR = 0x5;
        public const int SDL_PACKEDORDER_BGRX = 0x6;
        public const int SDL_PACKEDORDER_ABGR = 0x7;
        public const int SDL_PACKEDORDER_BGRA = 0x8;
        public const int SDL_ARRAYORDER_NONE = 0x0;
        public const int SDL_ARRAYORDER_RGB = 0x1;
        public const int SDL_ARRAYORDER_RGBA = 0x2;
        public const int SDL_ARRAYORDER_ARGB = 0x3;
        public const int SDL_ARRAYORDER_BGR = 0x4;
        public const int SDL_ARRAYORDER_BGRA = 0x5;
        public const int SDL_ARRAYORDER_ABGR = 0x6;
        public const int SDL_PACKEDLAYOUT_NONE = 0x0;
        public const int SDL_PACKEDLAYOUT_332 = 0x1;
        public const int SDL_PACKEDLAYOUT_4444 = 0x2;
        public const int SDL_PACKEDLAYOUT_1555 = 0x3;
        public const int SDL_PACKEDLAYOUT_5551 = 0x4;
        public const int SDL_PACKEDLAYOUT_565 = 0x5;
        public const int SDL_PACKEDLAYOUT_8888 = 0x6;
        public const int SDL_PACKEDLAYOUT_2101010 = 0x7;
        public const int SDL_PACKEDLAYOUT_1010102 = 0x8;
        public const int SDL_PIXELFORMAT_UNKNOWN = 0x0;
        public const int SDL_PIXELFORMAT_INDEX1LSB = 0x11100100;
        public const int SDL_PIXELFORMAT_INDEX1MSB = 0x11200100;
        public const int SDL_PIXELFORMAT_INDEX4LSB = 0x12100400;
        public const int SDL_PIXELFORMAT_INDEX4MSB = 0x12200400;
        public const int SDL_PIXELFORMAT_INDEX8 = 0x13000801;
        public const int SDL_PIXELFORMAT_RGB332 = 0x14110801;
        public const int SDL_PIXELFORMAT_RGB444 = 0x15120C02;
        public const int SDL_PIXELFORMAT_RGB555 = 0x15130F02;
        public const int SDL_PIXELFORMAT_BGR555 = 0x15530F02;
        public const int SDL_PIXELFORMAT_ARGB4444 = 0x15321002;
        public const int SDL_PIXELFORMAT_RGBA4444 = 0x15421002;
        public const int SDL_PIXELFORMAT_ABGR4444 = 0x15721002;
        public const int SDL_PIXELFORMAT_BGRA4444 = 0x15821002;
        public const int SDL_PIXELFORMAT_ARGB1555 = 0x15331002;
        public const int SDL_PIXELFORMAT_RGBA5551 = 0x15441002;
        public const int SDL_PIXELFORMAT_ABGR1555 = 0x15731002;
        public const int SDL_PIXELFORMAT_BGRA5551 = 0x15841002;
        public const int SDL_PIXELFORMAT_RGB565 = 0x15151002;
        public const int SDL_PIXELFORMAT_BGR565 = 0x15551002;
        public const int SDL_PIXELFORMAT_RGB24 = 0x17101803;
        public const int SDL_PIXELFORMAT_BGR24 = 0x17401803;
        public const int SDL_PIXELFORMAT_RGB888 = 0x16161804;
        public const int SDL_PIXELFORMAT_RGBX8888 = 0x16261804;
        public const int SDL_PIXELFORMAT_BGR888 = 0x16561804;
        public const int SDL_PIXELFORMAT_BGRX8888 = 0x16661804;
        public const int SDL_PIXELFORMAT_ARGB8888 = 0x16362004;
        public const int SDL_PIXELFORMAT_RGBA8888 = 0x16462004;
        public const int SDL_PIXELFORMAT_ABGR8888 = 0x16762004;
        public const int SDL_PIXELFORMAT_BGRA8888 = 0x16862004;
        public const int SDL_PIXELFORMAT_ARGB2101010 = 0x16372004;
        public const int SDL_PIXELFORMAT_RGBA32 = 0x16762004;
        public const int SDL_PIXELFORMAT_ARGB32 = 0x16862004;
        public const int SDL_PIXELFORMAT_BGRA32 = 0x16362004;
        public const int SDL_PIXELFORMAT_ABGR32 = 0x16462004;
        public const int SDL_PIXELFORMAT_YV12 = 0x32315659;
        public const int SDL_PIXELFORMAT_IYUV = 0x56555949;
        public const int SDL_PIXELFORMAT_YUY2 = 0x32595559;
        public const int SDL_PIXELFORMAT_UYVY = 0x59565955;
        public const int SDL_PIXELFORMAT_YVYU = 0x55595659;
        public const int SDL_PIXELFORMAT_NV12 = 0x3231564E;
        public const int SDL_PIXELFORMAT_NV21 = 0x3132564E;
        public const int SDLK_UNKNOWN = 0x0;
        public const int SDLK_RETURN = 0xD;
        public const int SDLK_ESCAPE = 0x1B;
        public const int SDLK_BACKSPACE = 0x8;
        public const int SDLK_TAB = 0x9;
        public const int SDLK_SPACE = 0x20;
        public const int SDLK_EXCLAIM = 0x21;
        public const int SDLK_QUOTEDBL = 0x22;
        public const int SDLK_HASH = 0x23;
        public const int SDLK_PERCENT = 0x25;
        public const int SDLK_DOLLAR = 0x24;
        public const int SDLK_AMPERSAND = 0x26;
        public const int SDLK_QUOTE = 0x27;
        public const int SDLK_LEFTPAREN = 0x28;
        public const int SDLK_RIGHTPAREN = 0x29;
        public const int SDLK_ASTERISK = 0x2A;
        public const int SDLK_PLUS = 0x2B;
        public const int SDLK_COMMA = 0x2C;
        public const int SDLK_MINUS = 0x2D;
        public const int SDLK_PERIOD = 0x2E;
        public const int SDLK_SLASH = 0x2F;
        public const int SDLK_0 = 0x30;
        public const int SDLK_1 = 0x31;
        public const int SDLK_2 = 0x32;
        public const int SDLK_3 = 0x33;
        public const int SDLK_4 = 0x34;
        public const int SDLK_5 = 0x35;
        public const int SDLK_6 = 0x36;
        public const int SDLK_7 = 0x37;
        public const int SDLK_8 = 0x38;
        public const int SDLK_9 = 0x39;
        public const int SDLK_COLON = 0x3A;
        public const int SDLK_SEMICOLON = 0x3B;
        public const int SDLK_LESS = 0x3C;
        public const int SDLK_EQUALS = 0x3D;
        public const int SDLK_GREATER = 0x3E;
        public const int SDLK_QUESTION = 0x3F;
        public const int SDLK_AT = 0x40;
        public const int SDLK_LEFTBRACKET = 0x5B;
        public const int SDLK_BACKSLASH = 0x5C;
        public const int SDLK_RIGHTBRACKET = 0x5D;
        public const int SDLK_CARET = 0x5E;
        public const int SDLK_UNDERSCORE = 0x5F;
        public const int SDLK_BACKQUOTE = 0x60;
        public const int SDLK_a = 0x61;
        public const int SDLK_b = 0x62;
        public const int SDLK_c = 0x63;
        public const int SDLK_d = 0x64;
        public const int SDLK_e = 0x65;
        public const int SDLK_f = 0x66;
        public const int SDLK_g = 0x67;
        public const int SDLK_h = 0x68;
        public const int SDLK_i = 0x69;
        public const int SDLK_j = 0x6A;
        public const int SDLK_k = 0x6B;
        public const int SDLK_l = 0x6C;
        public const int SDLK_m = 0x6D;
        public const int SDLK_n = 0x6E;
        public const int SDLK_o = 0x6F;
        public const int SDLK_p = 0x70;
        public const int SDLK_q = 0x71;
        public const int SDLK_r = 0x72;
        public const int SDLK_s = 0x73;
        public const int SDLK_t = 0x74;
        public const int SDLK_u = 0x75;
        public const int SDLK_v = 0x76;
        public const int SDLK_w = 0x77;
        public const int SDLK_x = 0x78;
        public const int SDLK_y = 0x79;
        public const int SDLK_z = 0x7A;
        public const int SDLK_CAPSLOCK = 0x40000039;
        public const int SDLK_F1 = 0x4000003A;
        public const int SDLK_F2 = 0x4000003B;
        public const int SDLK_F3 = 0x4000003C;
        public const int SDLK_F4 = 0x4000003D;
        public const int SDLK_F5 = 0x4000003E;
        public const int SDLK_F6 = 0x4000003F;
        public const int SDLK_F7 = 0x40000040;
        public const int SDLK_F8 = 0x40000041;
        public const int SDLK_F9 = 0x40000042;
        public const int SDLK_F10 = 0x40000043;
        public const int SDLK_F11 = 0x40000044;
        public const int SDLK_F12 = 0x40000045;
        public const int SDLK_PRINTSCREEN = 0x40000046;
        public const int SDLK_SCROLLLOCK = 0x40000047;
        public const int SDLK_PAUSE = 0x40000048;
        public const int SDLK_INSERT = 0x40000049;
        public const int SDLK_HOME = 0x4000004A;
        public const int SDLK_PAGEUP = 0x4000004B;
        public const int SDLK_DELETE = 0x7F;
        public const int SDLK_END = 0x4000004D;
        public const int SDLK_PAGEDOWN = 0x4000004E;
        public const int SDLK_RIGHT = 0x4000004F;
        public const int SDLK_LEFT = 0x40000050;
        public const int SDLK_DOWN = 0x40000051;
        public const int SDLK_UP = 0x40000052;
        public const int SDLK_NUMLOCKCLEAR = 0x40000053;
        public const int SDLK_KP_DIVIDE = 0x40000054;
        public const int SDLK_KP_MULTIPLY = 0x40000055;
        public const int SDLK_KP_MINUS = 0x40000056;
        public const int SDLK_KP_PLUS = 0x40000057;
        public const int SDLK_KP_ENTER = 0x40000058;
        public const int SDLK_KP_1 = 0x40000059;
        public const int SDLK_KP_2 = 0x4000005A;
        public const int SDLK_KP_3 = 0x4000005B;
        public const int SDLK_KP_4 = 0x4000005C;
        public const int SDLK_KP_5 = 0x4000005D;
        public const int SDLK_KP_6 = 0x4000005E;
        public const int SDLK_KP_7 = 0x4000005F;
        public const int SDLK_KP_8 = 0x40000060;
        public const int SDLK_KP_9 = 0x40000061;
        public const int SDLK_KP_0 = 0x40000062;
        public const int SDLK_KP_PERIOD = 0x40000063;
        public const int SDLK_APPLICATION = 0x40000065;
        public const int SDLK_POWER = 0x40000066;
        public const int SDLK_KP_EQUALS = 0x40000067;
        public const int SDLK_F13 = 0x40000068;
        public const int SDLK_F14 = 0x40000069;
        public const int SDLK_F15 = 0x4000006A;
        public const int SDLK_F16 = 0x4000006B;
        public const int SDLK_F17 = 0x4000006C;
        public const int SDLK_F18 = 0x4000006D;
        public const int SDLK_F19 = 0x4000006E;
        public const int SDLK_F20 = 0x4000006F;
        public const int SDLK_F21 = 0x40000070;
        public const int SDLK_F22 = 0x40000071;
        public const int SDLK_F23 = 0x40000072;
        public const int SDLK_F24 = 0x40000073;
        public const int SDLK_EXECUTE = 0x40000074;
        public const int SDLK_HELP = 0x40000075;
        public const int SDLK_MENU = 0x40000076;
        public const int SDLK_SELECT = 0x40000077;
        public const int SDLK_STOP = 0x40000078;
        public const int SDLK_AGAIN = 0x40000079;
        public const int SDLK_UNDO = 0x4000007A;
        public const int SDLK_CUT = 0x4000007B;
        public const int SDLK_COPY = 0x4000007C;
        public const int SDLK_PASTE = 0x4000007D;
        public const int SDLK_FIND = 0x4000007E;
        public const int SDLK_MUTE = 0x4000007F;
        public const int SDLK_VOLUMEUP = 0x40000080;
        public const int SDLK_VOLUMEDOWN = 0x40000081;
        public const int SDLK_KP_COMMA = 0x40000085;
        public const int SDLK_KP_EQUALSAS400 = 0x40000086;
        public const int SDLK_ALTERASE = 0x40000099;
        public const int SDLK_SYSREQ = 0x4000009A;
        public const int SDLK_CANCEL = 0x4000009B;
        public const int SDLK_CLEAR = 0x4000009C;
        public const int SDLK_PRIOR = 0x4000009D;
        public const int SDLK_RETURN2 = 0x4000009E;
        public const int SDLK_SEPARATOR = 0x4000009F;
        public const int SDLK_OUT = 0x400000A0;
        public const int SDLK_OPER = 0x400000A1;
        public const int SDLK_CLEARAGAIN = 0x400000A2;
        public const int SDLK_CRSEL = 0x400000A3;
        public const int SDLK_EXSEL = 0x400000A4;
        public const int SDLK_KP_00 = 0x400000B0;
        public const int SDLK_KP_000 = 0x400000B1;
        public const int SDLK_THOUSANDSSEPARATOR = 0x400000B2;
        public const int SDLK_DECIMALSEPARATOR = 0x400000B3;
        public const int SDLK_CURRENCYUNIT = 0x400000B4;
        public const int SDLK_CURRENCYSUBUNIT = 0x400000B5;
        public const int SDLK_KP_LEFTPAREN = 0x400000B6;
        public const int SDLK_KP_RIGHTPAREN = 0x400000B7;
        public const int SDLK_KP_LEFTBRACE = 0x400000B8;
        public const int SDLK_KP_RIGHTBRACE = 0x400000B9;
        public const int SDLK_KP_TAB = 0x400000BA;
        public const int SDLK_KP_BACKSPACE = 0x400000BB;
        public const int SDLK_KP_A = 0x400000BC;
        public const int SDLK_KP_B = 0x400000BD;
        public const int SDLK_KP_C = 0x400000BE;
        public const int SDLK_KP_D = 0x400000BF;
        public const int SDLK_KP_E = 0x400000C0;
        public const int SDLK_KP_F = 0x400000C1;
        public const int SDLK_KP_XOR = 0x400000C2;
        public const int SDLK_KP_POWER = 0x400000C3;
        public const int SDLK_KP_PERCENT = 0x400000C4;
        public const int SDLK_KP_LESS = 0x400000C5;
        public const int SDLK_KP_GREATER = 0x400000C6;
        public const int SDLK_KP_AMPERSAND = 0x400000C7;
        public const int SDLK_KP_DBLAMPERSAND = 0x400000C8;
        public const int SDLK_KP_VERTICALBAR = 0x400000C9;
        public const int SDLK_KP_DBLVERTICALBAR = 0x400000CA;
        public const int SDLK_KP_COLON = 0x400000CB;
        public const int SDLK_KP_HASH = 0x400000CC;
        public const int SDLK_KP_SPACE = 0x400000CD;
        public const int SDLK_KP_AT = 0x400000CE;
        public const int SDLK_KP_EXCLAM = 0x400000CF;
        public const int SDLK_KP_MEMSTORE = 0x400000D0;
        public const int SDLK_KP_MEMRECALL = 0x400000D1;
        public const int SDLK_KP_MEMCLEAR = 0x400000D2;
        public const int SDLK_KP_MEMADD = 0x400000D3;
        public const int SDLK_KP_MEMSUBTRACT = 0x400000D4;
        public const int SDLK_KP_MEMMULTIPLY = 0x400000D5;
        public const int SDLK_KP_MEMDIVIDE = 0x400000D6;
        public const int SDLK_KP_PLUSMINUS = 0x400000D7;
        public const int SDLK_KP_CLEAR = 0x400000D8;
        public const int SDLK_KP_CLEARENTRY = 0x400000D9;
        public const int SDLK_KP_BINARY = 0x400000DA;
        public const int SDLK_KP_OCTAL = 0x400000DB;
        public const int SDLK_KP_DECIMAL = 0x400000DC;
        public const int SDLK_KP_HEXADECIMAL = 0x400000DD;
        public const int SDLK_LCTRL = 0x400000E0;
        public const int SDLK_LSHIFT = 0x400000E1;
        public const int SDLK_LALT = 0x400000E2;
        public const int SDLK_LGUI = 0x400000E3;
        public const int SDLK_RCTRL = 0x400000E4;
        public const int SDLK_RSHIFT = 0x400000E5;
        public const int SDLK_RALT = 0x400000E6;
        public const int SDLK_RGUI = 0x400000E7;
        public const int SDLK_MODE = 0x40000101;
        public const int SDLK_AUDIONEXT = 0x40000102;
        public const int SDLK_AUDIOPREV = 0x40000103;
        public const int SDLK_AUDIOSTOP = 0x40000104;
        public const int SDLK_AUDIOPLAY = 0x40000105;
        public const int SDLK_AUDIOMUTE = 0x40000106;
        public const int SDLK_MEDIASELECT = 0x40000107;
        public const int SDLK_WWW = 0x40000108;
        public const int SDLK_MAIL = 0x40000109;
        public const int SDLK_CALCULATOR = 0x4000010A;
        public const int SDLK_COMPUTER = 0x4000010B;
        public const int SDLK_AC_SEARCH = 0x4000010C;
        public const int SDLK_AC_HOME = 0x4000010D;
        public const int SDLK_AC_BACK = 0x4000010E;
        public const int SDLK_AC_FORWARD = 0x4000010F;
        public const int SDLK_AC_STOP = 0x40000110;
        public const int SDLK_AC_REFRESH = 0x40000111;
        public const int SDLK_AC_BOOKMARKS = 0x40000112;
        public const int SDLK_BRIGHTNESSDOWN = 0x40000113;
        public const int SDLK_BRIGHTNESSUP = 0x40000114;
        public const int SDLK_DISPLAYSWITCH = 0x40000115;
        public const int SDLK_KBDILLUMTOGGLE = 0x40000116;
        public const int SDLK_KBDILLUMDOWN = 0x40000117;
        public const int SDLK_KBDILLUMUP = 0x40000118;
        public const int SDLK_EJECT = 0x40000119;
        public const int SDLK_SLEEP = 0x4000011A;
        public const int SDL_LOG_CATEGORY_APPLICATION = 0x0;
        public const int SDL_LOG_CATEGORY_ERROR = 0x1;
        public const int SDL_LOG_CATEGORY_ASSERT = 0x2;
        public const int SDL_LOG_CATEGORY_SYSTEM = 0x3;
        public const int SDL_LOG_CATEGORY_AUDIO = 0x4;
        public const int SDL_LOG_CATEGORY_VIDEO = 0x5;
        public const int SDL_LOG_CATEGORY_RENDER = 0x6;
        public const int SDL_LOG_CATEGORY_INPUT = 0x7;
        public const int SDL_LOG_CATEGORY_TEST = 0x8;
        public const int SDL_LOG_CATEGORY_RESERVED1 = 0x9;
        public const int SDL_LOG_CATEGORY_RESERVED2 = 0xA;
        public const int SDL_LOG_CATEGORY_RESERVED3 = 0xB;
        public const int SDL_LOG_CATEGORY_RESERVED4 = 0xC;
        public const int SDL_LOG_CATEGORY_RESERVED5 = 0xD;
        public const int SDL_LOG_CATEGORY_RESERVED6 = 0xE;
        public const int SDL_LOG_CATEGORY_RESERVED7 = 0xF;
        public const int SDL_LOG_CATEGORY_RESERVED8 = 0x10;
        public const int SDL_LOG_CATEGORY_RESERVED9 = 0x11;
        public const int SDL_LOG_CATEGORY_RESERVED10 = 0x12;
        public const int SDL_LOG_CATEGORY_CUSTOM = 0x13;
        public const int SDL_ASSERTION_RETRY = 0x0;
        public const int SDL_ASSERTION_BREAK = 0x1;
        public const int SDL_ASSERTION_ABORT = 0x2;
        public const int SDL_ASSERTION_IGNORE = 0x3;
        public const int SDL_ASSERTION_ALWAYS_IGNORE = 0x4;
        public const int SDL_AUDIO_STOPPED = 0x0;
        public const int SDL_AUDIO_PLAYING = 0x1;
        public const int SDL_AUDIO_PAUSED = 0x2;
        public const int SDL_BLENDMODE_NONE = 0x0;
        public const int SDL_BLENDMODE_BLEND = 0x1;
        public const int SDL_BLENDMODE_ADD = 0x2;
        public const int SDL_BLENDMODE_MOD = 0x4;
        public const int DUMMY_ENUM_VALUE = 0x0;
        public const int SDL_FIRSTEVENT = 0x0;
        public const int SDL_QUIT = 0x100;
        public const int SDL_APP_TERMINATING = 0x101;
        public const int SDL_APP_LOWMEMORY = 0x102;
        public const int SDL_APP_WILLENTERBACKGROUND = 0x103;
        public const int SDL_APP_DIDENTERBACKGROUND = 0x104;
        public const int SDL_APP_WILLENTERFOREGROUND = 0x105;
        public const int SDL_APP_DIDENTERFOREGROUND = 0x106;
        public const int SDL_WINDOWEVENT = 0x200;
        public const int SDL_SYSWMEVENT = 0x201;
        public const int SDL_KEYDOWN = 0x300;
        public const int SDL_KEYUP = 0x301;
        public const int SDL_TEXTEDITING = 0x302;
        public const int SDL_TEXTINPUT = 0x303;
        public const int SDL_KEYMAPCHANGED = 0x304;
        public const int SDL_MOUSEMOTION = 0x400;
        public const int SDL_MOUSEBUTTONDOWN = 0x401;
        public const int SDL_MOUSEBUTTONUP = 0x402;
        public const int SDL_MOUSEWHEEL = 0x403;
        public const int SDL_JOYAXISMOTION = 0x600;
        public const int SDL_JOYBALLMOTION = 0x601;
        public const int SDL_JOYHATMOTION = 0x602;
        public const int SDL_JOYBUTTONDOWN = 0x603;
        public const int SDL_JOYBUTTONUP = 0x604;
        public const int SDL_JOYDEVICEADDED = 0x605;
        public const int SDL_JOYDEVICEREMOVED = 0x606;
        public const int SDL_CONTROLLERAXISMOTION = 0x650;
        public const int SDL_CONTROLLERBUTTONDOWN = 0x651;
        public const int SDL_CONTROLLERBUTTONUP = 0x652;
        public const int SDL_CONTROLLERDEVICEADDED = 0x653;
        public const int SDL_CONTROLLERDEVICEREMOVED = 0x654;
        public const int SDL_CONTROLLERDEVICEREMAPPED = 0x655;
        public const int SDL_FINGERDOWN = 0x700;
        public const int SDL_FINGERUP = 0x701;
        public const int SDL_FINGERMOTION = 0x702;
        public const int SDL_DOLLARGESTURE = 0x800;
        public const int SDL_DOLLARRECORD = 0x801;
        public const int SDL_MULTIGESTURE = 0x802;
        public const int SDL_CLIPBOARDUPDATE = 0x900;
        public const int SDL_DROPFILE = 0x1000;
        public const int SDL_DROPTEXT = 0x1001;
        public const int SDL_DROPBEGIN = 0x1002;
        public const int SDL_DROPCOMPLETE = 0x1003;
        public const int SDL_AUDIODEVICEADDED = 0x1100;
        public const int SDL_AUDIODEVICEREMOVED = 0x1101;
        public const int SDL_RENDER_TARGETS_RESET = 0x2000;
        public const int SDL_RENDER_DEVICE_RESET = 0x2001;
        public const int SDL_USEREVENT = 0x8000;
        public const int SDL_LASTEVENT = 0xFFFF;
        public const int SDL_GL_RED_SIZE = 0x0;
        public const int SDL_GL_GREEN_SIZE = 0x1;
        public const int SDL_GL_BLUE_SIZE = 0x2;
        public const int SDL_GL_ALPHA_SIZE = 0x3;
        public const int SDL_GL_BUFFER_SIZE = 0x4;
        public const int SDL_GL_DOUBLEBUFFER = 0x5;
        public const int SDL_GL_DEPTH_SIZE = 0x6;
        public const int SDL_GL_STENCIL_SIZE = 0x7;
        public const int SDL_GL_ACCUM_RED_SIZE = 0x8;
        public const int SDL_GL_ACCUM_GREEN_SIZE = 0x9;
        public const int SDL_GL_ACCUM_BLUE_SIZE = 0xA;
        public const int SDL_GL_ACCUM_ALPHA_SIZE = 0xB;
        public const int SDL_GL_STEREO = 0xC;
        public const int SDL_GL_MULTISAMPLEBUFFERS = 0xD;
        public const int SDL_GL_MULTISAMPLESAMPLES = 0xE;
        public const int SDL_GL_ACCELERATED_VISUAL = 0xF;
        public const int SDL_GL_RETAINED_BACKING = 0x10;
        public const int SDL_GL_CONTEXT_MAJOR_VERSION = 0x11;
        public const int SDL_GL_CONTEXT_MINOR_VERSION = 0x12;
        public const int SDL_GL_CONTEXT_EGL = 0x13;
        public const int SDL_GL_CONTEXT_FLAGS = 0x14;
        public const int SDL_GL_CONTEXT_PROFILE_MASK = 0x15;
        public const int SDL_GL_SHARE_WITH_CURRENT_CONTEXT = 0x16;
        public const int SDL_GL_FRAMEBUFFER_SRGB_CAPABLE = 0x17;
        public const int SDL_GL_CONTEXT_RELEASE_BEHAVIOR = 0x18;
        public const int SDL_GL_CONTEXT_DEBUG_FLAG = 0x1;
        public const int SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG = 0x2;
        public const int SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG = 0x4;
        public const int SDL_GL_CONTEXT_RESET_ISOLATION_FLAG = 0x8;
        public const int SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE = 0x0;
        public const int SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x1;
        public const int SDL_GL_CONTEXT_PROFILE_CORE = 0x1;
        public const int SDL_GL_CONTEXT_PROFILE_COMPATIBILITY = 0x2;
        public const int SDL_GL_CONTEXT_PROFILE_ES = 0x4;
        public const int SDL_CONTROLLER_AXIS_INVALID = -1;
        public const int SDL_CONTROLLER_AXIS_LEFTX = 0x0;
        public const int SDL_CONTROLLER_AXIS_LEFTY = 0x1;
        public const int SDL_CONTROLLER_AXIS_RIGHTX = 0x2;
        public const int SDL_CONTROLLER_AXIS_RIGHTY = 0x3;
        public const int SDL_CONTROLLER_AXIS_TRIGGERLEFT = 0x4;
        public const int SDL_CONTROLLER_AXIS_TRIGGERRIGHT = 0x5;
        public const int SDL_CONTROLLER_AXIS_MAX = 0x6;
        public const int SDL_CONTROLLER_BINDTYPE_NONE = 0x0;
        public const int SDL_CONTROLLER_BINDTYPE_BUTTON = 0x1;
        public const int SDL_CONTROLLER_BINDTYPE_AXIS = 0x2;
        public const int SDL_CONTROLLER_BINDTYPE_HAT = 0x3;
        public const int SDL_CONTROLLER_BUTTON_INVALID = -1;
        public const int SDL_CONTROLLER_BUTTON_A = 0x0;
        public const int SDL_CONTROLLER_BUTTON_B = 0x1;
        public const int SDL_CONTROLLER_BUTTON_X = 0x2;
        public const int SDL_CONTROLLER_BUTTON_Y = 0x3;
        public const int SDL_CONTROLLER_BUTTON_BACK = 0x4;
        public const int SDL_CONTROLLER_BUTTON_GUIDE = 0x5;
        public const int SDL_CONTROLLER_BUTTON_START = 0x6;
        public const int SDL_CONTROLLER_BUTTON_LEFTSTICK = 0x7;
        public const int SDL_CONTROLLER_BUTTON_RIGHTSTICK = 0x8;
        public const int SDL_CONTROLLER_BUTTON_LEFTSHOULDER = 0x9;
        public const int SDL_CONTROLLER_BUTTON_RIGHTSHOULDER = 0xA;
        public const int SDL_CONTROLLER_BUTTON_DPAD_UP = 0xB;
        public const int SDL_CONTROLLER_BUTTON_DPAD_DOWN = 0xC;
        public const int SDL_CONTROLLER_BUTTON_DPAD_LEFT = 0xD;
        public const int SDL_CONTROLLER_BUTTON_DPAD_RIGHT = 0xE;
        public const int SDL_CONTROLLER_BUTTON_MAX = 0xF;
        public const int SDL_HINT_DEFAULT = 0x0;
        public const int SDL_HINT_NORMAL = 0x1;
        public const int SDL_HINT_OVERRIDE = 0x2;
        public const int SDL_HITTEST_NORMAL = 0x0;
        public const int SDL_HITTEST_DRAGGABLE = 0x1;
        public const int SDL_HITTEST_RESIZE_TOPLEFT = 0x2;
        public const int SDL_HITTEST_RESIZE_TOP = 0x3;
        public const int SDL_HITTEST_RESIZE_TOPRIGHT = 0x4;
        public const int SDL_HITTEST_RESIZE_RIGHT = 0x5;
        public const int SDL_HITTEST_RESIZE_BOTTOMRIGHT = 0x6;
        public const int SDL_HITTEST_RESIZE_BOTTOM = 0x7;
        public const int SDL_HITTEST_RESIZE_BOTTOMLEFT = 0x8;
        public const int SDL_HITTEST_RESIZE_LEFT = 0x9;
        public const int SDL_JOYSTICK_POWER_UNKNOWN = -1;
        public const int SDL_JOYSTICK_POWER_EMPTY = 0x0;
        public const int SDL_JOYSTICK_POWER_LOW = 0x1;
        public const int SDL_JOYSTICK_POWER_MEDIUM = 0x2;
        public const int SDL_JOYSTICK_POWER_FULL = 0x3;
        public const int SDL_JOYSTICK_POWER_WIRED = 0x4;
        public const int SDL_JOYSTICK_POWER_MAX = 0x5;
        public const int KMOD_NONE = 0x0;
        public const int KMOD_LSHIFT = 0x1;
        public const int KMOD_RSHIFT = 0x2;
        public const int KMOD_LCTRL = 0x40;
        public const int KMOD_RCTRL = 0x80;
        public const int KMOD_LALT = 0x100;
        public const int KMOD_RALT = 0x200;
        public const int KMOD_LGUI = 0x400;
        public const int KMOD_RGUI = 0x800;
        public const int KMOD_NUM = 0x1000;
        public const int KMOD_CAPS = 0x2000;
        public const int KMOD_MODE = 0x4000;
        public const int KMOD_RESERVED = 0x8000;
        public const int SDL_LOG_PRIORITY_VERBOSE = 0x1;
        public const int SDL_LOG_PRIORITY_DEBUG = 0x2;
        public const int SDL_LOG_PRIORITY_INFO = 0x3;
        public const int SDL_LOG_PRIORITY_WARN = 0x4;
        public const int SDL_LOG_PRIORITY_ERROR = 0x5;
        public const int SDL_LOG_PRIORITY_CRITICAL = 0x6;
        public const int SDL_NUM_LOG_PRIORITIES = 0x7;
        public const int SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT = 0x1;
        public const int SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT = 0x2;
        public const int SDL_MESSAGEBOX_COLOR_BACKGROUND = 0x0;
        public const int SDL_MESSAGEBOX_COLOR_TEXT = 0x1;
        public const int SDL_MESSAGEBOX_COLOR_BUTTON_BORDER = 0x2;
        public const int SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND = 0x3;
        public const int SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED = 0x4;
        public const int SDL_MESSAGEBOX_COLOR_MAX = 0x5;
        public const int SDL_MESSAGEBOX_ERROR = 0x10;
        public const int SDL_MESSAGEBOX_WARNING = 0x20;
        public const int SDL_MESSAGEBOX_INFORMATION = 0x40;
        public const int SDL_MOUSEWHEEL_NORMAL = 0x0;
        public const int SDL_MOUSEWHEEL_FLIPPED = 0x1;
        public const int SDL_POWERSTATE_UNKNOWN = 0x0;
        public const int SDL_POWERSTATE_ON_BATTERY = 0x1;
        public const int SDL_POWERSTATE_NO_BATTERY = 0x2;
        public const int SDL_POWERSTATE_CHARGING = 0x3;
        public const int SDL_POWERSTATE_CHARGED = 0x4;
        public const int SDL_RENDERER_SOFTWARE = 0x1;
        public const int SDL_RENDERER_ACCELERATED = 0x2;
        public const int SDL_RENDERER_PRESENTVSYNC = 0x4;
        public const int SDL_RENDERER_TARGETTEXTURE = 0x8;
        public const int SDL_FLIP_NONE = 0x0;
        public const int SDL_FLIP_HORIZONTAL = 0x1;
        public const int SDL_FLIP_VERTICAL = 0x2;
        public const int SDL_SCANCODE_UNKNOWN = 0x0;
        public const int SDL_SCANCODE_A = 0x4;
        public const int SDL_SCANCODE_B = 0x5;
        public const int SDL_SCANCODE_C = 0x6;
        public const int SDL_SCANCODE_D = 0x7;
        public const int SDL_SCANCODE_E = 0x8;
        public const int SDL_SCANCODE_F = 0x9;
        public const int SDL_SCANCODE_G = 0xA;
        public const int SDL_SCANCODE_H = 0xB;
        public const int SDL_SCANCODE_I = 0xC;
        public const int SDL_SCANCODE_J = 0xD;
        public const int SDL_SCANCODE_K = 0xE;
        public const int SDL_SCANCODE_L = 0xF;
        public const int SDL_SCANCODE_M = 0x10;
        public const int SDL_SCANCODE_N = 0x11;
        public const int SDL_SCANCODE_O = 0x12;
        public const int SDL_SCANCODE_P = 0x13;
        public const int SDL_SCANCODE_Q = 0x14;
        public const int SDL_SCANCODE_R = 0x15;
        public const int SDL_SCANCODE_S = 0x16;
        public const int SDL_SCANCODE_T = 0x17;
        public const int SDL_SCANCODE_U = 0x18;
        public const int SDL_SCANCODE_V = 0x19;
        public const int SDL_SCANCODE_W = 0x1A;
        public const int SDL_SCANCODE_X = 0x1B;
        public const int SDL_SCANCODE_Y = 0x1C;
        public const int SDL_SCANCODE_Z = 0x1D;
        public const int SDL_SCANCODE_1 = 0x1E;
        public const int SDL_SCANCODE_2 = 0x1F;
        public const int SDL_SCANCODE_3 = 0x20;
        public const int SDL_SCANCODE_4 = 0x21;
        public const int SDL_SCANCODE_5 = 0x22;
        public const int SDL_SCANCODE_6 = 0x23;
        public const int SDL_SCANCODE_7 = 0x24;
        public const int SDL_SCANCODE_8 = 0x25;
        public const int SDL_SCANCODE_9 = 0x26;
        public const int SDL_SCANCODE_0 = 0x27;
        public const int SDL_SCANCODE_RETURN = 0x28;
        public const int SDL_SCANCODE_ESCAPE = 0x29;
        public const int SDL_SCANCODE_BACKSPACE = 0x2A;
        public const int SDL_SCANCODE_TAB = 0x2B;
        public const int SDL_SCANCODE_SPACE = 0x2C;
        public const int SDL_SCANCODE_MINUS = 0x2D;
        public const int SDL_SCANCODE_EQUALS = 0x2E;
        public const int SDL_SCANCODE_LEFTBRACKET = 0x2F;
        public const int SDL_SCANCODE_RIGHTBRACKET = 0x30;
        public const int SDL_SCANCODE_BACKSLASH = 0x31;
        public const int SDL_SCANCODE_NONUSHASH = 0x32;
        public const int SDL_SCANCODE_SEMICOLON = 0x33;
        public const int SDL_SCANCODE_APOSTROPHE = 0x34;
        public const int SDL_SCANCODE_GRAVE = 0x35;
        public const int SDL_SCANCODE_COMMA = 0x36;
        public const int SDL_SCANCODE_PERIOD = 0x37;
        public const int SDL_SCANCODE_SLASH = 0x38;
        public const int SDL_SCANCODE_CAPSLOCK = 0x39;
        public const int SDL_SCANCODE_F1 = 0x3A;
        public const int SDL_SCANCODE_F2 = 0x3B;
        public const int SDL_SCANCODE_F3 = 0x3C;
        public const int SDL_SCANCODE_F4 = 0x3D;
        public const int SDL_SCANCODE_F5 = 0x3E;
        public const int SDL_SCANCODE_F6 = 0x3F;
        public const int SDL_SCANCODE_F7 = 0x40;
        public const int SDL_SCANCODE_F8 = 0x41;
        public const int SDL_SCANCODE_F9 = 0x42;
        public const int SDL_SCANCODE_F10 = 0x43;
        public const int SDL_SCANCODE_F11 = 0x44;
        public const int SDL_SCANCODE_F12 = 0x45;
        public const int SDL_SCANCODE_PRINTSCREEN = 0x46;
        public const int SDL_SCANCODE_SCROLLLOCK = 0x47;
        public const int SDL_SCANCODE_PAUSE = 0x48;
        public const int SDL_SCANCODE_INSERT = 0x49;
        public const int SDL_SCANCODE_HOME = 0x4A;
        public const int SDL_SCANCODE_PAGEUP = 0x4B;
        public const int SDL_SCANCODE_DELETE = 0x4C;
        public const int SDL_SCANCODE_END = 0x4D;
        public const int SDL_SCANCODE_PAGEDOWN = 0x4E;
        public const int SDL_SCANCODE_RIGHT = 0x4F;
        public const int SDL_SCANCODE_LEFT = 0x50;
        public const int SDL_SCANCODE_DOWN = 0x51;
        public const int SDL_SCANCODE_UP = 0x52;
        public const int SDL_SCANCODE_NUMLOCKCLEAR = 0x53;
        public const int SDL_SCANCODE_KP_DIVIDE = 0x54;
        public const int SDL_SCANCODE_KP_MULTIPLY = 0x55;
        public const int SDL_SCANCODE_KP_MINUS = 0x56;
        public const int SDL_SCANCODE_KP_PLUS = 0x57;
        public const int SDL_SCANCODE_KP_ENTER = 0x58;
        public const int SDL_SCANCODE_KP_1 = 0x59;
        public const int SDL_SCANCODE_KP_2 = 0x5A;
        public const int SDL_SCANCODE_KP_3 = 0x5B;
        public const int SDL_SCANCODE_KP_4 = 0x5C;
        public const int SDL_SCANCODE_KP_5 = 0x5D;
        public const int SDL_SCANCODE_KP_6 = 0x5E;
        public const int SDL_SCANCODE_KP_7 = 0x5F;
        public const int SDL_SCANCODE_KP_8 = 0x60;
        public const int SDL_SCANCODE_KP_9 = 0x61;
        public const int SDL_SCANCODE_KP_0 = 0x62;
        public const int SDL_SCANCODE_KP_PERIOD = 0x63;
        public const int SDL_SCANCODE_NONUSBACKSLASH = 0x64;
        public const int SDL_SCANCODE_APPLICATION = 0x65;
        public const int SDL_SCANCODE_POWER = 0x66;
        public const int SDL_SCANCODE_KP_EQUALS = 0x67;
        public const int SDL_SCANCODE_F13 = 0x68;
        public const int SDL_SCANCODE_F14 = 0x69;
        public const int SDL_SCANCODE_F15 = 0x6A;
        public const int SDL_SCANCODE_F16 = 0x6B;
        public const int SDL_SCANCODE_F17 = 0x6C;
        public const int SDL_SCANCODE_F18 = 0x6D;
        public const int SDL_SCANCODE_F19 = 0x6E;
        public const int SDL_SCANCODE_F20 = 0x6F;
        public const int SDL_SCANCODE_F21 = 0x70;
        public const int SDL_SCANCODE_F22 = 0x71;
        public const int SDL_SCANCODE_F23 = 0x72;
        public const int SDL_SCANCODE_F24 = 0x73;
        public const int SDL_SCANCODE_EXECUTE = 0x74;
        public const int SDL_SCANCODE_HELP = 0x75;
        public const int SDL_SCANCODE_MENU = 0x76;
        public const int SDL_SCANCODE_SELECT = 0x77;
        public const int SDL_SCANCODE_STOP = 0x78;
        public const int SDL_SCANCODE_AGAIN = 0x79;
        public const int SDL_SCANCODE_UNDO = 0x7A;
        public const int SDL_SCANCODE_CUT = 0x7B;
        public const int SDL_SCANCODE_COPY = 0x7C;
        public const int SDL_SCANCODE_PASTE = 0x7D;
        public const int SDL_SCANCODE_FIND = 0x7E;
        public const int SDL_SCANCODE_MUTE = 0x7F;
        public const int SDL_SCANCODE_VOLUMEUP = 0x80;
        public const int SDL_SCANCODE_VOLUMEDOWN = 0x81;
        public const int SDL_SCANCODE_KP_COMMA = 0x85;
        public const int SDL_SCANCODE_KP_EQUALSAS400 = 0x86;
        public const int SDL_SCANCODE_INTERNATIONAL1 = 0x87;
        public const int SDL_SCANCODE_INTERNATIONAL2 = 0x88;
        public const int SDL_SCANCODE_INTERNATIONAL3 = 0x89;
        public const int SDL_SCANCODE_INTERNATIONAL4 = 0x8A;
        public const int SDL_SCANCODE_INTERNATIONAL5 = 0x8B;
        public const int SDL_SCANCODE_INTERNATIONAL6 = 0x8C;
        public const int SDL_SCANCODE_INTERNATIONAL7 = 0x8D;
        public const int SDL_SCANCODE_INTERNATIONAL8 = 0x8E;
        public const int SDL_SCANCODE_INTERNATIONAL9 = 0x8F;
        public const int SDL_SCANCODE_LANG1 = 0x90;
        public const int SDL_SCANCODE_LANG2 = 0x91;
        public const int SDL_SCANCODE_LANG3 = 0x92;
        public const int SDL_SCANCODE_LANG4 = 0x93;
        public const int SDL_SCANCODE_LANG5 = 0x94;
        public const int SDL_SCANCODE_LANG6 = 0x95;
        public const int SDL_SCANCODE_LANG7 = 0x96;
        public const int SDL_SCANCODE_LANG8 = 0x97;
        public const int SDL_SCANCODE_LANG9 = 0x98;
        public const int SDL_SCANCODE_ALTERASE = 0x99;
        public const int SDL_SCANCODE_SYSREQ = 0x9A;
        public const int SDL_SCANCODE_CANCEL = 0x9B;
        public const int SDL_SCANCODE_CLEAR = 0x9C;
        public const int SDL_SCANCODE_PRIOR = 0x9D;
        public const int SDL_SCANCODE_RETURN2 = 0x9E;
        public const int SDL_SCANCODE_SEPARATOR = 0x9F;
        public const int SDL_SCANCODE_OUT = 0xA0;
        public const int SDL_SCANCODE_OPER = 0xA1;
        public const int SDL_SCANCODE_CLEARAGAIN = 0xA2;
        public const int SDL_SCANCODE_CRSEL = 0xA3;
        public const int SDL_SCANCODE_EXSEL = 0xA4;
        public const int SDL_SCANCODE_KP_00 = 0xB0;
        public const int SDL_SCANCODE_KP_000 = 0xB1;
        public const int SDL_SCANCODE_THOUSANDSSEPARATOR = 0xB2;
        public const int SDL_SCANCODE_DECIMALSEPARATOR = 0xB3;
        public const int SDL_SCANCODE_CURRENCYUNIT = 0xB4;
        public const int SDL_SCANCODE_CURRENCYSUBUNIT = 0xB5;
        public const int SDL_SCANCODE_KP_LEFTPAREN = 0xB6;
        public const int SDL_SCANCODE_KP_RIGHTPAREN = 0xB7;
        public const int SDL_SCANCODE_KP_LEFTBRACE = 0xB8;
        public const int SDL_SCANCODE_KP_RIGHTBRACE = 0xB9;
        public const int SDL_SCANCODE_KP_TAB = 0xBA;
        public const int SDL_SCANCODE_KP_BACKSPACE = 0xBB;
        public const int SDL_SCANCODE_KP_A = 0xBC;
        public const int SDL_SCANCODE_KP_B = 0xBD;
        public const int SDL_SCANCODE_KP_C = 0xBE;
        public const int SDL_SCANCODE_KP_D = 0xBF;
        public const int SDL_SCANCODE_KP_E = 0xC0;
        public const int SDL_SCANCODE_KP_F = 0xC1;
        public const int SDL_SCANCODE_KP_XOR = 0xC2;
        public const int SDL_SCANCODE_KP_POWER = 0xC3;
        public const int SDL_SCANCODE_KP_PERCENT = 0xC4;
        public const int SDL_SCANCODE_KP_LESS = 0xC5;
        public const int SDL_SCANCODE_KP_GREATER = 0xC6;
        public const int SDL_SCANCODE_KP_AMPERSAND = 0xC7;
        public const int SDL_SCANCODE_KP_DBLAMPERSAND = 0xC8;
        public const int SDL_SCANCODE_KP_VERTICALBAR = 0xC9;
        public const int SDL_SCANCODE_KP_DBLVERTICALBAR = 0xCA;
        public const int SDL_SCANCODE_KP_COLON = 0xCB;
        public const int SDL_SCANCODE_KP_HASH = 0xCC;
        public const int SDL_SCANCODE_KP_SPACE = 0xCD;
        public const int SDL_SCANCODE_KP_AT = 0xCE;
        public const int SDL_SCANCODE_KP_EXCLAM = 0xCF;
        public const int SDL_SCANCODE_KP_MEMSTORE = 0xD0;
        public const int SDL_SCANCODE_KP_MEMRECALL = 0xD1;
        public const int SDL_SCANCODE_KP_MEMCLEAR = 0xD2;
        public const int SDL_SCANCODE_KP_MEMADD = 0xD3;
        public const int SDL_SCANCODE_KP_MEMSUBTRACT = 0xD4;
        public const int SDL_SCANCODE_KP_MEMMULTIPLY = 0xD5;
        public const int SDL_SCANCODE_KP_MEMDIVIDE = 0xD6;
        public const int SDL_SCANCODE_KP_PLUSMINUS = 0xD7;
        public const int SDL_SCANCODE_KP_CLEAR = 0xD8;
        public const int SDL_SCANCODE_KP_CLEARENTRY = 0xD9;
        public const int SDL_SCANCODE_KP_BINARY = 0xDA;
        public const int SDL_SCANCODE_KP_OCTAL = 0xDB;
        public const int SDL_SCANCODE_KP_DECIMAL = 0xDC;
        public const int SDL_SCANCODE_KP_HEXADECIMAL = 0xDD;
        public const int SDL_SCANCODE_LCTRL = 0xE0;
        public const int SDL_SCANCODE_LSHIFT = 0xE1;
        public const int SDL_SCANCODE_LALT = 0xE2;
        public const int SDL_SCANCODE_LGUI = 0xE3;
        public const int SDL_SCANCODE_RCTRL = 0xE4;
        public const int SDL_SCANCODE_RSHIFT = 0xE5;
        public const int SDL_SCANCODE_RALT = 0xE6;
        public const int SDL_SCANCODE_RGUI = 0xE7;
        public const int SDL_SCANCODE_MODE = 0x101;
        public const int SDL_SCANCODE_AUDIONEXT = 0x102;
        public const int SDL_SCANCODE_AUDIOPREV = 0x103;
        public const int SDL_SCANCODE_AUDIOSTOP = 0x104;
        public const int SDL_SCANCODE_AUDIOPLAY = 0x105;
        public const int SDL_SCANCODE_AUDIOMUTE = 0x106;
        public const int SDL_SCANCODE_MEDIASELECT = 0x107;
        public const int SDL_SCANCODE_WWW = 0x108;
        public const int SDL_SCANCODE_MAIL = 0x109;
        public const int SDL_SCANCODE_CALCULATOR = 0x10A;
        public const int SDL_SCANCODE_COMPUTER = 0x10B;
        public const int SDL_SCANCODE_AC_SEARCH = 0x10C;
        public const int SDL_SCANCODE_AC_HOME = 0x10D;
        public const int SDL_SCANCODE_AC_BACK = 0x10E;
        public const int SDL_SCANCODE_AC_FORWARD = 0x10F;
        public const int SDL_SCANCODE_AC_STOP = 0x110;
        public const int SDL_SCANCODE_AC_REFRESH = 0x111;
        public const int SDL_SCANCODE_AC_BOOKMARKS = 0x112;
        public const int SDL_SCANCODE_BRIGHTNESSDOWN = 0x113;
        public const int SDL_SCANCODE_BRIGHTNESSUP = 0x114;
        public const int SDL_SCANCODE_DISPLAYSWITCH = 0x115;
        public const int SDL_SCANCODE_KBDILLUMTOGGLE = 0x116;
        public const int SDL_SCANCODE_KBDILLUMDOWN = 0x117;
        public const int SDL_SCANCODE_KBDILLUMUP = 0x118;
        public const int SDL_SCANCODE_EJECT = 0x119;
        public const int SDL_SCANCODE_SLEEP = 0x11A;
        public const int SDL_SCANCODE_APP1 = 0x11B;
        public const int SDL_SCANCODE_APP2 = 0x11C;
        public const int SDL_NUM_SCANCODES = 0x200;
        public const int SDL_SYSTEM_CURSOR_ARROW = 0x0;
        public const int SDL_SYSTEM_CURSOR_IBEAM = 0x1;
        public const int SDL_SYSTEM_CURSOR_WAIT = 0x2;
        public const int SDL_SYSTEM_CURSOR_CROSSHAIR = 0x3;
        public const int SDL_SYSTEM_CURSOR_WAITARROW = 0x4;
        public const int SDL_SYSTEM_CURSOR_SIZENWSE = 0x5;
        public const int SDL_SYSTEM_CURSOR_SIZENESW = 0x6;
        public const int SDL_SYSTEM_CURSOR_SIZEWE = 0x7;
        public const int SDL_SYSTEM_CURSOR_SIZENS = 0x8;
        public const int SDL_SYSTEM_CURSOR_SIZEALL = 0x9;
        public const int SDL_SYSTEM_CURSOR_NO = 0xA;
        public const int SDL_SYSTEM_CURSOR_HAND = 0xB;
        public const int SDL_NUM_SYSTEM_CURSORS = 0xC;
        public const int SDL_TEXTUREACCESS_STATIC = 0x0;
        public const int SDL_TEXTUREACCESS_STREAMING = 0x1;
        public const int SDL_TEXTUREACCESS_TARGET = 0x2;
        public const int SDL_TEXTUREMODULATE_NONE = 0x0;
        public const int SDL_TEXTUREMODULATE_COLOR = 0x1;
        public const int SDL_TEXTUREMODULATE_ALPHA = 0x2;
        public const int SDL_THREAD_PRIORITY_LOW = 0x0;
        public const int SDL_THREAD_PRIORITY_NORMAL = 0x1;
        public const int SDL_THREAD_PRIORITY_HIGH = 0x2;
        public const int SDL_WINDOWEVENT_NONE = 0x0;
        public const int SDL_WINDOWEVENT_SHOWN = 0x1;
        public const int SDL_WINDOWEVENT_HIDDEN = 0x2;
        public const int SDL_WINDOWEVENT_EXPOSED = 0x3;
        public const int SDL_WINDOWEVENT_MOVED = 0x4;
        public const int SDL_WINDOWEVENT_RESIZED = 0x5;
        public const int SDL_WINDOWEVENT_SIZE_CHANGED = 0x6;
        public const int SDL_WINDOWEVENT_MINIMIZED = 0x7;
        public const int SDL_WINDOWEVENT_MAXIMIZED = 0x8;
        public const int SDL_WINDOWEVENT_RESTORED = 0x9;
        public const int SDL_WINDOWEVENT_ENTER = 0xA;
        public const int SDL_WINDOWEVENT_LEAVE = 0xB;
        public const int SDL_WINDOWEVENT_FOCUS_GAINED = 0xC;
        public const int SDL_WINDOWEVENT_FOCUS_LOST = 0xD;
        public const int SDL_WINDOWEVENT_CLOSE = 0xE;
        public const int SDL_WINDOWEVENT_TAKE_FOCUS = 0xF;
        public const int SDL_WINDOWEVENT_HIT_TEST = 0x10;
        public const int SDL_WINDOW_FULLSCREEN = 0x1;
        public const int SDL_WINDOW_OPENGL = 0x2;
        public const int SDL_WINDOW_SHOWN = 0x4;
        public const int SDL_WINDOW_HIDDEN = 0x8;
        public const int SDL_WINDOW_BORDERLESS = 0x10;
        public const int SDL_WINDOW_RESIZABLE = 0x20;
        public const int SDL_WINDOW_MINIMIZED = 0x40;
        public const int SDL_WINDOW_MAXIMIZED = 0x80;
        public const int SDL_WINDOW_INPUT_GRABBED = 0x100;
        public const int SDL_WINDOW_INPUT_FOCUS = 0x200;
        public const int SDL_WINDOW_MOUSE_FOCUS = 0x400;
        public const int SDL_WINDOW_FULLSCREEN_DESKTOP = 0x1001;
        public const int SDL_WINDOW_FOREIGN = 0x800;
        public const int SDL_WINDOW_ALLOW_HIGHDPI = 0x2000;
        public const int SDL_WINDOW_MOUSE_CAPTURE = 0x4000;
        public const int SDL_WINDOW_ALWAYS_ON_TOP = 0x8000;
        public const int SDL_WINDOW_SKIP_TASKBAR = 0x10000;
        public const int SDL_WINDOW_UTILITY = 0x20000;
        public const int SDL_WINDOW_TOOLTIP = 0x40000;
        public const int SDL_WINDOW_POPUP_MENU = 0x80000;
        public const int SDL_FALSE = 0x0;
        public const int SDL_TRUE = 0x1;
        public const int SDL_ENOMEM = 0x0;
        public const int SDL_EFREAD = 0x1;
        public const int SDL_EFWRITE = 0x2;
        public const int SDL_EFSEEK = 0x3;
        public const int SDL_UNSUPPORTED = 0x4;
        public const int SDL_LASTERROR = 0x5;
        public const int SDL_ADDEVENT = 0x0;
        public const int SDL_PEEKEVENT = 0x1;
        public const int SDL_GETEVENT = 0x2;
        public const int _MM_MANT_NORM_1_2 = 0x0;
        public const int _MM_MANT_SIGN_src = 0x0;
        public const int _MM_PERM_AAAA = 0x0;
        public const int _MM_HINT_ET0 = 0x7;

        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AddEventWatch(IntPtr filter, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AddHintCallback(string name, IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AddHintCallback(IntPtr name, IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AddTimer(int interval, IntPtr callback, IntPtr param);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_AllocFormat(int pixel_format);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_AllocPalette(int ncolors);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_AllocRW();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicAdd(IntPtr a, int v);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicCAS(IntPtr a, int oldval, int newval);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicCASPtr(IntPtr a, IntPtr oldval, IntPtr newval);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicGet(IntPtr a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_AtomicGetPtr(IntPtr a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AtomicLock(IntPtr _lock);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicSet(IntPtr a, int v);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_AtomicSetPtr(IntPtr a, IntPtr v);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AtomicTryLock(IntPtr _lock);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AtomicUnlock(IntPtr _lock);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AudioInit(string driver_name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_AudioInit(IntPtr driver_name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_AudioQuit();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_BuildAudioCVT(IntPtr cvt, short src_format, sbyte src_channels, int src_rate, short dst_format, sbyte dst_channels, int dst_rate);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_CalculateGammaRamp(float gamma, IntPtr ramp);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CaptureMouse(int enabled);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_ClearError();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_ClearHints();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_ClearQueuedAudio(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_CloseAudio();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_CloseAudioDevice(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CondBroadcast(IntPtr cond);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CondSignal(IntPtr cond);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CondWait(IntPtr cond, IntPtr mutex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CondWaitTimeout(IntPtr cond, IntPtr mutex, int ms);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ConvertAudio(IntPtr cvt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ConvertPixels(int width, int height, int src_format, IntPtr src, int src_pitch, int dst_format, IntPtr dst, int dst_pitch);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_ConvertSurface(IntPtr src, IntPtr fmt, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_ConvertSurfaceFormat(IntPtr src, int pixel_format, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateColorCursor(IntPtr surface, int hot_x, int hot_y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateCond();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateCursor(IntPtr data, IntPtr mask, int w, int h, int hot_x, int hot_y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateMutex();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateRGBSurface(int flags, int width, int height, int depth, int Rmask, int Gmask, int Bmask, int Amask);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateRGBSurfaceFrom(IntPtr pixels, int width, int height, int depth, int pitch, int Rmask, int Gmask, int Bmask, int Amask);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateRGBSurfaceWithFormat(int flags, int width, int height, int depth, int format);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateRGBSurfaceWithFormatFrom(IntPtr pixels, int width, int height, int depth, int pitch, int format);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateRenderer(IntPtr window, int index, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateSemaphore(int initial_value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateSoftwareRenderer(IntPtr surface);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateSystemCursor(int id);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateTexture(IntPtr renderer, int format, int access, int w, int h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateTextureFromSurface(IntPtr renderer, IntPtr surface);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateThread(IntPtr fn, string name, IntPtr data);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateThread(IntPtr fn, IntPtr name, IntPtr data);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateWindow(string title, int x, int y, int w, int h, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateWindow(IntPtr title, int x, int y, int w, int h, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_CreateWindowAndRenderer(int width, int height, int window_flags, IntPtr window, IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_CreateWindowFrom(IntPtr data);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_DXGIGetOutputInfo(int displayIndex, IntPtr adapterIndex, IntPtr outputIndex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DelEventWatch(IntPtr filter, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DelHintCallback(string name, IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DelHintCallback(IntPtr name, IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_Delay(int ms);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_DequeueAudio(int dev, IntPtr data, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroyCond(IntPtr cond);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroyMutex(IntPtr mutex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroyRenderer(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroySemaphore(IntPtr sem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroyTexture(IntPtr texture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DestroyWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DetachThread(IntPtr thread);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_Direct3D9GetAdapterIndex(int displayIndex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_DisableScreenSaver();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_EnableScreenSaver();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_EnclosePoints(IntPtr points, int count, IntPtr clip, IntPtr result);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_Error(int code);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte SDL_EventState(int type, int state);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_FillRect(IntPtr dst, IntPtr rect, int color);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_FillRects(IntPtr dst, IntPtr rects, int count, int color);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FilterEvents(IntPtr filter, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FlushEvent(int type);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FlushEvents(int minType, int maxType);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreeCursor(IntPtr cursor);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreeFormat(IntPtr format);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreePalette(IntPtr palette);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreeRW(IntPtr area);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreeSurface(IntPtr surface);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_FreeWAV(IntPtr audio_buf);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_BindTexture(IntPtr texture, IntPtr texw, IntPtr texh);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GL_CreateContext(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GL_DeleteContext(IntPtr context);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_ExtensionSupported(string extension);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_ExtensionSupported(IntPtr extension);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_GetAttribute(int attr, IntPtr value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GL_GetCurrentContext();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GL_GetCurrentWindow();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GL_GetDrawableSize(IntPtr window, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GL_GetProcAddress(string proc);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GL_GetProcAddress(IntPtr proc);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_GetSwapInterval();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_LoadLibrary(string path);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_LoadLibrary(IntPtr path);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_MakeCurrent(IntPtr window, IntPtr context);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GL_ResetAttributes();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_SetAttribute(int attr, int value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_SetSwapInterval(int interval);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GL_SwapWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GL_UnbindTexture(IntPtr texture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GL_UnloadLibrary();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerAddMapping(string mappingString);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerAddMapping(IntPtr mappingString);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerAddMappingsFromRW(IntPtr rw, int freerw);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GameControllerClose(IntPtr gamecontroller);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerEventState(int state);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerFromInstanceID(int joyid);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerGetAttached(IntPtr gamecontroller);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern short SDL_GameControllerGetAxis(IntPtr gamecontroller, int axis);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerGetAxisFromString(string pchString);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerGetAxisFromString(IntPtr pchString);
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_GameControllerGetBindForAxis(IntPtr gamecontroller,int axis);
        */
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_GameControllerGetBindForButton(IntPtr gamecontroller,int button);
        */
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte SDL_GameControllerGetButton(IntPtr gamecontroller, int button);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerGetButtonFromString(string pchString);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GameControllerGetButtonFromString(IntPtr pchString);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerGetJoystick(IntPtr gamecontroller);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerGetStringForAxis(int axis);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerGetStringForButton(int button);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerMapping(IntPtr gamecontroller);
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern IntPtr SDL_GameControllerMappingForGUID(error type:record_type);
        */
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerName(IntPtr gamecontroller);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerNameForIndex(int joystick_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GameControllerOpen(int joystick_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GameControllerUpdate();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetAssertionHandler(IntPtr puserdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetAssertionReport();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetAudioDeviceName(int index, int iscapture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetAudioDeviceStatus(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetAudioDriver(int index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetAudioStatus();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetBasePath();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetCPUCacheLineSize();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetCPUCount();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetClipRect(IntPtr surface, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetClipboardText();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetClosestDisplayMode(int displayIndex, IntPtr mode, IntPtr closest);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetColorKey(IntPtr surface, IntPtr key);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetCurrentAudioDriver();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetCurrentDisplayMode(int displayIndex, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetCurrentVideoDriver();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetCursor();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetDefaultAssertionHandler();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetDefaultCursor();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetDesktopDisplayMode(int displayIndex, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetDisplayBounds(int displayIndex, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetDisplayDPI(int displayIndex, IntPtr ddpi, IntPtr hdpi, IntPtr vdpi);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetDisplayMode(int displayIndex, int modeIndex, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetDisplayName(int displayIndex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetDisplayUsableBounds(int displayIndex, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetError();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetEventFilter(IntPtr filter, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetGlobalMouseState(IntPtr x, IntPtr y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetGrabbedWindow();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetHint(string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetHint(IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetHintBoolean(string name, int default_value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetHintBoolean(IntPtr name, int default_value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetKeyFromName(string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetKeyFromName(IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetKeyFromScancode(int scancode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetKeyName(int key);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetKeyboardFocus();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetKeyboardState(IntPtr numkeys);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetModState();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetMouseFocus();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetMouseState(IntPtr x, IntPtr y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumAudioDevices(int iscapture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumAudioDrivers();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumDisplayModes(int displayIndex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumRenderDrivers();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumTouchDevices();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumTouchFingers(Int64 touchID);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumVideoDisplays();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetNumVideoDrivers();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_GetPerformanceCounter();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_GetPerformanceFrequency();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetPixelFormatName(int format);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetPlatform();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetPowerInfo(IntPtr secs, IntPtr pct);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetPrefPath(string org, string app);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetPrefPath(IntPtr org, IntPtr app);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetQueuedAudioSize(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetRGB(int pixel, IntPtr format, IntPtr r, IntPtr g, IntPtr b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetRGBA(int pixel, IntPtr format, IntPtr r, IntPtr g, IntPtr b, IntPtr a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRelativeMouseMode();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRelativeMouseState(IntPtr x, IntPtr y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRenderDrawBlendMode(IntPtr renderer, IntPtr blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRenderDrawColor(IntPtr renderer, IntPtr r, IntPtr g, IntPtr b, IntPtr a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRenderDriverInfo(int index, IntPtr info);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetRenderTarget(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetRenderer(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRendererInfo(IntPtr renderer, IntPtr info);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRendererOutputSize(IntPtr renderer, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetRevision();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetRevisionNumber();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetScancodeFromKey(int key);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetScancodeFromName(string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetScancodeFromName(IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetScancodeName(int scancode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetSurfaceAlphaMod(IntPtr surface, IntPtr alpha);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetSurfaceBlendMode(IntPtr surface, IntPtr blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetSurfaceColorMod(IntPtr surface, IntPtr r, IntPtr g, IntPtr b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetSystemRAM();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetTextureAlphaMod(IntPtr texture, IntPtr alpha);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetTextureBlendMode(IntPtr texture, IntPtr blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetTextureColorMod(IntPtr texture, IntPtr r, IntPtr g, IntPtr b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetThreadID(IntPtr thread);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetThreadName(IntPtr thread);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetTicks();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_GetTouchDevice(int index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetTouchFinger(Int64 touchID, int index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetVersion(IntPtr ver);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetVideoDriver(int index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowBordersSize(IntPtr window, IntPtr top, IntPtr left, IntPtr bottom, IntPtr right);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_GetWindowBrightness(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetWindowData(IntPtr window, string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetWindowData(IntPtr window, IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowDisplayIndex(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowDisplayMode(IntPtr window, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowFlags(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetWindowFromID(int id);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowGammaRamp(IntPtr window, IntPtr red, IntPtr green, IntPtr blue);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowGrab(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowID(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetWindowMaximumSize(IntPtr window, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetWindowMinimumSize(IntPtr window, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowOpacity(IntPtr window, IntPtr out_opacity);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_GetWindowPixelFormat(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetWindowPosition(IntPtr window, IntPtr x, IntPtr y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_GetWindowSize(IntPtr window, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetWindowSurface(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_GetWindowTitle(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_HapticClose(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_HapticDestroyEffect(IntPtr haptic, int effect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticEffectSupported(IntPtr haptic, IntPtr effect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticGetEffectStatus(IntPtr haptic, int effect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticIndex(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_HapticName(int device_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticNewEffect(IntPtr haptic, IntPtr effect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticNumAxes(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticNumEffects(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticNumEffectsPlaying(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_HapticOpen(int device_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_HapticOpenFromJoystick(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_HapticOpenFromMouse();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticOpened(int device_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticPause(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticQuery(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticRumbleInit(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticRumblePlay(IntPtr haptic, float strength, int length);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticRumbleStop(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticRumbleSupported(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticRunEffect(IntPtr haptic, int effect, int iterations);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticSetAutocenter(IntPtr haptic, int autocenter);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticSetGain(IntPtr haptic, int gain);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticStopAll(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticStopEffect(IntPtr haptic, int effect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticUnpause(IntPtr haptic);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HapticUpdateEffect(IntPtr haptic, int effect, IntPtr data);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_Has3DNow();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasAVX();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasAVX2();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasAltiVec();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasClipboardText();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasEvent(int type);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasEvents(int minType, int maxType);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasIntersection(IntPtr A, IntPtr B);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasMMX();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasRDTSC();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasSSE();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasSSE2();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasSSE3();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasSSE41();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasSSE42();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_HasScreenKeyboardSupport();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_HideWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_Init(int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_InitSubSystem(int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IntersectRect(IntPtr A, IntPtr B, IntPtr result);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IntersectRectAndLine(IntPtr rect, IntPtr X1, IntPtr Y1, IntPtr X2, IntPtr Y2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IsGameController(int joystick_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IsScreenKeyboardShown(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IsScreenSaverEnabled();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_IsTextInputActive();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_JoystickClose(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickCurrentPowerLevel(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickEventState(int state);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_JoystickFromInstanceID(int joyid);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickGetAttached(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern short SDL_JoystickGetAxis(IntPtr joystick, int axis);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickGetBall(IntPtr joystick, int ball, IntPtr dx, IntPtr dy);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte SDL_JoystickGetButton(IntPtr joystick, int button);
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_JoystickGetDeviceGUID(int device_index);
        */
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_JoystickGetGUID(IntPtr joystick);
        */
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_JoystickGetGUIDFromString(string pchGUID);
        */
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern error type:record_type SDL_JoystickGetGUIDFromString(IntPtr pchGUID);
        */
        /*            [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention=CallingConvention.Cdecl)]
                    public static extern void SDL_JoystickGetGUIDString(error type:record_type,IntPtr pszGUID,int cbGUID);
        */
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte SDL_JoystickGetHat(IntPtr joystick, int hat);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickInstanceID(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickIsHaptic(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_JoystickName(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_JoystickNameForIndex(int device_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickNumAxes(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickNumBalls(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickNumButtons(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_JoystickNumHats(IntPtr joystick);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_JoystickOpen(int device_index);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_JoystickUpdate();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadBMP_RW(IntPtr src, int freesrc);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LoadDollarTemplates(Int64 touchId, IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadFunction(IntPtr handle, string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadFunction(IntPtr handle, IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadObject(string sofile);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadObject(IntPtr sofile);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_LoadWAV_RW(IntPtr src, int freesrc, IntPtr spec, IntPtr audio_buf, IntPtr audio_len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LockAudio();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LockAudioDevice(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LockMutex(IntPtr mutex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LockSurface(IntPtr surface);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LockTexture(IntPtr texture, IntPtr rect, IntPtr pixels, IntPtr pitch);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_Log(string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_Log(IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogCritical(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogCritical(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogDebug(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogDebug(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogError(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogError(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogGetOutputFunction(IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LogGetPriority(int category);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogInfo(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogInfo(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogMessage(int category, int priority, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogMessage(int category, int priority, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogMessageV(int category, int priority, string fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogMessageV(int category, int priority, IntPtr fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogResetPriorities();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogSetAllPriority(int priority);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogSetOutputFunction(IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogSetPriority(int category, int priority);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogVerbose(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogVerbose(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogWarn(int category, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_LogWarn(int category, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LowerBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_LowerBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_MapRGB(IntPtr format, sbyte r, sbyte g, sbyte b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_MapRGBA(IntPtr format, sbyte r, sbyte g, sbyte b, sbyte a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_MasksToPixelFormatEnum(int bpp, int Rmask, int Gmask, int Bmask, int Amask);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_MaximizeWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_MinimizeWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_MixAudio(IntPtr dst, IntPtr src, int len, int volume);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_MixAudioFormat(IntPtr dst, IntPtr src, short format, int len, int volume);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_MouseIsHaptic();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_NumHaptics();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_NumJoysticks();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_OpenAudio(IntPtr desired, IntPtr obtained);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_OpenAudioDevice(string device, int iscapture, IntPtr desired, IntPtr obtained, int allowed_changes);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_OpenAudioDevice(IntPtr device, int iscapture, IntPtr desired, IntPtr obtained, int allowed_changes);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_PauseAudio(int pause_on);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_PauseAudioDevice(int dev, int pause_on);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_PeepEvents(IntPtr events, int numevents, int action, int minType, int maxType);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_PixelFormatEnumToMasks(int format, IntPtr bpp, IntPtr Rmask, IntPtr Gmask, IntPtr Bmask, IntPtr Amask);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_PointInRect(IntPtr p, IntPtr r);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_PollEvent(IntPtr _event);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_PumpEvents();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_PushEvent(IntPtr _event);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_QueryTexture(IntPtr texture, IntPtr format, IntPtr access, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_QueueAudio(int dev, IntPtr data, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_Quit();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_QuitSubSystem(int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RWFromConstMem(IntPtr mem, int size);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RWFromFP(IntPtr fp, int autoclose);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RWFromFile(string file, string mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RWFromFile(IntPtr file, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RWFromMem(IntPtr mem, int size);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RaiseWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern short SDL_ReadBE16(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ReadBE32(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_ReadBE64(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern short SDL_ReadLE16(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ReadLE32(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_ReadLE64(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte SDL_ReadU8(IntPtr src);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RecordGesture(Int64 touchId);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RectEmpty(IntPtr r);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RectEquals(IntPtr a, IntPtr b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RegisterApp(IntPtr name, int style, IntPtr hInst);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RegisterEvents(int numevents);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RemoveTimer(int id);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderClear(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderCopy(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderCopyEx(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect, double angle, IntPtr center, int flip);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawLine(IntPtr renderer, int x1, int y1, int x2, int y2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawLines(IntPtr renderer, IntPtr points, int count);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawPoint(IntPtr renderer, int x, int y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawPoints(IntPtr renderer, IntPtr points, int count);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawRect(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderDrawRects(IntPtr renderer, IntPtr rects, int count);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderFillRect(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderFillRects(IntPtr renderer, IntPtr rects, int count);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RenderGetClipRect(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_RenderGetD3D9Device(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderGetIntegerScale(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RenderGetLogicalSize(IntPtr renderer, IntPtr w, IntPtr h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RenderGetScale(IntPtr renderer, IntPtr scaleX, IntPtr scaleY);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RenderGetViewport(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderIsClipEnabled(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RenderPresent(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderReadPixels(IntPtr renderer, IntPtr rect, int format, IntPtr pixels, int pitch);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderSetClipRect(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderSetIntegerScale(IntPtr renderer, int enable);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderSetLogicalSize(IntPtr renderer, int w, int h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderSetScale(IntPtr renderer, float scaleX, float scaleY);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderSetViewport(IntPtr renderer, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_RenderTargetSupported(IntPtr renderer);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ReportAssertion(IntPtr a1, string a2, string a3, int a4);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ReportAssertion(IntPtr a1, IntPtr a2, IntPtr a3, int a4);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_ResetAssertionReport();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_RestoreWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SaveAllDollarTemplates(IntPtr dst);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SaveBMP_RW(IntPtr surface, IntPtr dst, int freedst);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SaveDollarTemplate(Int64 gestureId, IntPtr dst);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SemPost(IntPtr sem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SemTryWait(IntPtr sem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SemValue(IntPtr sem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SemWait(IntPtr sem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SemWaitTimeout(IntPtr sem, int ms);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetAssertionHandler(IntPtr handler, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetClipRect(IntPtr surface, IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetClipboardText(string text);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetClipboardText(IntPtr text);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetColorKey(IntPtr surface, int flag, int key);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetCursor(IntPtr cursor);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetError(string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetError(IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetEventFilter(IntPtr filter, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetHint(string name, string value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetHint(IntPtr name, IntPtr value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetHintWithPriority(string name, string value, int priority);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetHintWithPriority(IntPtr name, IntPtr value, int priority);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetMainReady();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetModState(int modstate);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetPaletteColors(IntPtr palette, IntPtr colors, int firstcolor, int ncolors);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetPixelFormatPalette(IntPtr format, IntPtr palette);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetRelativeMouseMode(int enabled);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetRenderDrawBlendMode(IntPtr renderer, int blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetRenderDrawColor(IntPtr renderer, sbyte r, sbyte g, sbyte b, sbyte a);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetRenderTarget(IntPtr renderer, IntPtr texture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetSurfaceAlphaMod(IntPtr surface, sbyte alpha);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetSurfaceBlendMode(IntPtr surface, int blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetSurfaceColorMod(IntPtr surface, sbyte r, sbyte g, sbyte b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetSurfacePalette(IntPtr surface, IntPtr palette);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetSurfaceRLE(IntPtr surface, int flag);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetTextInputRect(IntPtr rect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetTextureAlphaMod(IntPtr texture, sbyte alpha);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetTextureBlendMode(IntPtr texture, int blendMode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetTextureColorMod(IntPtr texture, sbyte r, sbyte g, sbyte b);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetThreadPriority(int priority);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowBordered(IntPtr window, int bordered);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowBrightness(IntPtr window, float brightness);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_SetWindowData(IntPtr window, string name, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_SetWindowData(IntPtr window, IntPtr name, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowDisplayMode(IntPtr window, IntPtr mode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowFullscreen(IntPtr window, int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowGammaRamp(IntPtr window, IntPtr red, IntPtr green, IntPtr blue);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowGrab(IntPtr window, int grabbed);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowHitTest(IntPtr window, IntPtr callback, IntPtr callback_data);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowIcon(IntPtr window, IntPtr icon);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowInputFocus(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowMaximumSize(IntPtr window, int max_w, int max_h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowMinimumSize(IntPtr window, int min_w, int min_h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowModalFor(IntPtr modal_window, IntPtr parent_window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SetWindowOpacity(IntPtr window, float opacity);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowPosition(IntPtr window, int x, int y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowResizable(IntPtr window, int resizable);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowSize(IntPtr window, int w, int h);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowTitle(IntPtr window, string title);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowTitle(IntPtr window, IntPtr title);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_SetWindowsMessageHook(IntPtr callback, IntPtr userdata);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ShowCursor(int toggle);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ShowMessageBox(IntPtr messageboxdata, IntPtr buttonid);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ShowSimpleMessageBox(int flags, string title, string message, IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ShowSimpleMessageBox(int flags, IntPtr title, IntPtr message, IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_ShowWindow(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_SoftStretch(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_StartTextInput();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_StopTextInput();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern short SDL_Swap16(short x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_Swap32(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_Swap64(Int64 x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_SwapFloat(float x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_TLSCreate();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_TLSGet(int id);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_TLSSet(int id, IntPtr value, IntPtr destructor);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_ThreadID();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_TryLockMutex(IntPtr mutex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnionRect(IntPtr A, IntPtr B, IntPtr result);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnloadObject(IntPtr handle);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnlockAudio();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnlockAudioDevice(int dev);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UnlockMutex(IntPtr mutex);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnlockSurface(IntPtr surface);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnlockTexture(IntPtr texture);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_UnregisterApp();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpdateTexture(IntPtr texture, IntPtr rect, IntPtr pixels, int pitch);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpdateWindowSurface(IntPtr window);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpdateWindowSurfaceRects(IntPtr window, IntPtr rects, int numrects);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpdateYUVTexture(IntPtr texture, IntPtr rect, IntPtr Yplane, int Ypitch, IntPtr Uplane, int Upitch, IntPtr Vplane, int Vpitch);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpperBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_UpperBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_VideoInit(string driver_name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_VideoInit(IntPtr driver_name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_VideoQuit();
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WaitEvent(IntPtr _event);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WaitEventTimeout(IntPtr _event, int timeout);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_WaitThread(IntPtr thread, IntPtr status);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WarpMouseGlobal(int x, int y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_WarpMouseInWindow(IntPtr window, int x, int y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WasInit(int flags);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteBE16(IntPtr dst, short value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteBE32(IntPtr dst, int value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteBE64(IntPtr dst, Int64 value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteLE16(IntPtr dst, short value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteLE32(IntPtr dst, int value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteLE64(IntPtr dst, Int64 value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_WriteU8(IntPtr dst, sbyte value);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_abs(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_acos(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_asin(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_atan(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_atan2(double x, double y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_atof(string str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_atof(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_atoi(string str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_atoi(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_calloc(int nmemb, int size);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_ceil(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_copysign(double x, double y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_cos(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_cosf(float x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_fabs(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_floor(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_free(IntPtr mem);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_getenv(string name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_getenv(IntPtr name);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_iconv(IntPtr cd, IntPtr inbuf, IntPtr inbytesleft, IntPtr outbuf, IntPtr outbytesleft);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_iconv_close(IntPtr cd);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_iconv_open(string tocode, string fromcode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_iconv_open(IntPtr tocode, IntPtr fromcode);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_iconv_string(string tocode, string fromcode, string inbuf, int inbytesleft);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_iconv_string(IntPtr tocode, IntPtr fromcode, IntPtr inbuf, int inbytesleft);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_isdigit(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_isspace(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_itoa(int value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_lltoa(Int64 value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_log(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_ltoa(int value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_main(int argc, IntPtr argv);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_malloc(int size);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_memcmp(IntPtr s1, IntPtr s2, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_memcpy(IntPtr dst, IntPtr src, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_memcpy4(IntPtr dst, IntPtr src, int dwords);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_memmove(IntPtr dst, IntPtr src, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_memset(IntPtr dst, int c, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_memset4(IntPtr dst, int val, int dwords);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_pow(double x, double y);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SDL_qsort(IntPtr _base, int nmemb, int size, IntPtr compare);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_realloc(IntPtr mem, int size);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_scalbn(double x, int n);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_setenv(string name, string value, int overwrite);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_setenv(IntPtr name, IntPtr value, int overwrite);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_sin(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_sinf(float x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_snprintf(IntPtr text, int maxlen, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_snprintf(IntPtr text, int maxlen, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_sqrt(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_sqrtf(float x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_sscanf(string text, string fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_sscanf(IntPtr text, IntPtr fmt);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strcasecmp(string str1, string str2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strcasecmp(IntPtr str1, IntPtr str2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strchr(string str, int c);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strchr(IntPtr str, int c);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strcmp(string str1, string str2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strcmp(IntPtr str1, IntPtr str2);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strdup(string str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strdup(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlcat(IntPtr dst, string src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlcat(IntPtr dst, IntPtr src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlcpy(IntPtr dst, string src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlcpy(IntPtr dst, IntPtr src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlen(string str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strlen(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strlwr(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strncasecmp(string str1, string str2, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strncasecmp(IntPtr str1, IntPtr str2, int len);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strncmp(string str1, string str2, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strncmp(IntPtr str1, IntPtr str2, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strrchr(string str, int c);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strrchr(IntPtr str, int c);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strrev(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strstr(string haystack, string needle);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strstr(IntPtr haystack, IntPtr needle);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_strtod(string str, IntPtr endp);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_strtod(IntPtr str, IntPtr endp);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strtol(string str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strtol(IntPtr str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_strtoll(string str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_strtoll(IntPtr str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strtoul(string str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_strtoul(IntPtr str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_strtoull(string str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int64 SDL_strtoull(IntPtr str, IntPtr endp, int _base);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_strupr(IntPtr str);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double SDL_tan(double x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern float SDL_tanf(float x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_tolower(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_toupper(int x);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_uitoa(int value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_ulltoa(Int64 value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SDL_ultoa(int value, IntPtr str, int radix);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_utf8strlcpy(IntPtr dst, string src, int dst_bytes);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_utf8strlcpy(IntPtr dst, IntPtr src, int dst_bytes);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_vsnprintf(IntPtr text, int maxlen, string fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_vsnprintf(IntPtr text, int maxlen, IntPtr fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_vsscanf(string text, string fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_vsscanf(IntPtr text, IntPtr fmt, IntPtr ap);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_wcslcat(IntPtr dst, IntPtr src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_wcslcpy(IntPtr dst, IntPtr src, int maxlen);
        [DllImport(dll_file, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SDL_wcslen(IntPtr wstr);

    }
}
