#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include <cairo/cairo.h>


#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lcairo_append_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_append_path(a1,a2);
  return 0;
};

static int lcairo_arc(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  cairo_arc(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lcairo_arc_negative(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  cairo_arc_negative(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lcairo_clip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_clip(a1);
  return 0;
};

static int lcairo_clip_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  cairo_clip_extents(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_clip_preserve(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_clip_preserve(a1);
  return 0;
};

static int lcairo_close_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_close_path(a1);
  return 0;
};

static int lcairo_copy_clip_rectangle_list(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_copy_clip_rectangle_list(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_copy_page(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_copy_page(a1);
  return 0;
};

static int lcairo_copy_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_copy_path(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_copy_path_flat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_copy_path_flat(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_create(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_create(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_curve_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  cairo_curve_to(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lcairo_debug_reset_static_data(lua_State*L){
  cairo_debug_reset_static_data();
  return 0;
};

static int lcairo_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_destroy(a1);
  return 0;
};

static int lcairo_device_acquire(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_device_acquire(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_device_destroy(a1);
  return 0;
};

static int lcairo_device_finish(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_device_finish(a1);
  return 0;
};

static int lcairo_device_flush(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_device_flush(a1);
  return 0;
};

static int lcairo_device_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_device_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_get_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_device_get_type(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_device_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_device_observer_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_observer_fill_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_fill_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_observer_glyphs_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_glyphs_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_observer_mask_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_mask_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_observer_paint_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_paint_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_observer_print(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_device_observer_print(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_observer_stroke_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_device_observer_stroke_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_device_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_device_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_device_release(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_device_release(a1);
  return 0;
};

static int lcairo_device_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_device_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_device_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_device_to_user(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_device_to_user(a1,a2,a3);
  return 0;
};

static int lcairo_device_to_user_distance(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_device_to_user_distance(a1,a2,a3);
  return 0;
};

static int lcairo_fill(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_fill(a1);
  return 0;
};

static int lcairo_fill_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  cairo_fill_extents(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_fill_preserve(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_fill_preserve(a1);
  return 0;
};

static int lcairo_font_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_font_extents(a1,a2);
  return 0;
};

static int lcairo_font_face_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_font_face_destroy(a1);
  return 0;
};

static int lcairo_font_face_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_face_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_face_get_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_face_get_type(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_face_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_font_face_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_font_face_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_font_face_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_font_face_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_font_face_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_face_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_face_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_font_options_copy(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_font_options_create(lua_State*L){
  void* ret = (void*)cairo_font_options_create();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_font_options_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_font_options_destroy(a1);
  return 0;
};

static int lcairo_font_options_equal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_font_options_equal(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_get_antialias(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_get_antialias(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_get_hint_metrics(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_get_hint_metrics(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_get_hint_style(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_get_hint_style(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_get_subpixel_order(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_get_subpixel_order(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_hash(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_hash(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_font_options_merge(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_font_options_merge(a1,a2);
  return 0;
};

static int lcairo_font_options_set_antialias(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_font_options_set_antialias(a1,a2);
  return 0;
};

static int lcairo_font_options_set_hint_metrics(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_font_options_set_hint_metrics(a1,a2);
  return 0;
};

static int lcairo_font_options_set_hint_style(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_font_options_set_hint_style(a1,a2);
  return 0;
};

static int lcairo_font_options_set_subpixel_order(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_font_options_set_subpixel_order(a1,a2);
  return 0;
};

static int lcairo_font_options_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_font_options_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_format_stride_for_width(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)cairo_format_stride_for_width(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_antialias(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_antialias(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_current_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_get_current_point(a1,a2,a3);
  return 0;
};

static int lcairo_get_dash(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_get_dash(a1,a2,a3);
  return 0;
};

static int lcairo_get_dash_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_dash_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_fill_rule(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_fill_rule(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_font_face(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_get_font_face(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_get_font_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_get_font_matrix(a1,a2);
  return 0;
};

static int lcairo_get_font_options(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_get_font_options(a1,a2);
  return 0;
};

static int lcairo_get_group_target(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_get_group_target(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_get_line_cap(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_line_cap(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_line_join(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_line_join(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_line_width(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_get_line_width(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_get_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_get_matrix(a1,a2);
  return 0;
};

static int lcairo_get_miter_limit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_get_miter_limit(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_get_operator(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_operator(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_get_scaled_font(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_get_scaled_font(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_get_source(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_get_source(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_get_target(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_get_target(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_get_tolerance(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_get_tolerance(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_glyph_allocate(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)cairo_glyph_allocate(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_glyph_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  cairo_glyph_extents(a1,a2,a3,a4);
  return 0;
};

static int lcairo_glyph_free(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_glyph_free(a1);
  return 0;
};

static int lcairo_glyph_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  cairo_glyph_path(a1,a2,a3);
  return 0;
};

static int lcairo_has_current_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_has_current_point(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_identity_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_identity_matrix(a1);
  return 0;
};

static int lcairo_image_surface_create(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)cairo_image_surface_create(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_image_surface_create_for_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)cairo_image_surface_create_for_data(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_image_surface_create_from_png(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_image_surface_create_from_png(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_image_surface_create_from_png_stream(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_image_surface_create_from_png_stream(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_image_surface_get_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_image_surface_get_data(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_image_surface_get_format(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_image_surface_get_format(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_image_surface_get_height(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_image_surface_get_height(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_image_surface_get_stride(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_image_surface_get_stride(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_image_surface_get_width(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_image_surface_get_width(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_in_clip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int ret = (int)cairo_in_clip(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_in_fill(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int ret = (int)cairo_in_fill(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_in_stroke(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int ret = (int)cairo_in_stroke(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_line_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_line_to(a1,a2,a3);
  return 0;
};

static int lcairo_mask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_mask(a1,a2);
  return 0;
};

static int lcairo_mask_surface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  cairo_mask_surface(a1,a2,a3,a4);
  return 0;
};

static int lcairo_matrix_init(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  cairo_matrix_init(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lcairo_matrix_init_identity(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_matrix_init_identity(a1);
  return 0;
};

static int lcairo_matrix_init_rotate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_matrix_init_rotate(a1,a2);
  return 0;
};

static int lcairo_matrix_init_scale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_matrix_init_scale(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_init_translate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_matrix_init_translate(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_invert(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_matrix_invert(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_matrix_multiply(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_matrix_multiply(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_rotate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_matrix_rotate(a1,a2);
  return 0;
};

static int lcairo_matrix_scale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_matrix_scale(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_transform_distance(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_matrix_transform_distance(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_transform_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_matrix_transform_point(a1,a2,a3);
  return 0;
};

static int lcairo_matrix_translate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_matrix_translate(a1,a2,a3);
  return 0;
};

static int lcairo_mesh_pattern_begin_patch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_mesh_pattern_begin_patch(a1);
  return 0;
};

static int lcairo_mesh_pattern_curve_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  cairo_mesh_pattern_curve_to(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lcairo_mesh_pattern_end_patch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_mesh_pattern_end_patch(a1);
  return 0;
};

static int lcairo_mesh_pattern_get_control_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)cairo_mesh_pattern_get_control_point(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_mesh_pattern_get_corner_color_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int ret = (int)cairo_mesh_pattern_get_corner_color_rgba(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_mesh_pattern_get_patch_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_mesh_pattern_get_patch_count(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_mesh_pattern_get_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)cairo_mesh_pattern_get_path(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_mesh_pattern_line_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_mesh_pattern_line_to(a1,a2,a3);
  return 0;
};

static int lcairo_mesh_pattern_move_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_mesh_pattern_move_to(a1,a2,a3);
  return 0;
};

static int lcairo_mesh_pattern_set_control_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  cairo_mesh_pattern_set_control_point(a1,a2,a3,a4);
  return 0;
};

static int lcairo_mesh_pattern_set_corner_color_rgb(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  cairo_mesh_pattern_set_corner_color_rgb(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_mesh_pattern_set_corner_color_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  cairo_mesh_pattern_set_corner_color_rgba(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lcairo_move_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_move_to(a1,a2,a3);
  return 0;
};

static int lcairo_new_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_new_path(a1);
  return 0;
};

static int lcairo_new_sub_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_new_sub_path(a1);
  return 0;
};

static int lcairo_paint(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_paint(a1);
  return 0;
};

static int lcairo_paint_with_alpha(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_paint_with_alpha(a1,a2);
  return 0;
};

static int lcairo_path_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_path_destroy(a1);
  return 0;
};

static int lcairo_path_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  cairo_path_extents(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_pattern_add_color_stop_rgb(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  cairo_pattern_add_color_stop_rgb(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_pattern_add_color_stop_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  cairo_pattern_add_color_stop_rgba(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lcairo_pattern_create_for_surface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_pattern_create_for_surface(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_linear(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  void* ret = (void*)cairo_pattern_create_linear(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_mesh(lua_State*L){
  void* ret = (void*)cairo_pattern_create_mesh();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_radial(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  void* ret = (void*)cairo_pattern_create_radial(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_raster_source(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)cairo_pattern_create_raster_source(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_rgb(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  void* ret = (void*)cairo_pattern_create_rgb(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_create_rgba(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  void* ret = (void*)cairo_pattern_create_rgba(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_pattern_destroy(a1);
  return 0;
};

static int lcairo_pattern_get_color_stop_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_pattern_get_color_stop_count(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_color_stop_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int ret = (int)cairo_pattern_get_color_stop_rgba(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_extend(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_pattern_get_extend(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_filter(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_pattern_get_filter(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_linear_points(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)cairo_pattern_get_linear_points(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_pattern_get_matrix(a1,a2);
  return 0;
};

static int lcairo_pattern_get_radial_circles(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int ret = (int)cairo_pattern_get_radial_circles(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_pattern_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)cairo_pattern_get_rgba(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_surface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_pattern_get_surface(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_pattern_get_type(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_pattern_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_pattern_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pattern_set_extend(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_pattern_set_extend(a1,a2);
  return 0;
};

static int lcairo_pattern_set_filter(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_pattern_set_filter(a1,a2);
  return 0;
};

static int lcairo_pattern_set_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_pattern_set_matrix(a1,a2);
  return 0;
};

static int lcairo_pattern_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_pattern_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pattern_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_pattern_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_pop_group(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_pop_group(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_pop_group_to_source(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_pop_group_to_source(a1);
  return 0;
};

static int lcairo_push_group(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_push_group(a1);
  return 0;
};

static int lcairo_push_group_with_content(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_push_group_with_content(a1,a2);
  return 0;
};

static int lcairo_raster_source_pattern_get_acquire(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_raster_source_pattern_get_acquire(a1,a2,a3);
  return 0;
};

static int lcairo_raster_source_pattern_get_callback_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_raster_source_pattern_get_callback_data(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_raster_source_pattern_get_copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_raster_source_pattern_get_copy(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_raster_source_pattern_get_finish(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_raster_source_pattern_get_finish(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_raster_source_pattern_get_snapshot(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_raster_source_pattern_get_snapshot(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_raster_source_pattern_set_acquire(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_raster_source_pattern_set_acquire(a1,a2,a3);
  return 0;
};

static int lcairo_raster_source_pattern_set_callback_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_raster_source_pattern_set_callback_data(a1,a2);
  return 0;
};

static int lcairo_raster_source_pattern_set_copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_raster_source_pattern_set_copy(a1,a2);
  return 0;
};

static int lcairo_raster_source_pattern_set_finish(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_raster_source_pattern_set_finish(a1,a2);
  return 0;
};

static int lcairo_raster_source_pattern_set_snapshot(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_raster_source_pattern_set_snapshot(a1,a2);
  return 0;
};

static int lcairo_recording_surface_create(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_recording_surface_create(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_recording_surface_get_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_recording_surface_get_extents(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_recording_surface_ink_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  cairo_recording_surface_ink_extents(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  cairo_rectangle(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_rectangle_list_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_rectangle_list_destroy(a1);
  return 0;
};

static int lcairo_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_contains_point(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)cairo_region_contains_point(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_contains_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_contains_rectangle(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_region_copy(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_create(lua_State*L){
  void* ret = (void*)cairo_region_create();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_create_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_region_create_rectangle(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_create_rectangles(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)cairo_region_create_rectangles(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_region_destroy(a1);
  return 0;
};

static int lcairo_region_equal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_equal(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_get_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_region_get_extents(a1,a2);
  return 0;
};

static int lcairo_region_get_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_region_get_rectangle(a1,a2,a3);
  return 0;
};

static int lcairo_region_intersect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_intersect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_intersect_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_intersect_rectangle(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_is_empty(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_region_is_empty(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_num_rectangles(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_region_num_rectangles(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_region_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_region_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_region_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_subtract(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_subtract(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_subtract_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_subtract_rectangle(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_translate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  cairo_region_translate(a1,a2,a3);
  return 0;
};

static int lcairo_region_union(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_union(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_union_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_union_rectangle(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_xor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_xor(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_region_xor_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_region_xor_rectangle(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_rel_curve_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  cairo_rel_curve_to(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lcairo_rel_line_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_rel_line_to(a1,a2,a3);
  return 0;
};

static int lcairo_rel_move_to(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_rel_move_to(a1,a2,a3);
  return 0;
};

static int lcairo_reset_clip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_reset_clip(a1);
  return 0;
};

static int lcairo_restore(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_restore(a1);
  return 0;
};

static int lcairo_rotate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_rotate(a1,a2);
  return 0;
};

static int lcairo_save(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_save(a1);
  return 0;
};

static int lcairo_scale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_scale(a1,a2,a3);
  return 0;
};

static int lcairo_scaled_font_create(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)cairo_scaled_font_create(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_scaled_font_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_scaled_font_destroy(a1);
  return 0;
};

static int lcairo_scaled_font_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_scaled_font_extents(a1,a2);
  return 0;
};

static int lcairo_scaled_font_get_ctm(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_scaled_font_get_ctm(a1,a2);
  return 0;
};

static int lcairo_scaled_font_get_font_face(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_scaled_font_get_font_face(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_scaled_font_get_font_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_scaled_font_get_font_matrix(a1,a2);
  return 0;
};

static int lcairo_scaled_font_get_font_options(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_scaled_font_get_font_options(a1,a2);
  return 0;
};

static int lcairo_scaled_font_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_scaled_font_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_scaled_font_get_scale_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_scaled_font_get_scale_matrix(a1,a2);
  return 0;
};

static int lcairo_scaled_font_get_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_scaled_font_get_type(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_scaled_font_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_scaled_font_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_scaled_font_glyph_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  cairo_scaled_font_glyph_extents(a1,a2,a3,a4);
  return 0;
};

static int lcairo_scaled_font_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_scaled_font_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_scaled_font_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_scaled_font_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_scaled_font_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_scaled_font_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_scaled_font_text_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_scaled_font_text_extents(a1,a2,a3);
  return 0;
};

static int lcairo_scaled_font_text_to_glyphs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  void* a10 = lcheck_toptr(L,10);
  int ret = (int)cairo_scaled_font_text_to_glyphs(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_select_font_face(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  cairo_select_font_face(a1,a2,a3,a4);
  return 0;
};

static int lcairo_set_antialias(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_set_antialias(a1,a2);
  return 0;
};

static int lcairo_set_dash(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  double a4 = luaL_checknumber(L,4);
  cairo_set_dash(a1,a2,a3,a4);
  return 0;
};

static int lcairo_set_fill_rule(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_set_fill_rule(a1,a2);
  return 0;
};

static int lcairo_set_font_face(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_font_face(a1,a2);
  return 0;
};

static int lcairo_set_font_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_font_matrix(a1,a2);
  return 0;
};

static int lcairo_set_font_options(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_font_options(a1,a2);
  return 0;
};

static int lcairo_set_font_size(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_set_font_size(a1,a2);
  return 0;
};

static int lcairo_set_line_cap(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_set_line_cap(a1,a2);
  return 0;
};

static int lcairo_set_line_join(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_set_line_join(a1,a2);
  return 0;
};

static int lcairo_set_line_width(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_set_line_width(a1,a2);
  return 0;
};

static int lcairo_set_matrix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_matrix(a1,a2);
  return 0;
};

static int lcairo_set_miter_limit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_set_miter_limit(a1,a2);
  return 0;
};

static int lcairo_set_operator(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  cairo_set_operator(a1,a2);
  return 0;
};

static int lcairo_set_scaled_font(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_scaled_font(a1,a2);
  return 0;
};

static int lcairo_set_source(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_set_source(a1,a2);
  return 0;
};

static int lcairo_set_source_rgb(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  cairo_set_source_rgb(a1,a2,a3,a4);
  return 0;
};

static int lcairo_set_source_rgba(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  cairo_set_source_rgba(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_set_source_surface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  cairo_set_source_surface(a1,a2,a3,a4);
  return 0;
};

static int lcairo_set_tolerance(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  cairo_set_tolerance(a1,a2);
  return 0;
};

static int lcairo_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_show_glyphs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  cairo_show_glyphs(a1,a2,a3);
  return 0;
};

static int lcairo_show_page(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_show_page(a1);
  return 0;
};

static int lcairo_show_text(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_show_text(a1,a2);
  return 0;
};

static int lcairo_show_text_glyphs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  cairo_show_text_glyphs(a1,a2,a3,a4,a5,a6,a7,a8);
  return 0;
};

static int lcairo_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_status_to_string(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)cairo_status_to_string(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_stroke(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_stroke(a1);
  return 0;
};

static int lcairo_stroke_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  cairo_stroke_extents(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_stroke_preserve(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_stroke_preserve(a1);
  return 0;
};

static int lcairo_surface_copy_page(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_copy_page(a1);
  return 0;
};

static int lcairo_surface_create_for_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  void* ret = (void*)cairo_surface_create_for_rectangle(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_create_observer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)cairo_surface_create_observer(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_create_similar(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)cairo_surface_create_similar(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_create_similar_image(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)cairo_surface_create_similar_image(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_destroy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_destroy(a1);
  return 0;
};

static int lcairo_surface_finish(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_finish(a1);
  return 0;
};

static int lcairo_surface_flush(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_flush(a1);
  return 0;
};

static int lcairo_surface_get_content(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_surface_get_content(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_get_device(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_surface_get_device(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_get_device_offset(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_surface_get_device_offset(a1,a2,a3);
  return 0;
};

static int lcairo_surface_get_device_scale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_surface_get_device_scale(a1,a2,a3);
  return 0;
};

static int lcairo_surface_get_fallback_resolution(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_surface_get_fallback_resolution(a1,a2,a3);
  return 0;
};

static int lcairo_surface_get_font_options(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_surface_get_font_options(a1,a2);
  return 0;
};

static int lcairo_surface_get_mime_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  cairo_surface_get_mime_data(a1,a2,a3,a4);
  return 0;
};

static int lcairo_surface_get_reference_count(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_surface_get_reference_count(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_get_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_surface_get_type(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_get_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_surface_get_user_data(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_has_show_text_glyphs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_surface_has_show_text_glyphs(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_map_to_image(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)cairo_surface_map_to_image(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_mark_dirty(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_mark_dirty(a1);
  return 0;
};

static int lcairo_surface_mark_dirty_rectangle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  cairo_surface_mark_dirty_rectangle(a1,a2,a3,a4,a5);
  return 0;
};

static int lcairo_surface_observer_add_fill_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_fill_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_finish_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_finish_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_flush_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_flush_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_glyphs_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_glyphs_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_mask_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_mask_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_paint_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_paint_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_add_stroke_callback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_add_stroke_callback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_observer_elapsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)cairo_surface_observer_elapsed(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lcairo_surface_observer_print(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_observer_print(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_reference(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_surface_reference(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_surface_set_device_offset(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_surface_set_device_offset(a1,a2,a3);
  return 0;
};

static int lcairo_surface_set_device_scale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_surface_set_device_scale(a1,a2,a3);
  return 0;
};

static int lcairo_surface_set_fallback_resolution(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_surface_set_fallback_resolution(a1,a2,a3);
  return 0;
};

static int lcairo_surface_set_mime_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)cairo_surface_set_mime_data(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_set_user_data(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)cairo_surface_set_user_data(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_show_page(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_surface_show_page(a1);
  return 0;
};

static int lcairo_surface_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_surface_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_supports_mime_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_surface_supports_mime_type(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_unmap_image(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_surface_unmap_image(a1,a2);
  return 0;
};

static int lcairo_surface_write_to_png(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)cairo_surface_write_to_png(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_surface_write_to_png_stream(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)cairo_surface_write_to_png_stream(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_text_cluster_allocate(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)cairo_text_cluster_allocate(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_text_cluster_free(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  cairo_text_cluster_free(a1);
  return 0;
};

static int lcairo_text_extents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_text_extents(a1,a2,a3);
  return 0;
};

static int lcairo_text_path(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_text_path(a1,a2);
  return 0;
};

static int lcairo_toy_font_face_create(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)cairo_toy_font_face_create(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_toy_font_face_get_family(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_toy_font_face_get_family(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_toy_font_face_get_slant(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_toy_font_face_get_slant(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_toy_font_face_get_weight(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)cairo_toy_font_face_get_weight(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_transform(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_transform(a1,a2);
  return 0;
};

static int lcairo_translate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  cairo_translate(a1,a2,a3);
  return 0;
};

static int lcairo_user_font_face_create(lua_State*L){
  void* ret = (void*)cairo_user_font_face_create();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_user_font_face_get_init_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_user_font_face_get_init_func(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_user_font_face_get_render_glyph_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_user_font_face_get_render_glyph_func(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_user_font_face_get_text_to_glyphs_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_user_font_face_get_text_to_glyphs_func(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_user_font_face_get_unicode_to_glyph_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)cairo_user_font_face_get_unicode_to_glyph_func(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lcairo_user_font_face_set_init_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_user_font_face_set_init_func(a1,a2);
  return 0;
};

static int lcairo_user_font_face_set_render_glyph_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_user_font_face_set_render_glyph_func(a1,a2);
  return 0;
};

static int lcairo_user_font_face_set_text_to_glyphs_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_user_font_face_set_text_to_glyphs_func(a1,a2);
  return 0;
};

static int lcairo_user_font_face_set_unicode_to_glyph_func(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  cairo_user_font_face_set_unicode_to_glyph_func(a1,a2);
  return 0;
};

static int lcairo_user_to_device(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_user_to_device(a1,a2,a3);
  return 0;
};

static int lcairo_user_to_device_distance(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  cairo_user_to_device_distance(a1,a2,a3);
  return 0;
};

static int lcairo_version(lua_State*L){
  int ret = (int)cairo_version();
  lua_pushinteger(L,ret);
  return 1;
};

static int lcairo_version_string(lua_State*L){
  void* ret = (void*)cairo_version_string();
  lua_pushlightuserdata(L,ret);
  return 1;
};

int lcairo_openlibs(lua_State*L){
  lua_register(L,"cairo_append_path",lcairo_append_path);
  lua_register(L,"cairo_arc",lcairo_arc);
  lua_register(L,"cairo_arc_negative",lcairo_arc_negative);
  lua_register(L,"cairo_clip",lcairo_clip);
  lua_register(L,"cairo_clip_extents",lcairo_clip_extents);
  lua_register(L,"cairo_clip_preserve",lcairo_clip_preserve);
  lua_register(L,"cairo_close_path",lcairo_close_path);
  lua_register(L,"cairo_copy_clip_rectangle_list",lcairo_copy_clip_rectangle_list);
  lua_register(L,"cairo_copy_page",lcairo_copy_page);
  lua_register(L,"cairo_copy_path",lcairo_copy_path);
  lua_register(L,"cairo_copy_path_flat",lcairo_copy_path_flat);
  lua_register(L,"cairo_create",lcairo_create);
  lua_register(L,"cairo_curve_to",lcairo_curve_to);
  lua_register(L,"cairo_debug_reset_static_data",lcairo_debug_reset_static_data);
  lua_register(L,"cairo_destroy",lcairo_destroy);
  lua_register(L,"cairo_device_acquire",lcairo_device_acquire);
  lua_register(L,"cairo_device_destroy",lcairo_device_destroy);
  lua_register(L,"cairo_device_finish",lcairo_device_finish);
  lua_register(L,"cairo_device_flush",lcairo_device_flush);
  lua_register(L,"cairo_device_get_reference_count",lcairo_device_get_reference_count);
  lua_register(L,"cairo_device_get_type",lcairo_device_get_type);
  lua_register(L,"cairo_device_get_user_data",lcairo_device_get_user_data);
  lua_register(L,"cairo_device_observer_elapsed",lcairo_device_observer_elapsed);
  lua_register(L,"cairo_device_observer_fill_elapsed",lcairo_device_observer_fill_elapsed);
  lua_register(L,"cairo_device_observer_glyphs_elapsed",lcairo_device_observer_glyphs_elapsed);
  lua_register(L,"cairo_device_observer_mask_elapsed",lcairo_device_observer_mask_elapsed);
  lua_register(L,"cairo_device_observer_paint_elapsed",lcairo_device_observer_paint_elapsed);
  lua_register(L,"cairo_device_observer_print",lcairo_device_observer_print);
  lua_register(L,"cairo_device_observer_stroke_elapsed",lcairo_device_observer_stroke_elapsed);
  lua_register(L,"cairo_device_reference",lcairo_device_reference);
  lua_register(L,"cairo_device_release",lcairo_device_release);
  lua_register(L,"cairo_device_set_user_data",lcairo_device_set_user_data);
  lua_register(L,"cairo_device_status",lcairo_device_status);
  lua_register(L,"cairo_device_to_user",lcairo_device_to_user);
  lua_register(L,"cairo_device_to_user_distance",lcairo_device_to_user_distance);
  lua_register(L,"cairo_fill",lcairo_fill);
  lua_register(L,"cairo_fill_extents",lcairo_fill_extents);
  lua_register(L,"cairo_fill_preserve",lcairo_fill_preserve);
  lua_register(L,"cairo_font_extents",lcairo_font_extents);
  lua_register(L,"cairo_font_face_destroy",lcairo_font_face_destroy);
  lua_register(L,"cairo_font_face_get_reference_count",lcairo_font_face_get_reference_count);
  lua_register(L,"cairo_font_face_get_type",lcairo_font_face_get_type);
  lua_register(L,"cairo_font_face_get_user_data",lcairo_font_face_get_user_data);
  lua_register(L,"cairo_font_face_reference",lcairo_font_face_reference);
  lua_register(L,"cairo_font_face_set_user_data",lcairo_font_face_set_user_data);
  lua_register(L,"cairo_font_face_status",lcairo_font_face_status);
  lua_register(L,"cairo_font_options_copy",lcairo_font_options_copy);
  lua_register(L,"cairo_font_options_create",lcairo_font_options_create);
  lua_register(L,"cairo_font_options_destroy",lcairo_font_options_destroy);
  lua_register(L,"cairo_font_options_equal",lcairo_font_options_equal);
  lua_register(L,"cairo_font_options_get_antialias",lcairo_font_options_get_antialias);
  lua_register(L,"cairo_font_options_get_hint_metrics",lcairo_font_options_get_hint_metrics);
  lua_register(L,"cairo_font_options_get_hint_style",lcairo_font_options_get_hint_style);
  lua_register(L,"cairo_font_options_get_subpixel_order",lcairo_font_options_get_subpixel_order);
  lua_register(L,"cairo_font_options_hash",lcairo_font_options_hash);
  lua_register(L,"cairo_font_options_merge",lcairo_font_options_merge);
  lua_register(L,"cairo_font_options_set_antialias",lcairo_font_options_set_antialias);
  lua_register(L,"cairo_font_options_set_hint_metrics",lcairo_font_options_set_hint_metrics);
  lua_register(L,"cairo_font_options_set_hint_style",lcairo_font_options_set_hint_style);
  lua_register(L,"cairo_font_options_set_subpixel_order",lcairo_font_options_set_subpixel_order);
  lua_register(L,"cairo_font_options_status",lcairo_font_options_status);
  lua_register(L,"cairo_format_stride_for_width",lcairo_format_stride_for_width);
  lua_register(L,"cairo_get_antialias",lcairo_get_antialias);
  lua_register(L,"cairo_get_current_point",lcairo_get_current_point);
  lua_register(L,"cairo_get_dash",lcairo_get_dash);
  lua_register(L,"cairo_get_dash_count",lcairo_get_dash_count);
  lua_register(L,"cairo_get_fill_rule",lcairo_get_fill_rule);
  lua_register(L,"cairo_get_font_face",lcairo_get_font_face);
  lua_register(L,"cairo_get_font_matrix",lcairo_get_font_matrix);
  lua_register(L,"cairo_get_font_options",lcairo_get_font_options);
  lua_register(L,"cairo_get_group_target",lcairo_get_group_target);
  lua_register(L,"cairo_get_line_cap",lcairo_get_line_cap);
  lua_register(L,"cairo_get_line_join",lcairo_get_line_join);
  lua_register(L,"cairo_get_line_width",lcairo_get_line_width);
  lua_register(L,"cairo_get_matrix",lcairo_get_matrix);
  lua_register(L,"cairo_get_miter_limit",lcairo_get_miter_limit);
  lua_register(L,"cairo_get_operator",lcairo_get_operator);
  lua_register(L,"cairo_get_reference_count",lcairo_get_reference_count);
  lua_register(L,"cairo_get_scaled_font",lcairo_get_scaled_font);
  lua_register(L,"cairo_get_source",lcairo_get_source);
  lua_register(L,"cairo_get_target",lcairo_get_target);
  lua_register(L,"cairo_get_tolerance",lcairo_get_tolerance);
  lua_register(L,"cairo_get_user_data",lcairo_get_user_data);
  lua_register(L,"cairo_glyph_allocate",lcairo_glyph_allocate);
  lua_register(L,"cairo_glyph_extents",lcairo_glyph_extents);
  lua_register(L,"cairo_glyph_free",lcairo_glyph_free);
  lua_register(L,"cairo_glyph_path",lcairo_glyph_path);
  lua_register(L,"cairo_has_current_point",lcairo_has_current_point);
  lua_register(L,"cairo_identity_matrix",lcairo_identity_matrix);
  lua_register(L,"cairo_image_surface_create",lcairo_image_surface_create);
  lua_register(L,"cairo_image_surface_create_for_data",lcairo_image_surface_create_for_data);
  lua_register(L,"cairo_image_surface_create_from_png",lcairo_image_surface_create_from_png);
  lua_register(L,"cairo_image_surface_create_from_png_stream",lcairo_image_surface_create_from_png_stream);
  lua_register(L,"cairo_image_surface_get_data",lcairo_image_surface_get_data);
  lua_register(L,"cairo_image_surface_get_format",lcairo_image_surface_get_format);
  lua_register(L,"cairo_image_surface_get_height",lcairo_image_surface_get_height);
  lua_register(L,"cairo_image_surface_get_stride",lcairo_image_surface_get_stride);
  lua_register(L,"cairo_image_surface_get_width",lcairo_image_surface_get_width);
  lua_register(L,"cairo_in_clip",lcairo_in_clip);
  lua_register(L,"cairo_in_fill",lcairo_in_fill);
  lua_register(L,"cairo_in_stroke",lcairo_in_stroke);
  lua_register(L,"cairo_line_to",lcairo_line_to);
  lua_register(L,"cairo_mask",lcairo_mask);
  lua_register(L,"cairo_mask_surface",lcairo_mask_surface);
  lua_register(L,"cairo_matrix_init",lcairo_matrix_init);
  lua_register(L,"cairo_matrix_init_identity",lcairo_matrix_init_identity);
  lua_register(L,"cairo_matrix_init_rotate",lcairo_matrix_init_rotate);
  lua_register(L,"cairo_matrix_init_scale",lcairo_matrix_init_scale);
  lua_register(L,"cairo_matrix_init_translate",lcairo_matrix_init_translate);
  lua_register(L,"cairo_matrix_invert",lcairo_matrix_invert);
  lua_register(L,"cairo_matrix_multiply",lcairo_matrix_multiply);
  lua_register(L,"cairo_matrix_rotate",lcairo_matrix_rotate);
  lua_register(L,"cairo_matrix_scale",lcairo_matrix_scale);
  lua_register(L,"cairo_matrix_transform_distance",lcairo_matrix_transform_distance);
  lua_register(L,"cairo_matrix_transform_point",lcairo_matrix_transform_point);
  lua_register(L,"cairo_matrix_translate",lcairo_matrix_translate);
  lua_register(L,"cairo_mesh_pattern_begin_patch",lcairo_mesh_pattern_begin_patch);
  lua_register(L,"cairo_mesh_pattern_curve_to",lcairo_mesh_pattern_curve_to);
  lua_register(L,"cairo_mesh_pattern_end_patch",lcairo_mesh_pattern_end_patch);
  lua_register(L,"cairo_mesh_pattern_get_control_point",lcairo_mesh_pattern_get_control_point);
  lua_register(L,"cairo_mesh_pattern_get_corner_color_rgba",lcairo_mesh_pattern_get_corner_color_rgba);
  lua_register(L,"cairo_mesh_pattern_get_patch_count",lcairo_mesh_pattern_get_patch_count);
  lua_register(L,"cairo_mesh_pattern_get_path",lcairo_mesh_pattern_get_path);
  lua_register(L,"cairo_mesh_pattern_line_to",lcairo_mesh_pattern_line_to);
  lua_register(L,"cairo_mesh_pattern_move_to",lcairo_mesh_pattern_move_to);
  lua_register(L,"cairo_mesh_pattern_set_control_point",lcairo_mesh_pattern_set_control_point);
  lua_register(L,"cairo_mesh_pattern_set_corner_color_rgb",lcairo_mesh_pattern_set_corner_color_rgb);
  lua_register(L,"cairo_mesh_pattern_set_corner_color_rgba",lcairo_mesh_pattern_set_corner_color_rgba);
  lua_register(L,"cairo_move_to",lcairo_move_to);
  lua_register(L,"cairo_new_path",lcairo_new_path);
  lua_register(L,"cairo_new_sub_path",lcairo_new_sub_path);
  lua_register(L,"cairo_paint",lcairo_paint);
  lua_register(L,"cairo_paint_with_alpha",lcairo_paint_with_alpha);
  lua_register(L,"cairo_path_destroy",lcairo_path_destroy);
  lua_register(L,"cairo_path_extents",lcairo_path_extents);
  lua_register(L,"cairo_pattern_add_color_stop_rgb",lcairo_pattern_add_color_stop_rgb);
  lua_register(L,"cairo_pattern_add_color_stop_rgba",lcairo_pattern_add_color_stop_rgba);
  lua_register(L,"cairo_pattern_create_for_surface",lcairo_pattern_create_for_surface);
  lua_register(L,"cairo_pattern_create_linear",lcairo_pattern_create_linear);
  lua_register(L,"cairo_pattern_create_mesh",lcairo_pattern_create_mesh);
  lua_register(L,"cairo_pattern_create_radial",lcairo_pattern_create_radial);
  lua_register(L,"cairo_pattern_create_raster_source",lcairo_pattern_create_raster_source);
  lua_register(L,"cairo_pattern_create_rgb",lcairo_pattern_create_rgb);
  lua_register(L,"cairo_pattern_create_rgba",lcairo_pattern_create_rgba);
  lua_register(L,"cairo_pattern_destroy",lcairo_pattern_destroy);
  lua_register(L,"cairo_pattern_get_color_stop_count",lcairo_pattern_get_color_stop_count);
  lua_register(L,"cairo_pattern_get_color_stop_rgba",lcairo_pattern_get_color_stop_rgba);
  lua_register(L,"cairo_pattern_get_extend",lcairo_pattern_get_extend);
  lua_register(L,"cairo_pattern_get_filter",lcairo_pattern_get_filter);
  lua_register(L,"cairo_pattern_get_linear_points",lcairo_pattern_get_linear_points);
  lua_register(L,"cairo_pattern_get_matrix",lcairo_pattern_get_matrix);
  lua_register(L,"cairo_pattern_get_radial_circles",lcairo_pattern_get_radial_circles);
  lua_register(L,"cairo_pattern_get_reference_count",lcairo_pattern_get_reference_count);
  lua_register(L,"cairo_pattern_get_rgba",lcairo_pattern_get_rgba);
  lua_register(L,"cairo_pattern_get_surface",lcairo_pattern_get_surface);
  lua_register(L,"cairo_pattern_get_type",lcairo_pattern_get_type);
  lua_register(L,"cairo_pattern_get_user_data",lcairo_pattern_get_user_data);
  lua_register(L,"cairo_pattern_reference",lcairo_pattern_reference);
  lua_register(L,"cairo_pattern_set_extend",lcairo_pattern_set_extend);
  lua_register(L,"cairo_pattern_set_filter",lcairo_pattern_set_filter);
  lua_register(L,"cairo_pattern_set_matrix",lcairo_pattern_set_matrix);
  lua_register(L,"cairo_pattern_set_user_data",lcairo_pattern_set_user_data);
  lua_register(L,"cairo_pattern_status",lcairo_pattern_status);
  lua_register(L,"cairo_pop_group",lcairo_pop_group);
  lua_register(L,"cairo_pop_group_to_source",lcairo_pop_group_to_source);
  lua_register(L,"cairo_push_group",lcairo_push_group);
  lua_register(L,"cairo_push_group_with_content",lcairo_push_group_with_content);
  lua_register(L,"cairo_raster_source_pattern_get_acquire",lcairo_raster_source_pattern_get_acquire);
  lua_register(L,"cairo_raster_source_pattern_get_callback_data",lcairo_raster_source_pattern_get_callback_data);
  lua_register(L,"cairo_raster_source_pattern_get_copy",lcairo_raster_source_pattern_get_copy);
  lua_register(L,"cairo_raster_source_pattern_get_finish",lcairo_raster_source_pattern_get_finish);
  lua_register(L,"cairo_raster_source_pattern_get_snapshot",lcairo_raster_source_pattern_get_snapshot);
  lua_register(L,"cairo_raster_source_pattern_set_acquire",lcairo_raster_source_pattern_set_acquire);
  lua_register(L,"cairo_raster_source_pattern_set_callback_data",lcairo_raster_source_pattern_set_callback_data);
  lua_register(L,"cairo_raster_source_pattern_set_copy",lcairo_raster_source_pattern_set_copy);
  lua_register(L,"cairo_raster_source_pattern_set_finish",lcairo_raster_source_pattern_set_finish);
  lua_register(L,"cairo_raster_source_pattern_set_snapshot",lcairo_raster_source_pattern_set_snapshot);
  lua_register(L,"cairo_recording_surface_create",lcairo_recording_surface_create);
  lua_register(L,"cairo_recording_surface_get_extents",lcairo_recording_surface_get_extents);
  lua_register(L,"cairo_recording_surface_ink_extents",lcairo_recording_surface_ink_extents);
  lua_register(L,"cairo_rectangle",lcairo_rectangle);
  lua_register(L,"cairo_rectangle_list_destroy",lcairo_rectangle_list_destroy);
  lua_register(L,"cairo_reference",lcairo_reference);
  lua_register(L,"cairo_region_contains_point",lcairo_region_contains_point);
  lua_register(L,"cairo_region_contains_rectangle",lcairo_region_contains_rectangle);
  lua_register(L,"cairo_region_copy",lcairo_region_copy);
  lua_register(L,"cairo_region_create",lcairo_region_create);
  lua_register(L,"cairo_region_create_rectangle",lcairo_region_create_rectangle);
  lua_register(L,"cairo_region_create_rectangles",lcairo_region_create_rectangles);
  lua_register(L,"cairo_region_destroy",lcairo_region_destroy);
  lua_register(L,"cairo_region_equal",lcairo_region_equal);
  lua_register(L,"cairo_region_get_extents",lcairo_region_get_extents);
  lua_register(L,"cairo_region_get_rectangle",lcairo_region_get_rectangle);
  lua_register(L,"cairo_region_intersect",lcairo_region_intersect);
  lua_register(L,"cairo_region_intersect_rectangle",lcairo_region_intersect_rectangle);
  lua_register(L,"cairo_region_is_empty",lcairo_region_is_empty);
  lua_register(L,"cairo_region_num_rectangles",lcairo_region_num_rectangles);
  lua_register(L,"cairo_region_reference",lcairo_region_reference);
  lua_register(L,"cairo_region_status",lcairo_region_status);
  lua_register(L,"cairo_region_subtract",lcairo_region_subtract);
  lua_register(L,"cairo_region_subtract_rectangle",lcairo_region_subtract_rectangle);
  lua_register(L,"cairo_region_translate",lcairo_region_translate);
  lua_register(L,"cairo_region_union",lcairo_region_union);
  lua_register(L,"cairo_region_union_rectangle",lcairo_region_union_rectangle);
  lua_register(L,"cairo_region_xor",lcairo_region_xor);
  lua_register(L,"cairo_region_xor_rectangle",lcairo_region_xor_rectangle);
  lua_register(L,"cairo_rel_curve_to",lcairo_rel_curve_to);
  lua_register(L,"cairo_rel_line_to",lcairo_rel_line_to);
  lua_register(L,"cairo_rel_move_to",lcairo_rel_move_to);
  lua_register(L,"cairo_reset_clip",lcairo_reset_clip);
  lua_register(L,"cairo_restore",lcairo_restore);
  lua_register(L,"cairo_rotate",lcairo_rotate);
  lua_register(L,"cairo_save",lcairo_save);
  lua_register(L,"cairo_scale",lcairo_scale);
  lua_register(L,"cairo_scaled_font_create",lcairo_scaled_font_create);
  lua_register(L,"cairo_scaled_font_destroy",lcairo_scaled_font_destroy);
  lua_register(L,"cairo_scaled_font_extents",lcairo_scaled_font_extents);
  lua_register(L,"cairo_scaled_font_get_ctm",lcairo_scaled_font_get_ctm);
  lua_register(L,"cairo_scaled_font_get_font_face",lcairo_scaled_font_get_font_face);
  lua_register(L,"cairo_scaled_font_get_font_matrix",lcairo_scaled_font_get_font_matrix);
  lua_register(L,"cairo_scaled_font_get_font_options",lcairo_scaled_font_get_font_options);
  lua_register(L,"cairo_scaled_font_get_reference_count",lcairo_scaled_font_get_reference_count);
  lua_register(L,"cairo_scaled_font_get_scale_matrix",lcairo_scaled_font_get_scale_matrix);
  lua_register(L,"cairo_scaled_font_get_type",lcairo_scaled_font_get_type);
  lua_register(L,"cairo_scaled_font_get_user_data",lcairo_scaled_font_get_user_data);
  lua_register(L,"cairo_scaled_font_glyph_extents",lcairo_scaled_font_glyph_extents);
  lua_register(L,"cairo_scaled_font_reference",lcairo_scaled_font_reference);
  lua_register(L,"cairo_scaled_font_set_user_data",lcairo_scaled_font_set_user_data);
  lua_register(L,"cairo_scaled_font_status",lcairo_scaled_font_status);
  lua_register(L,"cairo_scaled_font_text_extents",lcairo_scaled_font_text_extents);
  lua_register(L,"cairo_scaled_font_text_to_glyphs",lcairo_scaled_font_text_to_glyphs);
  lua_register(L,"cairo_select_font_face",lcairo_select_font_face);
  lua_register(L,"cairo_set_antialias",lcairo_set_antialias);
  lua_register(L,"cairo_set_dash",lcairo_set_dash);
  lua_register(L,"cairo_set_fill_rule",lcairo_set_fill_rule);
  lua_register(L,"cairo_set_font_face",lcairo_set_font_face);
  lua_register(L,"cairo_set_font_matrix",lcairo_set_font_matrix);
  lua_register(L,"cairo_set_font_options",lcairo_set_font_options);
  lua_register(L,"cairo_set_font_size",lcairo_set_font_size);
  lua_register(L,"cairo_set_line_cap",lcairo_set_line_cap);
  lua_register(L,"cairo_set_line_join",lcairo_set_line_join);
  lua_register(L,"cairo_set_line_width",lcairo_set_line_width);
  lua_register(L,"cairo_set_matrix",lcairo_set_matrix);
  lua_register(L,"cairo_set_miter_limit",lcairo_set_miter_limit);
  lua_register(L,"cairo_set_operator",lcairo_set_operator);
  lua_register(L,"cairo_set_scaled_font",lcairo_set_scaled_font);
  lua_register(L,"cairo_set_source",lcairo_set_source);
  lua_register(L,"cairo_set_source_rgb",lcairo_set_source_rgb);
  lua_register(L,"cairo_set_source_rgba",lcairo_set_source_rgba);
  lua_register(L,"cairo_set_source_surface",lcairo_set_source_surface);
  lua_register(L,"cairo_set_tolerance",lcairo_set_tolerance);
  lua_register(L,"cairo_set_user_data",lcairo_set_user_data);
  lua_register(L,"cairo_show_glyphs",lcairo_show_glyphs);
  lua_register(L,"cairo_show_page",lcairo_show_page);
  lua_register(L,"cairo_show_text",lcairo_show_text);
  lua_register(L,"cairo_show_text_glyphs",lcairo_show_text_glyphs);
  lua_register(L,"cairo_status",lcairo_status);
  lua_register(L,"cairo_status_to_string",lcairo_status_to_string);
  lua_register(L,"cairo_stroke",lcairo_stroke);
  lua_register(L,"cairo_stroke_extents",lcairo_stroke_extents);
  lua_register(L,"cairo_stroke_preserve",lcairo_stroke_preserve);
  lua_register(L,"cairo_surface_copy_page",lcairo_surface_copy_page);
  lua_register(L,"cairo_surface_create_for_rectangle",lcairo_surface_create_for_rectangle);
  lua_register(L,"cairo_surface_create_observer",lcairo_surface_create_observer);
  lua_register(L,"cairo_surface_create_similar",lcairo_surface_create_similar);
  lua_register(L,"cairo_surface_create_similar_image",lcairo_surface_create_similar_image);
  lua_register(L,"cairo_surface_destroy",lcairo_surface_destroy);
  lua_register(L,"cairo_surface_finish",lcairo_surface_finish);
  lua_register(L,"cairo_surface_flush",lcairo_surface_flush);
  lua_register(L,"cairo_surface_get_content",lcairo_surface_get_content);
  lua_register(L,"cairo_surface_get_device",lcairo_surface_get_device);
  lua_register(L,"cairo_surface_get_device_offset",lcairo_surface_get_device_offset);
  lua_register(L,"cairo_surface_get_device_scale",lcairo_surface_get_device_scale);
  lua_register(L,"cairo_surface_get_fallback_resolution",lcairo_surface_get_fallback_resolution);
  lua_register(L,"cairo_surface_get_font_options",lcairo_surface_get_font_options);
  lua_register(L,"cairo_surface_get_mime_data",lcairo_surface_get_mime_data);
  lua_register(L,"cairo_surface_get_reference_count",lcairo_surface_get_reference_count);
  lua_register(L,"cairo_surface_get_type",lcairo_surface_get_type);
  lua_register(L,"cairo_surface_get_user_data",lcairo_surface_get_user_data);
  lua_register(L,"cairo_surface_has_show_text_glyphs",lcairo_surface_has_show_text_glyphs);
  lua_register(L,"cairo_surface_map_to_image",lcairo_surface_map_to_image);
  lua_register(L,"cairo_surface_mark_dirty",lcairo_surface_mark_dirty);
  lua_register(L,"cairo_surface_mark_dirty_rectangle",lcairo_surface_mark_dirty_rectangle);
  lua_register(L,"cairo_surface_observer_add_fill_callback",lcairo_surface_observer_add_fill_callback);
  lua_register(L,"cairo_surface_observer_add_finish_callback",lcairo_surface_observer_add_finish_callback);
  lua_register(L,"cairo_surface_observer_add_flush_callback",lcairo_surface_observer_add_flush_callback);
  lua_register(L,"cairo_surface_observer_add_glyphs_callback",lcairo_surface_observer_add_glyphs_callback);
  lua_register(L,"cairo_surface_observer_add_mask_callback",lcairo_surface_observer_add_mask_callback);
  lua_register(L,"cairo_surface_observer_add_paint_callback",lcairo_surface_observer_add_paint_callback);
  lua_register(L,"cairo_surface_observer_add_stroke_callback",lcairo_surface_observer_add_stroke_callback);
  lua_register(L,"cairo_surface_observer_elapsed",lcairo_surface_observer_elapsed);
  lua_register(L,"cairo_surface_observer_print",lcairo_surface_observer_print);
  lua_register(L,"cairo_surface_reference",lcairo_surface_reference);
  lua_register(L,"cairo_surface_set_device_offset",lcairo_surface_set_device_offset);
  lua_register(L,"cairo_surface_set_device_scale",lcairo_surface_set_device_scale);
  lua_register(L,"cairo_surface_set_fallback_resolution",lcairo_surface_set_fallback_resolution);
  lua_register(L,"cairo_surface_set_mime_data",lcairo_surface_set_mime_data);
  lua_register(L,"cairo_surface_set_user_data",lcairo_surface_set_user_data);
  lua_register(L,"cairo_surface_show_page",lcairo_surface_show_page);
  lua_register(L,"cairo_surface_status",lcairo_surface_status);
  lua_register(L,"cairo_surface_supports_mime_type",lcairo_surface_supports_mime_type);
  lua_register(L,"cairo_surface_unmap_image",lcairo_surface_unmap_image);
  lua_register(L,"cairo_surface_write_to_png",lcairo_surface_write_to_png);
  lua_register(L,"cairo_surface_write_to_png_stream",lcairo_surface_write_to_png_stream);
  lua_register(L,"cairo_text_cluster_allocate",lcairo_text_cluster_allocate);
  lua_register(L,"cairo_text_cluster_free",lcairo_text_cluster_free);
  lua_register(L,"cairo_text_extents",lcairo_text_extents);
  lua_register(L,"cairo_text_path",lcairo_text_path);
  lua_register(L,"cairo_toy_font_face_create",lcairo_toy_font_face_create);
  lua_register(L,"cairo_toy_font_face_get_family",lcairo_toy_font_face_get_family);
  lua_register(L,"cairo_toy_font_face_get_slant",lcairo_toy_font_face_get_slant);
  lua_register(L,"cairo_toy_font_face_get_weight",lcairo_toy_font_face_get_weight);
  lua_register(L,"cairo_transform",lcairo_transform);
  lua_register(L,"cairo_translate",lcairo_translate);
  lua_register(L,"cairo_user_font_face_create",lcairo_user_font_face_create);
  lua_register(L,"cairo_user_font_face_get_init_func",lcairo_user_font_face_get_init_func);
  lua_register(L,"cairo_user_font_face_get_render_glyph_func",lcairo_user_font_face_get_render_glyph_func);
  lua_register(L,"cairo_user_font_face_get_text_to_glyphs_func",lcairo_user_font_face_get_text_to_glyphs_func);
  lua_register(L,"cairo_user_font_face_get_unicode_to_glyph_func",lcairo_user_font_face_get_unicode_to_glyph_func);
  lua_register(L,"cairo_user_font_face_set_init_func",lcairo_user_font_face_set_init_func);
  lua_register(L,"cairo_user_font_face_set_render_glyph_func",lcairo_user_font_face_set_render_glyph_func);
  lua_register(L,"cairo_user_font_face_set_text_to_glyphs_func",lcairo_user_font_face_set_text_to_glyphs_func);
  lua_register(L,"cairo_user_font_face_set_unicode_to_glyph_func",lcairo_user_font_face_set_unicode_to_glyph_func);
  lua_register(L,"cairo_user_to_device",lcairo_user_to_device);
  lua_register(L,"cairo_user_to_device_distance",lcairo_user_to_device_distance);
  lua_register(L,"cairo_version",lcairo_version);
  lua_register(L,"cairo_version_string",lcairo_version_string);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"CAIRO_ANTIALIAS_BEST",6);
  lua_registerInt(L,"CAIRO_ANTIALIAS_DEFAULT",0);
  lua_registerInt(L,"CAIRO_ANTIALIAS_DEFAULT",0);
  lua_registerInt(L,"CAIRO_ANTIALIAS_FAST",4);
  lua_registerInt(L,"CAIRO_ANTIALIAS_GOOD",5);
  lua_registerInt(L,"CAIRO_ANTIALIAS_GRAY",2);
  lua_registerInt(L,"CAIRO_ANTIALIAS_NONE",1);
  lua_registerInt(L,"CAIRO_ANTIALIAS_SUBPIXEL",3);
  lua_registerInt(L,"CAIRO_CONTENT_ALPHA",8192);
  lua_registerInt(L,"CAIRO_CONTENT_COLOR",4096);
  lua_registerInt(L,"CAIRO_CONTENT_COLOR",4096);
  lua_registerInt(L,"CAIRO_CONTENT_COLOR_ALPHA",12288);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_COGL",6);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_DRM",0);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_DRM",0);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_GL",1);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_INVALID",-1);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_SCRIPT",2);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_WIN32",7);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_XCB",3);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_XLIB",4);
  lua_registerInt(L,"CAIRO_DEVICE_TYPE_XML",5);
  lua_registerInt(L,"CAIRO_EXTEND_NONE",0);
  lua_registerInt(L,"CAIRO_EXTEND_NONE",0);
  lua_registerInt(L,"CAIRO_EXTEND_PAD",3);
  lua_registerInt(L,"CAIRO_EXTEND_REFLECT",2);
  lua_registerInt(L,"CAIRO_EXTEND_REPEAT",1);
  lua_registerInt(L,"CAIRO_FILL_RULE_EVEN_ODD",1);
  lua_registerInt(L,"CAIRO_FILL_RULE_WINDING",0);
  lua_registerInt(L,"CAIRO_FILL_RULE_WINDING",0);
  lua_registerInt(L,"CAIRO_FILTER_BEST",2);
  lua_registerInt(L,"CAIRO_FILTER_BILINEAR",4);
  lua_registerInt(L,"CAIRO_FILTER_FAST",0);
  lua_registerInt(L,"CAIRO_FILTER_FAST",0);
  lua_registerInt(L,"CAIRO_FILTER_GAUSSIAN",5);
  lua_registerInt(L,"CAIRO_FILTER_GOOD",1);
  lua_registerInt(L,"CAIRO_FILTER_NEAREST",3);
  lua_registerInt(L,"CAIRO_FONT_SLANT_ITALIC",1);
  lua_registerInt(L,"CAIRO_FONT_SLANT_NORMAL",0);
  lua_registerInt(L,"CAIRO_FONT_SLANT_NORMAL",0);
  lua_registerInt(L,"CAIRO_FONT_SLANT_OBLIQUE",2);
  lua_registerInt(L,"CAIRO_FONT_TYPE_FT",1);
  lua_registerInt(L,"CAIRO_FONT_TYPE_QUARTZ",3);
  lua_registerInt(L,"CAIRO_FONT_TYPE_TOY",0);
  lua_registerInt(L,"CAIRO_FONT_TYPE_TOY",0);
  lua_registerInt(L,"CAIRO_FONT_TYPE_USER",4);
  lua_registerInt(L,"CAIRO_FONT_TYPE_WIN32",2);
  lua_registerInt(L,"CAIRO_FONT_WEIGHT_BOLD",1);
  lua_registerInt(L,"CAIRO_FONT_WEIGHT_NORMAL",0);
  lua_registerInt(L,"CAIRO_FONT_WEIGHT_NORMAL",0);
  lua_registerInt(L,"CAIRO_FORMAT_A1",3);
  lua_registerInt(L,"CAIRO_FORMAT_A8",2);
  lua_registerInt(L,"CAIRO_FORMAT_ARGB32",0);
  lua_registerInt(L,"CAIRO_FORMAT_INVALID",-1);
  lua_registerInt(L,"CAIRO_FORMAT_INVALID",-1);
  lua_registerInt(L,"CAIRO_FORMAT_RGB16_565",4);
  lua_registerInt(L,"CAIRO_FORMAT_RGB24",1);
  lua_registerInt(L,"CAIRO_FORMAT_RGB30",5);
  lua_registerInt(L,"CAIRO_HINT_METRICS_DEFAULT",0);
  lua_registerInt(L,"CAIRO_HINT_METRICS_DEFAULT",0);
  lua_registerInt(L,"CAIRO_HINT_METRICS_OFF",1);
  lua_registerInt(L,"CAIRO_HINT_METRICS_ON",2);
  lua_registerInt(L,"CAIRO_HINT_STYLE_DEFAULT",0);
  lua_registerInt(L,"CAIRO_HINT_STYLE_DEFAULT",0);
  lua_registerInt(L,"CAIRO_HINT_STYLE_FULL",4);
  lua_registerInt(L,"CAIRO_HINT_STYLE_MEDIUM",3);
  lua_registerInt(L,"CAIRO_HINT_STYLE_NONE",1);
  lua_registerInt(L,"CAIRO_HINT_STYLE_SLIGHT",2);
  lua_registerInt(L,"CAIRO_LINE_CAP_BUTT",0);
  lua_registerInt(L,"CAIRO_LINE_CAP_BUTT",0);
  lua_registerInt(L,"CAIRO_LINE_CAP_ROUND",1);
  lua_registerInt(L,"CAIRO_LINE_CAP_SQUARE",2);
  lua_registerInt(L,"CAIRO_LINE_JOIN_BEVEL",2);
  lua_registerInt(L,"CAIRO_LINE_JOIN_MITER",0);
  lua_registerInt(L,"CAIRO_LINE_JOIN_MITER",0);
  lua_registerInt(L,"CAIRO_LINE_JOIN_ROUND",1);
  lua_registerInt(L,"CAIRO_OPERATOR_ADD",12);
  lua_registerInt(L,"CAIRO_OPERATOR_ATOP",5);
  lua_registerInt(L,"CAIRO_OPERATOR_CLEAR",0);
  lua_registerInt(L,"CAIRO_OPERATOR_CLEAR",0);
  lua_registerInt(L,"CAIRO_OPERATOR_COLOR_BURN",20);
  lua_registerInt(L,"CAIRO_OPERATOR_COLOR_DODGE",19);
  lua_registerInt(L,"CAIRO_OPERATOR_DARKEN",17);
  lua_registerInt(L,"CAIRO_OPERATOR_DEST",6);
  lua_registerInt(L,"CAIRO_OPERATOR_DEST_ATOP",10);
  lua_registerInt(L,"CAIRO_OPERATOR_DEST_IN",8);
  lua_registerInt(L,"CAIRO_OPERATOR_DEST_OUT",9);
  lua_registerInt(L,"CAIRO_OPERATOR_DEST_OVER",7);
  lua_registerInt(L,"CAIRO_OPERATOR_DIFFERENCE",23);
  lua_registerInt(L,"CAIRO_OPERATOR_EXCLUSION",24);
  lua_registerInt(L,"CAIRO_OPERATOR_HARD_LIGHT",21);
  lua_registerInt(L,"CAIRO_OPERATOR_HSL_COLOR",27);
  lua_registerInt(L,"CAIRO_OPERATOR_HSL_HUE",25);
  lua_registerInt(L,"CAIRO_OPERATOR_HSL_LUMINOSITY",28);
  lua_registerInt(L,"CAIRO_OPERATOR_HSL_SATURATION",26);
  lua_registerInt(L,"CAIRO_OPERATOR_IN",3);
  lua_registerInt(L,"CAIRO_OPERATOR_LIGHTEN",18);
  lua_registerInt(L,"CAIRO_OPERATOR_MULTIPLY",14);
  lua_registerInt(L,"CAIRO_OPERATOR_OUT",4);
  lua_registerInt(L,"CAIRO_OPERATOR_OVER",2);
  lua_registerInt(L,"CAIRO_OPERATOR_OVERLAY",16);
  lua_registerInt(L,"CAIRO_OPERATOR_SATURATE",13);
  lua_registerInt(L,"CAIRO_OPERATOR_SCREEN",15);
  lua_registerInt(L,"CAIRO_OPERATOR_SOFT_LIGHT",22);
  lua_registerInt(L,"CAIRO_OPERATOR_SOURCE",1);
  lua_registerInt(L,"CAIRO_OPERATOR_XOR",11);
  lua_registerInt(L,"CAIRO_PATH_CLOSE_PATH",3);
  lua_registerInt(L,"CAIRO_PATH_CURVE_TO",2);
  lua_registerInt(L,"CAIRO_PATH_LINE_TO",1);
  lua_registerInt(L,"CAIRO_PATH_MOVE_TO",0);
  lua_registerInt(L,"CAIRO_PATH_MOVE_TO",0);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_LINEAR",2);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_MESH",4);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_RADIAL",3);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_RASTER_SOURCE",5);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_SOLID",0);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_SOLID",0);
  lua_registerInt(L,"CAIRO_PATTERN_TYPE_SURFACE",1);
  lua_registerInt(L,"CAIRO_REGION_OVERLAP_IN",0);
  lua_registerInt(L,"CAIRO_REGION_OVERLAP_IN",0);
  lua_registerInt(L,"CAIRO_REGION_OVERLAP_OUT",1);
  lua_registerInt(L,"CAIRO_REGION_OVERLAP_PART",2);
  lua_registerInt(L,"CAIRO_STATUS_CLIP_NOT_REPRESENTABLE",22);
  lua_registerInt(L,"CAIRO_STATUS_DEVICE_ERROR",35);
  lua_registerInt(L,"CAIRO_STATUS_DEVICE_FINISHED",37);
  lua_registerInt(L,"CAIRO_STATUS_DEVICE_TYPE_MISMATCH",34);
  lua_registerInt(L,"CAIRO_STATUS_FILE_NOT_FOUND",18);
  lua_registerInt(L,"CAIRO_STATUS_FONT_TYPE_MISMATCH",25);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_CLUSTERS",29);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_CONTENT",15);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_DASH",19);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_DSC_COMMENT",20);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_FORMAT",16);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_INDEX",21);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_MATRIX",5);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_MESH_CONSTRUCTION",36);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_PATH_DATA",9);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_POP_GROUP",3);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_RESTORE",2);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_SIZE",32);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_SLANT",30);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_STATUS",6);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_STRIDE",24);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_STRING",8);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_VISUAL",17);
  lua_registerInt(L,"CAIRO_STATUS_INVALID_WEIGHT",31);
  lua_registerInt(L,"CAIRO_STATUS_JBIG2_GLOBAL_MISSING",38);
  lua_registerInt(L,"CAIRO_STATUS_LAST_STATUS",39);
  lua_registerInt(L,"CAIRO_STATUS_NEGATIVE_COUNT",28);
  lua_registerInt(L,"CAIRO_STATUS_NO_CURRENT_POINT",4);
  lua_registerInt(L,"CAIRO_STATUS_NO_MEMORY",1);
  lua_registerInt(L,"CAIRO_STATUS_NULL_POINTER",7);
  lua_registerInt(L,"CAIRO_STATUS_PATTERN_TYPE_MISMATCH",14);
  lua_registerInt(L,"CAIRO_STATUS_READ_ERROR",10);
  lua_registerInt(L,"CAIRO_STATUS_SUCCESS",0);
  lua_registerInt(L,"CAIRO_STATUS_SUCCESS",0);
  lua_registerInt(L,"CAIRO_STATUS_SURFACE_FINISHED",12);
  lua_registerInt(L,"CAIRO_STATUS_SURFACE_TYPE_MISMATCH",13);
  lua_registerInt(L,"CAIRO_STATUS_TEMP_FILE_ERROR",23);
  lua_registerInt(L,"CAIRO_STATUS_USER_FONT_ERROR",27);
  lua_registerInt(L,"CAIRO_STATUS_USER_FONT_IMMUTABLE",26);
  lua_registerInt(L,"CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED",33);
  lua_registerInt(L,"CAIRO_STATUS_WRITE_ERROR",11);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_BGR",2);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_DEFAULT",0);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_DEFAULT",0);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_RGB",1);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_VBGR",4);
  lua_registerInt(L,"CAIRO_SUBPIXEL_ORDER_VRGB",3);
  lua_registerInt(L,"CAIRO_SURFACE_OBSERVER_NORMAL",0);
  lua_registerInt(L,"CAIRO_SURFACE_OBSERVER_NORMAL",0);
  lua_registerInt(L,"CAIRO_SURFACE_OBSERVER_RECORD_OPERATIONS",1);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_BEOS",8);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_COGL",24);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_DIRECTFB",9);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_DRM",19);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_GL",18);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_GLITZ",5);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_IMAGE",0);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_IMAGE",0);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_OS2",11);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_PDF",1);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_PS",2);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_QT",15);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_QUARTZ",6);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_QUARTZ_IMAGE",13);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_RECORDING",16);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_SCRIPT",14);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_SKIA",22);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_SUBSURFACE",23);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_SVG",10);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_TEE",20);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_VG",17);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_WIN32",7);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_WIN32_PRINTING",12);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_XCB",4);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_XLIB",3);
  lua_registerInt(L,"CAIRO_SURFACE_TYPE_XML",21);
  lua_registerInt(L,"CAIRO_TEXT_CLUSTER_FLAG_BACKWARD",1);
  lua_registerInt(L,"CAIRO_TEXT_CLUSTER_FLAG_BACKWARD",1);


  return 0;
};

int luaopen_lcairo(lua_State*L){
  return lcairo_openlibs(L);
};

