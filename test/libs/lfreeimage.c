#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include <freeimage.h>

#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lFreeImage_AcquireMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)FreeImage_AcquireMemory(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_AdjustBrightness(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int ret = (int)FreeImage_AdjustBrightness(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_AdjustColors(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_AdjustColors(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_AdjustContrast(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int ret = (int)FreeImage_AdjustContrast(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_AdjustCurve(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_AdjustCurve(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_AdjustGamma(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int ret = (int)FreeImage_AdjustGamma(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Allocate(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)FreeImage_Allocate(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_AllocateEx(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* ret = (void*)FreeImage_AllocateEx(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_AllocateExT(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  int a10 = (int)luaL_checkinteger(L,10);
  void* ret = (void*)FreeImage_AllocateExT(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_AllocateT(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* ret = (void*)FreeImage_AllocateT(a1,a2,a3,a4,a5,a6,a7);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_AppendPage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  FreeImage_AppendPage(a1,a2);
  return 0;
};

static int lFreeImage_ApplyColorMapping(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int ret = (int)FreeImage_ApplyColorMapping(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ApplyPaletteIndexMapping(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_ApplyPaletteIndexMapping(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Clone(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_Clone(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_CloneMetadata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_CloneMetadata(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_CloneTag(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_CloneTag(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_CloseMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_CloseMemory(a1);
  return 0;
};

static int lFreeImage_CloseMultiBitmap(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_CloseMultiBitmap(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ColorQuantize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_ColorQuantize(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ColorQuantizeEx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)FreeImage_ColorQuantizeEx(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_Composite(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)FreeImage_Composite(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertFromRawBits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* ret = (void*)FreeImage_ConvertFromRawBits(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertFromRawBitsEx(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  int a10 = (int)luaL_checkinteger(L,10);
  int a11 = (int)luaL_checkinteger(L,11);
  void* ret = (void*)FreeImage_ConvertFromRawBitsEx(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertLine16To24_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To24_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To24_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To24_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To32_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To32_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To32_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To32_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To4_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To4_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To4_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To4_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To8_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To8_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16To8_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16To8_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16_555_To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16_555_To16_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine16_565_To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine16_565_To16_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine1To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine1To16_555(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine1To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine1To16_565(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine1To24(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine1To24(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine1To32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine1To32(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine1To4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine1To4(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine1To8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine1To8(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine24To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine24To16_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine24To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine24To16_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine24To32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine24To32(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine24To4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine24To4(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine24To8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine24To8(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine32To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine32To16_555(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine32To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine32To16_565(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine32To24(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine32To24(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine32To4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine32To4(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine32To8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine32To8(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine4To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine4To16_555(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine4To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine4To16_565(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine4To24(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine4To24(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine4To32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine4To32(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine4To8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_ConvertLine4To8(a1,a2,a3);
  return 0;
};

static int lFreeImage_ConvertLine8To16_555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine8To16_555(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine8To16_565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine8To16_565(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine8To24(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine8To24(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine8To32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine8To32(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertLine8To4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  FreeImage_ConvertLine8To4(a1,a2,a3,a4);
  return 0;
};

static int lFreeImage_ConvertTo16Bits555(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo16Bits555(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertTo16Bits565(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo16Bits565(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertTo24Bits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo24Bits(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertTo32Bits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo32Bits(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertTo4Bits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo4Bits(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertTo8Bits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertTo8Bits(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToFloat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToFloat(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToGreyscale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToGreyscale(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToRGB16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToRGB16(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToRGBA16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToRGBA16(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToRGBAF(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToRGBAF(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToRGBF(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToRGBF(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToRawBits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  FreeImage_ConvertToRawBits(a1,a2,a3,a4,a5,a6,a7,a8);
  return 0;
};

static int lFreeImage_ConvertToStandardType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_ConvertToStandardType(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_ConvertToType(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ConvertToUINT16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_ConvertToUINT16(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_Copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)FreeImage_Copy(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_CreateICCProfile(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_CreateICCProfile(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_CreateTag(lua_State*L){
  void* ret = (void*)FreeImage_CreateTag();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_CreateView(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)FreeImage_CreateView(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_DeInitialise(lua_State*L){
  FreeImage_DeInitialise();
  return 0;
};

static int lFreeImage_DeletePage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  FreeImage_DeletePage(a1,a2);
  return 0;
};

static int lFreeImage_DeleteTag(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_DeleteTag(a1);
  return 0;
};

static int lFreeImage_DestroyICCProfile(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_DestroyICCProfile(a1);
  return 0;
};

static int lFreeImage_Dither(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_Dither(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_EnlargeCanvas(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* ret = (void*)FreeImage_EnlargeCanvas(a1,a2,a3,a4,a5,a6,a7);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsExportBPP(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_FIFSupportsExportBPP(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsExportType(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_FIFSupportsExportType(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsICCProfiles(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)FreeImage_FIFSupportsICCProfiles(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsNoPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)FreeImage_FIFSupportsNoPixels(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsReading(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)FreeImage_FIFSupportsReading(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FIFSupportsWriting(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)FreeImage_FIFSupportsWriting(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FillBackground(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_FillBackground(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FindCloseMetadata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_FindCloseMetadata(a1);
  return 0;
};

static int lFreeImage_FindFirstMetadata(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)FreeImage_FindFirstMetadata(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_FindNextMetadata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_FindNextMetadata(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FlipHorizontal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_FlipHorizontal(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_FlipVertical(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_FlipVertical(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetAdjustColorsLookupTable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_GetAdjustColorsLookupTable(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetBPP(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetBPP(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetBackgroundColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_GetBackgroundColor(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetBits(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetBits(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetBlueMask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetBlueMask(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetChannel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_GetChannel(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetColorType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetColorType(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetColorsUsed(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetColorsUsed(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetComplexChannel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_GetComplexChannel(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetCopyrightMessage(lua_State*L){
  void* ret = (void*)FreeImage_GetCopyrightMessage();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetDIBSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetDIBSize(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetDotsPerMeterX(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetDotsPerMeterX(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetDotsPerMeterY(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetDotsPerMeterY(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFCount(lua_State*L){
  int ret = (int)FreeImage_GetFIFCount();
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFDescription(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)FreeImage_GetFIFDescription(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetFIFExtensionList(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)FreeImage_GetFIFExtensionList(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetFIFFromFilename(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetFIFFromFilename(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFFromFilenameU(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetFIFFromFilenameU(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFFromFormat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetFIFFromFormat(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFFromMime(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetFIFFromMime(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFIFMimeType(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)FreeImage_GetFIFMimeType(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetFIFRegExpr(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)FreeImage_GetFIFRegExpr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetFileType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_GetFileType(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFileTypeFromHandle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_GetFileTypeFromHandle(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFileTypeFromMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_GetFileTypeFromMemory(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFileTypeU(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_GetFileTypeU(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetFormatFromFIF(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)FreeImage_GetFormatFromFIF(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetGreenMask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetGreenMask(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetHeight(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetHeight(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetHistogram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_GetHistogram(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetICCProfile(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetICCProfile(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetImageType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetImageType(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetInfo(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetInfoHeader(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetInfoHeader(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetLine(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetLine(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetLockedPageNumbers(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)FreeImage_GetLockedPageNumbers(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetMemorySize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetMemorySize(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetMetadata(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_GetMetadata(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetMetadataCount(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_GetMetadataCount(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetPageCount(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetPageCount(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetPalette(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetPalette(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetPitch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetPitch(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetPixelColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_GetPixelColor(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetPixelIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_GetPixelIndex(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetRedMask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetRedMask(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetScanLine(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_GetScanLine(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetTagCount(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTagCount(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetTagDescription(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetTagDescription(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetTagID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTagID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetTagKey(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetTagKey(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetTagLength(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTagLength(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetTagType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTagType(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetTagValue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetTagValue(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetThumbnail(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetThumbnail(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetTransparencyCount(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTransparencyCount(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetTransparencyTable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)FreeImage_GetTransparencyTable(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetTransparentIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetTransparentIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_GetVersion(lua_State*L){
  void* ret = (void*)FreeImage_GetVersion();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_GetWidth(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_GetWidth(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_HasBackgroundColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_HasBackgroundColor(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_HasPixels(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_HasPixels(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Initialise(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  FreeImage_Initialise(a1);
  return 0;
};

static int lFreeImage_InsertPage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  FreeImage_InsertPage(a1,a2,a3);
  return 0;
};

static int lFreeImage_Invert(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_Invert(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_IsLittleEndian(lua_State*L){
  int ret = (int)FreeImage_IsLittleEndian();
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_IsPluginEnabled(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)FreeImage_IsPluginEnabled(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_IsTransparent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_IsTransparent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Load(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_Load(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LoadFromHandle(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)FreeImage_LoadFromHandle(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LoadFromMemory(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_LoadFromMemory(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LoadMultiBitmapFromMemory(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_LoadMultiBitmapFromMemory(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LoadU(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_LoadU(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LockPage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_LockPage(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_LookupSVGColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_LookupSVGColor(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_LookupX11Color(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_LookupX11Color(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_MakeThumbnail(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)FreeImage_MakeThumbnail(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_MovePage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_MovePage(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_MultigridPoissonSolver(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_MultigridPoissonSolver(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_OpenMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_OpenMemory(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_OpenMultiBitmap(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)FreeImage_OpenMultiBitmap(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_OpenMultiBitmapFromHandle(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)FreeImage_OpenMultiBitmapFromHandle(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_OutputMessageProc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  FreeImage_OutputMessageProc(a1,a2);
  return 0;
};

static int lFreeImage_Paste(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_Paste(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_PreMultiplyWithAlpha(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_PreMultiplyWithAlpha(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ReadMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_ReadMemory(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_RegisterExternalPlugin(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)FreeImage_RegisterExternalPlugin(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_RegisterLocalPlugin(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)FreeImage_RegisterLocalPlugin(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Rescale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)FreeImage_Rescale(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_RescaleRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* ret = (void*)FreeImage_RescaleRect(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_Rotate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)FreeImage_Rotate(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_RotateClassic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  void* ret = (void*)FreeImage_RotateClassic(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_RotateEx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* ret = (void*)FreeImage_RotateEx(a1,a2,a3,a4,a5,a6,a7);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_Save(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_Save(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SaveMultiBitmapToHandle(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_SaveMultiBitmapToHandle(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SaveMultiBitmapToMemory(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_SaveMultiBitmapToMemory(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SaveToHandle(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)FreeImage_SaveToHandle(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SaveToMemory(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_SaveToMemory(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SaveU(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_SaveU(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SeekMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_SeekMemory(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetBackgroundColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_SetBackgroundColor(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetChannel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_SetChannel(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetComplexChannel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_SetComplexChannel(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetDotsPerMeterX(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  FreeImage_SetDotsPerMeterX(a1,a2);
  return 0;
};

static int lFreeImage_SetDotsPerMeterY(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  FreeImage_SetDotsPerMeterY(a1,a2);
  return 0;
};

static int lFreeImage_SetMetadata(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_SetMetadata(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetMetadataKeyValue(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_SetMetadataKeyValue(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetOutputMessage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_SetOutputMessage(a1);
  return 0;
};

static int lFreeImage_SetOutputMessageStdCall(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_SetOutputMessageStdCall(a1);
  return 0;
};

static int lFreeImage_SetPixelColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_SetPixelColor(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetPixelIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_SetPixelIndex(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetPluginEnabled(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_SetPluginEnabled(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagCount(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_SetTagCount(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagDescription(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_SetTagDescription(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_SetTagID(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagKey(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_SetTagKey(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagLength(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_SetTagLength(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)FreeImage_SetTagType(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTagValue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_SetTagValue(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetThumbnail(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)FreeImage_SetThumbnail(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SetTransparencyTable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_SetTransparencyTable(a1,a2,a3);
  return 0;
};

static int lFreeImage_SetTransparent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  FreeImage_SetTransparent(a1,a2);
  return 0;
};

static int lFreeImage_SetTransparentIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  FreeImage_SetTransparentIndex(a1,a2);
  return 0;
};

static int lFreeImage_SwapColors(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_SwapColors(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_SwapPaletteIndices(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)FreeImage_SwapPaletteIndices(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_TagToString(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)FreeImage_TagToString(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_TellMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)FreeImage_TellMemory(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_Threshold(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)FreeImage_Threshold(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_TmoDrago03(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  void* ret = (void*)FreeImage_TmoDrago03(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_TmoFattal02(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  void* ret = (void*)FreeImage_TmoFattal02(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_TmoReinhard05(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  void* ret = (void*)FreeImage_TmoReinhard05(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_TmoReinhard05Ex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  void* ret = (void*)FreeImage_TmoReinhard05Ex(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_ToneMapping(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  void* ret = (void*)FreeImage_ToneMapping(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lFreeImage_Unload(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  FreeImage_Unload(a1);
  return 0;
};

static int lFreeImage_UnlockPage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  FreeImage_UnlockPage(a1,a2,a3);
  return 0;
};

static int lFreeImage_WriteMemory(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)FreeImage_WriteMemory(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ZLibCRC32(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)FreeImage_ZLibCRC32(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ZLibCompress(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_ZLibCompress(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ZLibGUnzip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_ZLibGUnzip(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ZLibGZip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_ZLibGZip(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lFreeImage_ZLibUncompress(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)FreeImage_ZLibUncompress(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

int lFreeImage_openlibs(lua_State*L){
  lua_register(L,"FreeImage_AcquireMemory",lFreeImage_AcquireMemory);
  lua_register(L,"FreeImage_AdjustBrightness",lFreeImage_AdjustBrightness);
  lua_register(L,"FreeImage_AdjustColors",lFreeImage_AdjustColors);
  lua_register(L,"FreeImage_AdjustContrast",lFreeImage_AdjustContrast);
  lua_register(L,"FreeImage_AdjustCurve",lFreeImage_AdjustCurve);
  lua_register(L,"FreeImage_AdjustGamma",lFreeImage_AdjustGamma);
  lua_register(L,"FreeImage_Allocate",lFreeImage_Allocate);
  lua_register(L,"FreeImage_AllocateEx",lFreeImage_AllocateEx);
  lua_register(L,"FreeImage_AllocateExT",lFreeImage_AllocateExT);
  lua_register(L,"FreeImage_AllocateT",lFreeImage_AllocateT);
  lua_register(L,"FreeImage_AppendPage",lFreeImage_AppendPage);
  lua_register(L,"FreeImage_ApplyColorMapping",lFreeImage_ApplyColorMapping);
  lua_register(L,"FreeImage_ApplyPaletteIndexMapping",lFreeImage_ApplyPaletteIndexMapping);
  lua_register(L,"FreeImage_Clone",lFreeImage_Clone);
  lua_register(L,"FreeImage_CloneMetadata",lFreeImage_CloneMetadata);
  lua_register(L,"FreeImage_CloneTag",lFreeImage_CloneTag);
  lua_register(L,"FreeImage_CloseMemory",lFreeImage_CloseMemory);
  lua_register(L,"FreeImage_CloseMultiBitmap",lFreeImage_CloseMultiBitmap);
  lua_register(L,"FreeImage_ColorQuantize",lFreeImage_ColorQuantize);
  lua_register(L,"FreeImage_ColorQuantizeEx",lFreeImage_ColorQuantizeEx);
  lua_register(L,"FreeImage_Composite",lFreeImage_Composite);
  lua_register(L,"FreeImage_ConvertFromRawBits",lFreeImage_ConvertFromRawBits);
  lua_register(L,"FreeImage_ConvertFromRawBitsEx",lFreeImage_ConvertFromRawBitsEx);
  lua_register(L,"FreeImage_ConvertLine16To24_555",lFreeImage_ConvertLine16To24_555);
  lua_register(L,"FreeImage_ConvertLine16To24_565",lFreeImage_ConvertLine16To24_565);
  lua_register(L,"FreeImage_ConvertLine16To32_555",lFreeImage_ConvertLine16To32_555);
  lua_register(L,"FreeImage_ConvertLine16To32_565",lFreeImage_ConvertLine16To32_565);
  lua_register(L,"FreeImage_ConvertLine16To4_555",lFreeImage_ConvertLine16To4_555);
  lua_register(L,"FreeImage_ConvertLine16To4_565",lFreeImage_ConvertLine16To4_565);
  lua_register(L,"FreeImage_ConvertLine16To8_555",lFreeImage_ConvertLine16To8_555);
  lua_register(L,"FreeImage_ConvertLine16To8_565",lFreeImage_ConvertLine16To8_565);
  lua_register(L,"FreeImage_ConvertLine16_555_To16_565",lFreeImage_ConvertLine16_555_To16_565);
  lua_register(L,"FreeImage_ConvertLine16_565_To16_555",lFreeImage_ConvertLine16_565_To16_555);
  lua_register(L,"FreeImage_ConvertLine1To16_555",lFreeImage_ConvertLine1To16_555);
  lua_register(L,"FreeImage_ConvertLine1To16_565",lFreeImage_ConvertLine1To16_565);
  lua_register(L,"FreeImage_ConvertLine1To24",lFreeImage_ConvertLine1To24);
  lua_register(L,"FreeImage_ConvertLine1To32",lFreeImage_ConvertLine1To32);
  lua_register(L,"FreeImage_ConvertLine1To4",lFreeImage_ConvertLine1To4);
  lua_register(L,"FreeImage_ConvertLine1To8",lFreeImage_ConvertLine1To8);
  lua_register(L,"FreeImage_ConvertLine24To16_555",lFreeImage_ConvertLine24To16_555);
  lua_register(L,"FreeImage_ConvertLine24To16_565",lFreeImage_ConvertLine24To16_565);
  lua_register(L,"FreeImage_ConvertLine24To32",lFreeImage_ConvertLine24To32);
  lua_register(L,"FreeImage_ConvertLine24To4",lFreeImage_ConvertLine24To4);
  lua_register(L,"FreeImage_ConvertLine24To8",lFreeImage_ConvertLine24To8);
  lua_register(L,"FreeImage_ConvertLine32To16_555",lFreeImage_ConvertLine32To16_555);
  lua_register(L,"FreeImage_ConvertLine32To16_565",lFreeImage_ConvertLine32To16_565);
  lua_register(L,"FreeImage_ConvertLine32To24",lFreeImage_ConvertLine32To24);
  lua_register(L,"FreeImage_ConvertLine32To4",lFreeImage_ConvertLine32To4);
  lua_register(L,"FreeImage_ConvertLine32To8",lFreeImage_ConvertLine32To8);
  lua_register(L,"FreeImage_ConvertLine4To16_555",lFreeImage_ConvertLine4To16_555);
  lua_register(L,"FreeImage_ConvertLine4To16_565",lFreeImage_ConvertLine4To16_565);
  lua_register(L,"FreeImage_ConvertLine4To24",lFreeImage_ConvertLine4To24);
  lua_register(L,"FreeImage_ConvertLine4To32",lFreeImage_ConvertLine4To32);
  lua_register(L,"FreeImage_ConvertLine4To8",lFreeImage_ConvertLine4To8);
  lua_register(L,"FreeImage_ConvertLine8To16_555",lFreeImage_ConvertLine8To16_555);
  lua_register(L,"FreeImage_ConvertLine8To16_565",lFreeImage_ConvertLine8To16_565);
  lua_register(L,"FreeImage_ConvertLine8To24",lFreeImage_ConvertLine8To24);
  lua_register(L,"FreeImage_ConvertLine8To32",lFreeImage_ConvertLine8To32);
  lua_register(L,"FreeImage_ConvertLine8To4",lFreeImage_ConvertLine8To4);
  lua_register(L,"FreeImage_ConvertTo16Bits555",lFreeImage_ConvertTo16Bits555);
  lua_register(L,"FreeImage_ConvertTo16Bits565",lFreeImage_ConvertTo16Bits565);
  lua_register(L,"FreeImage_ConvertTo24Bits",lFreeImage_ConvertTo24Bits);
  lua_register(L,"FreeImage_ConvertTo32Bits",lFreeImage_ConvertTo32Bits);
  lua_register(L,"FreeImage_ConvertTo4Bits",lFreeImage_ConvertTo4Bits);
  lua_register(L,"FreeImage_ConvertTo8Bits",lFreeImage_ConvertTo8Bits);
  lua_register(L,"FreeImage_ConvertToFloat",lFreeImage_ConvertToFloat);
  lua_register(L,"FreeImage_ConvertToGreyscale",lFreeImage_ConvertToGreyscale);
  lua_register(L,"FreeImage_ConvertToRGB16",lFreeImage_ConvertToRGB16);
  lua_register(L,"FreeImage_ConvertToRGBA16",lFreeImage_ConvertToRGBA16);
  lua_register(L,"FreeImage_ConvertToRGBAF",lFreeImage_ConvertToRGBAF);
  lua_register(L,"FreeImage_ConvertToRGBF",lFreeImage_ConvertToRGBF);
  lua_register(L,"FreeImage_ConvertToRawBits",lFreeImage_ConvertToRawBits);
  lua_register(L,"FreeImage_ConvertToStandardType",lFreeImage_ConvertToStandardType);
  lua_register(L,"FreeImage_ConvertToType",lFreeImage_ConvertToType);
  lua_register(L,"FreeImage_ConvertToUINT16",lFreeImage_ConvertToUINT16);
  lua_register(L,"FreeImage_Copy",lFreeImage_Copy);
  lua_register(L,"FreeImage_CreateICCProfile",lFreeImage_CreateICCProfile);
  lua_register(L,"FreeImage_CreateTag",lFreeImage_CreateTag);
  lua_register(L,"FreeImage_CreateView",lFreeImage_CreateView);
  lua_register(L,"FreeImage_DeInitialise",lFreeImage_DeInitialise);
  lua_register(L,"FreeImage_DeletePage",lFreeImage_DeletePage);
  lua_register(L,"FreeImage_DeleteTag",lFreeImage_DeleteTag);
  lua_register(L,"FreeImage_DestroyICCProfile",lFreeImage_DestroyICCProfile);
  lua_register(L,"FreeImage_Dither",lFreeImage_Dither);
  lua_register(L,"FreeImage_EnlargeCanvas",lFreeImage_EnlargeCanvas);
  lua_register(L,"FreeImage_FIFSupportsExportBPP",lFreeImage_FIFSupportsExportBPP);
  lua_register(L,"FreeImage_FIFSupportsExportType",lFreeImage_FIFSupportsExportType);
  lua_register(L,"FreeImage_FIFSupportsICCProfiles",lFreeImage_FIFSupportsICCProfiles);
  lua_register(L,"FreeImage_FIFSupportsNoPixels",lFreeImage_FIFSupportsNoPixels);
  lua_register(L,"FreeImage_FIFSupportsReading",lFreeImage_FIFSupportsReading);
  lua_register(L,"FreeImage_FIFSupportsWriting",lFreeImage_FIFSupportsWriting);
  lua_register(L,"FreeImage_FillBackground",lFreeImage_FillBackground);
  lua_register(L,"FreeImage_FindCloseMetadata",lFreeImage_FindCloseMetadata);
  lua_register(L,"FreeImage_FindFirstMetadata",lFreeImage_FindFirstMetadata);
  lua_register(L,"FreeImage_FindNextMetadata",lFreeImage_FindNextMetadata);
  lua_register(L,"FreeImage_FlipHorizontal",lFreeImage_FlipHorizontal);
  lua_register(L,"FreeImage_FlipVertical",lFreeImage_FlipVertical);
  lua_register(L,"FreeImage_GetAdjustColorsLookupTable",lFreeImage_GetAdjustColorsLookupTable);
  lua_register(L,"FreeImage_GetBPP",lFreeImage_GetBPP);
  lua_register(L,"FreeImage_GetBackgroundColor",lFreeImage_GetBackgroundColor);
  lua_register(L,"FreeImage_GetBits",lFreeImage_GetBits);
  lua_register(L,"FreeImage_GetBlueMask",lFreeImage_GetBlueMask);
  lua_register(L,"FreeImage_GetChannel",lFreeImage_GetChannel);
  lua_register(L,"FreeImage_GetColorType",lFreeImage_GetColorType);
  lua_register(L,"FreeImage_GetColorsUsed",lFreeImage_GetColorsUsed);
  lua_register(L,"FreeImage_GetComplexChannel",lFreeImage_GetComplexChannel);
  lua_register(L,"FreeImage_GetCopyrightMessage",lFreeImage_GetCopyrightMessage);
  lua_register(L,"FreeImage_GetDIBSize",lFreeImage_GetDIBSize);
  lua_register(L,"FreeImage_GetDotsPerMeterX",lFreeImage_GetDotsPerMeterX);
  lua_register(L,"FreeImage_GetDotsPerMeterY",lFreeImage_GetDotsPerMeterY);
  lua_register(L,"FreeImage_GetFIFCount",lFreeImage_GetFIFCount);
  lua_register(L,"FreeImage_GetFIFDescription",lFreeImage_GetFIFDescription);
  lua_register(L,"FreeImage_GetFIFExtensionList",lFreeImage_GetFIFExtensionList);
  lua_register(L,"FreeImage_GetFIFFromFilename",lFreeImage_GetFIFFromFilename);
  lua_register(L,"FreeImage_GetFIFFromFilenameU",lFreeImage_GetFIFFromFilenameU);
  lua_register(L,"FreeImage_GetFIFFromFormat",lFreeImage_GetFIFFromFormat);
  lua_register(L,"FreeImage_GetFIFFromMime",lFreeImage_GetFIFFromMime);
  lua_register(L,"FreeImage_GetFIFMimeType",lFreeImage_GetFIFMimeType);
  lua_register(L,"FreeImage_GetFIFRegExpr",lFreeImage_GetFIFRegExpr);
  lua_register(L,"FreeImage_GetFileType",lFreeImage_GetFileType);
  lua_register(L,"FreeImage_GetFileTypeFromHandle",lFreeImage_GetFileTypeFromHandle);
  lua_register(L,"FreeImage_GetFileTypeFromMemory",lFreeImage_GetFileTypeFromMemory);
  lua_register(L,"FreeImage_GetFileTypeU",lFreeImage_GetFileTypeU);
  lua_register(L,"FreeImage_GetFormatFromFIF",lFreeImage_GetFormatFromFIF);
  lua_register(L,"FreeImage_GetGreenMask",lFreeImage_GetGreenMask);
  lua_register(L,"FreeImage_GetHeight",lFreeImage_GetHeight);
  lua_register(L,"FreeImage_GetHistogram",lFreeImage_GetHistogram);
  lua_register(L,"FreeImage_GetICCProfile",lFreeImage_GetICCProfile);
  lua_register(L,"FreeImage_GetImageType",lFreeImage_GetImageType);
  lua_register(L,"FreeImage_GetInfo",lFreeImage_GetInfo);
  lua_register(L,"FreeImage_GetInfoHeader",lFreeImage_GetInfoHeader);
  lua_register(L,"FreeImage_GetLine",lFreeImage_GetLine);
  lua_register(L,"FreeImage_GetLockedPageNumbers",lFreeImage_GetLockedPageNumbers);
  lua_register(L,"FreeImage_GetMemorySize",lFreeImage_GetMemorySize);
  lua_register(L,"FreeImage_GetMetadata",lFreeImage_GetMetadata);
  lua_register(L,"FreeImage_GetMetadataCount",lFreeImage_GetMetadataCount);
  lua_register(L,"FreeImage_GetPageCount",lFreeImage_GetPageCount);
  lua_register(L,"FreeImage_GetPalette",lFreeImage_GetPalette);
  lua_register(L,"FreeImage_GetPitch",lFreeImage_GetPitch);
  lua_register(L,"FreeImage_GetPixelColor",lFreeImage_GetPixelColor);
  lua_register(L,"FreeImage_GetPixelIndex",lFreeImage_GetPixelIndex);
  lua_register(L,"FreeImage_GetRedMask",lFreeImage_GetRedMask);
  lua_register(L,"FreeImage_GetScanLine",lFreeImage_GetScanLine);
  lua_register(L,"FreeImage_GetTagCount",lFreeImage_GetTagCount);
  lua_register(L,"FreeImage_GetTagDescription",lFreeImage_GetTagDescription);
  lua_register(L,"FreeImage_GetTagID",lFreeImage_GetTagID);
  lua_register(L,"FreeImage_GetTagKey",lFreeImage_GetTagKey);
  lua_register(L,"FreeImage_GetTagLength",lFreeImage_GetTagLength);
  lua_register(L,"FreeImage_GetTagType",lFreeImage_GetTagType);
  lua_register(L,"FreeImage_GetTagValue",lFreeImage_GetTagValue);
  lua_register(L,"FreeImage_GetThumbnail",lFreeImage_GetThumbnail);
  lua_register(L,"FreeImage_GetTransparencyCount",lFreeImage_GetTransparencyCount);
  lua_register(L,"FreeImage_GetTransparencyTable",lFreeImage_GetTransparencyTable);
  lua_register(L,"FreeImage_GetTransparentIndex",lFreeImage_GetTransparentIndex);
  lua_register(L,"FreeImage_GetVersion",lFreeImage_GetVersion);
  lua_register(L,"FreeImage_GetWidth",lFreeImage_GetWidth);
  lua_register(L,"FreeImage_HasBackgroundColor",lFreeImage_HasBackgroundColor);
  lua_register(L,"FreeImage_HasPixels",lFreeImage_HasPixels);
  lua_register(L,"FreeImage_Initialise",lFreeImage_Initialise);
  lua_register(L,"FreeImage_InsertPage",lFreeImage_InsertPage);
  lua_register(L,"FreeImage_Invert",lFreeImage_Invert);
  lua_register(L,"FreeImage_IsLittleEndian",lFreeImage_IsLittleEndian);
  lua_register(L,"FreeImage_IsPluginEnabled",lFreeImage_IsPluginEnabled);
  lua_register(L,"FreeImage_IsTransparent",lFreeImage_IsTransparent);
  lua_register(L,"FreeImage_Load",lFreeImage_Load);
  lua_register(L,"FreeImage_LoadFromHandle",lFreeImage_LoadFromHandle);
  lua_register(L,"FreeImage_LoadFromMemory",lFreeImage_LoadFromMemory);
  lua_register(L,"FreeImage_LoadMultiBitmapFromMemory",lFreeImage_LoadMultiBitmapFromMemory);
  lua_register(L,"FreeImage_LoadU",lFreeImage_LoadU);
  lua_register(L,"FreeImage_LockPage",lFreeImage_LockPage);
  lua_register(L,"FreeImage_LookupSVGColor",lFreeImage_LookupSVGColor);
  lua_register(L,"FreeImage_LookupX11Color",lFreeImage_LookupX11Color);
  lua_register(L,"FreeImage_MakeThumbnail",lFreeImage_MakeThumbnail);
  lua_register(L,"FreeImage_MovePage",lFreeImage_MovePage);
  lua_register(L,"FreeImage_MultigridPoissonSolver",lFreeImage_MultigridPoissonSolver);
  lua_register(L,"FreeImage_OpenMemory",lFreeImage_OpenMemory);
  lua_register(L,"FreeImage_OpenMultiBitmap",lFreeImage_OpenMultiBitmap);
  lua_register(L,"FreeImage_OpenMultiBitmapFromHandle",lFreeImage_OpenMultiBitmapFromHandle);
  lua_register(L,"FreeImage_OutputMessageProc",lFreeImage_OutputMessageProc);
  lua_register(L,"FreeImage_Paste",lFreeImage_Paste);
  lua_register(L,"FreeImage_PreMultiplyWithAlpha",lFreeImage_PreMultiplyWithAlpha);
  lua_register(L,"FreeImage_ReadMemory",lFreeImage_ReadMemory);
  lua_register(L,"FreeImage_RegisterExternalPlugin",lFreeImage_RegisterExternalPlugin);
  lua_register(L,"FreeImage_RegisterLocalPlugin",lFreeImage_RegisterLocalPlugin);
  lua_register(L,"FreeImage_Rescale",lFreeImage_Rescale);
  lua_register(L,"FreeImage_RescaleRect",lFreeImage_RescaleRect);
  lua_register(L,"FreeImage_Rotate",lFreeImage_Rotate);
  lua_register(L,"FreeImage_RotateClassic",lFreeImage_RotateClassic);
  lua_register(L,"FreeImage_RotateEx",lFreeImage_RotateEx);
  lua_register(L,"FreeImage_Save",lFreeImage_Save);
  lua_register(L,"FreeImage_SaveMultiBitmapToHandle",lFreeImage_SaveMultiBitmapToHandle);
  lua_register(L,"FreeImage_SaveMultiBitmapToMemory",lFreeImage_SaveMultiBitmapToMemory);
  lua_register(L,"FreeImage_SaveToHandle",lFreeImage_SaveToHandle);
  lua_register(L,"FreeImage_SaveToMemory",lFreeImage_SaveToMemory);
  lua_register(L,"FreeImage_SaveU",lFreeImage_SaveU);
  lua_register(L,"FreeImage_SeekMemory",lFreeImage_SeekMemory);
  lua_register(L,"FreeImage_SetBackgroundColor",lFreeImage_SetBackgroundColor);
  lua_register(L,"FreeImage_SetChannel",lFreeImage_SetChannel);
  lua_register(L,"FreeImage_SetComplexChannel",lFreeImage_SetComplexChannel);
  lua_register(L,"FreeImage_SetDotsPerMeterX",lFreeImage_SetDotsPerMeterX);
  lua_register(L,"FreeImage_SetDotsPerMeterY",lFreeImage_SetDotsPerMeterY);
  lua_register(L,"FreeImage_SetMetadata",lFreeImage_SetMetadata);
  lua_register(L,"FreeImage_SetMetadataKeyValue",lFreeImage_SetMetadataKeyValue);
  lua_register(L,"FreeImage_SetOutputMessage",lFreeImage_SetOutputMessage);
  lua_register(L,"FreeImage_SetOutputMessageStdCall",lFreeImage_SetOutputMessageStdCall);
  lua_register(L,"FreeImage_SetPixelColor",lFreeImage_SetPixelColor);
  lua_register(L,"FreeImage_SetPixelIndex",lFreeImage_SetPixelIndex);
  lua_register(L,"FreeImage_SetPluginEnabled",lFreeImage_SetPluginEnabled);
  lua_register(L,"FreeImage_SetTagCount",lFreeImage_SetTagCount);
  lua_register(L,"FreeImage_SetTagDescription",lFreeImage_SetTagDescription);
  lua_register(L,"FreeImage_SetTagID",lFreeImage_SetTagID);
  lua_register(L,"FreeImage_SetTagKey",lFreeImage_SetTagKey);
  lua_register(L,"FreeImage_SetTagLength",lFreeImage_SetTagLength);
  lua_register(L,"FreeImage_SetTagType",lFreeImage_SetTagType);
  lua_register(L,"FreeImage_SetTagValue",lFreeImage_SetTagValue);
  lua_register(L,"FreeImage_SetThumbnail",lFreeImage_SetThumbnail);
  lua_register(L,"FreeImage_SetTransparencyTable",lFreeImage_SetTransparencyTable);
  lua_register(L,"FreeImage_SetTransparent",lFreeImage_SetTransparent);
  lua_register(L,"FreeImage_SetTransparentIndex",lFreeImage_SetTransparentIndex);
  lua_register(L,"FreeImage_SwapColors",lFreeImage_SwapColors);
  lua_register(L,"FreeImage_SwapPaletteIndices",lFreeImage_SwapPaletteIndices);
  lua_register(L,"FreeImage_TagToString",lFreeImage_TagToString);
  lua_register(L,"FreeImage_TellMemory",lFreeImage_TellMemory);
  lua_register(L,"FreeImage_Threshold",lFreeImage_Threshold);
  lua_register(L,"FreeImage_TmoDrago03",lFreeImage_TmoDrago03);
  lua_register(L,"FreeImage_TmoFattal02",lFreeImage_TmoFattal02);
  lua_register(L,"FreeImage_TmoReinhard05",lFreeImage_TmoReinhard05);
  lua_register(L,"FreeImage_TmoReinhard05Ex",lFreeImage_TmoReinhard05Ex);
  lua_register(L,"FreeImage_ToneMapping",lFreeImage_ToneMapping);
  lua_register(L,"FreeImage_Unload",lFreeImage_Unload);
  lua_register(L,"FreeImage_UnlockPage",lFreeImage_UnlockPage);
  lua_register(L,"FreeImage_WriteMemory",lFreeImage_WriteMemory);
  lua_register(L,"FreeImage_ZLibCRC32",lFreeImage_ZLibCRC32);
  lua_register(L,"FreeImage_ZLibCompress",lFreeImage_ZLibCompress);
  lua_register(L,"FreeImage_ZLibGUnzip",lFreeImage_ZLibGUnzip);
  lua_register(L,"FreeImage_ZLibGZip",lFreeImage_ZLibGZip);
  lua_register(L,"FreeImage_ZLibUncompress",lFreeImage_ZLibUncompress);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"FICC_RGB",0);
  lua_registerInt(L,"FIC_MINISWHITE",0);
  lua_registerInt(L,"FIDT_NOTYPE",0);
  lua_registerInt(L,"FID_FS",0);
  lua_registerInt(L,"FIF_UNKNOWN",-1);
  lua_registerInt(L,"FILTER_BOX",0);
  lua_registerInt(L,"FIMD_NODATA",-1);
  lua_registerInt(L,"FIQ_WUQUANT",0);
  lua_registerInt(L,"FITMO_DRAGO03",0);
  lua_registerInt(L,"FIT_UNKNOWN",0);




//struct 
  lua_getglobal(L,"structs");
  if(lua_isnil(L,-1)){
    lua_newtable(L);
    lua_setglobal(L,"structs");
    lua_getglobal(L,"structs");
  };
  
#ifndef lua_StructBegin
#define lua_setField(name,bpos,bsiz,type,typeName)  \
    lua_createtable(L,4,1);\
    lua_pushinteger(L,bpos);lua_setfield(L,-2,"bpos");\
    lua_pushinteger(L,bsiz);lua_setfield(L,-2,"bsiz");\
    lua_pushstring(L,type);lua_setfield(L,-2,"type");\
    lua_pushstring(L,typeName);lua_setfield(L,-2,"tname");\
    lua_setfield(L,-2,name);

#ifndef lua_StructSize
#define lua_StructSize(siz)   lua_pushinteger(L,siz); lua_setfield(L,-2,"1size");
#endif
#define lua_StructBegin(len)  lua_createtable(L,len+1,4);
#define lua_StructEnd(name)   lua_setfield(L,-2,name);
#endif
  
  lua_StructBegin(5);
    lua_setField("read_proc",0,32,"pointer_type","FI_ReadProc");
    lua_setField("write_proc",32,32,"pointer_type","FI_WriteProc");
    lua_setField("seek_proc",64,32,"pointer_type","FI_SeekProc");
    lua_setField("tell_proc",96,32,"pointer_type","FI_TellProc");
  lua_StructSize(128);

  lua_StructEnd("FreeImageIO");
  
  return 0;
};

int luaopen_lFreeImage(lua_State*L){
  return lFreeImage_openlibs(L);
};

