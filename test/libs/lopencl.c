#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include <cl/opencl.h>

#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lclBuildProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clBuildProgram(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclCloneKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)clCloneKernel(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCompileProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clCompileProgram(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclCreateBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateBuffer(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateCommandQueue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)clCreateCommandQueue(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateCommandQueueWithProperties(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)clCreateCommandQueueWithProperties(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateContext(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreateContext(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateContextFromType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateContextFromType(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateEventFromGLsyncKHR(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)clCreateEventFromGLsyncKHR(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateFromGLBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)clCreateFromGLBuffer(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateFromGLRenderbuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)clCreateFromGLRenderbuffer(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateFromGLTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreateFromGLTexture(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateFromGLTexture2D(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreateFromGLTexture2D(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateFromGLTexture3D(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreateFromGLTexture3D(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreateImage(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateImage2D(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* ret = (void*)clCreateImage2D(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateImage3D(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* a9 = lcheck_toptr(L,9);
  void* a10 = lcheck_toptr(L,10);
  void* ret = (void*)clCreateImage3D(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)clCreateKernel(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateKernelsInProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clCreateKernelsInProgram(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclCreatePipe(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clCreatePipe(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateProgramWithBinary(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* ret = (void*)clCreateProgramWithBinary(a1,a2,a3,a4,a5,a6,a7);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateProgramWithBuiltInKernels(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateProgramWithBuiltInKernels(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateProgramWithIL(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)clCreateProgramWithIL(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateProgramWithSource(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateProgramWithSource(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateSampler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateSampler(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateSamplerWithProperties(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)clCreateSamplerWithProperties(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateSubBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)clCreateSubBuffer(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclCreateSubDevices(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clCreateSubDevices(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclCreateSubDevicesEXT(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clCreateSubDevicesEXT(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclCreateUserEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)clCreateUserEvent(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclEnqueueAcquireGLObjects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clEnqueueAcquireGLObjects(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueAcquireGrallocObjectsIMG(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clEnqueueAcquireGrallocObjectsIMG(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueBarrier(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clEnqueueBarrier(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueBarrierWithWaitList(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clEnqueueBarrierWithWaitList(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueCopyBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueCopyBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueCopyBufferRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  int a10 = (int)luaL_checkinteger(L,10);
  int a11 = (int)luaL_checkinteger(L,11);
  void* a12 = lcheck_toptr(L,12);
  void* a13 = lcheck_toptr(L,13);
  int ret = (int)clEnqueueCopyBufferRect(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueCopyBufferToImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueCopyBufferToImage(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueCopyImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueCopyImage(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueCopyImageToBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueCopyImageToBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueFillBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueFillBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueFillImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueFillImage(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueMapBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  void* a10 = lcheck_toptr(L,10);
  void* ret = (void*)clEnqueueMapBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclEnqueueMapImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* a10 = lcheck_toptr(L,10);
  void* a11 = lcheck_toptr(L,11);
  void* a12 = lcheck_toptr(L,12);
  void* ret = (void*)clEnqueueMapImage(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclEnqueueMarker(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)clEnqueueMarker(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueMarkerWithWaitList(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clEnqueueMarkerWithWaitList(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueMigrateMemObjects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int ret = (int)clEnqueueMigrateMemObjects(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueNDRangeKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueNDRangeKernel(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueNativeKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* a9 = lcheck_toptr(L,9);
  void* a10 = lcheck_toptr(L,10);
  int ret = (int)clEnqueueNativeKernel(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueReadBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueReadBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueReadBufferRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  int a10 = (int)luaL_checkinteger(L,10);
  void* a11 = lcheck_toptr(L,11);
  int a12 = (int)luaL_checkinteger(L,12);
  void* a13 = lcheck_toptr(L,13);
  void* a14 = lcheck_toptr(L,14);
  int ret = (int)clEnqueueReadBufferRect(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueReadImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* a10 = lcheck_toptr(L,10);
  void* a11 = lcheck_toptr(L,11);
  int ret = (int)clEnqueueReadImage(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueReleaseGLObjects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clEnqueueReleaseGLObjects(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueReleaseGrallocObjectsIMG(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clEnqueueReleaseGrallocObjectsIMG(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMFree(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMFree(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMFreeARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMFreeARM(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMap(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMap(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMapARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMapARM(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMemFill(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMemFill(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMemFillARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMemFillARM(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMemcpy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMemcpy(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMemcpyARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMemcpyARM(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMMigrateMem(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clEnqueueSVMMigrateMem(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMUnmap(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clEnqueueSVMUnmap(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueSVMUnmapARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clEnqueueSVMUnmapARM(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueTask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clEnqueueTask(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueUnmapMemObject(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clEnqueueUnmapMemObject(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueWaitForEvents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clEnqueueWaitForEvents(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueWriteBuffer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  int ret = (int)clEnqueueWriteBuffer(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueWriteBufferRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  int a10 = (int)luaL_checkinteger(L,10);
  void* a11 = lcheck_toptr(L,11);
  int a12 = (int)luaL_checkinteger(L,12);
  void* a13 = lcheck_toptr(L,13);
  void* a14 = lcheck_toptr(L,14);
  int ret = (int)clEnqueueWriteBufferRect(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclEnqueueWriteImage(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = lcheck_toptr(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* a10 = lcheck_toptr(L,10);
  void* a11 = lcheck_toptr(L,11);
  int ret = (int)clEnqueueWriteImage(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclFinish(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clFinish(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclFlush(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clFlush(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetCommandQueueInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetCommandQueueInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetContextInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetContextInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetDeviceAndHostTimer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clGetDeviceAndHostTimer(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetDeviceIDs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetDeviceIDs(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetDeviceImageInfoQCOM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clGetDeviceImageInfoQCOM(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetDeviceInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetDeviceInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetEventInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetEventInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetEventProfilingInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetEventProfilingInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetExtensionFunctionAddress(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)clGetExtensionFunctionAddress(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclGetExtensionFunctionAddressForPlatform(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)clGetExtensionFunctionAddressForPlatform(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclGetGLContextInfoKHR(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetGLContextInfoKHR(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetGLObjectInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clGetGLObjectInfo(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetGLTextureInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetGLTextureInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetHostTimer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)clGetHostTimer(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetImageInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetImageInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetKernelArgInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clGetKernelArgInfo(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetKernelInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetKernelInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetKernelSubGroupInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clGetKernelSubGroupInfo(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetKernelSubGroupInfoKHR(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  int ret = (int)clGetKernelSubGroupInfoKHR(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetKernelWorkGroupInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clGetKernelWorkGroupInfo(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetMemObjectInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetMemObjectInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetPipeInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetPipeInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetPlatformIDs(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clGetPlatformIDs(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetPlatformInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetPlatformInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetProgramBuildInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clGetProgramBuildInfo(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetProgramInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetProgramInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetSamplerInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)clGetSamplerInfo(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclGetSupportedImageFormats(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)clGetSupportedImageFormats(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclIcdGetPlatformIDsKHR(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clIcdGetPlatformIDsKHR(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclImportMemoryARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* ret = (void*)clImportMemoryARM(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclLinkProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  void* a8 = lcheck_toptr(L,8);
  void* a9 = lcheck_toptr(L,9);
  void* ret = (void*)clLinkProgram(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclLogMessagesToStderrAPPLE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  clLogMessagesToStderrAPPLE(a1,a2,a3,a4);
  return 0;
};

static int lclLogMessagesToStdoutAPPLE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  clLogMessagesToStdoutAPPLE(a1,a2,a3,a4);
  return 0;
};

static int lclLogMessagesToSystemLogAPPLE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  clLogMessagesToSystemLogAPPLE(a1,a2,a3,a4);
  return 0;
};

static int lclReleaseCommandQueue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseCommandQueue(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseContext(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseContext(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseDevice(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseDevice(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseDeviceEXT(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseDeviceEXT(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseKernel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseMemObject(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseMemObject(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseProgram(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclReleaseSampler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clReleaseSampler(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainCommandQueue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainCommandQueue(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainContext(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainContext(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainDevice(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainDevice(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainDeviceEXT(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainDeviceEXT(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainKernel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainKernel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainMemObject(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainMemObject(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainProgram(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainProgram(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclRetainSampler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clRetainSampler(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSVMAlloc(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)clSVMAlloc(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclSVMAllocARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)clSVMAllocARM(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lclSVMFree(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  clSVMFree(a1,a2);
  return 0;
};

static int lclSVMFreeARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  clSVMFreeARM(a1,a2);
  return 0;
};

static int lclSetDefaultDeviceCommandQueue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clSetDefaultDeviceCommandQueue(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetEventCallback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clSetEventCallback(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetKernelArg(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clSetKernelArg(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetKernelArgSVMPointer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clSetKernelArgSVMPointer(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetKernelArgSVMPointerARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clSetKernelArgSVMPointerARM(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetKernelExecInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clSetKernelExecInfo(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetKernelExecInfoARM(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)clSetKernelExecInfoARM(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetMemObjectDestructorAPPLE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clSetMemObjectDestructorAPPLE(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetMemObjectDestructorCallback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)clSetMemObjectDestructorCallback(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclSetUserEventStatus(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)clSetUserEventStatus(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclTerminateContextKHR(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clTerminateContextKHR(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclUnloadCompiler(lua_State*L){
  int ret = (int)clUnloadCompiler();
  lua_pushinteger(L,ret);
  return 1;
};

static int lclUnloadPlatformCompiler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)clUnloadPlatformCompiler(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclWaitForEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)clWaitForEvents(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lclog(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

static int lclog10(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

static int lclog10f(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

static int lclog10l(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

static int lclogf(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

static int lclogl(lua_State*L){
/* no support argument type*/
  lua_pushstring(L,"no support complex_type\n");
  lua_error(L);
  return 0;
};

int lopencl_openlibs(lua_State*L){
  const char*name = "lopencl";
#ifndef lregister
//1
#define lregister(L,n) lua_register(L,#n,l##n)
//2

/*
  int ref = LUA_REFNIL;
  lua_newtable(L);
  lua_setglobal(L,name);
  ref = luaL_ref(L,LUA_REGISTRYINDEX);
#define lregister(L,n)    \
    lua_pushcfunction(L,l##n);lua_setglobal(L,#n)      \
    lua_rawgeti(L,LUA_REGISTRYINDEX,ref);              \
    lua_pushlightuserdata(L,n);lua_setfield(L,-2,#n);

*/

#endif
  lregister(L,clBuildProgram);
  lregister(L,clCloneKernel);
  lregister(L,clCompileProgram);
  lregister(L,clCreateBuffer);
  lregister(L,clCreateCommandQueue);
  lregister(L,clCreateCommandQueueWithProperties);
  lregister(L,clCreateContext);
  lregister(L,clCreateContextFromType);
  lregister(L,clCreateEventFromGLsyncKHR);
  lregister(L,clCreateFromGLBuffer);
  lregister(L,clCreateFromGLRenderbuffer);
  lregister(L,clCreateFromGLTexture);
  lregister(L,clCreateFromGLTexture2D);
  lregister(L,clCreateFromGLTexture3D);
  lregister(L,clCreateImage);
  lregister(L,clCreateImage2D);
  lregister(L,clCreateImage3D);
  lregister(L,clCreateKernel);
  lregister(L,clCreateKernelsInProgram);
  lregister(L,clCreatePipe);
  lregister(L,clCreateProgramWithBinary);
  lregister(L,clCreateProgramWithBuiltInKernels);
  lregister(L,clCreateProgramWithIL);
  lregister(L,clCreateProgramWithSource);
  lregister(L,clCreateSampler);
  lregister(L,clCreateSamplerWithProperties);
  lregister(L,clCreateSubBuffer);
  lregister(L,clCreateSubDevices);
  lregister(L,clCreateSubDevicesEXT);
  lregister(L,clCreateUserEvent);
  lregister(L,clEnqueueAcquireGLObjects);
  lregister(L,clEnqueueAcquireGrallocObjectsIMG);
  lregister(L,clEnqueueBarrier);
  lregister(L,clEnqueueBarrierWithWaitList);
  lregister(L,clEnqueueCopyBuffer);
  lregister(L,clEnqueueCopyBufferRect);
  lregister(L,clEnqueueCopyBufferToImage);
  lregister(L,clEnqueueCopyImage);
  lregister(L,clEnqueueCopyImageToBuffer);
  lregister(L,clEnqueueFillBuffer);
  lregister(L,clEnqueueFillImage);
  lregister(L,clEnqueueMapBuffer);
  lregister(L,clEnqueueMapImage);
  lregister(L,clEnqueueMarker);
  lregister(L,clEnqueueMarkerWithWaitList);
  lregister(L,clEnqueueMigrateMemObjects);
  lregister(L,clEnqueueNDRangeKernel);
  lregister(L,clEnqueueNativeKernel);
  lregister(L,clEnqueueReadBuffer);
  lregister(L,clEnqueueReadBufferRect);
  lregister(L,clEnqueueReadImage);
  lregister(L,clEnqueueReleaseGLObjects);
  lregister(L,clEnqueueReleaseGrallocObjectsIMG);
  lregister(L,clEnqueueSVMFree);
  lregister(L,clEnqueueSVMFreeARM);
  lregister(L,clEnqueueSVMMap);
  lregister(L,clEnqueueSVMMapARM);
  lregister(L,clEnqueueSVMMemFill);
  lregister(L,clEnqueueSVMMemFillARM);
  lregister(L,clEnqueueSVMMemcpy);
  lregister(L,clEnqueueSVMMemcpyARM);
  lregister(L,clEnqueueSVMMigrateMem);
  lregister(L,clEnqueueSVMUnmap);
  lregister(L,clEnqueueSVMUnmapARM);
  lregister(L,clEnqueueTask);
  lregister(L,clEnqueueUnmapMemObject);
  lregister(L,clEnqueueWaitForEvents);
  lregister(L,clEnqueueWriteBuffer);
  lregister(L,clEnqueueWriteBufferRect);
  lregister(L,clEnqueueWriteImage);
  lregister(L,clFinish);
  lregister(L,clFlush);
  lregister(L,clGetCommandQueueInfo);
  lregister(L,clGetContextInfo);
  lregister(L,clGetDeviceAndHostTimer);
  lregister(L,clGetDeviceIDs);
  lregister(L,clGetDeviceImageInfoQCOM);
  lregister(L,clGetDeviceInfo);
  lregister(L,clGetEventInfo);
  lregister(L,clGetEventProfilingInfo);
  lregister(L,clGetExtensionFunctionAddress);
  lregister(L,clGetExtensionFunctionAddressForPlatform);
  lregister(L,clGetGLContextInfoKHR);
  lregister(L,clGetGLObjectInfo);
  lregister(L,clGetGLTextureInfo);
  lregister(L,clGetHostTimer);
  lregister(L,clGetImageInfo);
  lregister(L,clGetKernelArgInfo);
  lregister(L,clGetKernelInfo);
  lregister(L,clGetKernelSubGroupInfo);
  lregister(L,clGetKernelSubGroupInfoKHR);
  lregister(L,clGetKernelWorkGroupInfo);
  lregister(L,clGetMemObjectInfo);
  lregister(L,clGetPipeInfo);
  lregister(L,clGetPlatformIDs);
  lregister(L,clGetPlatformInfo);
  lregister(L,clGetProgramBuildInfo);
  lregister(L,clGetProgramInfo);
  lregister(L,clGetSamplerInfo);
  lregister(L,clGetSupportedImageFormats);
  lregister(L,clIcdGetPlatformIDsKHR);
  lregister(L,clImportMemoryARM);
  lregister(L,clLinkProgram);
  lregister(L,clLogMessagesToStderrAPPLE);
  lregister(L,clLogMessagesToStdoutAPPLE);
  lregister(L,clLogMessagesToSystemLogAPPLE);
  lregister(L,clReleaseCommandQueue);
  lregister(L,clReleaseContext);
  lregister(L,clReleaseDevice);
  lregister(L,clReleaseDeviceEXT);
  lregister(L,clReleaseEvent);
  lregister(L,clReleaseKernel);
  lregister(L,clReleaseMemObject);
  lregister(L,clReleaseProgram);
  lregister(L,clReleaseSampler);
  lregister(L,clRetainCommandQueue);
  lregister(L,clRetainContext);
  lregister(L,clRetainDevice);
  lregister(L,clRetainDeviceEXT);
  lregister(L,clRetainEvent);
  lregister(L,clRetainKernel);
  lregister(L,clRetainMemObject);
  lregister(L,clRetainProgram);
  lregister(L,clRetainSampler);
  lregister(L,clSVMAlloc);
  lregister(L,clSVMAllocARM);
  lregister(L,clSVMFree);
  lregister(L,clSVMFreeARM);
  lregister(L,clSetDefaultDeviceCommandQueue);
  lregister(L,clSetEventCallback);
  lregister(L,clSetKernelArg);
  lregister(L,clSetKernelArgSVMPointer);
  lregister(L,clSetKernelArgSVMPointerARM);
  lregister(L,clSetKernelExecInfo);
  lregister(L,clSetKernelExecInfoARM);
  lregister(L,clSetMemObjectDestructorAPPLE);
  lregister(L,clSetMemObjectDestructorCallback);
  lregister(L,clSetUserEventStatus);
  lregister(L,clTerminateContextKHR);
  lregister(L,clUnloadCompiler);
  lregister(L,clUnloadPlatformCompiler);
  lregister(L,clWaitForEvents);
  lregister(L,clog);
  lregister(L,clog10);
  lregister(L,clog10f);
  lregister(L,clog10l);
  lregister(L,clogf);
  lregister(L,clogl);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif



  return 0;

};

int luaopen_lopencl(lua_State*L){
  return lopencl_openlibs(L);
};

