require('clibs');

require('lcairo');

print(cairo_image_surface_create);

surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,600,400);

cairo = cairo_create(surface);


cr = cairo;
if(test==nil)then
cairo_set_font_size (cr, 90.0);
cairo_rectangle (cr, 0, 0, 300, 200);
-- cairo_move_to (cr, 10.0, 135.0);
-- cairo_show_text (cr, "Hello");
--cairo_fill (cr);
cairo_stroke (cr);
elseif(test==1)then
M_PI = 3.14
xc = 128.0;
yc = 128.0;
radius = 100.0;
angle1 = 45.0  * (M_PI/180.0);  
angle2 = 180.0 * (M_PI/180.0);  

cairo_set_line_width (cr, 10.0);
cairo_arc (cr, xc, yc, radius, angle1, angle2);
cairo_stroke (cr);

cairo_set_source_rgba (cr, 1, 0.2, 0.2, 0.6);
cairo_set_line_width (cr, 6.0);

cairo_arc (cr, xc, yc, 10.0, 0, 2*M_PI);
cairo_fill (cr);

cairo_arc (cr, xc, yc, radius, angle1, angle1);
cairo_line_to (cr, xc, yc);
cairo_arc (cr, xc, yc, radius, angle2, angle2);
cairo_line_to (cr, xc, yc);
cairo_stroke (cr);
end



cairo_destroy(cairo);
cairo_surface_write_to_png(surface,"coiro.png");
cairo_surface_destroy(surface);


