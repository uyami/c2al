print(require("lFreeImage"));


function loadImage(name)
    local typ = FreeImage_GetFileType(name,0);
    return FreeImage_Load(typ,name,0);
end

function outFname(name)
    local len = #name;
    local list = {};
    for k,v in pairs(_G)do
        if(k:sub(1,len)==name)then
            table.insert(list,k);
        end
    end
    table.sort(list,function(a,b) return a<b; end);
    for k,v in ipairs(list)do
        print(v);
    end
end

local bitmap = loadImage("../ogl/test.png");
print(bitmap);
print(FreeImage_GetWidth(bitmap),FreeImage_GetHeight(bitmap));
FreeImage_Unload(bitmap);

outFname("FreeImage");
