#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>


#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lluaL_addlstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  luaL_addlstring(a1,a2,a3);
  return 0;
};

static int lluaL_addstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  luaL_addstring(a1,a2);
  return 0;
};

static int lluaL_addvalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  luaL_addvalue(a1);
  return 0;
};

static int lluaL_argerror(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_argerror(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_buffinit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  luaL_buffinit(a1,a2);
  return 0;
};

static int lluaL_buffinitsize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)luaL_buffinitsize(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_callmeta(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_callmeta(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_checkany(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  luaL_checkany(a1,a2);
  return 0;
};

static int lluaL_checkinteger(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)luaL_checkinteger(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_checklstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)luaL_checklstring(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_checknumber(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double ret = (double)luaL_checknumber(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lluaL_checkoption(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)luaL_checkoption(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_checkstack(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  luaL_checkstack(a1,a2,a3);
  return 0;
};

static int lluaL_checktype(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  luaL_checktype(a1,a2,a3);
  return 0;
};

static int lluaL_checkudata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)luaL_checkudata(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_checkversion_(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  luaL_checkversion_(a1,a2,a3);
  return 0;
};

static int lluaL_error(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)luaL_error(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_execresult(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)luaL_execresult(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_fileresult(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_fileresult(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_getmetafield(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_getmetafield(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_getsubtable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_getsubtable(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_gsub(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)luaL_gsub(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_len(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)luaL_len(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_loadbufferx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)luaL_loadbufferx(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_loadfilex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)luaL_loadfilex(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_loadstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)luaL_loadstring(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_newmetatable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)luaL_newmetatable(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_newstate(lua_State*L){
  void* ret = (void*)luaL_newstate();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_openlibs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  luaL_openlibs(a1);
  return 0;
};

static int lluaL_optinteger(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)luaL_optinteger(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_optlstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* ret = (void*)luaL_optlstring(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_optnumber(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double ret = (double)luaL_optnumber(a1,a2,a3);
  lua_pushnumber(L,ret);
  return 1;
};

static int lluaL_prepbuffsize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)luaL_prepbuffsize(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_pushresult(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  luaL_pushresult(a1);
  return 0;
};

static int lluaL_pushresultsize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  luaL_pushresultsize(a1,a2);
  return 0;
};

static int lluaL_ref(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)luaL_ref(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaL_requiref(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  luaL_requiref(a1,a2,a3,a4);
  return 0;
};

static int lluaL_setfuncs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  luaL_setfuncs(a1,a2,a3);
  return 0;
};

static int lluaL_setmetatable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  luaL_setmetatable(a1,a2);
  return 0;
};

static int lluaL_testudata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)luaL_testudata(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_tolstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)luaL_tolstring(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lluaL_traceback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  luaL_traceback(a1,a2,a3,a4);
  return 0;
};

static int lluaL_unref(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  luaL_unref(a1,a2,a3);
  return 0;
};

static int lluaL_where(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  luaL_where(a1,a2);
  return 0;
};

static int llua_absindex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_absindex(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_arith(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_arith(a1,a2);
  return 0;
};

static int llua_atpanic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)lua_atpanic(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_callk(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  lua_callk(a1,a2,a3,a4,a5);
  return 0;
};

static int llua_checkstack(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_checkstack(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_close(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  lua_close(a1);
  return 0;
};

static int llua_compare(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)lua_compare(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_concat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_concat(a1,a2);
  return 0;
};

static int llua_copy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_copy(a1,a2,a3);
  return 0;
};

static int llua_createtable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_createtable(a1,a2,a3);
  return 0;
};

static int llua_dump(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)lua_dump(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_error(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_error(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_gc(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)lua_gc(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getallocf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)lua_getallocf(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_getfield(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)lua_getfield(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getglobal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)lua_getglobal(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_gethook(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)lua_gethook(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_gethookcount(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_gethookcount(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_gethookmask(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_gethookmask(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_geti(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)lua_geti(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getinfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)lua_getinfo(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getlocal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_getlocal(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_getmetatable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_getmetatable(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getstack(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)lua_getstack(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_gettable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_gettable(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_gettop(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_gettop(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_getupvalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_getupvalue(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_getuservalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_getuservalue(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_iscfunction(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_iscfunction(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_isinteger(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_isinteger(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_isnumber(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_isnumber(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_isstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_isstring(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_isuserdata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_isuserdata(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_isyieldable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_isyieldable(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_len(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_len(a1,a2);
  return 0;
};

static int llua_load(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)lua_load(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_newstate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)lua_newstate(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_newthread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)lua_newthread(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_newuserdata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_newuserdata(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_next(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_next(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_pcallk(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)lua_pcallk(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_pushboolean(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_pushboolean(a1,a2);
  return 0;
};

static int llua_pushcclosure(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_pushcclosure(a1,a2,a3);
  return 0;
};

static int llua_pushfstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)lua_pushfstring(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_pushinteger(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_pushinteger(a1,a2);
  return 0;
};

static int llua_pushlightuserdata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  lua_pushlightuserdata(a1,a2);
  return 0;
};

static int llua_pushlstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_pushlstring(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_pushnil(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  lua_pushnil(a1);
  return 0;
};

static int llua_pushnumber(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  lua_pushnumber(a1,a2);
  return 0;
};

static int llua_pushstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)lua_pushstring(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_pushthread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_pushthread(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_pushvalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_pushvalue(a1,a2);
  return 0;
};

static int llua_pushvfstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)lua_pushvfstring(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_rawequal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)lua_rawequal(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rawget(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_rawget(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rawgeti(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)lua_rawgeti(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rawgetp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)lua_rawgetp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rawlen(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_rawlen(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rawset(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_rawset(a1,a2);
  return 0;
};

static int llua_rawseti(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_rawseti(a1,a2,a3);
  return 0;
};

static int llua_rawsetp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  lua_rawsetp(a1,a2,a3);
  return 0;
};

static int llua_resume(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)lua_resume(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_rotate(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_rotate(a1,a2,a3);
  return 0;
};

static int llua_setallocf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  lua_setallocf(a1,a2,a3);
  return 0;
};

static int llua_setfield(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  lua_setfield(a1,a2,a3);
  return 0;
};

static int llua_setglobal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  lua_setglobal(a1,a2);
  return 0;
};

static int llua_sethook(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  lua_sethook(a1,a2,a3,a4);
  return 0;
};

static int llua_seti(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_seti(a1,a2,a3);
  return 0;
};

static int llua_setlocal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_setlocal(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_setmetatable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_setmetatable(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_settable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_settable(a1,a2);
  return 0;
};

static int llua_settop(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_settop(a1,a2);
  return 0;
};

static int llua_setupvalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_setupvalue(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_setuservalue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  lua_setuservalue(a1,a2);
  return 0;
};

static int llua_status(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)lua_status(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_stringtonumber(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)lua_stringtonumber(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_toboolean(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_toboolean(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_tocfunction(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_tocfunction(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_tointegerx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)lua_tointegerx(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_tolstring(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)lua_tolstring(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_tonumberx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  double ret = (double)lua_tonumberx(a1,a2,a3);
  lua_pushnumber(L,ret);
  return 1;
};

static int llua_topointer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_topointer(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_tothread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_tothread(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_touserdata(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_touserdata(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_type(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)lua_type(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int llua_typename(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)lua_typename(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_upvalueid(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)lua_upvalueid(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_upvaluejoin(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  lua_upvaluejoin(a1,a2,a3,a4,a5);
  return 0;
};

static int llua_version(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)lua_version(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int llua_xmove(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  lua_xmove(a1,a2,a3);
  return 0;
};

static int llua_yieldk(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)lua_yieldk(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_base(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_base(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_bit32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_bit32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_coroutine(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_coroutine(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_debug(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_debug(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_io(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_io(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_math(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_math(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_os(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_os(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_package(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_package(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_string(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_string(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_table(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_table(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lluaopen_utf8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)luaopen_utf8(a1);
  lua_pushinteger(L,ret);
  return 1;
};

int llua_openlibs(lua_State*L){
  lua_register(L,"luaL_addlstring",lluaL_addlstring);
  lua_register(L,"luaL_addstring",lluaL_addstring);
  lua_register(L,"luaL_addvalue",lluaL_addvalue);
  lua_register(L,"luaL_argerror",lluaL_argerror);
  lua_register(L,"luaL_buffinit",lluaL_buffinit);
  lua_register(L,"luaL_buffinitsize",lluaL_buffinitsize);
  lua_register(L,"luaL_callmeta",lluaL_callmeta);
  lua_register(L,"luaL_checkany",lluaL_checkany);
  lua_register(L,"luaL_checkinteger",lluaL_checkinteger);
  lua_register(L,"luaL_checklstring",lluaL_checklstring);
  lua_register(L,"luaL_checknumber",lluaL_checknumber);
  lua_register(L,"luaL_checkoption",lluaL_checkoption);
  lua_register(L,"luaL_checkstack",lluaL_checkstack);
  lua_register(L,"luaL_checktype",lluaL_checktype);
  lua_register(L,"luaL_checkudata",lluaL_checkudata);
  lua_register(L,"luaL_checkversion_",lluaL_checkversion_);
  lua_register(L,"luaL_error",lluaL_error);
  lua_register(L,"luaL_execresult",lluaL_execresult);
  lua_register(L,"luaL_fileresult",lluaL_fileresult);
  lua_register(L,"luaL_getmetafield",lluaL_getmetafield);
  lua_register(L,"luaL_getsubtable",lluaL_getsubtable);
  lua_register(L,"luaL_gsub",lluaL_gsub);
  lua_register(L,"luaL_len",lluaL_len);
  lua_register(L,"luaL_loadbufferx",lluaL_loadbufferx);
  lua_register(L,"luaL_loadfilex",lluaL_loadfilex);
  lua_register(L,"luaL_loadstring",lluaL_loadstring);
  lua_register(L,"luaL_newmetatable",lluaL_newmetatable);
  lua_register(L,"luaL_newstate",lluaL_newstate);
  lua_register(L,"luaL_openlibs",lluaL_openlibs);
  lua_register(L,"luaL_optinteger",lluaL_optinteger);
  lua_register(L,"luaL_optlstring",lluaL_optlstring);
  lua_register(L,"luaL_optnumber",lluaL_optnumber);
  lua_register(L,"luaL_prepbuffsize",lluaL_prepbuffsize);
  lua_register(L,"luaL_pushresult",lluaL_pushresult);
  lua_register(L,"luaL_pushresultsize",lluaL_pushresultsize);
  lua_register(L,"luaL_ref",lluaL_ref);
  lua_register(L,"luaL_requiref",lluaL_requiref);
  lua_register(L,"luaL_setfuncs",lluaL_setfuncs);
  lua_register(L,"luaL_setmetatable",lluaL_setmetatable);
  lua_register(L,"luaL_testudata",lluaL_testudata);
  lua_register(L,"luaL_tolstring",lluaL_tolstring);
  lua_register(L,"luaL_traceback",lluaL_traceback);
  lua_register(L,"luaL_unref",lluaL_unref);
  lua_register(L,"luaL_where",lluaL_where);
  lua_register(L,"lua_absindex",llua_absindex);
  lua_register(L,"lua_arith",llua_arith);
  lua_register(L,"lua_atpanic",llua_atpanic);
  lua_register(L,"lua_callk",llua_callk);
  lua_register(L,"lua_checkstack",llua_checkstack);
  lua_register(L,"lua_close",llua_close);
  lua_register(L,"lua_compare",llua_compare);
  lua_register(L,"lua_concat",llua_concat);
  lua_register(L,"lua_copy",llua_copy);
  lua_register(L,"lua_createtable",llua_createtable);
  lua_register(L,"lua_dump",llua_dump);
  lua_register(L,"lua_error",llua_error);
  lua_register(L,"lua_gc",llua_gc);
  lua_register(L,"lua_getallocf",llua_getallocf);
  lua_register(L,"lua_getfield",llua_getfield);
  lua_register(L,"lua_getglobal",llua_getglobal);
  lua_register(L,"lua_gethook",llua_gethook);
  lua_register(L,"lua_gethookcount",llua_gethookcount);
  lua_register(L,"lua_gethookmask",llua_gethookmask);
  lua_register(L,"lua_geti",llua_geti);
  lua_register(L,"lua_getinfo",llua_getinfo);
  lua_register(L,"lua_getlocal",llua_getlocal);
  lua_register(L,"lua_getmetatable",llua_getmetatable);
  lua_register(L,"lua_getstack",llua_getstack);
  lua_register(L,"lua_gettable",llua_gettable);
  lua_register(L,"lua_gettop",llua_gettop);
  lua_register(L,"lua_getupvalue",llua_getupvalue);
  lua_register(L,"lua_getuservalue",llua_getuservalue);
  lua_register(L,"lua_iscfunction",llua_iscfunction);
  lua_register(L,"lua_isinteger",llua_isinteger);
  lua_register(L,"lua_isnumber",llua_isnumber);
  lua_register(L,"lua_isstring",llua_isstring);
  lua_register(L,"lua_isuserdata",llua_isuserdata);
  lua_register(L,"lua_isyieldable",llua_isyieldable);
  lua_register(L,"lua_len",llua_len);
  lua_register(L,"lua_load",llua_load);
  lua_register(L,"lua_newstate",llua_newstate);
  lua_register(L,"lua_newthread",llua_newthread);
  lua_register(L,"lua_newuserdata",llua_newuserdata);
  lua_register(L,"lua_next",llua_next);
  lua_register(L,"lua_pcallk",llua_pcallk);
  lua_register(L,"lua_pushboolean",llua_pushboolean);
  lua_register(L,"lua_pushcclosure",llua_pushcclosure);
  lua_register(L,"lua_pushfstring",llua_pushfstring);
  lua_register(L,"lua_pushinteger",llua_pushinteger);
  lua_register(L,"lua_pushlightuserdata",llua_pushlightuserdata);
  lua_register(L,"lua_pushlstring",llua_pushlstring);
  lua_register(L,"lua_pushnil",llua_pushnil);
  lua_register(L,"lua_pushnumber",llua_pushnumber);
  lua_register(L,"lua_pushstring",llua_pushstring);
  lua_register(L,"lua_pushthread",llua_pushthread);
  lua_register(L,"lua_pushvalue",llua_pushvalue);
  lua_register(L,"lua_pushvfstring",llua_pushvfstring);
  lua_register(L,"lua_rawequal",llua_rawequal);
  lua_register(L,"lua_rawget",llua_rawget);
  lua_register(L,"lua_rawgeti",llua_rawgeti);
  lua_register(L,"lua_rawgetp",llua_rawgetp);
  lua_register(L,"lua_rawlen",llua_rawlen);
  lua_register(L,"lua_rawset",llua_rawset);
  lua_register(L,"lua_rawseti",llua_rawseti);
  lua_register(L,"lua_rawsetp",llua_rawsetp);
  lua_register(L,"lua_resume",llua_resume);
  lua_register(L,"lua_rotate",llua_rotate);
  lua_register(L,"lua_setallocf",llua_setallocf);
  lua_register(L,"lua_setfield",llua_setfield);
  lua_register(L,"lua_setglobal",llua_setglobal);
  lua_register(L,"lua_sethook",llua_sethook);
  lua_register(L,"lua_seti",llua_seti);
  lua_register(L,"lua_setlocal",llua_setlocal);
  lua_register(L,"lua_setmetatable",llua_setmetatable);
  lua_register(L,"lua_settable",llua_settable);
  lua_register(L,"lua_settop",llua_settop);
  lua_register(L,"lua_setupvalue",llua_setupvalue);
  lua_register(L,"lua_setuservalue",llua_setuservalue);
  lua_register(L,"lua_status",llua_status);
  lua_register(L,"lua_stringtonumber",llua_stringtonumber);
  lua_register(L,"lua_toboolean",llua_toboolean);
  lua_register(L,"lua_tocfunction",llua_tocfunction);
  lua_register(L,"lua_tointegerx",llua_tointegerx);
  lua_register(L,"lua_tolstring",llua_tolstring);
  lua_register(L,"lua_tonumberx",llua_tonumberx);
  lua_register(L,"lua_topointer",llua_topointer);
  lua_register(L,"lua_tothread",llua_tothread);
  lua_register(L,"lua_touserdata",llua_touserdata);
  lua_register(L,"lua_type",llua_type);
  lua_register(L,"lua_typename",llua_typename);
  lua_register(L,"lua_upvalueid",llua_upvalueid);
  lua_register(L,"lua_upvaluejoin",llua_upvaluejoin);
  lua_register(L,"lua_version",llua_version);
  lua_register(L,"lua_xmove",llua_xmove);
  lua_register(L,"lua_yieldk",llua_yieldk);
  lua_register(L,"luaopen_base",lluaopen_base);
  lua_register(L,"luaopen_bit32",lluaopen_bit32);
  lua_register(L,"luaopen_coroutine",lluaopen_coroutine);
  lua_register(L,"luaopen_debug",lluaopen_debug);
  lua_register(L,"luaopen_io",lluaopen_io);
  lua_register(L,"luaopen_math",lluaopen_math);
  lua_register(L,"luaopen_os",lluaopen_os);
  lua_register(L,"luaopen_package",lluaopen_package);
  lua_register(L,"luaopen_string",lluaopen_string);
  lua_register(L,"luaopen_table",lluaopen_table);
  lua_register(L,"luaopen_utf8",lluaopen_utf8);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif



  return 0;
};

int luaopen_llua(lua_State*L){
  return llua_openlibs(L);
};

