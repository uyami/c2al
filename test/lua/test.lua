package.cpath = package.cpath .. ";../ogl/?.dll"
require('drw');
print(require('llua'));

null = dpointer(0);

function lua_pcall(L,n,r,f)
    return lua_pcallk(L,n,r,f,0,null);
end

function luaL_dostring(L,str)
    luaL_loadstring(L,str)
    return lua_pcall(L,0,-1,0);
end

printA = print;

function printX()
    printA("hello world!",printX);
end

local L = luaL_newstate();
print(L);
luaL_openlibs(L);
luaL_dostring(L,[[
print(20+30);
]]);


local x = string.dump(printX);

print(x);

--lua 1
f = load(x);
f();

--lua 2
lua_pushlstring(L,x,#x);
lua_setglobal(L,"aaa");
if(
luaL_dostring(L,[[
    print(#aaa);
    f = load(aaa);
    f();
]]))then
    print((lua_tolstring(L,-1,null)));
    print(dtostring(lua_tolstring(L,-1,null)));
end

--lua 2.1



--luaL_dostring(L,x);


