#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <SDL2/SDL_image.h>

static int lIMG_Init(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)IMG_Init(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_Linked_Version(lua_State*L){
  void* ret = (void*)IMG_Linked_Version();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_Load(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_Load(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadBMP_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadBMP_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadCUR_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadCUR_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadGIF_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadGIF_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadICO_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadICO_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadJPG_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadJPG_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadLBM_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadLBM_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadPCX_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadPCX_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadPNG_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadPNG_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadPNM_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadPNM_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTGA_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadTGA_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTIF_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadTIF_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadTexture(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTextureTyped_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadTextureTyped_RW(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTexture_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)IMG_LoadTexture_RW(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadTyped_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadTyped_RW(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadWEBP_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadWEBP_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadXCF_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadXCF_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadXPM_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadXPM_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_LoadXV_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_LoadXV_RW(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_Load_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)IMG_Load_RW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_Quit(lua_State*L){
  IMG_Quit();
  return 0;
};

static int lIMG_ReadXPMFromArray(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)IMG_ReadXPMFromArray(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lIMG_SavePNG(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_SavePNG(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_SavePNG_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)IMG_SavePNG_RW(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isBMP(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isBMP(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isCUR(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isCUR(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isGIF(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isGIF(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isICO(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isICO(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isJPG(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isJPG(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isLBM(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isLBM(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isPCX(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isPCX(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isPNG(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isPNG(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isPNM(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isPNM(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isTIF(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isTIF(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isWEBP(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isWEBP(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isXCF(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isXCF(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isXPM(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isXPM(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lIMG_isXV(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)IMG_isXV(a1);
  lua_pushinteger(L,ret);
  return 1;
};

int limg_openlibs(lua_State*L){
  lua_register(L,"IMG_Init",lIMG_Init);
  lua_register(L,"IMG_Linked_Version",lIMG_Linked_Version);
  lua_register(L,"IMG_Load",lIMG_Load);
  lua_register(L,"IMG_LoadBMP_RW",lIMG_LoadBMP_RW);
  lua_register(L,"IMG_LoadCUR_RW",lIMG_LoadCUR_RW);
  lua_register(L,"IMG_LoadGIF_RW",lIMG_LoadGIF_RW);
  lua_register(L,"IMG_LoadICO_RW",lIMG_LoadICO_RW);
  lua_register(L,"IMG_LoadJPG_RW",lIMG_LoadJPG_RW);
  lua_register(L,"IMG_LoadLBM_RW",lIMG_LoadLBM_RW);
  lua_register(L,"IMG_LoadPCX_RW",lIMG_LoadPCX_RW);
  lua_register(L,"IMG_LoadPNG_RW",lIMG_LoadPNG_RW);
  lua_register(L,"IMG_LoadPNM_RW",lIMG_LoadPNM_RW);
  lua_register(L,"IMG_LoadTGA_RW",lIMG_LoadTGA_RW);
  lua_register(L,"IMG_LoadTIF_RW",lIMG_LoadTIF_RW);
  lua_register(L,"IMG_LoadTexture",lIMG_LoadTexture);
  lua_register(L,"IMG_LoadTextureTyped_RW",lIMG_LoadTextureTyped_RW);
  lua_register(L,"IMG_LoadTexture_RW",lIMG_LoadTexture_RW);
  lua_register(L,"IMG_LoadTyped_RW",lIMG_LoadTyped_RW);
  lua_register(L,"IMG_LoadWEBP_RW",lIMG_LoadWEBP_RW);
  lua_register(L,"IMG_LoadXCF_RW",lIMG_LoadXCF_RW);
  lua_register(L,"IMG_LoadXPM_RW",lIMG_LoadXPM_RW);
  lua_register(L,"IMG_LoadXV_RW",lIMG_LoadXV_RW);
  lua_register(L,"IMG_Load_RW",lIMG_Load_RW);
  lua_register(L,"IMG_Quit",lIMG_Quit);
  lua_register(L,"IMG_ReadXPMFromArray",lIMG_ReadXPMFromArray);
  lua_register(L,"IMG_SavePNG",lIMG_SavePNG);
  lua_register(L,"IMG_SavePNG_RW",lIMG_SavePNG_RW);
  lua_register(L,"IMG_isBMP",lIMG_isBMP);
  lua_register(L,"IMG_isCUR",lIMG_isCUR);
  lua_register(L,"IMG_isGIF",lIMG_isGIF);
  lua_register(L,"IMG_isICO",lIMG_isICO);
  lua_register(L,"IMG_isJPG",lIMG_isJPG);
  lua_register(L,"IMG_isLBM",lIMG_isLBM);
  lua_register(L,"IMG_isPCX",lIMG_isPCX);
  lua_register(L,"IMG_isPNG",lIMG_isPNG);
  lua_register(L,"IMG_isPNM",lIMG_isPNM);
  lua_register(L,"IMG_isTIF",lIMG_isTIF);
  lua_register(L,"IMG_isWEBP",lIMG_isWEBP);
  lua_register(L,"IMG_isXCF",lIMG_isXCF);
  lua_register(L,"IMG_isXPM",lIMG_isXPM);
  lua_register(L,"IMG_isXV",lIMG_isXV);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"SDL_PACKEDLAYOUT_NONE",0);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"IMG_INIT_JPG",1);
  lua_registerInt(L,"IMG_INIT_PNG",2);
  lua_registerInt(L,"IMG_INIT_TIF",4);
  lua_registerInt(L,"IMG_INIT_WEBP",8);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"_MM_HINT_ET0",7);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_PACKEDORDER_NONE",0);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_ARRAYORDER_NONE",0);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"IMG_INIT_JPG",1);
  lua_registerInt(L,"IMG_INIT_PNG",2);
  lua_registerInt(L,"IMG_INIT_TIF",4);
  lua_registerInt(L,"IMG_INIT_WEBP",8);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"SDL_ADDEVENT",0);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);
  lua_registerInt(L,"SDL_BITMAPORDER_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDLK_UNKNOWN",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"SDL_PIXELFORMAT_UNKNOWN",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"SDL_LOG_CATEGORY_APPLICATION",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_PIXELTYPE_UNKNOWN",0);
  lua_registerInt(L,"SDL_ADDEVENT",0);




//struct 
//no structs
  return 0;
};

int luaopen_lSDL_image(lua_State*L){
  return limg_openlibs(L);
};

