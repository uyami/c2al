#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include <SDL2/SDL_mixer.h>


#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lMix_AllocateChannels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_AllocateChannels(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_ChannelFinished(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  Mix_ChannelFinished(a1);
  return 0;
};

static int lMix_CloseAudio(lua_State*L){
  Mix_CloseAudio();
  return 0;
};

static int lMix_EachSoundFont(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)Mix_EachSoundFont(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_ExpireChannel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_ExpireChannel(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeInChannelTimed(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)Mix_FadeInChannelTimed(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeInMusic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)Mix_FadeInMusic(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeInMusicPos(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  double a4 = luaL_checknumber(L,4);
  int ret = (int)Mix_FadeInMusicPos(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeOutChannel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_FadeOutChannel(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeOutGroup(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_FadeOutGroup(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadeOutMusic(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_FadeOutMusic(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadingChannel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_FadingChannel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FadingMusic(lua_State*L){
  int ret = (int)Mix_FadingMusic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_FreeChunk(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  Mix_FreeChunk(a1);
  return 0;
};

static int lMix_FreeMusic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  Mix_FreeMusic(a1);
  return 0;
};

static int lMix_GetChunk(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)Mix_GetChunk(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_GetChunkDecoder(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)Mix_GetChunkDecoder(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_GetMusicDecoder(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)Mix_GetMusicDecoder(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_GetMusicHookData(lua_State*L){
  void* ret = (void*)Mix_GetMusicHookData();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_GetMusicType(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)Mix_GetMusicType(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GetNumChunkDecoders(lua_State*L){
  int ret = (int)Mix_GetNumChunkDecoders();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GetNumMusicDecoders(lua_State*L){
  int ret = (int)Mix_GetNumMusicDecoders();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GetSoundFonts(lua_State*L){
  void* ret = (void*)Mix_GetSoundFonts();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_GetSynchroValue(lua_State*L){
  int ret = (int)Mix_GetSynchroValue();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupAvailable(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_GroupAvailable(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupChannel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_GroupChannel(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupChannels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)Mix_GroupChannels(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupCount(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_GroupCount(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupNewer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_GroupNewer(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_GroupOldest(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_GroupOldest(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_HaltChannel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_HaltChannel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_HaltGroup(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_HaltGroup(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_HaltMusic(lua_State*L){
  int ret = (int)Mix_HaltMusic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_HookMusic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  Mix_HookMusic(a1,a2);
  return 0;
};

static int lMix_HookMusicFinished(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  Mix_HookMusicFinished(a1);
  return 0;
};

static int lMix_Init(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_Init(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_Linked_Version(lua_State*L){
  void* ret = (void*)Mix_Linked_Version();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_LoadMUS(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)Mix_LoadMUS(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_LoadMUSType_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)Mix_LoadMUSType_RW(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_LoadMUS_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)Mix_LoadMUS_RW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_LoadWAV_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)Mix_LoadWAV_RW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_OpenAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)Mix_OpenAudio(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_Pause(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  Mix_Pause(a1);
  return 0;
};

static int lMix_PauseMusic(lua_State*L){
  Mix_PauseMusic();
  return 0;
};

static int lMix_Paused(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_Paused(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_PausedMusic(lua_State*L){
  int ret = (int)Mix_PausedMusic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_PlayChannelTimed(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)Mix_PlayChannelTimed(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_PlayMusic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_PlayMusic(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_Playing(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_Playing(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_PlayingMusic(lua_State*L){
  int ret = (int)Mix_PlayingMusic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_QuerySpec(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)Mix_QuerySpec(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_QuickLoad_RAW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)Mix_QuickLoad_RAW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_QuickLoad_WAV(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)Mix_QuickLoad_WAV(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lMix_Quit(lua_State*L){
  Mix_Quit();
  return 0;
};

static int lMix_RegisterEffect(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)Mix_RegisterEffect(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_ReserveChannels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_ReserveChannels(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_Resume(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  Mix_Resume(a1);
  return 0;
};

static int lMix_ResumeMusic(lua_State*L){
  Mix_ResumeMusic();
  return 0;
};

static int lMix_RewindMusic(lua_State*L){
  Mix_RewindMusic();
  return 0;
};

static int lMix_SetDistance(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_SetDistance(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetMusicCMD(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)Mix_SetMusicCMD(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetMusicPosition(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  int ret = (int)Mix_SetMusicPosition(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetPanning(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)Mix_SetPanning(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetPosition(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)Mix_SetPosition(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetPostMix(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  Mix_SetPostMix(a1,a2);
  return 0;
};

static int lMix_SetReverseStereo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_SetReverseStereo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetSoundFonts(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)Mix_SetSoundFonts(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_SetSynchroValue(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_SetSynchroValue(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_UnregisterAllEffects(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_UnregisterAllEffects(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_UnregisterEffect(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)Mix_UnregisterEffect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_Volume(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_Volume(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_VolumeChunk(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)Mix_VolumeChunk(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lMix_VolumeMusic(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)Mix_VolumeMusic(a1);
  lua_pushinteger(L,ret);
  return 1;
};

int lSDL_mixer_openlibs(lua_State*L){
  lua_register(L,"Mix_AllocateChannels",lMix_AllocateChannels);
  lua_register(L,"Mix_ChannelFinished",lMix_ChannelFinished);
  lua_register(L,"Mix_CloseAudio",lMix_CloseAudio);
  lua_register(L,"Mix_EachSoundFont",lMix_EachSoundFont);
  lua_register(L,"Mix_ExpireChannel",lMix_ExpireChannel);
  lua_register(L,"Mix_FadeInChannelTimed",lMix_FadeInChannelTimed);
  lua_register(L,"Mix_FadeInMusic",lMix_FadeInMusic);
  lua_register(L,"Mix_FadeInMusicPos",lMix_FadeInMusicPos);
  lua_register(L,"Mix_FadeOutChannel",lMix_FadeOutChannel);
  lua_register(L,"Mix_FadeOutGroup",lMix_FadeOutGroup);
  lua_register(L,"Mix_FadeOutMusic",lMix_FadeOutMusic);
  lua_register(L,"Mix_FadingChannel",lMix_FadingChannel);
  lua_register(L,"Mix_FadingMusic",lMix_FadingMusic);
  lua_register(L,"Mix_FreeChunk",lMix_FreeChunk);
  lua_register(L,"Mix_FreeMusic",lMix_FreeMusic);
  lua_register(L,"Mix_GetChunk",lMix_GetChunk);
  lua_register(L,"Mix_GetChunkDecoder",lMix_GetChunkDecoder);
  lua_register(L,"Mix_GetMusicDecoder",lMix_GetMusicDecoder);
  lua_register(L,"Mix_GetMusicHookData",lMix_GetMusicHookData);
  lua_register(L,"Mix_GetMusicType",lMix_GetMusicType);
  lua_register(L,"Mix_GetNumChunkDecoders",lMix_GetNumChunkDecoders);
  lua_register(L,"Mix_GetNumMusicDecoders",lMix_GetNumMusicDecoders);
  lua_register(L,"Mix_GetSoundFonts",lMix_GetSoundFonts);
  lua_register(L,"Mix_GetSynchroValue",lMix_GetSynchroValue);
  lua_register(L,"Mix_GroupAvailable",lMix_GroupAvailable);
  lua_register(L,"Mix_GroupChannel",lMix_GroupChannel);
  lua_register(L,"Mix_GroupChannels",lMix_GroupChannels);
  lua_register(L,"Mix_GroupCount",lMix_GroupCount);
  lua_register(L,"Mix_GroupNewer",lMix_GroupNewer);
  lua_register(L,"Mix_GroupOldest",lMix_GroupOldest);
  lua_register(L,"Mix_HaltChannel",lMix_HaltChannel);
  lua_register(L,"Mix_HaltGroup",lMix_HaltGroup);
  lua_register(L,"Mix_HaltMusic",lMix_HaltMusic);
  lua_register(L,"Mix_HookMusic",lMix_HookMusic);
  lua_register(L,"Mix_HookMusicFinished",lMix_HookMusicFinished);
  lua_register(L,"Mix_Init",lMix_Init);
  lua_register(L,"Mix_Linked_Version",lMix_Linked_Version);
  lua_register(L,"Mix_LoadMUS",lMix_LoadMUS);
  lua_register(L,"Mix_LoadMUSType_RW",lMix_LoadMUSType_RW);
  lua_register(L,"Mix_LoadMUS_RW",lMix_LoadMUS_RW);
  lua_register(L,"Mix_LoadWAV_RW",lMix_LoadWAV_RW);
  lua_register(L,"Mix_OpenAudio",lMix_OpenAudio);
  lua_register(L,"Mix_Pause",lMix_Pause);
  lua_register(L,"Mix_PauseMusic",lMix_PauseMusic);
  lua_register(L,"Mix_Paused",lMix_Paused);
  lua_register(L,"Mix_PausedMusic",lMix_PausedMusic);
  lua_register(L,"Mix_PlayChannelTimed",lMix_PlayChannelTimed);
  lua_register(L,"Mix_PlayMusic",lMix_PlayMusic);
  lua_register(L,"Mix_Playing",lMix_Playing);
  lua_register(L,"Mix_PlayingMusic",lMix_PlayingMusic);
  lua_register(L,"Mix_QuerySpec",lMix_QuerySpec);
  lua_register(L,"Mix_QuickLoad_RAW",lMix_QuickLoad_RAW);
  lua_register(L,"Mix_QuickLoad_WAV",lMix_QuickLoad_WAV);
  lua_register(L,"Mix_Quit",lMix_Quit);
  lua_register(L,"Mix_RegisterEffect",lMix_RegisterEffect);
  lua_register(L,"Mix_ReserveChannels",lMix_ReserveChannels);
  lua_register(L,"Mix_Resume",lMix_Resume);
  lua_register(L,"Mix_ResumeMusic",lMix_ResumeMusic);
  lua_register(L,"Mix_RewindMusic",lMix_RewindMusic);
  lua_register(L,"Mix_SetDistance",lMix_SetDistance);
  lua_register(L,"Mix_SetMusicCMD",lMix_SetMusicCMD);
  lua_register(L,"Mix_SetMusicPosition",lMix_SetMusicPosition);
  lua_register(L,"Mix_SetPanning",lMix_SetPanning);
  lua_register(L,"Mix_SetPosition",lMix_SetPosition);
  lua_register(L,"Mix_SetPostMix",lMix_SetPostMix);
  lua_register(L,"Mix_SetReverseStereo",lMix_SetReverseStereo);
  lua_register(L,"Mix_SetSoundFonts",lMix_SetSoundFonts);
  lua_register(L,"Mix_SetSynchroValue",lMix_SetSynchroValue);
  lua_register(L,"Mix_UnregisterAllEffects",lMix_UnregisterAllEffects);
  lua_register(L,"Mix_UnregisterEffect",lMix_UnregisterEffect);
  lua_register(L,"Mix_Volume",lMix_Volume);
  lua_register(L,"Mix_VolumeChunk",lMix_VolumeChunk);
  lua_register(L,"Mix_VolumeMusic",lMix_VolumeMusic);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"MIX_NO_FADING",0);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"MIX_NO_FADING",0);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"MUS_NONE",0);
  lua_registerInt(L,"MUS_NONE",0);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"MIX_INIT_FLAC",1);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"MIX_INIT_FLAC",1);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);




//struct 
  lua_getglobal(L,"structs");
  if(lua_isnil(L,-1)){
    lua_newtable(L);
    lua_setglobal(L,"structs");
    lua_getglobal(L,"structs");
  };
  
#ifndef lua_StructBegin
#define lua_setField(name,bpos,bsiz,type,typeName)  \
    lua_createtable(L,4,1);\
    lua_pushinteger(L,bpos);lua_setfield(L,-2,"bpos");\
    lua_pushinteger(L,bsiz);lua_setfield(L,-2,"bsiz");\
    lua_pushstring(L,type);lua_setfield(L,-2,"type");\
    lua_pushstring(L,typeName);lua_setfield(L,-2,"tname");\
    lua_setfield(L,-2,name);

#ifndef lua_StructSize
#define lua_StructSize(siz)   lua_pushinteger(L,siz); lua_setfield(L,-2,"1size");
#endif
#define lua_StructBegin(len)  lua_createtable(L,len+1,4);
#define lua_StructEnd(name)   lua_setfield(L,-2,name);
#endif
  
  lua_StructBegin(5);
    lua_setField("allocated",0,32,"integer_type","int");
    lua_setField("abuf",32,32,"pointer_type","unsigned char");
    lua_setField("alen",64,32,"integer_type","unsigned int");
    lua_setField("volume",96,8,"integer_type","unsigned char");
  lua_StructSize(128);

  lua_StructEnd("Mix_Chunk");
  
  return 0;
};

int luaopen_lSDL_mixer(lua_State*L){
  return lSDL_mixer_openlibs(L);
};

