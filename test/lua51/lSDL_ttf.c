#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include <SDL2/SDL_ttf.h>

#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lTTF_ByteSwappedUNICODE(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  TTF_ByteSwappedUNICODE(a1);
  return 0;
};

static int lTTF_CloseFont(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  TTF_CloseFont(a1);
  return 0;
};

static int lTTF_FontAscent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontAscent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_FontDescent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontDescent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_FontFaceFamilyName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)TTF_FontFaceFamilyName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_FontFaceIsFixedWidth(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontFaceIsFixedWidth(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_FontFaceStyleName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)TTF_FontFaceStyleName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_FontFaces(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontFaces(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_FontHeight(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontHeight(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_FontLineSkip(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_FontLineSkip(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontHinting(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_GetFontHinting(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontKerning(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_GetFontKerning(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontKerningSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = 0;//(int)TTF_GetFontKerningSize(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontKerningSizeGlyphs(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)TTF_GetFontKerningSizeGlyphs(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontOutline(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_GetFontOutline(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GetFontStyle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)TTF_GetFontStyle(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GlyphIsProvided(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)TTF_GlyphIsProvided(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_GlyphMetrics(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  void* a7 = lcheck_toptr(L,7);
  int ret = (int)TTF_GlyphMetrics(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_Init(lua_State*L){
  int ret = (int)TTF_Init();
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_Linked_Version(lua_State*L){
  void* ret = (void*)TTF_Linked_Version();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_OpenFont(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)TTF_OpenFont(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_OpenFontIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)TTF_OpenFontIndex(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_OpenFontIndexRW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)TTF_OpenFontIndexRW(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_OpenFontRW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)TTF_OpenFontRW(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_Quit(lua_State*L){
  TTF_Quit();
  return 0;
};

static int lTTF_RenderGlyph_Blended(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  //printf("unsupported type");  assert(0);  
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderGlyph_Blended(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderGlyph_Shaded(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  //printf("unsupported type");  assert(0);  printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderGlyph_Shaded(a1,a2,*(SDL_Color*)&a3,*(SDL_Color*)&a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderGlyph_Solid(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderGlyph_Solid(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderText_Blended(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderText_Blended(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderText_Blended_Wrapped(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  int a4 = (int)luaL_checkinteger(L,4);
  //void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderText_Blended_Wrapped(a1,a2,*(SDL_Color*)&a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderText_Shaded(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderText_Shaded(a1,a2,*(SDL_Color*)&a3,*(SDL_Color*)&a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderText_Solid(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderText_Solid(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUNICODE_Blended(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  //int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderUNICODE_Blended(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUNICODE_Blended_Wrapped(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  int a4 = (int)luaL_checkinteger(L,4);
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderUNICODE_Blended_Wrapped(a1,a2,*(SDL_Color*)&a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUNICODE_Shaded(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderUNICODE_Shaded(a1,a2,*(SDL_Color*)&a3,*(SDL_Color*)&a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUNICODE_Solid(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderUNICODE_Solid(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUTF8_Blended(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderUTF8_Blended(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUTF8_Blended_Wrapped(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  
  int a3 = luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = TTF_RenderUTF8_Blended_Wrapped(a1,a2,*(SDL_Color*)&a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUTF8_Shaded(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  printf("unsupported type");  assert(0); 
  int a3 = luaL_checkinteger(L,3);
  int a4 = luaL_checkinteger(L,4); 
  void* ret = TTF_RenderUTF8_Shaded(a1,a2,*(SDL_Color*)&a3,*(SDL_Color*)&a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_RenderUTF8_Solid(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  //printf("unsupported type");  assert(0);  void* ret = 0;
  int a3 = luaL_checkinteger(L,3);
  void* ret = TTF_RenderUTF8_Solid(a1,a2,*(SDL_Color*)&a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lTTF_SetFontHinting(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  TTF_SetFontHinting(a1,a2);
  return 0;
};

static int lTTF_SetFontKerning(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  TTF_SetFontKerning(a1,a2);
  return 0;
};

static int lTTF_SetFontOutline(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  TTF_SetFontOutline(a1,a2);
  return 0;
};

static int lTTF_SetFontStyle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  TTF_SetFontStyle(a1,a2);
  return 0;
};

static int lTTF_SizeText(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)TTF_SizeText(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_SizeUNICODE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)TTF_SizeUNICODE(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_SizeUTF8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)TTF_SizeUTF8(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lTTF_WasInit(lua_State*L){
  int ret = (int)TTF_WasInit();
  lua_pushinteger(L,ret);
  return 1;
};

int lSDL_ttf_openlibs(lua_State*L){
  lua_register(L,"TTF_ByteSwappedUNICODE",lTTF_ByteSwappedUNICODE);
  lua_register(L,"TTF_CloseFont",lTTF_CloseFont);
  lua_register(L,"TTF_FontAscent",lTTF_FontAscent);
  lua_register(L,"TTF_FontDescent",lTTF_FontDescent);
  lua_register(L,"TTF_FontFaceFamilyName",lTTF_FontFaceFamilyName);
  lua_register(L,"TTF_FontFaceIsFixedWidth",lTTF_FontFaceIsFixedWidth);
  lua_register(L,"TTF_FontFaceStyleName",lTTF_FontFaceStyleName);
  lua_register(L,"TTF_FontFaces",lTTF_FontFaces);
  lua_register(L,"TTF_FontHeight",lTTF_FontHeight);
  lua_register(L,"TTF_FontLineSkip",lTTF_FontLineSkip);
  lua_register(L,"TTF_GetFontHinting",lTTF_GetFontHinting);
  lua_register(L,"TTF_GetFontKerning",lTTF_GetFontKerning);
  lua_register(L,"TTF_GetFontKerningSize",lTTF_GetFontKerningSize);
  lua_register(L,"TTF_GetFontKerningSizeGlyphs",lTTF_GetFontKerningSizeGlyphs);
  lua_register(L,"TTF_GetFontOutline",lTTF_GetFontOutline);
  lua_register(L,"TTF_GetFontStyle",lTTF_GetFontStyle);
  lua_register(L,"TTF_GlyphIsProvided",lTTF_GlyphIsProvided);
  lua_register(L,"TTF_GlyphMetrics",lTTF_GlyphMetrics);
  lua_register(L,"TTF_Init",lTTF_Init);
  lua_register(L,"TTF_Linked_Version",lTTF_Linked_Version);
  lua_register(L,"TTF_OpenFont",lTTF_OpenFont);
  lua_register(L,"TTF_OpenFontIndex",lTTF_OpenFontIndex);
  lua_register(L,"TTF_OpenFontIndexRW",lTTF_OpenFontIndexRW);
  lua_register(L,"TTF_OpenFontRW",lTTF_OpenFontRW);
  lua_register(L,"TTF_Quit",lTTF_Quit);
  lua_register(L,"TTF_RenderGlyph_Blended",lTTF_RenderGlyph_Blended);
  lua_register(L,"TTF_RenderGlyph_Shaded",lTTF_RenderGlyph_Shaded);
  lua_register(L,"TTF_RenderGlyph_Solid",lTTF_RenderGlyph_Solid);
  lua_register(L,"TTF_RenderText_Blended",lTTF_RenderText_Blended);
  lua_register(L,"TTF_RenderText_Blended_Wrapped",lTTF_RenderText_Blended_Wrapped);
  lua_register(L,"TTF_RenderText_Shaded",lTTF_RenderText_Shaded);
  lua_register(L,"TTF_RenderText_Solid",lTTF_RenderText_Solid);
  lua_register(L,"TTF_RenderUNICODE_Blended",lTTF_RenderUNICODE_Blended);
  lua_register(L,"TTF_RenderUNICODE_Blended_Wrapped",lTTF_RenderUNICODE_Blended_Wrapped);
  lua_register(L,"TTF_RenderUNICODE_Shaded",lTTF_RenderUNICODE_Shaded);
  lua_register(L,"TTF_RenderUNICODE_Solid",lTTF_RenderUNICODE_Solid);
  lua_register(L,"TTF_RenderUTF8_Blended",lTTF_RenderUTF8_Blended);
  lua_register(L,"TTF_RenderUTF8_Blended_Wrapped",lTTF_RenderUTF8_Blended_Wrapped);
  lua_register(L,"TTF_RenderUTF8_Shaded",lTTF_RenderUTF8_Shaded);
  lua_register(L,"TTF_RenderUTF8_Solid",lTTF_RenderUTF8_Solid);
  lua_register(L,"TTF_SetFontHinting",lTTF_SetFontHinting);
  lua_register(L,"TTF_SetFontKerning",lTTF_SetFontKerning);
  lua_register(L,"TTF_SetFontOutline",lTTF_SetFontOutline);
  lua_register(L,"TTF_SetFontStyle",lTTF_SetFontStyle);
  lua_register(L,"TTF_SizeText",lTTF_SizeText);
  lua_register(L,"TTF_SizeUNICODE",lTTF_SizeUNICODE);
  lua_register(L,"TTF_SizeUTF8",lTTF_SizeUTF8);
  lua_register(L,"TTF_WasInit",lTTF_WasInit);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"SDL_ADDEVENT",0);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_ADDEVENT",0);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"SDL_PACKEDORDER_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_PIXELTYPE_UNKNOWN",0);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"SDL_ARRAYORDER_NONE",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"SDL_PIXELFORMAT_UNKNOWN",0);
  lua_registerInt(L,"SDLK_UNKNOWN",0);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_LOG_CATEGORY_APPLICATION",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"_MM_HINT_ET0",7);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"SDL_BITMAPORDER_NONE",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);




//struct 
//no structs
  return 0;
};

int luaopen_lSDL_ttf(lua_State*L){
  return lSDL_ttf_openlibs(L);
};

