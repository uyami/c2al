#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <SDL2/SDL.h>
#include <assert.h>

#define lua_isstring(a,idx) (lua_type(a,idx)==LUA_TSTRING)
#ifndef lcheck_toptr
#define lcheck_toptr _lcheck_toptr
static void* _lcheck_toptr(lua_State*L,int idx){
  void* data = 0;
  if(lua_isstring(L,idx)){
    data = (void*)lua_tostring(L,idx);
  }
  else if(lua_islightuserdata(L,idx)/*||lua_isuserdata(L,idx)*/){
    data = (void*)lua_touserdata(L,idx);
  }
  else if(lua_isuserdata(L,idx)){
    data = *(void**)lua_touserdata(L,idx);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  };
  return data;
};
#endif

static int lSDL_AddEventWatch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_AddEventWatch(a1,a2);
  return 0;
};

static int lSDL_AddHintCallback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_AddHintCallback(a1,a2,a3);
  return 0;
};

static int lSDL_AddTimer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_AddTimer(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AllocFormat(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_AllocFormat(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AllocPalette(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_AllocPalette(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AllocRW(lua_State*L){
  void* ret = (void*)SDL_AllocRW();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicAdd(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_AtomicAdd(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicCAS(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_AtomicCAS(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicCASPtr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_AtomicCASPtr(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicGet(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_AtomicGet(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicGetPtr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_AtomicGetPtr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicLock(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_AtomicLock(a1);
  return 0;
};

static int lSDL_AtomicSet(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_AtomicSet(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicSetPtr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_AtomicSetPtr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicTryLock(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_AtomicTryLock(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicUnlock(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_AtomicUnlock(a1);
  return 0;
};

static int lSDL_AudioInit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_AudioInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AudioQuit(lua_State*L){
  SDL_AudioQuit();
  return 0;
};

static int lSDL_BuildAudioCVT(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int ret = (int)SDL_BuildAudioCVT(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CalculateGammaRamp(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_CalculateGammaRamp(a1,a2);
  return 0;
};

static int lSDL_CaptureMouse(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_CaptureMouse(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ClearError(lua_State*L){
  SDL_ClearError();
  return 0;
};

static int lSDL_ClearHints(lua_State*L){
  SDL_ClearHints();
  return 0;
};

static int lSDL_ClearQueuedAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_ClearQueuedAudio(a1);
  return 0;
};

static int lSDL_CloseAudio(lua_State*L){
  SDL_CloseAudio();
  return 0;
};

static int lSDL_CloseAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_CloseAudioDevice(a1);
  return 0;
};

static int lSDL_CondBroadcast(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_CondBroadcast(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondSignal(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_CondSignal(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondWait(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_CondWait(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondWaitTimeout(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_CondWaitTimeout(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertAudio(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ConvertAudio(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int ret = (int)SDL_ConvertPixels(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ConvertSurface(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ConvertSurfaceFormat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ConvertSurfaceFormat(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateColorCursor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_CreateColorCursor(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateCond(lua_State*L){
  void* ret = (void*)SDL_CreateCond();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateCursor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateCursor(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateMutex(lua_State*L){
  void* ret = (void*)SDL_CreateMutex();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurface(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* ret = (void*)SDL_CreateRGBSurface(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceFrom(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* ret = (void*)SDL_CreateRGBSurfaceFrom(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceWithFormat(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)SDL_CreateRGBSurfaceWithFormat(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceWithFormatFrom(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateRGBSurfaceWithFormatFrom(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRenderer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_CreateRenderer(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSemaphore(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_CreateSemaphore(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSoftwareRenderer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_CreateSoftwareRenderer(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSystemCursor(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_CreateSystemCursor(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)SDL_CreateTexture(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateTextureFromSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_CreateTextureFromSurface(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateThread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)SDL_CreateThread(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateWindow(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateWindowAndRenderer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_CreateWindowAndRenderer(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CreateWindowFrom(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_CreateWindowFrom(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_DXGIGetOutputInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_DXGIGetOutputInfo(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DelEventWatch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_DelEventWatch(a1,a2);
  return 0;
};

static int lSDL_DelHintCallback(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_DelHintCallback(a1,a2,a3);
  return 0;
};

static int lSDL_Delay(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_Delay(a1);
  return 0;
};

static int lSDL_DequeueAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_DequeueAudio(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DestroyCond(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroyCond(a1);
  return 0;
};

static int lSDL_DestroyMutex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroyMutex(a1);
  return 0;
};

static int lSDL_DestroyRenderer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroyRenderer(a1);
  return 0;
};

static int lSDL_DestroySemaphore(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroySemaphore(a1);
  return 0;
};

static int lSDL_DestroyTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroyTexture(a1);
  return 0;
};

static int lSDL_DestroyWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DestroyWindow(a1);
  return 0;
};

static int lSDL_DetachThread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_DetachThread(a1);
  return 0;
};

static int lSDL_Direct3D9GetAdapterIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Direct3D9GetAdapterIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DisableScreenSaver(lua_State*L){
  SDL_DisableScreenSaver();
  return 0;
};

static int lSDL_EnableScreenSaver(lua_State*L){
  SDL_EnableScreenSaver();
  return 0;
};

static int lSDL_EnclosePoints(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_EnclosePoints(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Error(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Error(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_EventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_EventState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FillRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_FillRect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FillRects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_FillRects(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FilterEvents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_FilterEvents(a1,a2);
  return 0;
};

static int lSDL_FlushEvent(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_FlushEvent(a1);
  return 0;
};

static int lSDL_FlushEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_FlushEvents(a1,a2);
  return 0;
};

static int lSDL_FreeCursor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreeCursor(a1);
  return 0;
};

static int lSDL_FreeFormat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreeFormat(a1);
  return 0;
};

static int lSDL_FreePalette(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreePalette(a1);
  return 0;
};

static int lSDL_FreeRW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreeRW(a1);
  return 0;
};

static int lSDL_FreeSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreeSurface(a1);
  return 0;
};

static int lSDL_FreeWAV(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_FreeWAV(a1);
  return 0;
};

static int lSDL_GL_BindTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_GL_BindTexture(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_CreateContext(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GL_CreateContext(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_DeleteContext(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_GL_DeleteContext(a1);
  return 0;
};

static int lSDL_GL_ExtensionSupported(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GL_ExtensionSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_GetAttribute(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GL_GetAttribute(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_GetCurrentContext(lua_State*L){
  void* ret = (void*)SDL_GL_GetCurrentContext();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetCurrentWindow(lua_State*L){
  void* ret = (void*)SDL_GL_GetCurrentWindow();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetDrawableSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_GL_GetDrawableSize(a1,a2,a3);
  return 0;
};

static int lSDL_GL_GetProcAddress(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GL_GetProcAddress(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetSwapInterval(lua_State*L){
  int ret = (int)SDL_GL_GetSwapInterval();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_LoadLibrary(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GL_LoadLibrary(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_MakeCurrent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GL_MakeCurrent(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_ResetAttributes(lua_State*L){
  SDL_GL_ResetAttributes();
  return 0;
};

static int lSDL_GL_SetAttribute(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GL_SetAttribute(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_SetSwapInterval(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GL_SetSwapInterval(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_SwapWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_GL_SwapWindow(a1);
  return 0;
};

static int lSDL_GL_UnbindTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GL_UnbindTexture(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_UnloadLibrary(lua_State*L){
  SDL_GL_UnloadLibrary();
  return 0;
};

static int lSDL_GameControllerAddMapping(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GameControllerAddMapping(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerAddMappingsFromRW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerAddMappingsFromRW(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerClose(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_GameControllerClose(a1);
  return 0;
};

static int lSDL_GameControllerEventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GameControllerEventState(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerFromInstanceID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerFromInstanceID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAttached(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GameControllerGetAttached(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAxis(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerGetAxis(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAxisFromString(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GameControllerGetAxisFromString(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetBindForAxis(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_GameControllerGetBindForAxis(a1,a2);
  printf("unsupported type");  assert(0);};

static int lSDL_GameControllerGetBindForButton(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_GameControllerGetBindForButton(a1,a2);
  printf("unsupported type");  assert(0);};

static int lSDL_GameControllerGetButton(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerGetButton(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetButtonFromString(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GameControllerGetButtonFromString(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetJoystick(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GameControllerGetJoystick(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetStringForAxis(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerGetStringForAxis(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetStringForButton(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerGetStringForButton(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerMapping(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GameControllerMapping(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerMappingForGUID(lua_State*L){
  printf("unsupported type");  assert(0);  void* ret = 0;
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GameControllerName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerNameForIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerNameForIndex(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerUpdate(lua_State*L){
  SDL_GameControllerUpdate();
  return 0;
};

static int lSDL_GetAssertionHandler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetAssertionHandler(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAssertionReport(lua_State*L){
  void* ret = (void*)SDL_GetAssertionReport();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioDeviceName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_GetAudioDeviceName(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioDeviceStatus(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetAudioDeviceStatus(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetAudioDriver(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetAudioDriver(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioStatus(lua_State*L){
  int ret = (int)SDL_GetAudioStatus();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetBasePath(lua_State*L){
  void* ret = (void*)SDL_GetBasePath();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCPUCacheLineSize(lua_State*L){
  int ret = (int)SDL_GetCPUCacheLineSize();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCPUCount(lua_State*L){
  int ret = (int)SDL_GetCPUCount();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetClipRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_GetClipRect(a1,a2);
  return 0;
};

static int lSDL_GetClipboardText(lua_State*L){
  void* ret = (void*)SDL_GetClipboardText();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetClosestDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)SDL_GetClosestDisplayMode(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetColorKey(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetColorKey(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCurrentAudioDriver(lua_State*L){
  void* ret = (void*)SDL_GetCurrentAudioDriver();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCurrentDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetCurrentDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCurrentVideoDriver(lua_State*L){
  void* ret = (void*)SDL_GetCurrentVideoDriver();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCursor(lua_State*L){
  void* ret = (void*)SDL_GetCursor();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDefaultAssertionHandler(lua_State*L){
  void* ret = (void*)SDL_GetDefaultAssertionHandler();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDefaultCursor(lua_State*L){
  void* ret = (void*)SDL_GetDefaultCursor();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDesktopDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetDesktopDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayBounds(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetDisplayBounds(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayDPI(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_GetDisplayDPI(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_GetDisplayMode(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetDisplayName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDisplayUsableBounds(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetDisplayUsableBounds(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetError(lua_State*L){
  void* ret = (void*)SDL_GetError();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetEventFilter(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetEventFilter(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetGlobalMouseState(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetGlobalMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetGrabbedWindow(lua_State*L){
  void* ret = (void*)SDL_GetGrabbedWindow();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetHint(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetHint(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetHintBoolean(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GetHintBoolean(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyFromName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetKeyFromName(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyFromScancode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetKeyFromScancode(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetKeyName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetKeyboardFocus(lua_State*L){
  void* ret = (void*)SDL_GetKeyboardFocus();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetKeyboardState(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetKeyboardState(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetModState(lua_State*L){
  int ret = (int)SDL_GetModState();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetMouseFocus(lua_State*L){
  void* ret = (void*)SDL_GetMouseFocus();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetMouseState(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumAudioDevices(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumAudioDevices(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumAudioDrivers(lua_State*L){
  int ret = (int)SDL_GetNumAudioDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumDisplayModes(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumDisplayModes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumRenderDrivers(lua_State*L){
  int ret = (int)SDL_GetNumRenderDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumTouchDevices(lua_State*L){
  int ret = (int)SDL_GetNumTouchDevices();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumTouchFingers(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumTouchFingers(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumVideoDisplays(lua_State*L){
  int ret = (int)SDL_GetNumVideoDisplays();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumVideoDrivers(lua_State*L){
  int ret = (int)SDL_GetNumVideoDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPerformanceCounter(lua_State*L){
  int ret = (int)SDL_GetPerformanceCounter();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPerformanceFrequency(lua_State*L){
  int ret = (int)SDL_GetPerformanceFrequency();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPixelFormatName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetPixelFormatName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetPlatform(lua_State*L){
  void* ret = (void*)SDL_GetPlatform();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetPowerInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetPowerInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPrefPath(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_GetPrefPath(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetQueuedAudioSize(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetQueuedAudioSize(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRGB(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  SDL_GetRGB(a1,a2,a3,a4,a5);
  return 0;
};

static int lSDL_GetRGBA(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  SDL_GetRGBA(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lSDL_GetRelativeMouseMode(lua_State*L){
  int ret = (int)SDL_GetRelativeMouseMode();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRelativeMouseState(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetRelativeMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDrawBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetRenderDrawBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDrawColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_GetRenderDrawColor(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDriverInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetRenderDriverInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderTarget(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetRenderTarget(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRenderer(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetRenderer(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRendererInfo(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetRendererInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRendererOutputSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_GetRendererOutputSize(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRevision(lua_State*L){
  void* ret = (void*)SDL_GetRevision();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRevisionNumber(lua_State*L){
  int ret = (int)SDL_GetRevisionNumber();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeFromKey(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetScancodeFromKey(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeFromName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetScancodeFromName(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetScancodeName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetSurfaceAlphaMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetSurfaceAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSurfaceBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetSurfaceBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSurfaceColorMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_GetSurfaceColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSystemRAM(lua_State*L){
  int ret = (int)SDL_GetSystemRAM();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureAlphaMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetTextureAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetTextureBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureColorMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_GetTextureColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetThreadID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetThreadID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetThreadName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetThreadName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetTicks(lua_State*L){
  int ret = (int)SDL_GetTicks();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTouchDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetTouchDevice(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTouchFinger(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_GetTouchFinger(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetVersion(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_GetVersion(a1);
  return 0;
};

static int lSDL_GetVideoDriver(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetVideoDriver(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowBordersSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_GetWindowBordersSize(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowBrightness(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)SDL_GetWindowBrightness(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_GetWindowData(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_GetWindowData(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowDisplayIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetWindowDisplayIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowDisplayMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetWindowDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowFlags(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetWindowFlags(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowFromID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetWindowFromID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowGammaRamp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_GetWindowGammaRamp(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowGrab(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetWindowGrab(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetWindowID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowMaximumSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_GetWindowMaximumSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowMinimumSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_GetWindowMinimumSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowOpacity(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_GetWindowOpacity(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowPixelFormat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_GetWindowPixelFormat(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowPosition(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_GetWindowPosition(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_GetWindowSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetWindowSurface(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowTitle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_GetWindowTitle(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticClose(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_HapticClose(a1);
  return 0;
};

static int lSDL_HapticDestroyEffect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_HapticDestroyEffect(a1,a2);
  return 0;
};

static int lSDL_HapticEffectSupported(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_HapticEffectSupported(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticGetEffectStatus(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticGetEffectStatus(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticIndex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_HapticName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticNewEffect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_HapticNewEffect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumAxes(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticNumAxes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumEffects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticNumEffects(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumEffectsPlaying(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticNumEffectsPlaying(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_HapticOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpenFromJoystick(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_HapticOpenFromJoystick(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpenFromMouse(lua_State*L){
  void* ret = (void*)SDL_HapticOpenFromMouse();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpened(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_HapticOpened(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticPause(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticPause(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticQuery(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticQuery(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleInit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticRumbleInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumblePlay(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_HapticRumblePlay(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleStop(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticRumbleStop(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleSupported(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticRumbleSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRunEffect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_HapticRunEffect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticSetAutocenter(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticSetAutocenter(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticSetGain(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticSetGain(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticStopAll(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticStopAll(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticStopEffect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticStopEffect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticUnpause(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_HapticUnpause(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticUpdateEffect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_HapticUpdateEffect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Has3DNow(lua_State*L){
  int ret = (int)SDL_Has3DNow();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAVX(lua_State*L){
  int ret = (int)SDL_HasAVX();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAVX2(lua_State*L){
  int ret = (int)SDL_HasAVX2();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAltiVec(lua_State*L){
  int ret = (int)SDL_HasAltiVec();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasClipboardText(lua_State*L){
  int ret = (int)SDL_HasClipboardText();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasEvent(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_HasEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HasEvents(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasIntersection(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_HasIntersection(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasMMX(lua_State*L){
  int ret = (int)SDL_HasMMX();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasRDTSC(lua_State*L){
  int ret = (int)SDL_HasRDTSC();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE(lua_State*L){
  int ret = (int)SDL_HasSSE();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE2(lua_State*L){
  int ret = (int)SDL_HasSSE2();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE3(lua_State*L){
  int ret = (int)SDL_HasSSE3();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE41(lua_State*L){
  int ret = (int)SDL_HasSSE41();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE42(lua_State*L){
  int ret = (int)SDL_HasSSE42();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasScreenKeyboardSupport(lua_State*L){
  int ret = (int)SDL_HasScreenKeyboardSupport();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HideWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_HideWindow(a1);
  return 0;
};

static int lSDL_Init(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Init(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_InitSubSystem(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_InitSubSystem(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IntersectRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_IntersectRect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IntersectRectAndLine(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_IntersectRectAndLine(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsGameController(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_IsGameController(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsScreenKeyboardShown(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_IsScreenKeyboardShown(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsScreenSaverEnabled(lua_State*L){
  int ret = (int)SDL_IsScreenSaverEnabled();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsTextInputActive(lua_State*L){
  int ret = (int)SDL_IsTextInputActive();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickClose(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_JoystickClose(a1);
  return 0;
};

static int lSDL_JoystickCurrentPowerLevel(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickCurrentPowerLevel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickEventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_JoystickEventState(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickFromInstanceID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickFromInstanceID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickGetAttached(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickGetAttached(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetAxis(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetAxis(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetBall(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_JoystickGetBall(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetButton(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetButton(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetDeviceGUID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_JoystickGetDeviceGUID(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_JoystickGetGUID(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUIDFromString(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_JoystickGetGUIDFromString(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUIDString(lua_State*L){
  printf("unsupported type");  assert(0);  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
/* no support argument type*/
  return 0;
};

static int lSDL_JoystickGetHat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetHat(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickInstanceID(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickInstanceID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickIsHaptic(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickIsHaptic(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickName(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_JoystickName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickNameForIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickNameForIndex(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickNumAxes(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickNumAxes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumBalls(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickNumBalls(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumButtons(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickNumButtons(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumHats(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_JoystickNumHats(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickUpdate(lua_State*L){
  SDL_JoystickUpdate();
  return 0;
};

static int lSDL_LoadBMP_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_LoadBMP_RW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadDollarTemplates(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_LoadDollarTemplates(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LoadFunction(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_LoadFunction(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadObject(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_LoadObject(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadWAV_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* ret = (void*)SDL_LoadWAV_RW(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LockAudio(lua_State*L){
  SDL_LockAudio();
  return 0;
};

static int lSDL_LockAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_LockAudioDevice(a1);
  return 0;
};

static int lSDL_LockMutex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_LockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LockSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_LockSurface(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LockTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_LockTexture(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Log(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_Log(a1);
  return 0;
};

static int lSDL_LogCritical(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogCritical(a1,a2);
  return 0;
};

static int lSDL_LogDebug(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogDebug(a1,a2);
  return 0;
};

static int lSDL_LogError(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogError(a1,a2);
  return 0;
};

static int lSDL_LogGetOutputFunction(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogGetOutputFunction(a1,a2);
  return 0;
};

static int lSDL_LogGetPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_LogGetPriority(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LogInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogInfo(a1,a2);
  return 0;
};

static int lSDL_LogMessage(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_LogMessage(a1,a2,a3);
  return 0;
};

static int lSDL_LogMessageV(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  SDL_LogMessageV(a1,a2,a3,a4);
  return 0;
};

static int lSDL_LogResetPriorities(lua_State*L){
  SDL_LogResetPriorities();
  return 0;
};

static int lSDL_LogSetAllPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_LogSetAllPriority(a1);
  return 0;
};

static int lSDL_LogSetOutputFunction(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogSetOutputFunction(a1,a2);
  return 0;
};

static int lSDL_LogSetPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_LogSetPriority(a1,a2);
  return 0;
};

static int lSDL_LogVerbose(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogVerbose(a1,a2);
  return 0;
};

static int lSDL_LogWarn(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_LogWarn(a1,a2);
  return 0;
};

static int lSDL_LowerBlit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_LowerBlit(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LowerBlitScaled(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_LowerBlitScaled(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MapRGB(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_MapRGB(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MapRGBA(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_MapRGBA(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MasksToPixelFormatEnum(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_MasksToPixelFormatEnum(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MaximizeWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_MaximizeWindow(a1);
  return 0;
};

static int lSDL_MinimizeWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_MinimizeWindow(a1);
  return 0;
};

static int lSDL_MixAudio(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  SDL_MixAudio(a1,a2,a3,a4);
  return 0;
};

static int lSDL_MixAudioFormat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  SDL_MixAudioFormat(a1,a2,a3,a4,a5);
  return 0;
};

static int lSDL_MouseIsHaptic(lua_State*L){
  int ret = (int)SDL_MouseIsHaptic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_NumHaptics(lua_State*L){
  int ret = (int)SDL_NumHaptics();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_NumJoysticks(lua_State*L){
  int ret = (int)SDL_NumJoysticks();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_OpenAudio(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_OpenAudio(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_OpenAudioDevice(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_OpenAudioDevice(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PauseAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_PauseAudio(a1);
  return 0;
};

static int lSDL_PauseAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_PauseAudioDevice(a1,a2);
  return 0;
};

static int lSDL_PeepEvents(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_PeepEvents(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PixelFormatEnumToMasks(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  void* a6 = lcheck_toptr(L,6);
  int ret = (int)SDL_PixelFormatEnumToMasks(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PointInRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_PointInRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PollEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_PollEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PumpEvents(lua_State*L){
  SDL_PumpEvents();
  return 0;
};

static int lSDL_PushEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_PushEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_QueryTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_QueryTexture(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_QueueAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_QueueAudio(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Quit(lua_State*L){
  SDL_Quit();
  return 0;
};

static int lSDL_QuitSubSystem(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_QuitSubSystem(a1);
  return 0;
};

static int lSDL_RWFromConstMem(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromConstMem(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromFP(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromFP(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromFile(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_RWFromFile(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromMem(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromMem(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RaiseWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_RaiseWindow(a1);
  return 0;
};

static int lSDL_ReadBE16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadBE16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadBE32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadBE32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadBE64(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadBE64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadLE16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadLE32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE64(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadLE64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadU8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_ReadU8(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RecordGesture(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RecordGesture(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RectEmpty(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_RectEmpty(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RectEquals(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_RectEquals(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RegisterApp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_RegisterApp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RegisterEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RegisterEvents(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RemoveTimer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RemoveTimer(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderClear(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_RenderClear(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderCopy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_RenderCopy(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderCopyEx(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  double a5 = luaL_checknumber(L,5);
  void* a6 = lcheck_toptr(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int ret = (int)SDL_RenderCopyEx(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawLine(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_RenderDrawLine(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawLines(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawLines(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawPoint(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawPoint(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawPoints(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawPoints(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_RenderDrawRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawRects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderFillRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_RenderFillRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderFillRects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderFillRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderGetClipRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_RenderGetClipRect(a1,a2);
  return 0;
};

static int lSDL_RenderGetD3D9Device(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_RenderGetD3D9Device(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RenderGetIntegerScale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_RenderGetIntegerScale(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderGetLogicalSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_RenderGetLogicalSize(a1,a2,a3);
  return 0;
};

static int lSDL_RenderGetScale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_RenderGetScale(a1,a2,a3);
  return 0;
};

static int lSDL_RenderGetViewport(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_RenderGetViewport(a1,a2);
  return 0;
};

static int lSDL_RenderIsClipEnabled(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_RenderIsClipEnabled(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderPresent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_RenderPresent(a1);
  return 0;
};

static int lSDL_RenderReadPixels(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_RenderReadPixels(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetClipRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_RenderSetClipRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetIntegerScale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_RenderSetIntegerScale(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetLogicalSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderSetLogicalSize(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetScale(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int ret = (int)SDL_RenderSetScale(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetViewport(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_RenderSetViewport(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderTargetSupported(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_RenderTargetSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReportAssertion(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_ReportAssertion(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ResetAssertionReport(lua_State*L){
  SDL_ResetAssertionReport();
  return 0;
};

static int lSDL_RestoreWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_RestoreWindow(a1);
  return 0;
};

static int lSDL_SaveAllDollarTemplates(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SaveAllDollarTemplates(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SaveBMP_RW(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SaveBMP_RW(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SaveDollarTemplate(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SaveDollarTemplate(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemPost(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SemPost(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemTryWait(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SemTryWait(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemValue(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SemValue(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemWait(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SemWait(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemWaitTimeout(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SemWaitTimeout(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetAssertionHandler(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_SetAssertionHandler(a1,a2);
  return 0;
};

static int lSDL_SetClipRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetClipRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetClipboardText(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SetClipboardText(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetColorKey(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SetColorKey(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetCursor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_SetCursor(a1);
  return 0;
};

static int lSDL_SetError(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SetError(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetEventFilter(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_SetEventFilter(a1,a2);
  return 0;
};

static int lSDL_SetHint(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetHint(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetHintWithPriority(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SetHintWithPriority(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetMainReady(lua_State*L){
  SDL_SetMainReady();
  return 0;
};

static int lSDL_SetModState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_SetModState(a1);
  return 0;
};

static int lSDL_SetPaletteColors(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetPaletteColors(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetPixelFormatPalette(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetPixelFormatPalette(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRelativeMouseMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_SetRelativeMouseMode(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderDrawBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetRenderDrawBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderDrawColor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_SetRenderDrawColor(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderTarget(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetRenderTarget(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceAlphaMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceColorMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetSurfaceColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfacePalette(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetSurfacePalette(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceRLE(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceRLE(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextInputRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_SetTextInputRect(a1);
  return 0;
};

static int lSDL_SetTextureAlphaMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetTextureAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextureBlendMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetTextureBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextureColorMod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetTextureColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetThreadPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_SetThreadPriority(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowBordered(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowBordered(a1,a2);
  return 0;
};

static int lSDL_SetWindowBrightness(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int ret = (int)SDL_SetWindowBrightness(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowData(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* ret = (void*)SDL_SetWindowData(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_SetWindowDisplayMode(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetWindowDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowFullscreen(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetWindowFullscreen(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowGammaRamp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_SetWindowGammaRamp(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowGrab(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowGrab(a1,a2);
  return 0;
};

static int lSDL_SetWindowHitTest(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_SetWindowHitTest(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowIcon(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_SetWindowIcon(a1,a2);
  return 0;
};

static int lSDL_SetWindowInputFocus(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_SetWindowInputFocus(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowMaximumSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowMaximumSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowMinimumSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowMinimumSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowModalFor(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_SetWindowModalFor(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowOpacity(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double a2 = luaL_checknumber(L,2);
  int ret = (int)SDL_SetWindowOpacity(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowPosition(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowPosition(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowResizable(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowResizable(a1,a2);
  return 0;
};

static int lSDL_SetWindowSize(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowTitle(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_SetWindowTitle(a1,a2);
  return 0;
};

static int lSDL_SetWindowsMessageHook(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_SetWindowsMessageHook(a1,a2);
  return 0;
};

static int lSDL_ShowCursor(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_ShowCursor(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowMessageBox(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_ShowMessageBox(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowSimpleMessageBox(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_ShowSimpleMessageBox(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_ShowWindow(a1);
  return 0;
};

static int lSDL_SoftStretch(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_SoftStretch(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_StartTextInput(lua_State*L){
  SDL_StartTextInput();
  return 0;
};

static int lSDL_StopTextInput(lua_State*L){
  SDL_StopTextInput();
  return 0;
};

static int lSDL_Swap16(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Swap32(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Swap64(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SwapFloat(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_SwapFloat(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_TLSCreate(lua_State*L){
  int ret = (int)SDL_TLSCreate();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_TLSGet(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_TLSGet(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_TLSSet(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_TLSSet(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ThreadID(lua_State*L){
  int ret = (int)SDL_ThreadID();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_TryLockMutex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_TryLockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UnionRect(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  SDL_UnionRect(a1,a2,a3);
  return 0;
};

static int lSDL_UnloadObject(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_UnloadObject(a1);
  return 0;
};

static int lSDL_UnlockAudio(lua_State*L){
  SDL_UnlockAudio();
  return 0;
};

static int lSDL_UnlockAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_UnlockAudioDevice(a1);
  return 0;
};

static int lSDL_UnlockMutex(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_UnlockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UnlockSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_UnlockSurface(a1);
  return 0;
};

static int lSDL_UnlockTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_UnlockTexture(a1);
  return 0;
};

static int lSDL_UnregisterApp(lua_State*L){
  SDL_UnregisterApp();
  return 0;
};

static int lSDL_UpdateTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_UpdateTexture(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateWindowSurface(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_UpdateWindowSurface(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateWindowSurfaceRects(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_UpdateWindowSurfaceRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateYUVTexture(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = lcheck_toptr(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = lcheck_toptr(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int ret = (int)SDL_UpdateYUVTexture(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpperBlit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_UpperBlit(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpperBlitScaled(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_UpperBlitScaled(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_VideoInit(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_VideoInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_VideoQuit(lua_State*L){
  SDL_VideoQuit();
  return 0;
};

static int lSDL_WaitEvent(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_WaitEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WaitEventTimeout(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WaitEventTimeout(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WaitThread(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  SDL_WaitThread(a1,a2);
  return 0;
};

static int lSDL_WarpMouseGlobal(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WarpMouseGlobal(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WarpMouseInWindow(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_WarpMouseInWindow(a1,a2,a3);
  return 0;
};

static int lSDL_WasInit(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_WasInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE16(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE32(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE64(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE64(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE16(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE16(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE32(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE32(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE64(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE64(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteU8(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteU8(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_abs(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_abs(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_acos(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_acos(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_asin(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_asin(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atan(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_atan(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atan2(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_atan2(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atof(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  double ret = (double)SDL_atof(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atoi(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_atoi(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_calloc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_calloc(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ceil(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_ceil(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_copysign(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_copysign(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_cos(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_cos(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_cosf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_cosf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_fabs(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_fabs(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_floor(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_floor(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_free(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  SDL_free(a1);
  return 0;
};

static int lSDL_getenv(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_getenv(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_iconv(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  void* a5 = lcheck_toptr(L,5);
  int ret = (int)SDL_iconv(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_iconv_close(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_iconv_close(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_iconv_open(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_iconv_open(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_iconv_string(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)SDL_iconv_string(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_isdigit(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_isdigit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_isspace(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_isspace(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_itoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_itoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_lltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_lltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_log(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_log(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_ltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_main(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  lua_pushinteger(L,0);
  return 1;
};

static int lSDL_malloc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_malloc(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memcmp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_memcmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_memcpy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memcpy(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memcpy4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memcpy4(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memmove(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memmove(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memset(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memset(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memset4(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_memset4(a1,a2,a3);
  return 0;
};

static int lSDL_pow(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_pow(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_qsort(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = lcheck_toptr(L,4);
  SDL_qsort(a1,a2,a3,a4);
  return 0;
};

static int lSDL_realloc(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_realloc(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_scalbn(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double ret = (double)SDL_scalbn(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_setenv(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_setenv(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_sin(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sin(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sinf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sinf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_snprintf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_snprintf(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_sqrt(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sqrt(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sqrtf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sqrtf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sscanf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_sscanf(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strcasecmp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_strcasecmp(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strchr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_strchr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strcmp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int ret = (int)SDL_strcmp(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strdup(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_strdup(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strlcat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strlcat(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlcpy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strlcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlen(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_strlen(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlwr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_strlwr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strncasecmp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strncasecmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strncmp(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strncmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strrchr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_strrchr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strrev(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_strrev(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strstr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* ret = (void*)SDL_strstr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strtod(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  double ret = (double)SDL_strtod(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_strtol(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtol(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoll(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoll(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoul(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoul(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoull(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoull(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strupr(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* ret = (void*)SDL_strupr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_tan(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_tan(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_tanf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_tanf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_tolower(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_tolower(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_toupper(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_toupper(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_uitoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_uitoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ulltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ulltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ultoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ultoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_utf8strlcpy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_utf8strlcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_vsnprintf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = lcheck_toptr(L,3);
  void* a4 = lcheck_toptr(L,4);
  int ret = (int)SDL_vsnprintf(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_vsscanf(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  void* a3 = lcheck_toptr(L,3);
  int ret = (int)SDL_vsscanf(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslcat(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_wcslcat(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslcpy(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  void* a2 = lcheck_toptr(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_wcslcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslen(lua_State*L){
  void* a1 = lcheck_toptr(L,1);
  int ret = (int)SDL_wcslen(a1);
  lua_pushinteger(L,ret);
  return 1;
};

int lSDL_openlibs(lua_State*L){
  lua_register(L,"SDL_AddEventWatch",lSDL_AddEventWatch);
  lua_register(L,"SDL_AddHintCallback",lSDL_AddHintCallback);
  lua_register(L,"SDL_AddTimer",lSDL_AddTimer);
  lua_register(L,"SDL_AllocFormat",lSDL_AllocFormat);
  lua_register(L,"SDL_AllocPalette",lSDL_AllocPalette);
  lua_register(L,"SDL_AllocRW",lSDL_AllocRW);
  lua_register(L,"SDL_AtomicAdd",lSDL_AtomicAdd);
  lua_register(L,"SDL_AtomicCAS",lSDL_AtomicCAS);
  lua_register(L,"SDL_AtomicCASPtr",lSDL_AtomicCASPtr);
  lua_register(L,"SDL_AtomicGet",lSDL_AtomicGet);
  lua_register(L,"SDL_AtomicGetPtr",lSDL_AtomicGetPtr);
  lua_register(L,"SDL_AtomicLock",lSDL_AtomicLock);
  lua_register(L,"SDL_AtomicSet",lSDL_AtomicSet);
  lua_register(L,"SDL_AtomicSetPtr",lSDL_AtomicSetPtr);
  lua_register(L,"SDL_AtomicTryLock",lSDL_AtomicTryLock);
  lua_register(L,"SDL_AtomicUnlock",lSDL_AtomicUnlock);
  lua_register(L,"SDL_AudioInit",lSDL_AudioInit);
  lua_register(L,"SDL_AudioQuit",lSDL_AudioQuit);
  lua_register(L,"SDL_BuildAudioCVT",lSDL_BuildAudioCVT);
  lua_register(L,"SDL_CalculateGammaRamp",lSDL_CalculateGammaRamp);
  lua_register(L,"SDL_CaptureMouse",lSDL_CaptureMouse);
  lua_register(L,"SDL_ClearError",lSDL_ClearError);
  lua_register(L,"SDL_ClearHints",lSDL_ClearHints);
  lua_register(L,"SDL_ClearQueuedAudio",lSDL_ClearQueuedAudio);
  lua_register(L,"SDL_CloseAudio",lSDL_CloseAudio);
  lua_register(L,"SDL_CloseAudioDevice",lSDL_CloseAudioDevice);
  lua_register(L,"SDL_CondBroadcast",lSDL_CondBroadcast);
  lua_register(L,"SDL_CondSignal",lSDL_CondSignal);
  lua_register(L,"SDL_CondWait",lSDL_CondWait);
  lua_register(L,"SDL_CondWaitTimeout",lSDL_CondWaitTimeout);
  lua_register(L,"SDL_ConvertAudio",lSDL_ConvertAudio);
  lua_register(L,"SDL_ConvertPixels",lSDL_ConvertPixels);
  lua_register(L,"SDL_ConvertSurface",lSDL_ConvertSurface);
  lua_register(L,"SDL_ConvertSurfaceFormat",lSDL_ConvertSurfaceFormat);
  lua_register(L,"SDL_CreateColorCursor",lSDL_CreateColorCursor);
  lua_register(L,"SDL_CreateCond",lSDL_CreateCond);
  lua_register(L,"SDL_CreateCursor",lSDL_CreateCursor);
  lua_register(L,"SDL_CreateMutex",lSDL_CreateMutex);
  lua_register(L,"SDL_CreateRGBSurface",lSDL_CreateRGBSurface);
  lua_register(L,"SDL_CreateRGBSurfaceFrom",lSDL_CreateRGBSurfaceFrom);
  lua_register(L,"SDL_CreateRGBSurfaceWithFormat",lSDL_CreateRGBSurfaceWithFormat);
  lua_register(L,"SDL_CreateRGBSurfaceWithFormatFrom",lSDL_CreateRGBSurfaceWithFormatFrom);
  lua_register(L,"SDL_CreateRenderer",lSDL_CreateRenderer);
  lua_register(L,"SDL_CreateSemaphore",lSDL_CreateSemaphore);
  lua_register(L,"SDL_CreateSoftwareRenderer",lSDL_CreateSoftwareRenderer);
  lua_register(L,"SDL_CreateSystemCursor",lSDL_CreateSystemCursor);
  lua_register(L,"SDL_CreateTexture",lSDL_CreateTexture);
  lua_register(L,"SDL_CreateTextureFromSurface",lSDL_CreateTextureFromSurface);
  lua_register(L,"SDL_CreateThread",lSDL_CreateThread);
  lua_register(L,"SDL_CreateWindow",lSDL_CreateWindow);
  lua_register(L,"SDL_CreateWindowAndRenderer",lSDL_CreateWindowAndRenderer);
  lua_register(L,"SDL_CreateWindowFrom",lSDL_CreateWindowFrom);
  lua_register(L,"SDL_DXGIGetOutputInfo",lSDL_DXGIGetOutputInfo);
  lua_register(L,"SDL_DelEventWatch",lSDL_DelEventWatch);
  lua_register(L,"SDL_DelHintCallback",lSDL_DelHintCallback);
  lua_register(L,"SDL_Delay",lSDL_Delay);
  lua_register(L,"SDL_DequeueAudio",lSDL_DequeueAudio);
  lua_register(L,"SDL_DestroyCond",lSDL_DestroyCond);
  lua_register(L,"SDL_DestroyMutex",lSDL_DestroyMutex);
  lua_register(L,"SDL_DestroyRenderer",lSDL_DestroyRenderer);
  lua_register(L,"SDL_DestroySemaphore",lSDL_DestroySemaphore);
  lua_register(L,"SDL_DestroyTexture",lSDL_DestroyTexture);
  lua_register(L,"SDL_DestroyWindow",lSDL_DestroyWindow);
  lua_register(L,"SDL_DetachThread",lSDL_DetachThread);
  lua_register(L,"SDL_Direct3D9GetAdapterIndex",lSDL_Direct3D9GetAdapterIndex);
  lua_register(L,"SDL_DisableScreenSaver",lSDL_DisableScreenSaver);
  lua_register(L,"SDL_EnableScreenSaver",lSDL_EnableScreenSaver);
  lua_register(L,"SDL_EnclosePoints",lSDL_EnclosePoints);
  lua_register(L,"SDL_Error",lSDL_Error);
  lua_register(L,"SDL_EventState",lSDL_EventState);
  lua_register(L,"SDL_FillRect",lSDL_FillRect);
  lua_register(L,"SDL_FillRects",lSDL_FillRects);
  lua_register(L,"SDL_FilterEvents",lSDL_FilterEvents);
  lua_register(L,"SDL_FlushEvent",lSDL_FlushEvent);
  lua_register(L,"SDL_FlushEvents",lSDL_FlushEvents);
  lua_register(L,"SDL_FreeCursor",lSDL_FreeCursor);
  lua_register(L,"SDL_FreeFormat",lSDL_FreeFormat);
  lua_register(L,"SDL_FreePalette",lSDL_FreePalette);
  lua_register(L,"SDL_FreeRW",lSDL_FreeRW);
  lua_register(L,"SDL_FreeSurface",lSDL_FreeSurface);
  lua_register(L,"SDL_FreeWAV",lSDL_FreeWAV);
  lua_register(L,"SDL_GL_BindTexture",lSDL_GL_BindTexture);
  lua_register(L,"SDL_GL_CreateContext",lSDL_GL_CreateContext);
  lua_register(L,"SDL_GL_DeleteContext",lSDL_GL_DeleteContext);
  lua_register(L,"SDL_GL_ExtensionSupported",lSDL_GL_ExtensionSupported);
  lua_register(L,"SDL_GL_GetAttribute",lSDL_GL_GetAttribute);
  lua_register(L,"SDL_GL_GetCurrentContext",lSDL_GL_GetCurrentContext);
  lua_register(L,"SDL_GL_GetCurrentWindow",lSDL_GL_GetCurrentWindow);
  lua_register(L,"SDL_GL_GetDrawableSize",lSDL_GL_GetDrawableSize);
  lua_register(L,"SDL_GL_GetProcAddress",lSDL_GL_GetProcAddress);
  lua_register(L,"SDL_GL_GetSwapInterval",lSDL_GL_GetSwapInterval);
  lua_register(L,"SDL_GL_LoadLibrary",lSDL_GL_LoadLibrary);
  lua_register(L,"SDL_GL_MakeCurrent",lSDL_GL_MakeCurrent);
  lua_register(L,"SDL_GL_ResetAttributes",lSDL_GL_ResetAttributes);
  lua_register(L,"SDL_GL_SetAttribute",lSDL_GL_SetAttribute);
  lua_register(L,"SDL_GL_SetSwapInterval",lSDL_GL_SetSwapInterval);
  lua_register(L,"SDL_GL_SwapWindow",lSDL_GL_SwapWindow);
  lua_register(L,"SDL_GL_UnbindTexture",lSDL_GL_UnbindTexture);
  lua_register(L,"SDL_GL_UnloadLibrary",lSDL_GL_UnloadLibrary);
  lua_register(L,"SDL_GameControllerAddMapping",lSDL_GameControllerAddMapping);
  lua_register(L,"SDL_GameControllerAddMappingsFromRW",lSDL_GameControllerAddMappingsFromRW);
  lua_register(L,"SDL_GameControllerClose",lSDL_GameControllerClose);
  lua_register(L,"SDL_GameControllerEventState",lSDL_GameControllerEventState);
  lua_register(L,"SDL_GameControllerFromInstanceID",lSDL_GameControllerFromInstanceID);
  lua_register(L,"SDL_GameControllerGetAttached",lSDL_GameControllerGetAttached);
  lua_register(L,"SDL_GameControllerGetAxis",lSDL_GameControllerGetAxis);
  lua_register(L,"SDL_GameControllerGetAxisFromString",lSDL_GameControllerGetAxisFromString);
  lua_register(L,"SDL_GameControllerGetBindForAxis",lSDL_GameControllerGetBindForAxis);
  lua_register(L,"SDL_GameControllerGetBindForButton",lSDL_GameControllerGetBindForButton);
  lua_register(L,"SDL_GameControllerGetButton",lSDL_GameControllerGetButton);
  lua_register(L,"SDL_GameControllerGetButtonFromString",lSDL_GameControllerGetButtonFromString);
  lua_register(L,"SDL_GameControllerGetJoystick",lSDL_GameControllerGetJoystick);
  lua_register(L,"SDL_GameControllerGetStringForAxis",lSDL_GameControllerGetStringForAxis);
  lua_register(L,"SDL_GameControllerGetStringForButton",lSDL_GameControllerGetStringForButton);
  lua_register(L,"SDL_GameControllerMapping",lSDL_GameControllerMapping);
  lua_register(L,"SDL_GameControllerMappingForGUID",lSDL_GameControllerMappingForGUID);
  lua_register(L,"SDL_GameControllerName",lSDL_GameControllerName);
  lua_register(L,"SDL_GameControllerNameForIndex",lSDL_GameControllerNameForIndex);
  lua_register(L,"SDL_GameControllerOpen",lSDL_GameControllerOpen);
  lua_register(L,"SDL_GameControllerUpdate",lSDL_GameControllerUpdate);
  lua_register(L,"SDL_GetAssertionHandler",lSDL_GetAssertionHandler);
  lua_register(L,"SDL_GetAssertionReport",lSDL_GetAssertionReport);
  lua_register(L,"SDL_GetAudioDeviceName",lSDL_GetAudioDeviceName);
  lua_register(L,"SDL_GetAudioDeviceStatus",lSDL_GetAudioDeviceStatus);
  lua_register(L,"SDL_GetAudioDriver",lSDL_GetAudioDriver);
  lua_register(L,"SDL_GetAudioStatus",lSDL_GetAudioStatus);
  lua_register(L,"SDL_GetBasePath",lSDL_GetBasePath);
  lua_register(L,"SDL_GetCPUCacheLineSize",lSDL_GetCPUCacheLineSize);
  lua_register(L,"SDL_GetCPUCount",lSDL_GetCPUCount);
  lua_register(L,"SDL_GetClipRect",lSDL_GetClipRect);
  lua_register(L,"SDL_GetClipboardText",lSDL_GetClipboardText);
  lua_register(L,"SDL_GetClosestDisplayMode",lSDL_GetClosestDisplayMode);
  lua_register(L,"SDL_GetColorKey",lSDL_GetColorKey);
  lua_register(L,"SDL_GetCurrentAudioDriver",lSDL_GetCurrentAudioDriver);
  lua_register(L,"SDL_GetCurrentDisplayMode",lSDL_GetCurrentDisplayMode);
  lua_register(L,"SDL_GetCurrentVideoDriver",lSDL_GetCurrentVideoDriver);
  lua_register(L,"SDL_GetCursor",lSDL_GetCursor);
  lua_register(L,"SDL_GetDefaultAssertionHandler",lSDL_GetDefaultAssertionHandler);
  lua_register(L,"SDL_GetDefaultCursor",lSDL_GetDefaultCursor);
  lua_register(L,"SDL_GetDesktopDisplayMode",lSDL_GetDesktopDisplayMode);
  lua_register(L,"SDL_GetDisplayBounds",lSDL_GetDisplayBounds);
  lua_register(L,"SDL_GetDisplayDPI",lSDL_GetDisplayDPI);
  lua_register(L,"SDL_GetDisplayMode",lSDL_GetDisplayMode);
  lua_register(L,"SDL_GetDisplayName",lSDL_GetDisplayName);
  lua_register(L,"SDL_GetDisplayUsableBounds",lSDL_GetDisplayUsableBounds);
  lua_register(L,"SDL_GetError",lSDL_GetError);
  lua_register(L,"SDL_GetEventFilter",lSDL_GetEventFilter);
  lua_register(L,"SDL_GetGlobalMouseState",lSDL_GetGlobalMouseState);
  lua_register(L,"SDL_GetGrabbedWindow",lSDL_GetGrabbedWindow);
  lua_register(L,"SDL_GetHint",lSDL_GetHint);
  lua_register(L,"SDL_GetHintBoolean",lSDL_GetHintBoolean);
  lua_register(L,"SDL_GetKeyFromName",lSDL_GetKeyFromName);
  lua_register(L,"SDL_GetKeyFromScancode",lSDL_GetKeyFromScancode);
  lua_register(L,"SDL_GetKeyName",lSDL_GetKeyName);
  lua_register(L,"SDL_GetKeyboardFocus",lSDL_GetKeyboardFocus);
  lua_register(L,"SDL_GetKeyboardState",lSDL_GetKeyboardState);
  lua_register(L,"SDL_GetModState",lSDL_GetModState);
  lua_register(L,"SDL_GetMouseFocus",lSDL_GetMouseFocus);
  lua_register(L,"SDL_GetMouseState",lSDL_GetMouseState);
  lua_register(L,"SDL_GetNumAudioDevices",lSDL_GetNumAudioDevices);
  lua_register(L,"SDL_GetNumAudioDrivers",lSDL_GetNumAudioDrivers);
  lua_register(L,"SDL_GetNumDisplayModes",lSDL_GetNumDisplayModes);
  lua_register(L,"SDL_GetNumRenderDrivers",lSDL_GetNumRenderDrivers);
  lua_register(L,"SDL_GetNumTouchDevices",lSDL_GetNumTouchDevices);
  lua_register(L,"SDL_GetNumTouchFingers",lSDL_GetNumTouchFingers);
  lua_register(L,"SDL_GetNumVideoDisplays",lSDL_GetNumVideoDisplays);
  lua_register(L,"SDL_GetNumVideoDrivers",lSDL_GetNumVideoDrivers);
  lua_register(L,"SDL_GetPerformanceCounter",lSDL_GetPerformanceCounter);
  lua_register(L,"SDL_GetPerformanceFrequency",lSDL_GetPerformanceFrequency);
  lua_register(L,"SDL_GetPixelFormatName",lSDL_GetPixelFormatName);
  lua_register(L,"SDL_GetPlatform",lSDL_GetPlatform);
  lua_register(L,"SDL_GetPowerInfo",lSDL_GetPowerInfo);
  lua_register(L,"SDL_GetPrefPath",lSDL_GetPrefPath);
  lua_register(L,"SDL_GetQueuedAudioSize",lSDL_GetQueuedAudioSize);
  lua_register(L,"SDL_GetRGB",lSDL_GetRGB);
  lua_register(L,"SDL_GetRGBA",lSDL_GetRGBA);
  lua_register(L,"SDL_GetRelativeMouseMode",lSDL_GetRelativeMouseMode);
  lua_register(L,"SDL_GetRelativeMouseState",lSDL_GetRelativeMouseState);
  lua_register(L,"SDL_GetRenderDrawBlendMode",lSDL_GetRenderDrawBlendMode);
  lua_register(L,"SDL_GetRenderDrawColor",lSDL_GetRenderDrawColor);
  lua_register(L,"SDL_GetRenderDriverInfo",lSDL_GetRenderDriverInfo);
  lua_register(L,"SDL_GetRenderTarget",lSDL_GetRenderTarget);
  lua_register(L,"SDL_GetRenderer",lSDL_GetRenderer);
  lua_register(L,"SDL_GetRendererInfo",lSDL_GetRendererInfo);
  lua_register(L,"SDL_GetRendererOutputSize",lSDL_GetRendererOutputSize);
  lua_register(L,"SDL_GetRevision",lSDL_GetRevision);
  lua_register(L,"SDL_GetRevisionNumber",lSDL_GetRevisionNumber);
  lua_register(L,"SDL_GetScancodeFromKey",lSDL_GetScancodeFromKey);
  lua_register(L,"SDL_GetScancodeFromName",lSDL_GetScancodeFromName);
  lua_register(L,"SDL_GetScancodeName",lSDL_GetScancodeName);
  lua_register(L,"SDL_GetSurfaceAlphaMod",lSDL_GetSurfaceAlphaMod);
  lua_register(L,"SDL_GetSurfaceBlendMode",lSDL_GetSurfaceBlendMode);
  lua_register(L,"SDL_GetSurfaceColorMod",lSDL_GetSurfaceColorMod);
  lua_register(L,"SDL_GetSystemRAM",lSDL_GetSystemRAM);
  lua_register(L,"SDL_GetTextureAlphaMod",lSDL_GetTextureAlphaMod);
  lua_register(L,"SDL_GetTextureBlendMode",lSDL_GetTextureBlendMode);
  lua_register(L,"SDL_GetTextureColorMod",lSDL_GetTextureColorMod);
  lua_register(L,"SDL_GetThreadID",lSDL_GetThreadID);
  lua_register(L,"SDL_GetThreadName",lSDL_GetThreadName);
  lua_register(L,"SDL_GetTicks",lSDL_GetTicks);
  lua_register(L,"SDL_GetTouchDevice",lSDL_GetTouchDevice);
  lua_register(L,"SDL_GetTouchFinger",lSDL_GetTouchFinger);
  lua_register(L,"SDL_GetVersion",lSDL_GetVersion);
  lua_register(L,"SDL_GetVideoDriver",lSDL_GetVideoDriver);
  lua_register(L,"SDL_GetWindowBordersSize",lSDL_GetWindowBordersSize);
  lua_register(L,"SDL_GetWindowBrightness",lSDL_GetWindowBrightness);
  lua_register(L,"SDL_GetWindowData",lSDL_GetWindowData);
  lua_register(L,"SDL_GetWindowDisplayIndex",lSDL_GetWindowDisplayIndex);
  lua_register(L,"SDL_GetWindowDisplayMode",lSDL_GetWindowDisplayMode);
  lua_register(L,"SDL_GetWindowFlags",lSDL_GetWindowFlags);
  lua_register(L,"SDL_GetWindowFromID",lSDL_GetWindowFromID);
  lua_register(L,"SDL_GetWindowGammaRamp",lSDL_GetWindowGammaRamp);
  lua_register(L,"SDL_GetWindowGrab",lSDL_GetWindowGrab);
  lua_register(L,"SDL_GetWindowID",lSDL_GetWindowID);
  lua_register(L,"SDL_GetWindowMaximumSize",lSDL_GetWindowMaximumSize);
  lua_register(L,"SDL_GetWindowMinimumSize",lSDL_GetWindowMinimumSize);
  lua_register(L,"SDL_GetWindowOpacity",lSDL_GetWindowOpacity);
  lua_register(L,"SDL_GetWindowPixelFormat",lSDL_GetWindowPixelFormat);
  lua_register(L,"SDL_GetWindowPosition",lSDL_GetWindowPosition);
  lua_register(L,"SDL_GetWindowSize",lSDL_GetWindowSize);
  lua_register(L,"SDL_GetWindowSurface",lSDL_GetWindowSurface);
  lua_register(L,"SDL_GetWindowTitle",lSDL_GetWindowTitle);
  lua_register(L,"SDL_HapticClose",lSDL_HapticClose);
  lua_register(L,"SDL_HapticDestroyEffect",lSDL_HapticDestroyEffect);
  lua_register(L,"SDL_HapticEffectSupported",lSDL_HapticEffectSupported);
  lua_register(L,"SDL_HapticGetEffectStatus",lSDL_HapticGetEffectStatus);
  lua_register(L,"SDL_HapticIndex",lSDL_HapticIndex);
  lua_register(L,"SDL_HapticName",lSDL_HapticName);
  lua_register(L,"SDL_HapticNewEffect",lSDL_HapticNewEffect);
  lua_register(L,"SDL_HapticNumAxes",lSDL_HapticNumAxes);
  lua_register(L,"SDL_HapticNumEffects",lSDL_HapticNumEffects);
  lua_register(L,"SDL_HapticNumEffectsPlaying",lSDL_HapticNumEffectsPlaying);
  lua_register(L,"SDL_HapticOpen",lSDL_HapticOpen);
  lua_register(L,"SDL_HapticOpenFromJoystick",lSDL_HapticOpenFromJoystick);
  lua_register(L,"SDL_HapticOpenFromMouse",lSDL_HapticOpenFromMouse);
  lua_register(L,"SDL_HapticOpened",lSDL_HapticOpened);
  lua_register(L,"SDL_HapticPause",lSDL_HapticPause);
  lua_register(L,"SDL_HapticQuery",lSDL_HapticQuery);
  lua_register(L,"SDL_HapticRumbleInit",lSDL_HapticRumbleInit);
  lua_register(L,"SDL_HapticRumblePlay",lSDL_HapticRumblePlay);
  lua_register(L,"SDL_HapticRumbleStop",lSDL_HapticRumbleStop);
  lua_register(L,"SDL_HapticRumbleSupported",lSDL_HapticRumbleSupported);
  lua_register(L,"SDL_HapticRunEffect",lSDL_HapticRunEffect);
  lua_register(L,"SDL_HapticSetAutocenter",lSDL_HapticSetAutocenter);
  lua_register(L,"SDL_HapticSetGain",lSDL_HapticSetGain);
  lua_register(L,"SDL_HapticStopAll",lSDL_HapticStopAll);
  lua_register(L,"SDL_HapticStopEffect",lSDL_HapticStopEffect);
  lua_register(L,"SDL_HapticUnpause",lSDL_HapticUnpause);
  lua_register(L,"SDL_HapticUpdateEffect",lSDL_HapticUpdateEffect);
  lua_register(L,"SDL_Has3DNow",lSDL_Has3DNow);
  lua_register(L,"SDL_HasAVX",lSDL_HasAVX);
  lua_register(L,"SDL_HasAVX2",lSDL_HasAVX2);
  lua_register(L,"SDL_HasAltiVec",lSDL_HasAltiVec);
  lua_register(L,"SDL_HasClipboardText",lSDL_HasClipboardText);
  lua_register(L,"SDL_HasEvent",lSDL_HasEvent);
  lua_register(L,"SDL_HasEvents",lSDL_HasEvents);
  lua_register(L,"SDL_HasIntersection",lSDL_HasIntersection);
  lua_register(L,"SDL_HasMMX",lSDL_HasMMX);
  lua_register(L,"SDL_HasRDTSC",lSDL_HasRDTSC);
  lua_register(L,"SDL_HasSSE",lSDL_HasSSE);
  lua_register(L,"SDL_HasSSE2",lSDL_HasSSE2);
  lua_register(L,"SDL_HasSSE3",lSDL_HasSSE3);
  lua_register(L,"SDL_HasSSE41",lSDL_HasSSE41);
  lua_register(L,"SDL_HasSSE42",lSDL_HasSSE42);
  lua_register(L,"SDL_HasScreenKeyboardSupport",lSDL_HasScreenKeyboardSupport);
  lua_register(L,"SDL_HideWindow",lSDL_HideWindow);
  lua_register(L,"SDL_Init",lSDL_Init);
  lua_register(L,"SDL_InitSubSystem",lSDL_InitSubSystem);
  lua_register(L,"SDL_IntersectRect",lSDL_IntersectRect);
  lua_register(L,"SDL_IntersectRectAndLine",lSDL_IntersectRectAndLine);
  lua_register(L,"SDL_IsGameController",lSDL_IsGameController);
  lua_register(L,"SDL_IsScreenKeyboardShown",lSDL_IsScreenKeyboardShown);
  lua_register(L,"SDL_IsScreenSaverEnabled",lSDL_IsScreenSaverEnabled);
  lua_register(L,"SDL_IsTextInputActive",lSDL_IsTextInputActive);
  lua_register(L,"SDL_JoystickClose",lSDL_JoystickClose);
  lua_register(L,"SDL_JoystickCurrentPowerLevel",lSDL_JoystickCurrentPowerLevel);
  lua_register(L,"SDL_JoystickEventState",lSDL_JoystickEventState);
  lua_register(L,"SDL_JoystickFromInstanceID",lSDL_JoystickFromInstanceID);
  lua_register(L,"SDL_JoystickGetAttached",lSDL_JoystickGetAttached);
  lua_register(L,"SDL_JoystickGetAxis",lSDL_JoystickGetAxis);
  lua_register(L,"SDL_JoystickGetBall",lSDL_JoystickGetBall);
  lua_register(L,"SDL_JoystickGetButton",lSDL_JoystickGetButton);
  lua_register(L,"SDL_JoystickGetDeviceGUID",lSDL_JoystickGetDeviceGUID);
  lua_register(L,"SDL_JoystickGetGUID",lSDL_JoystickGetGUID);
  lua_register(L,"SDL_JoystickGetGUIDFromString",lSDL_JoystickGetGUIDFromString);
  lua_register(L,"SDL_JoystickGetGUIDString",lSDL_JoystickGetGUIDString);
  lua_register(L,"SDL_JoystickGetHat",lSDL_JoystickGetHat);
  lua_register(L,"SDL_JoystickInstanceID",lSDL_JoystickInstanceID);
  lua_register(L,"SDL_JoystickIsHaptic",lSDL_JoystickIsHaptic);
  lua_register(L,"SDL_JoystickName",lSDL_JoystickName);
  lua_register(L,"SDL_JoystickNameForIndex",lSDL_JoystickNameForIndex);
  lua_register(L,"SDL_JoystickNumAxes",lSDL_JoystickNumAxes);
  lua_register(L,"SDL_JoystickNumBalls",lSDL_JoystickNumBalls);
  lua_register(L,"SDL_JoystickNumButtons",lSDL_JoystickNumButtons);
  lua_register(L,"SDL_JoystickNumHats",lSDL_JoystickNumHats);
  lua_register(L,"SDL_JoystickOpen",lSDL_JoystickOpen);
  lua_register(L,"SDL_JoystickUpdate",lSDL_JoystickUpdate);
  lua_register(L,"SDL_LoadBMP_RW",lSDL_LoadBMP_RW);
  lua_register(L,"SDL_LoadDollarTemplates",lSDL_LoadDollarTemplates);
  lua_register(L,"SDL_LoadFunction",lSDL_LoadFunction);
  lua_register(L,"SDL_LoadObject",lSDL_LoadObject);
  lua_register(L,"SDL_LoadWAV_RW",lSDL_LoadWAV_RW);
  lua_register(L,"SDL_LockAudio",lSDL_LockAudio);
  lua_register(L,"SDL_LockAudioDevice",lSDL_LockAudioDevice);
  lua_register(L,"SDL_LockMutex",lSDL_LockMutex);
  lua_register(L,"SDL_LockSurface",lSDL_LockSurface);
  lua_register(L,"SDL_LockTexture",lSDL_LockTexture);
  lua_register(L,"SDL_Log",lSDL_Log);
  lua_register(L,"SDL_LogCritical",lSDL_LogCritical);
  lua_register(L,"SDL_LogDebug",lSDL_LogDebug);
  lua_register(L,"SDL_LogError",lSDL_LogError);
  lua_register(L,"SDL_LogGetOutputFunction",lSDL_LogGetOutputFunction);
  lua_register(L,"SDL_LogGetPriority",lSDL_LogGetPriority);
  lua_register(L,"SDL_LogInfo",lSDL_LogInfo);
  lua_register(L,"SDL_LogMessage",lSDL_LogMessage);
  lua_register(L,"SDL_LogMessageV",lSDL_LogMessageV);
  lua_register(L,"SDL_LogResetPriorities",lSDL_LogResetPriorities);
  lua_register(L,"SDL_LogSetAllPriority",lSDL_LogSetAllPriority);
  lua_register(L,"SDL_LogSetOutputFunction",lSDL_LogSetOutputFunction);
  lua_register(L,"SDL_LogSetPriority",lSDL_LogSetPriority);
  lua_register(L,"SDL_LogVerbose",lSDL_LogVerbose);
  lua_register(L,"SDL_LogWarn",lSDL_LogWarn);
  lua_register(L,"SDL_LowerBlit",lSDL_LowerBlit);
  lua_register(L,"SDL_LowerBlitScaled",lSDL_LowerBlitScaled);
  lua_register(L,"SDL_MapRGB",lSDL_MapRGB);
  lua_register(L,"SDL_MapRGBA",lSDL_MapRGBA);
  lua_register(L,"SDL_MasksToPixelFormatEnum",lSDL_MasksToPixelFormatEnum);
  lua_register(L,"SDL_MaximizeWindow",lSDL_MaximizeWindow);
  lua_register(L,"SDL_MinimizeWindow",lSDL_MinimizeWindow);
  lua_register(L,"SDL_MixAudio",lSDL_MixAudio);
  lua_register(L,"SDL_MixAudioFormat",lSDL_MixAudioFormat);
  lua_register(L,"SDL_MouseIsHaptic",lSDL_MouseIsHaptic);
  lua_register(L,"SDL_NumHaptics",lSDL_NumHaptics);
  lua_register(L,"SDL_NumJoysticks",lSDL_NumJoysticks);
  lua_register(L,"SDL_OpenAudio",lSDL_OpenAudio);
  lua_register(L,"SDL_OpenAudioDevice",lSDL_OpenAudioDevice);
  lua_register(L,"SDL_PauseAudio",lSDL_PauseAudio);
  lua_register(L,"SDL_PauseAudioDevice",lSDL_PauseAudioDevice);
  lua_register(L,"SDL_PeepEvents",lSDL_PeepEvents);
  lua_register(L,"SDL_PixelFormatEnumToMasks",lSDL_PixelFormatEnumToMasks);
  lua_register(L,"SDL_PointInRect",lSDL_PointInRect);
  lua_register(L,"SDL_PollEvent",lSDL_PollEvent);
  lua_register(L,"SDL_PumpEvents",lSDL_PumpEvents);
  lua_register(L,"SDL_PushEvent",lSDL_PushEvent);
  lua_register(L,"SDL_QueryTexture",lSDL_QueryTexture);
  lua_register(L,"SDL_QueueAudio",lSDL_QueueAudio);
  lua_register(L,"SDL_Quit",lSDL_Quit);
  lua_register(L,"SDL_QuitSubSystem",lSDL_QuitSubSystem);
  lua_register(L,"SDL_RWFromConstMem",lSDL_RWFromConstMem);
  lua_register(L,"SDL_RWFromFP",lSDL_RWFromFP);
  lua_register(L,"SDL_RWFromFile",lSDL_RWFromFile);
  lua_register(L,"SDL_RWFromMem",lSDL_RWFromMem);
  lua_register(L,"SDL_RaiseWindow",lSDL_RaiseWindow);
  lua_register(L,"SDL_ReadBE16",lSDL_ReadBE16);
  lua_register(L,"SDL_ReadBE32",lSDL_ReadBE32);
  lua_register(L,"SDL_ReadBE64",lSDL_ReadBE64);
  lua_register(L,"SDL_ReadLE16",lSDL_ReadLE16);
  lua_register(L,"SDL_ReadLE32",lSDL_ReadLE32);
  lua_register(L,"SDL_ReadLE64",lSDL_ReadLE64);
  lua_register(L,"SDL_ReadU8",lSDL_ReadU8);
  lua_register(L,"SDL_RecordGesture",lSDL_RecordGesture);
  lua_register(L,"SDL_RectEmpty",lSDL_RectEmpty);
  lua_register(L,"SDL_RectEquals",lSDL_RectEquals);
  lua_register(L,"SDL_RegisterApp",lSDL_RegisterApp);
  lua_register(L,"SDL_RegisterEvents",lSDL_RegisterEvents);
  lua_register(L,"SDL_RemoveTimer",lSDL_RemoveTimer);
  lua_register(L,"SDL_RenderClear",lSDL_RenderClear);
  lua_register(L,"SDL_RenderCopy",lSDL_RenderCopy);
  lua_register(L,"SDL_RenderCopyEx",lSDL_RenderCopyEx);
  lua_register(L,"SDL_RenderDrawLine",lSDL_RenderDrawLine);
  lua_register(L,"SDL_RenderDrawLines",lSDL_RenderDrawLines);
  lua_register(L,"SDL_RenderDrawPoint",lSDL_RenderDrawPoint);
  lua_register(L,"SDL_RenderDrawPoints",lSDL_RenderDrawPoints);
  lua_register(L,"SDL_RenderDrawRect",lSDL_RenderDrawRect);
  lua_register(L,"SDL_RenderDrawRects",lSDL_RenderDrawRects);
  lua_register(L,"SDL_RenderFillRect",lSDL_RenderFillRect);
  lua_register(L,"SDL_RenderFillRects",lSDL_RenderFillRects);
  lua_register(L,"SDL_RenderGetClipRect",lSDL_RenderGetClipRect);
  lua_register(L,"SDL_RenderGetD3D9Device",lSDL_RenderGetD3D9Device);
  lua_register(L,"SDL_RenderGetIntegerScale",lSDL_RenderGetIntegerScale);
  lua_register(L,"SDL_RenderGetLogicalSize",lSDL_RenderGetLogicalSize);
  lua_register(L,"SDL_RenderGetScale",lSDL_RenderGetScale);
  lua_register(L,"SDL_RenderGetViewport",lSDL_RenderGetViewport);
  lua_register(L,"SDL_RenderIsClipEnabled",lSDL_RenderIsClipEnabled);
  lua_register(L,"SDL_RenderPresent",lSDL_RenderPresent);
  lua_register(L,"SDL_RenderReadPixels",lSDL_RenderReadPixels);
  lua_register(L,"SDL_RenderSetClipRect",lSDL_RenderSetClipRect);
  lua_register(L,"SDL_RenderSetIntegerScale",lSDL_RenderSetIntegerScale);
  lua_register(L,"SDL_RenderSetLogicalSize",lSDL_RenderSetLogicalSize);
  lua_register(L,"SDL_RenderSetScale",lSDL_RenderSetScale);
  lua_register(L,"SDL_RenderSetViewport",lSDL_RenderSetViewport);
  lua_register(L,"SDL_RenderTargetSupported",lSDL_RenderTargetSupported);
  lua_register(L,"SDL_ReportAssertion",lSDL_ReportAssertion);
  lua_register(L,"SDL_ResetAssertionReport",lSDL_ResetAssertionReport);
  lua_register(L,"SDL_RestoreWindow",lSDL_RestoreWindow);
  lua_register(L,"SDL_SaveAllDollarTemplates",lSDL_SaveAllDollarTemplates);
  lua_register(L,"SDL_SaveBMP_RW",lSDL_SaveBMP_RW);
  lua_register(L,"SDL_SaveDollarTemplate",lSDL_SaveDollarTemplate);
  lua_register(L,"SDL_SemPost",lSDL_SemPost);
  lua_register(L,"SDL_SemTryWait",lSDL_SemTryWait);
  lua_register(L,"SDL_SemValue",lSDL_SemValue);
  lua_register(L,"SDL_SemWait",lSDL_SemWait);
  lua_register(L,"SDL_SemWaitTimeout",lSDL_SemWaitTimeout);
  lua_register(L,"SDL_SetAssertionHandler",lSDL_SetAssertionHandler);
  lua_register(L,"SDL_SetClipRect",lSDL_SetClipRect);
  lua_register(L,"SDL_SetClipboardText",lSDL_SetClipboardText);
  lua_register(L,"SDL_SetColorKey",lSDL_SetColorKey);
  lua_register(L,"SDL_SetCursor",lSDL_SetCursor);
  lua_register(L,"SDL_SetError",lSDL_SetError);
  lua_register(L,"SDL_SetEventFilter",lSDL_SetEventFilter);
  lua_register(L,"SDL_SetHint",lSDL_SetHint);
  lua_register(L,"SDL_SetHintWithPriority",lSDL_SetHintWithPriority);
  lua_register(L,"SDL_SetMainReady",lSDL_SetMainReady);
  lua_register(L,"SDL_SetModState",lSDL_SetModState);
  lua_register(L,"SDL_SetPaletteColors",lSDL_SetPaletteColors);
  lua_register(L,"SDL_SetPixelFormatPalette",lSDL_SetPixelFormatPalette);
  lua_register(L,"SDL_SetRelativeMouseMode",lSDL_SetRelativeMouseMode);
  lua_register(L,"SDL_SetRenderDrawBlendMode",lSDL_SetRenderDrawBlendMode);
  lua_register(L,"SDL_SetRenderDrawColor",lSDL_SetRenderDrawColor);
  lua_register(L,"SDL_SetRenderTarget",lSDL_SetRenderTarget);
  lua_register(L,"SDL_SetSurfaceAlphaMod",lSDL_SetSurfaceAlphaMod);
  lua_register(L,"SDL_SetSurfaceBlendMode",lSDL_SetSurfaceBlendMode);
  lua_register(L,"SDL_SetSurfaceColorMod",lSDL_SetSurfaceColorMod);
  lua_register(L,"SDL_SetSurfacePalette",lSDL_SetSurfacePalette);
  lua_register(L,"SDL_SetSurfaceRLE",lSDL_SetSurfaceRLE);
  lua_register(L,"SDL_SetTextInputRect",lSDL_SetTextInputRect);
  lua_register(L,"SDL_SetTextureAlphaMod",lSDL_SetTextureAlphaMod);
  lua_register(L,"SDL_SetTextureBlendMode",lSDL_SetTextureBlendMode);
  lua_register(L,"SDL_SetTextureColorMod",lSDL_SetTextureColorMod);
  lua_register(L,"SDL_SetThreadPriority",lSDL_SetThreadPriority);
  lua_register(L,"SDL_SetWindowBordered",lSDL_SetWindowBordered);
  lua_register(L,"SDL_SetWindowBrightness",lSDL_SetWindowBrightness);
  lua_register(L,"SDL_SetWindowData",lSDL_SetWindowData);
  lua_register(L,"SDL_SetWindowDisplayMode",lSDL_SetWindowDisplayMode);
  lua_register(L,"SDL_SetWindowFullscreen",lSDL_SetWindowFullscreen);
  lua_register(L,"SDL_SetWindowGammaRamp",lSDL_SetWindowGammaRamp);
  lua_register(L,"SDL_SetWindowGrab",lSDL_SetWindowGrab);
  lua_register(L,"SDL_SetWindowHitTest",lSDL_SetWindowHitTest);
  lua_register(L,"SDL_SetWindowIcon",lSDL_SetWindowIcon);
  lua_register(L,"SDL_SetWindowInputFocus",lSDL_SetWindowInputFocus);
  lua_register(L,"SDL_SetWindowMaximumSize",lSDL_SetWindowMaximumSize);
  lua_register(L,"SDL_SetWindowMinimumSize",lSDL_SetWindowMinimumSize);
  lua_register(L,"SDL_SetWindowModalFor",lSDL_SetWindowModalFor);
  lua_register(L,"SDL_SetWindowOpacity",lSDL_SetWindowOpacity);
  lua_register(L,"SDL_SetWindowPosition",lSDL_SetWindowPosition);
  lua_register(L,"SDL_SetWindowResizable",lSDL_SetWindowResizable);
  lua_register(L,"SDL_SetWindowSize",lSDL_SetWindowSize);
  lua_register(L,"SDL_SetWindowTitle",lSDL_SetWindowTitle);
  lua_register(L,"SDL_SetWindowsMessageHook",lSDL_SetWindowsMessageHook);
  lua_register(L,"SDL_ShowCursor",lSDL_ShowCursor);
  lua_register(L,"SDL_ShowMessageBox",lSDL_ShowMessageBox);
  lua_register(L,"SDL_ShowSimpleMessageBox",lSDL_ShowSimpleMessageBox);
  lua_register(L,"SDL_ShowWindow",lSDL_ShowWindow);
  lua_register(L,"SDL_SoftStretch",lSDL_SoftStretch);
  lua_register(L,"SDL_StartTextInput",lSDL_StartTextInput);
  lua_register(L,"SDL_StopTextInput",lSDL_StopTextInput);
  lua_register(L,"SDL_Swap16",lSDL_Swap16);
  lua_register(L,"SDL_Swap32",lSDL_Swap32);
  lua_register(L,"SDL_Swap64",lSDL_Swap64);
  lua_register(L,"SDL_SwapFloat",lSDL_SwapFloat);
  lua_register(L,"SDL_TLSCreate",lSDL_TLSCreate);
  lua_register(L,"SDL_TLSGet",lSDL_TLSGet);
  lua_register(L,"SDL_TLSSet",lSDL_TLSSet);
  lua_register(L,"SDL_ThreadID",lSDL_ThreadID);
  lua_register(L,"SDL_TryLockMutex",lSDL_TryLockMutex);
  lua_register(L,"SDL_UnionRect",lSDL_UnionRect);
  lua_register(L,"SDL_UnloadObject",lSDL_UnloadObject);
  lua_register(L,"SDL_UnlockAudio",lSDL_UnlockAudio);
  lua_register(L,"SDL_UnlockAudioDevice",lSDL_UnlockAudioDevice);
  lua_register(L,"SDL_UnlockMutex",lSDL_UnlockMutex);
  lua_register(L,"SDL_UnlockSurface",lSDL_UnlockSurface);
  lua_register(L,"SDL_UnlockTexture",lSDL_UnlockTexture);
  lua_register(L,"SDL_UnregisterApp",lSDL_UnregisterApp);
  lua_register(L,"SDL_UpdateTexture",lSDL_UpdateTexture);
  lua_register(L,"SDL_UpdateWindowSurface",lSDL_UpdateWindowSurface);
  lua_register(L,"SDL_UpdateWindowSurfaceRects",lSDL_UpdateWindowSurfaceRects);
  lua_register(L,"SDL_UpdateYUVTexture",lSDL_UpdateYUVTexture);
  lua_register(L,"SDL_UpperBlit",lSDL_UpperBlit);
  lua_register(L,"SDL_UpperBlitScaled",lSDL_UpperBlitScaled);
  lua_register(L,"SDL_VideoInit",lSDL_VideoInit);
  lua_register(L,"SDL_VideoQuit",lSDL_VideoQuit);
  lua_register(L,"SDL_WaitEvent",lSDL_WaitEvent);
  lua_register(L,"SDL_WaitEventTimeout",lSDL_WaitEventTimeout);
  lua_register(L,"SDL_WaitThread",lSDL_WaitThread);
  lua_register(L,"SDL_WarpMouseGlobal",lSDL_WarpMouseGlobal);
  lua_register(L,"SDL_WarpMouseInWindow",lSDL_WarpMouseInWindow);
  lua_register(L,"SDL_WasInit",lSDL_WasInit);
  lua_register(L,"SDL_WriteBE16",lSDL_WriteBE16);
  lua_register(L,"SDL_WriteBE32",lSDL_WriteBE32);
  lua_register(L,"SDL_WriteBE64",lSDL_WriteBE64);
  lua_register(L,"SDL_WriteLE16",lSDL_WriteLE16);
  lua_register(L,"SDL_WriteLE32",lSDL_WriteLE32);
  lua_register(L,"SDL_WriteLE64",lSDL_WriteLE64);
  lua_register(L,"SDL_WriteU8",lSDL_WriteU8);
  lua_register(L,"SDL_abs",lSDL_abs);
  lua_register(L,"SDL_acos",lSDL_acos);
  lua_register(L,"SDL_asin",lSDL_asin);
  lua_register(L,"SDL_atan",lSDL_atan);
  lua_register(L,"SDL_atan2",lSDL_atan2);
  lua_register(L,"SDL_atof",lSDL_atof);
  lua_register(L,"SDL_atoi",lSDL_atoi);
  lua_register(L,"SDL_calloc",lSDL_calloc);
  lua_register(L,"SDL_ceil",lSDL_ceil);
  lua_register(L,"SDL_copysign",lSDL_copysign);
  lua_register(L,"SDL_cos",lSDL_cos);
  lua_register(L,"SDL_cosf",lSDL_cosf);
  lua_register(L,"SDL_fabs",lSDL_fabs);
  lua_register(L,"SDL_floor",lSDL_floor);
  lua_register(L,"SDL_free",lSDL_free);
  lua_register(L,"SDL_getenv",lSDL_getenv);
  lua_register(L,"SDL_iconv",lSDL_iconv);
  lua_register(L,"SDL_iconv_close",lSDL_iconv_close);
  lua_register(L,"SDL_iconv_open",lSDL_iconv_open);
  lua_register(L,"SDL_iconv_string",lSDL_iconv_string);
  lua_register(L,"SDL_isdigit",lSDL_isdigit);
  lua_register(L,"SDL_isspace",lSDL_isspace);
  lua_register(L,"SDL_itoa",lSDL_itoa);
  lua_register(L,"SDL_lltoa",lSDL_lltoa);
  lua_register(L,"SDL_log",lSDL_log);
  lua_register(L,"SDL_ltoa",lSDL_ltoa);
  lua_register(L,"SDL_main",lSDL_main);
  lua_register(L,"SDL_malloc",lSDL_malloc);
  lua_register(L,"SDL_memcmp",lSDL_memcmp);
  lua_register(L,"SDL_memcpy",lSDL_memcpy);
  lua_register(L,"SDL_memcpy4",lSDL_memcpy4);
  lua_register(L,"SDL_memmove",lSDL_memmove);
  lua_register(L,"SDL_memset",lSDL_memset);
  lua_register(L,"SDL_memset4",lSDL_memset4);
  lua_register(L,"SDL_pow",lSDL_pow);
  lua_register(L,"SDL_qsort",lSDL_qsort);
  lua_register(L,"SDL_realloc",lSDL_realloc);
  lua_register(L,"SDL_scalbn",lSDL_scalbn);
  lua_register(L,"SDL_setenv",lSDL_setenv);
  lua_register(L,"SDL_sin",lSDL_sin);
  lua_register(L,"SDL_sinf",lSDL_sinf);
  lua_register(L,"SDL_snprintf",lSDL_snprintf);
  lua_register(L,"SDL_sqrt",lSDL_sqrt);
  lua_register(L,"SDL_sqrtf",lSDL_sqrtf);
  lua_register(L,"SDL_sscanf",lSDL_sscanf);
  lua_register(L,"SDL_strcasecmp",lSDL_strcasecmp);
  lua_register(L,"SDL_strchr",lSDL_strchr);
  lua_register(L,"SDL_strcmp",lSDL_strcmp);
  lua_register(L,"SDL_strdup",lSDL_strdup);
  lua_register(L,"SDL_strlcat",lSDL_strlcat);
  lua_register(L,"SDL_strlcpy",lSDL_strlcpy);
  lua_register(L,"SDL_strlen",lSDL_strlen);
  lua_register(L,"SDL_strlwr",lSDL_strlwr);
  lua_register(L,"SDL_strncasecmp",lSDL_strncasecmp);
  lua_register(L,"SDL_strncmp",lSDL_strncmp);
  lua_register(L,"SDL_strrchr",lSDL_strrchr);
  lua_register(L,"SDL_strrev",lSDL_strrev);
  lua_register(L,"SDL_strstr",lSDL_strstr);
  lua_register(L,"SDL_strtod",lSDL_strtod);
  lua_register(L,"SDL_strtol",lSDL_strtol);
  lua_register(L,"SDL_strtoll",lSDL_strtoll);
  lua_register(L,"SDL_strtoul",lSDL_strtoul);
  lua_register(L,"SDL_strtoull",lSDL_strtoull);
  lua_register(L,"SDL_strupr",lSDL_strupr);
  lua_register(L,"SDL_tan",lSDL_tan);
  lua_register(L,"SDL_tanf",lSDL_tanf);
  lua_register(L,"SDL_tolower",lSDL_tolower);
  lua_register(L,"SDL_toupper",lSDL_toupper);
  lua_register(L,"SDL_uitoa",lSDL_uitoa);
  lua_register(L,"SDL_ulltoa",lSDL_ulltoa);
  lua_register(L,"SDL_ultoa",lSDL_ultoa);
  lua_register(L,"SDL_utf8strlcpy",lSDL_utf8strlcpy);
  lua_register(L,"SDL_vsnprintf",lSDL_vsnprintf);
  lua_register(L,"SDL_vsscanf",lSDL_vsscanf);
  lua_register(L,"SDL_wcslcat",lSDL_wcslcat);
  lua_register(L,"SDL_wcslcpy",lSDL_wcslcpy);
  lua_register(L,"SDL_wcslen",lSDL_wcslen);


//enum 
#ifndef lua_registerInt
#define lua_registerInt(L,name,value) lua_pushinteger(L,value); lua_setglobal(L,name);
#endif

  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_BUTTON",1);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_AXIS",2);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_HAT",3);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_COMPATIBILITY",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_ES",4);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_FLIPPED",1);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_NONE",0);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_BUTTON",1);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_AXIS",2);
  lua_registerInt(L,"SDL_CONTROLLER_BINDTYPE_HAT",3);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_TRUE",1);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_FLIP_HORIZONTAL",1);
  lua_registerInt(L,"SDL_FLIP_VERTICAL",2);
  lua_registerInt(L,"SDL_MOUSEWHEEL_NORMAL",0);
  lua_registerInt(L,"SDL_MOUSEWHEEL_FLIPPED",1);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_EFREAD",1);
  lua_registerInt(L,"SDL_EFWRITE",2);
  lua_registerInt(L,"SDL_EFSEEK",3);
  lua_registerInt(L,"SDL_UNSUPPORTED",4);
  lua_registerInt(L,"SDL_LASTERROR",5);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_ASSERTION_BREAK",1);
  lua_registerInt(L,"SDL_ASSERTION_ABORT",2);
  lua_registerInt(L,"SDL_ASSERTION_IGNORE",3);
  lua_registerInt(L,"SDL_ASSERTION_ALWAYS_IGNORE",4);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"SDL_WINDOWEVENT_SHOWN",1);
  lua_registerInt(L,"SDL_WINDOWEVENT_HIDDEN",2);
  lua_registerInt(L,"SDL_WINDOWEVENT_EXPOSED",3);
  lua_registerInt(L,"SDL_WINDOWEVENT_MOVED",4);
  lua_registerInt(L,"SDL_WINDOWEVENT_RESIZED",5);
  lua_registerInt(L,"SDL_WINDOWEVENT_SIZE_CHANGED",6);
  lua_registerInt(L,"SDL_WINDOWEVENT_MINIMIZED",7);
  lua_registerInt(L,"SDL_WINDOWEVENT_MAXIMIZED",8);
  lua_registerInt(L,"SDL_WINDOWEVENT_RESTORED",9);
  lua_registerInt(L,"SDL_WINDOWEVENT_ENTER",10);
  lua_registerInt(L,"SDL_WINDOWEVENT_LEAVE",11);
  lua_registerInt(L,"SDL_WINDOWEVENT_FOCUS_GAINED",12);
  lua_registerInt(L,"SDL_WINDOWEVENT_FOCUS_LOST",13);
  lua_registerInt(L,"SDL_WINDOWEVENT_CLOSE",14);
  lua_registerInt(L,"SDL_WINDOWEVENT_TAKE_FOCUS",15);
  lua_registerInt(L,"SDL_WINDOWEVENT_HIT_TEST",16);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_AUDIO_PLAYING",1);
  lua_registerInt(L,"SDL_AUDIO_PAUSED",2);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_SCANCODE_A",4);
  lua_registerInt(L,"SDL_SCANCODE_B",5);
  lua_registerInt(L,"SDL_SCANCODE_C",6);
  lua_registerInt(L,"SDL_SCANCODE_D",7);
  lua_registerInt(L,"SDL_SCANCODE_E",8);
  lua_registerInt(L,"SDL_SCANCODE_F",9);
  lua_registerInt(L,"SDL_SCANCODE_G",10);
  lua_registerInt(L,"SDL_SCANCODE_H",11);
  lua_registerInt(L,"SDL_SCANCODE_I",12);
  lua_registerInt(L,"SDL_SCANCODE_J",13);
  lua_registerInt(L,"SDL_SCANCODE_K",14);
  lua_registerInt(L,"SDL_SCANCODE_L",15);
  lua_registerInt(L,"SDL_SCANCODE_M",16);
  lua_registerInt(L,"SDL_SCANCODE_N",17);
  lua_registerInt(L,"SDL_SCANCODE_O",18);
  lua_registerInt(L,"SDL_SCANCODE_P",19);
  lua_registerInt(L,"SDL_SCANCODE_Q",20);
  lua_registerInt(L,"SDL_SCANCODE_R",21);
  lua_registerInt(L,"SDL_SCANCODE_S",22);
  lua_registerInt(L,"SDL_SCANCODE_T",23);
  lua_registerInt(L,"SDL_SCANCODE_U",24);
  lua_registerInt(L,"SDL_SCANCODE_V",25);
  lua_registerInt(L,"SDL_SCANCODE_W",26);
  lua_registerInt(L,"SDL_SCANCODE_X",27);
  lua_registerInt(L,"SDL_SCANCODE_Y",28);
  lua_registerInt(L,"SDL_SCANCODE_Z",29);
  lua_registerInt(L,"SDL_SCANCODE_1",30);
  lua_registerInt(L,"SDL_SCANCODE_2",31);
  lua_registerInt(L,"SDL_SCANCODE_3",32);
  lua_registerInt(L,"SDL_SCANCODE_4",33);
  lua_registerInt(L,"SDL_SCANCODE_5",34);
  lua_registerInt(L,"SDL_SCANCODE_6",35);
  lua_registerInt(L,"SDL_SCANCODE_7",36);
  lua_registerInt(L,"SDL_SCANCODE_8",37);
  lua_registerInt(L,"SDL_SCANCODE_9",38);
  lua_registerInt(L,"SDL_SCANCODE_0",39);
  lua_registerInt(L,"SDL_SCANCODE_RETURN",40);
  lua_registerInt(L,"SDL_SCANCODE_ESCAPE",41);
  lua_registerInt(L,"SDL_SCANCODE_BACKSPACE",42);
  lua_registerInt(L,"SDL_SCANCODE_TAB",43);
  lua_registerInt(L,"SDL_SCANCODE_SPACE",44);
  lua_registerInt(L,"SDL_SCANCODE_MINUS",45);
  lua_registerInt(L,"SDL_SCANCODE_EQUALS",46);
  lua_registerInt(L,"SDL_SCANCODE_LEFTBRACKET",47);
  lua_registerInt(L,"SDL_SCANCODE_RIGHTBRACKET",48);
  lua_registerInt(L,"SDL_SCANCODE_BACKSLASH",49);
  lua_registerInt(L,"SDL_SCANCODE_NONUSHASH",50);
  lua_registerInt(L,"SDL_SCANCODE_SEMICOLON",51);
  lua_registerInt(L,"SDL_SCANCODE_APOSTROPHE",52);
  lua_registerInt(L,"SDL_SCANCODE_GRAVE",53);
  lua_registerInt(L,"SDL_SCANCODE_COMMA",54);
  lua_registerInt(L,"SDL_SCANCODE_PERIOD",55);
  lua_registerInt(L,"SDL_SCANCODE_SLASH",56);
  lua_registerInt(L,"SDL_SCANCODE_CAPSLOCK",57);
  lua_registerInt(L,"SDL_SCANCODE_F1",58);
  lua_registerInt(L,"SDL_SCANCODE_F2",59);
  lua_registerInt(L,"SDL_SCANCODE_F3",60);
  lua_registerInt(L,"SDL_SCANCODE_F4",61);
  lua_registerInt(L,"SDL_SCANCODE_F5",62);
  lua_registerInt(L,"SDL_SCANCODE_F6",63);
  lua_registerInt(L,"SDL_SCANCODE_F7",64);
  lua_registerInt(L,"SDL_SCANCODE_F8",65);
  lua_registerInt(L,"SDL_SCANCODE_F9",66);
  lua_registerInt(L,"SDL_SCANCODE_F10",67);
  lua_registerInt(L,"SDL_SCANCODE_F11",68);
  lua_registerInt(L,"SDL_SCANCODE_F12",69);
  lua_registerInt(L,"SDL_SCANCODE_PRINTSCREEN",70);
  lua_registerInt(L,"SDL_SCANCODE_SCROLLLOCK",71);
  lua_registerInt(L,"SDL_SCANCODE_PAUSE",72);
  lua_registerInt(L,"SDL_SCANCODE_INSERT",73);
  lua_registerInt(L,"SDL_SCANCODE_HOME",74);
  lua_registerInt(L,"SDL_SCANCODE_PAGEUP",75);
  lua_registerInt(L,"SDL_SCANCODE_DELETE",76);
  lua_registerInt(L,"SDL_SCANCODE_END",77);
  lua_registerInt(L,"SDL_SCANCODE_PAGEDOWN",78);
  lua_registerInt(L,"SDL_SCANCODE_RIGHT",79);
  lua_registerInt(L,"SDL_SCANCODE_LEFT",80);
  lua_registerInt(L,"SDL_SCANCODE_DOWN",81);
  lua_registerInt(L,"SDL_SCANCODE_UP",82);
  lua_registerInt(L,"SDL_SCANCODE_NUMLOCKCLEAR",83);
  lua_registerInt(L,"SDL_SCANCODE_KP_DIVIDE",84);
  lua_registerInt(L,"SDL_SCANCODE_KP_MULTIPLY",85);
  lua_registerInt(L,"SDL_SCANCODE_KP_MINUS",86);
  lua_registerInt(L,"SDL_SCANCODE_KP_PLUS",87);
  lua_registerInt(L,"SDL_SCANCODE_KP_ENTER",88);
  lua_registerInt(L,"SDL_SCANCODE_KP_1",89);
  lua_registerInt(L,"SDL_SCANCODE_KP_2",90);
  lua_registerInt(L,"SDL_SCANCODE_KP_3",91);
  lua_registerInt(L,"SDL_SCANCODE_KP_4",92);
  lua_registerInt(L,"SDL_SCANCODE_KP_5",93);
  lua_registerInt(L,"SDL_SCANCODE_KP_6",94);
  lua_registerInt(L,"SDL_SCANCODE_KP_7",95);
  lua_registerInt(L,"SDL_SCANCODE_KP_8",96);
  lua_registerInt(L,"SDL_SCANCODE_KP_9",97);
  lua_registerInt(L,"SDL_SCANCODE_KP_0",98);
  lua_registerInt(L,"SDL_SCANCODE_KP_PERIOD",99);
  lua_registerInt(L,"SDL_SCANCODE_NONUSBACKSLASH",100);
  lua_registerInt(L,"SDL_SCANCODE_APPLICATION",101);
  lua_registerInt(L,"SDL_SCANCODE_POWER",102);
  lua_registerInt(L,"SDL_SCANCODE_KP_EQUALS",103);
  lua_registerInt(L,"SDL_SCANCODE_F13",104);
  lua_registerInt(L,"SDL_SCANCODE_F14",105);
  lua_registerInt(L,"SDL_SCANCODE_F15",106);
  lua_registerInt(L,"SDL_SCANCODE_F16",107);
  lua_registerInt(L,"SDL_SCANCODE_F17",108);
  lua_registerInt(L,"SDL_SCANCODE_F18",109);
  lua_registerInt(L,"SDL_SCANCODE_F19",110);
  lua_registerInt(L,"SDL_SCANCODE_F20",111);
  lua_registerInt(L,"SDL_SCANCODE_F21",112);
  lua_registerInt(L,"SDL_SCANCODE_F22",113);
  lua_registerInt(L,"SDL_SCANCODE_F23",114);
  lua_registerInt(L,"SDL_SCANCODE_F24",115);
  lua_registerInt(L,"SDL_SCANCODE_EXECUTE",116);
  lua_registerInt(L,"SDL_SCANCODE_HELP",117);
  lua_registerInt(L,"SDL_SCANCODE_MENU",118);
  lua_registerInt(L,"SDL_SCANCODE_SELECT",119);
  lua_registerInt(L,"SDL_SCANCODE_STOP",120);
  lua_registerInt(L,"SDL_SCANCODE_AGAIN",121);
  lua_registerInt(L,"SDL_SCANCODE_UNDO",122);
  lua_registerInt(L,"SDL_SCANCODE_CUT",123);
  lua_registerInt(L,"SDL_SCANCODE_COPY",124);
  lua_registerInt(L,"SDL_SCANCODE_PASTE",125);
  lua_registerInt(L,"SDL_SCANCODE_FIND",126);
  lua_registerInt(L,"SDL_SCANCODE_MUTE",127);
  lua_registerInt(L,"SDL_SCANCODE_VOLUMEUP",128);
  lua_registerInt(L,"SDL_SCANCODE_VOLUMEDOWN",129);
  lua_registerInt(L,"SDL_SCANCODE_KP_COMMA",133);
  lua_registerInt(L,"SDL_SCANCODE_KP_EQUALSAS400",134);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL1",135);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL2",136);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL3",137);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL4",138);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL5",139);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL6",140);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL7",141);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL8",142);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL9",143);
  lua_registerInt(L,"SDL_SCANCODE_LANG1",144);
  lua_registerInt(L,"SDL_SCANCODE_LANG2",145);
  lua_registerInt(L,"SDL_SCANCODE_LANG3",146);
  lua_registerInt(L,"SDL_SCANCODE_LANG4",147);
  lua_registerInt(L,"SDL_SCANCODE_LANG5",148);
  lua_registerInt(L,"SDL_SCANCODE_LANG6",149);
  lua_registerInt(L,"SDL_SCANCODE_LANG7",150);
  lua_registerInt(L,"SDL_SCANCODE_LANG8",151);
  lua_registerInt(L,"SDL_SCANCODE_LANG9",152);
  lua_registerInt(L,"SDL_SCANCODE_ALTERASE",153);
  lua_registerInt(L,"SDL_SCANCODE_SYSREQ",154);
  lua_registerInt(L,"SDL_SCANCODE_CANCEL",155);
  lua_registerInt(L,"SDL_SCANCODE_CLEAR",156);
  lua_registerInt(L,"SDL_SCANCODE_PRIOR",157);
  lua_registerInt(L,"SDL_SCANCODE_RETURN2",158);
  lua_registerInt(L,"SDL_SCANCODE_SEPARATOR",159);
  lua_registerInt(L,"SDL_SCANCODE_OUT",160);
  lua_registerInt(L,"SDL_SCANCODE_OPER",161);
  lua_registerInt(L,"SDL_SCANCODE_CLEARAGAIN",162);
  lua_registerInt(L,"SDL_SCANCODE_CRSEL",163);
  lua_registerInt(L,"SDL_SCANCODE_EXSEL",164);
  lua_registerInt(L,"SDL_SCANCODE_KP_00",176);
  lua_registerInt(L,"SDL_SCANCODE_KP_000",177);
  lua_registerInt(L,"SDL_SCANCODE_THOUSANDSSEPARATOR",178);
  lua_registerInt(L,"SDL_SCANCODE_DECIMALSEPARATOR",179);
  lua_registerInt(L,"SDL_SCANCODE_CURRENCYUNIT",180);
  lua_registerInt(L,"SDL_SCANCODE_CURRENCYSUBUNIT",181);
  lua_registerInt(L,"SDL_SCANCODE_KP_LEFTPAREN",182);
  lua_registerInt(L,"SDL_SCANCODE_KP_RIGHTPAREN",183);
  lua_registerInt(L,"SDL_SCANCODE_KP_LEFTBRACE",184);
  lua_registerInt(L,"SDL_SCANCODE_KP_RIGHTBRACE",185);
  lua_registerInt(L,"SDL_SCANCODE_KP_TAB",186);
  lua_registerInt(L,"SDL_SCANCODE_KP_BACKSPACE",187);
  lua_registerInt(L,"SDL_SCANCODE_KP_A",188);
  lua_registerInt(L,"SDL_SCANCODE_KP_B",189);
  lua_registerInt(L,"SDL_SCANCODE_KP_C",190);
  lua_registerInt(L,"SDL_SCANCODE_KP_D",191);
  lua_registerInt(L,"SDL_SCANCODE_KP_E",192);
  lua_registerInt(L,"SDL_SCANCODE_KP_F",193);
  lua_registerInt(L,"SDL_SCANCODE_KP_XOR",194);
  lua_registerInt(L,"SDL_SCANCODE_KP_POWER",195);
  lua_registerInt(L,"SDL_SCANCODE_KP_PERCENT",196);
  lua_registerInt(L,"SDL_SCANCODE_KP_LESS",197);
  lua_registerInt(L,"SDL_SCANCODE_KP_GREATER",198);
  lua_registerInt(L,"SDL_SCANCODE_KP_AMPERSAND",199);
  lua_registerInt(L,"SDL_SCANCODE_KP_DBLAMPERSAND",200);
  lua_registerInt(L,"SDL_SCANCODE_KP_VERTICALBAR",201);
  lua_registerInt(L,"SDL_SCANCODE_KP_DBLVERTICALBAR",202);
  lua_registerInt(L,"SDL_SCANCODE_KP_COLON",203);
  lua_registerInt(L,"SDL_SCANCODE_KP_HASH",204);
  lua_registerInt(L,"SDL_SCANCODE_KP_SPACE",205);
  lua_registerInt(L,"SDL_SCANCODE_KP_AT",206);
  lua_registerInt(L,"SDL_SCANCODE_KP_EXCLAM",207);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMSTORE",208);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMRECALL",209);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMCLEAR",210);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMADD",211);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMSUBTRACT",212);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMMULTIPLY",213);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMDIVIDE",214);
  lua_registerInt(L,"SDL_SCANCODE_KP_PLUSMINUS",215);
  lua_registerInt(L,"SDL_SCANCODE_KP_CLEAR",216);
  lua_registerInt(L,"SDL_SCANCODE_KP_CLEARENTRY",217);
  lua_registerInt(L,"SDL_SCANCODE_KP_BINARY",218);
  lua_registerInt(L,"SDL_SCANCODE_KP_OCTAL",219);
  lua_registerInt(L,"SDL_SCANCODE_KP_DECIMAL",220);
  lua_registerInt(L,"SDL_SCANCODE_KP_HEXADECIMAL",221);
  lua_registerInt(L,"SDL_SCANCODE_LCTRL",224);
  lua_registerInt(L,"SDL_SCANCODE_LSHIFT",225);
  lua_registerInt(L,"SDL_SCANCODE_LALT",226);
  lua_registerInt(L,"SDL_SCANCODE_LGUI",227);
  lua_registerInt(L,"SDL_SCANCODE_RCTRL",228);
  lua_registerInt(L,"SDL_SCANCODE_RSHIFT",229);
  lua_registerInt(L,"SDL_SCANCODE_RALT",230);
  lua_registerInt(L,"SDL_SCANCODE_RGUI",231);
  lua_registerInt(L,"SDL_SCANCODE_MODE",257);
  lua_registerInt(L,"SDL_SCANCODE_AUDIONEXT",258);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOPREV",259);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOSTOP",260);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOPLAY",261);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOMUTE",262);
  lua_registerInt(L,"SDL_SCANCODE_MEDIASELECT",263);
  lua_registerInt(L,"SDL_SCANCODE_WWW",264);
  lua_registerInt(L,"SDL_SCANCODE_MAIL",265);
  lua_registerInt(L,"SDL_SCANCODE_CALCULATOR",266);
  lua_registerInt(L,"SDL_SCANCODE_COMPUTER",267);
  lua_registerInt(L,"SDL_SCANCODE_AC_SEARCH",268);
  lua_registerInt(L,"SDL_SCANCODE_AC_HOME",269);
  lua_registerInt(L,"SDL_SCANCODE_AC_BACK",270);
  lua_registerInt(L,"SDL_SCANCODE_AC_FORWARD",271);
  lua_registerInt(L,"SDL_SCANCODE_AC_STOP",272);
  lua_registerInt(L,"SDL_SCANCODE_AC_REFRESH",273);
  lua_registerInt(L,"SDL_SCANCODE_AC_BOOKMARKS",274);
  lua_registerInt(L,"SDL_SCANCODE_BRIGHTNESSDOWN",275);
  lua_registerInt(L,"SDL_SCANCODE_BRIGHTNESSUP",276);
  lua_registerInt(L,"SDL_SCANCODE_DISPLAYSWITCH",277);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMTOGGLE",278);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMDOWN",279);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMUP",280);
  lua_registerInt(L,"SDL_SCANCODE_EJECT",281);
  lua_registerInt(L,"SDL_SCANCODE_SLEEP",282);
  lua_registerInt(L,"SDL_SCANCODE_APP1",283);
  lua_registerInt(L,"SDL_SCANCODE_APP2",284);
  lua_registerInt(L,"SDL_NUM_SCANCODES",512);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"SDL_WINDOW_OPENGL",2);
  lua_registerInt(L,"SDL_WINDOW_SHOWN",4);
  lua_registerInt(L,"SDL_WINDOW_HIDDEN",8);
  lua_registerInt(L,"SDL_WINDOW_BORDERLESS",16);
  lua_registerInt(L,"SDL_WINDOW_RESIZABLE",32);
  lua_registerInt(L,"SDL_WINDOW_MINIMIZED",64);
  lua_registerInt(L,"SDL_WINDOW_MAXIMIZED",128);
  lua_registerInt(L,"SDL_WINDOW_INPUT_GRABBED",256);
  lua_registerInt(L,"SDL_WINDOW_INPUT_FOCUS",512);
  lua_registerInt(L,"SDL_WINDOW_MOUSE_FOCUS",1024);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN_DESKTOP",4097);
  lua_registerInt(L,"SDL_WINDOW_FOREIGN",2048);
  lua_registerInt(L,"SDL_WINDOW_ALLOW_HIGHDPI",8192);
  lua_registerInt(L,"SDL_WINDOW_MOUSE_CAPTURE",16384);
  lua_registerInt(L,"SDL_WINDOW_ALWAYS_ON_TOP",32768);
  lua_registerInt(L,"SDL_WINDOW_SKIP_TASKBAR",65536);
  lua_registerInt(L,"SDL_WINDOW_UTILITY",131072);
  lua_registerInt(L,"SDL_WINDOW_TOOLTIP",262144);
  lua_registerInt(L,"SDL_WINDOW_POPUP_MENU",524288);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_TEXT",1);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_BORDER",2);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND",3);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED",4);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_MAX",5);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_IBEAM",1);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_WAIT",2);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_CROSSHAIR",3);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_WAITARROW",4);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENWSE",5);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENESW",6);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZEWE",7);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENS",8);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZEALL",9);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_NO",10);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_HAND",11);
  lua_registerInt(L,"SDL_NUM_SYSTEM_CURSORS",12);
  lua_registerInt(L,"_MM_MANT_NORM_1_2",0);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN",1);
  lua_registerInt(L,"SDL_WINDOW_OPENGL",2);
  lua_registerInt(L,"SDL_WINDOW_SHOWN",4);
  lua_registerInt(L,"SDL_WINDOW_HIDDEN",8);
  lua_registerInt(L,"SDL_WINDOW_BORDERLESS",16);
  lua_registerInt(L,"SDL_WINDOW_RESIZABLE",32);
  lua_registerInt(L,"SDL_WINDOW_MINIMIZED",64);
  lua_registerInt(L,"SDL_WINDOW_MAXIMIZED",128);
  lua_registerInt(L,"SDL_WINDOW_INPUT_GRABBED",256);
  lua_registerInt(L,"SDL_WINDOW_INPUT_FOCUS",512);
  lua_registerInt(L,"SDL_WINDOW_MOUSE_FOCUS",1024);
  lua_registerInt(L,"SDL_WINDOW_FULLSCREEN_DESKTOP",4097);
  lua_registerInt(L,"SDL_WINDOW_FOREIGN",2048);
  lua_registerInt(L,"SDL_WINDOW_ALLOW_HIGHDPI",8192);
  lua_registerInt(L,"SDL_WINDOW_MOUSE_CAPTURE",16384);
  lua_registerInt(L,"SDL_WINDOW_ALWAYS_ON_TOP",32768);
  lua_registerInt(L,"SDL_WINDOW_SKIP_TASKBAR",65536);
  lua_registerInt(L,"SDL_WINDOW_UTILITY",131072);
  lua_registerInt(L,"SDL_WINDOW_TOOLTIP",262144);
  lua_registerInt(L,"SDL_WINDOW_POPUP_MENU",524288);
  lua_registerInt(L,"SDL_ARRAYORDER_NONE",0);
  lua_registerInt(L,"SDL_ARRAYORDER_RGB",1);
  lua_registerInt(L,"SDL_ARRAYORDER_RGBA",2);
  lua_registerInt(L,"SDL_ARRAYORDER_ARGB",3);
  lua_registerInt(L,"SDL_ARRAYORDER_BGR",4);
  lua_registerInt(L,"SDL_ARRAYORDER_BGRA",5);
  lua_registerInt(L,"SDL_ARRAYORDER_ABGR",6);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_HITTEST_DRAGGABLE",1);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOPLEFT",2);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOP",3);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOPRIGHT",4);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_RIGHT",5);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOMRIGHT",6);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOM",7);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOMLEFT",8);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_LEFT",9);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_BLENDMODE_BLEND",1);
  lua_registerInt(L,"SDL_BLENDMODE_ADD",2);
  lua_registerInt(L,"SDL_BLENDMODE_MOD",4);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH",1);
  lua_registerInt(L,"_MM_MANT_SIGN_src",0);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_PACKEDORDER_NONE",0);
  lua_registerInt(L,"SDL_PACKEDORDER_XRGB",1);
  lua_registerInt(L,"SDL_PACKEDORDER_RGBX",2);
  lua_registerInt(L,"SDL_PACKEDORDER_ARGB",3);
  lua_registerInt(L,"SDL_PACKEDORDER_RGBA",4);
  lua_registerInt(L,"SDL_PACKEDORDER_XBGR",5);
  lua_registerInt(L,"SDL_PACKEDORDER_BGRX",6);
  lua_registerInt(L,"SDL_PACKEDORDER_ABGR",7);
  lua_registerInt(L,"SDL_PACKEDORDER_BGRA",8);
  lua_registerInt(L,"SDL_WINDOWEVENT_NONE",0);
  lua_registerInt(L,"SDL_WINDOWEVENT_SHOWN",1);
  lua_registerInt(L,"SDL_WINDOWEVENT_HIDDEN",2);
  lua_registerInt(L,"SDL_WINDOWEVENT_EXPOSED",3);
  lua_registerInt(L,"SDL_WINDOWEVENT_MOVED",4);
  lua_registerInt(L,"SDL_WINDOWEVENT_RESIZED",5);
  lua_registerInt(L,"SDL_WINDOWEVENT_SIZE_CHANGED",6);
  lua_registerInt(L,"SDL_WINDOWEVENT_MINIMIZED",7);
  lua_registerInt(L,"SDL_WINDOWEVENT_MAXIMIZED",8);
  lua_registerInt(L,"SDL_WINDOWEVENT_RESTORED",9);
  lua_registerInt(L,"SDL_WINDOWEVENT_ENTER",10);
  lua_registerInt(L,"SDL_WINDOWEVENT_LEAVE",11);
  lua_registerInt(L,"SDL_WINDOWEVENT_FOCUS_GAINED",12);
  lua_registerInt(L,"SDL_WINDOWEVENT_FOCUS_LOST",13);
  lua_registerInt(L,"SDL_WINDOWEVENT_CLOSE",14);
  lua_registerInt(L,"SDL_WINDOWEVENT_TAKE_FOCUS",15);
  lua_registerInt(L,"SDL_WINDOWEVENT_HIT_TEST",16);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT",2);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_A",0);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_B",1);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_X",2);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_Y",3);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_BACK",4);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_GUIDE",5);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_START",6);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_LEFTSTICK",7);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_RIGHTSTICK",8);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_LEFTSHOULDER",9);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_RIGHTSHOULDER",10);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_UP",11);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_DOWN",12);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_LEFT",13);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_RIGHT",14);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_MAX",15);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_INVALID",-1);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_A",0);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_B",1);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_X",2);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_Y",3);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_BACK",4);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_GUIDE",5);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_START",6);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_LEFTSTICK",7);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_RIGHTSTICK",8);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_LEFTSHOULDER",9);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_RIGHTSHOULDER",10);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_UP",11);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_DOWN",12);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_LEFT",13);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_DPAD_RIGHT",14);
  lua_registerInt(L,"SDL_CONTROLLER_BUTTON_MAX",15);
  lua_registerInt(L,"SDL_PIXELTYPE_UNKNOWN",0);
  lua_registerInt(L,"SDL_PIXELTYPE_INDEX1",1);
  lua_registerInt(L,"SDL_PIXELTYPE_INDEX4",2);
  lua_registerInt(L,"SDL_PIXELTYPE_INDEX8",3);
  lua_registerInt(L,"SDL_PIXELTYPE_PACKED8",4);
  lua_registerInt(L,"SDL_PIXELTYPE_PACKED16",5);
  lua_registerInt(L,"SDL_PIXELTYPE_PACKED32",6);
  lua_registerInt(L,"SDL_PIXELTYPE_ARRAYU8",7);
  lua_registerInt(L,"SDL_PIXELTYPE_ARRAYU16",8);
  lua_registerInt(L,"SDL_PIXELTYPE_ARRAYU32",9);
  lua_registerInt(L,"SDL_PIXELTYPE_ARRAYF16",10);
  lua_registerInt(L,"SDL_PIXELTYPE_ARRAYF32",11);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG",4);
  lua_registerInt(L,"SDL_GL_CONTEXT_RESET_ISOLATION_FLAG",8);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STREAMING",1);
  lua_registerInt(L,"SDL_TEXTUREACCESS_TARGET",2);
  lua_registerInt(L,"SDL_BITMAPORDER_NONE",0);
  lua_registerInt(L,"SDL_BITMAPORDER_4321",1);
  lua_registerInt(L,"SDL_BITMAPORDER_1234",2);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_MESSAGEBOX_WARNING",32);
  lua_registerInt(L,"SDL_MESSAGEBOX_INFORMATION",64);
  lua_registerInt(L,"KMOD_NONE",0);
  lua_registerInt(L,"SDL_ADDEVENT",0);
  lua_registerInt(L,"SDL_PEEKEVENT",1);
  lua_registerInt(L,"SDL_GETEVENT",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE",0);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH",1);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_NONE",0);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_332",1);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_4444",2);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_1555",3);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_5551",4);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_565",5);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_8888",6);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_2101010",7);
  lua_registerInt(L,"SDL_PACKEDLAYOUT_1010102",8);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_FLIP_HORIZONTAL",1);
  lua_registerInt(L,"SDL_FLIP_VERTICAL",2);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_COLOR",1);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_ALPHA",2);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"SDL_GL_GREEN_SIZE",1);
  lua_registerInt(L,"SDL_GL_BLUE_SIZE",2);
  lua_registerInt(L,"SDL_GL_ALPHA_SIZE",3);
  lua_registerInt(L,"SDL_GL_BUFFER_SIZE",4);
  lua_registerInt(L,"SDL_GL_DOUBLEBUFFER",5);
  lua_registerInt(L,"SDL_GL_DEPTH_SIZE",6);
  lua_registerInt(L,"SDL_GL_STENCIL_SIZE",7);
  lua_registerInt(L,"SDL_GL_ACCUM_RED_SIZE",8);
  lua_registerInt(L,"SDL_GL_ACCUM_GREEN_SIZE",9);
  lua_registerInt(L,"SDL_GL_ACCUM_BLUE_SIZE",10);
  lua_registerInt(L,"SDL_GL_ACCUM_ALPHA_SIZE",11);
  lua_registerInt(L,"SDL_GL_STEREO",12);
  lua_registerInt(L,"SDL_GL_MULTISAMPLEBUFFERS",13);
  lua_registerInt(L,"SDL_GL_MULTISAMPLESAMPLES",14);
  lua_registerInt(L,"SDL_GL_ACCELERATED_VISUAL",15);
  lua_registerInt(L,"SDL_GL_RETAINED_BACKING",16);
  lua_registerInt(L,"SDL_GL_CONTEXT_MAJOR_VERSION",17);
  lua_registerInt(L,"SDL_GL_CONTEXT_MINOR_VERSION",18);
  lua_registerInt(L,"SDL_GL_CONTEXT_EGL",19);
  lua_registerInt(L,"SDL_GL_CONTEXT_FLAGS",20);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_MASK",21);
  lua_registerInt(L,"SDL_GL_SHARE_WITH_CURRENT_CONTEXT",22);
  lua_registerInt(L,"SDL_GL_FRAMEBUFFER_SRGB_CAPABLE",23);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR",24);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_LEFTX",0);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_LEFTY",1);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_RIGHTX",2);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_RIGHTY",3);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_TRIGGERLEFT",4);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_TRIGGERRIGHT",5);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_MAX",6);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"SDL_QUIT",256);
  lua_registerInt(L,"SDL_APP_TERMINATING",257);
  lua_registerInt(L,"SDL_APP_LOWMEMORY",258);
  lua_registerInt(L,"SDL_APP_WILLENTERBACKGROUND",259);
  lua_registerInt(L,"SDL_APP_DIDENTERBACKGROUND",260);
  lua_registerInt(L,"SDL_APP_WILLENTERFOREGROUND",261);
  lua_registerInt(L,"SDL_APP_DIDENTERFOREGROUND",262);
  lua_registerInt(L,"SDL_WINDOWEVENT",512);
  lua_registerInt(L,"SDL_SYSWMEVENT",513);
  lua_registerInt(L,"SDL_KEYDOWN",768);
  lua_registerInt(L,"SDL_KEYUP",769);
  lua_registerInt(L,"SDL_TEXTEDITING",770);
  lua_registerInt(L,"SDL_TEXTINPUT",771);
  lua_registerInt(L,"SDL_KEYMAPCHANGED",772);
  lua_registerInt(L,"SDL_MOUSEMOTION",1024);
  lua_registerInt(L,"SDL_MOUSEBUTTONDOWN",1025);
  lua_registerInt(L,"SDL_MOUSEBUTTONUP",1026);
  lua_registerInt(L,"SDL_MOUSEWHEEL",1027);
  lua_registerInt(L,"SDL_JOYAXISMOTION",1536);
  lua_registerInt(L,"SDL_JOYBALLMOTION",1537);
  lua_registerInt(L,"SDL_JOYHATMOTION",1538);
  lua_registerInt(L,"SDL_JOYBUTTONDOWN",1539);
  lua_registerInt(L,"SDL_JOYBUTTONUP",1540);
  lua_registerInt(L,"SDL_JOYDEVICEADDED",1541);
  lua_registerInt(L,"SDL_JOYDEVICEREMOVED",1542);
  lua_registerInt(L,"SDL_CONTROLLERAXISMOTION",1616);
  lua_registerInt(L,"SDL_CONTROLLERBUTTONDOWN",1617);
  lua_registerInt(L,"SDL_CONTROLLERBUTTONUP",1618);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEADDED",1619);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEREMOVED",1620);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEREMAPPED",1621);
  lua_registerInt(L,"SDL_FINGERDOWN",1792);
  lua_registerInt(L,"SDL_FINGERUP",1793);
  lua_registerInt(L,"SDL_FINGERMOTION",1794);
  lua_registerInt(L,"SDL_DOLLARGESTURE",2048);
  lua_registerInt(L,"SDL_DOLLARRECORD",2049);
  lua_registerInt(L,"SDL_MULTIGESTURE",2050);
  lua_registerInt(L,"SDL_CLIPBOARDUPDATE",2304);
  lua_registerInt(L,"SDL_DROPFILE",4096);
  lua_registerInt(L,"SDL_DROPTEXT",4097);
  lua_registerInt(L,"SDL_DROPBEGIN",4098);
  lua_registerInt(L,"SDL_DROPCOMPLETE",4099);
  lua_registerInt(L,"SDL_AUDIODEVICEADDED",4352);
  lua_registerInt(L,"SDL_AUDIODEVICEREMOVED",4353);
  lua_registerInt(L,"SDL_RENDER_TARGETS_RESET",8192);
  lua_registerInt(L,"SDL_RENDER_DEVICE_RESET",8193);
  lua_registerInt(L,"SDL_USEREVENT",32768);
  lua_registerInt(L,"SDL_LASTEVENT",65535);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_CORE",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_COMPATIBILITY",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_ES",4);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BACKGROUND",0);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_TEXT",1);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_BORDER",2);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND",3);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED",4);
  lua_registerInt(L,"SDL_MESSAGEBOX_COLOR_MAX",5);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_NORMAL",1);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_HIGH",2);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"SDL_LOG_CATEGORY_APPLICATION",0);
  lua_registerInt(L,"SDL_LOG_CATEGORY_ERROR",1);
  lua_registerInt(L,"SDL_LOG_CATEGORY_ASSERT",2);
  lua_registerInt(L,"SDL_LOG_CATEGORY_SYSTEM",3);
  lua_registerInt(L,"SDL_LOG_CATEGORY_AUDIO",4);
  lua_registerInt(L,"SDL_LOG_CATEGORY_VIDEO",5);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RENDER",6);
  lua_registerInt(L,"SDL_LOG_CATEGORY_INPUT",7);
  lua_registerInt(L,"SDL_LOG_CATEGORY_TEST",8);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED1",9);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED2",10);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED3",11);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED4",12);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED5",13);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED6",14);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED7",15);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED8",16);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED9",17);
  lua_registerInt(L,"SDL_LOG_CATEGORY_RESERVED10",18);
  lua_registerInt(L,"SDL_LOG_CATEGORY_CUSTOM",19);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_LOW",0);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_NORMAL",1);
  lua_registerInt(L,"SDL_THREAD_PRIORITY_HIGH",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_DEBUG_FLAG",1);
  lua_registerInt(L,"SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG",2);
  lua_registerInt(L,"SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG",4);
  lua_registerInt(L,"SDL_GL_CONTEXT_RESET_ISOLATION_FLAG",8);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_HINT_NORMAL",1);
  lua_registerInt(L,"SDL_HINT_OVERRIDE",2);
  lua_registerInt(L,"SDL_AUDIO_STOPPED",0);
  lua_registerInt(L,"SDL_AUDIO_PLAYING",1);
  lua_registerInt(L,"SDL_AUDIO_PAUSED",2);
  lua_registerInt(L,"_MM_PERM_AAAA",0);
  lua_registerInt(L,"SDL_HITTEST_NORMAL",0);
  lua_registerInt(L,"SDL_HITTEST_DRAGGABLE",1);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOPLEFT",2);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOP",3);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_TOPRIGHT",4);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_RIGHT",5);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOMRIGHT",6);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOM",7);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_BOTTOMLEFT",8);
  lua_registerInt(L,"SDL_HITTEST_RESIZE_LEFT",9);
  lua_registerInt(L,"SDL_SCANCODE_UNKNOWN",0);
  lua_registerInt(L,"SDL_SCANCODE_A",4);
  lua_registerInt(L,"SDL_SCANCODE_B",5);
  lua_registerInt(L,"SDL_SCANCODE_C",6);
  lua_registerInt(L,"SDL_SCANCODE_D",7);
  lua_registerInt(L,"SDL_SCANCODE_E",8);
  lua_registerInt(L,"SDL_SCANCODE_F",9);
  lua_registerInt(L,"SDL_SCANCODE_G",10);
  lua_registerInt(L,"SDL_SCANCODE_H",11);
  lua_registerInt(L,"SDL_SCANCODE_I",12);
  lua_registerInt(L,"SDL_SCANCODE_J",13);
  lua_registerInt(L,"SDL_SCANCODE_K",14);
  lua_registerInt(L,"SDL_SCANCODE_L",15);
  lua_registerInt(L,"SDL_SCANCODE_M",16);
  lua_registerInt(L,"SDL_SCANCODE_N",17);
  lua_registerInt(L,"SDL_SCANCODE_O",18);
  lua_registerInt(L,"SDL_SCANCODE_P",19);
  lua_registerInt(L,"SDL_SCANCODE_Q",20);
  lua_registerInt(L,"SDL_SCANCODE_R",21);
  lua_registerInt(L,"SDL_SCANCODE_S",22);
  lua_registerInt(L,"SDL_SCANCODE_T",23);
  lua_registerInt(L,"SDL_SCANCODE_U",24);
  lua_registerInt(L,"SDL_SCANCODE_V",25);
  lua_registerInt(L,"SDL_SCANCODE_W",26);
  lua_registerInt(L,"SDL_SCANCODE_X",27);
  lua_registerInt(L,"SDL_SCANCODE_Y",28);
  lua_registerInt(L,"SDL_SCANCODE_Z",29);
  lua_registerInt(L,"SDL_SCANCODE_1",30);
  lua_registerInt(L,"SDL_SCANCODE_2",31);
  lua_registerInt(L,"SDL_SCANCODE_3",32);
  lua_registerInt(L,"SDL_SCANCODE_4",33);
  lua_registerInt(L,"SDL_SCANCODE_5",34);
  lua_registerInt(L,"SDL_SCANCODE_6",35);
  lua_registerInt(L,"SDL_SCANCODE_7",36);
  lua_registerInt(L,"SDL_SCANCODE_8",37);
  lua_registerInt(L,"SDL_SCANCODE_9",38);
  lua_registerInt(L,"SDL_SCANCODE_0",39);
  lua_registerInt(L,"SDL_SCANCODE_RETURN",40);
  lua_registerInt(L,"SDL_SCANCODE_ESCAPE",41);
  lua_registerInt(L,"SDL_SCANCODE_BACKSPACE",42);
  lua_registerInt(L,"SDL_SCANCODE_TAB",43);
  lua_registerInt(L,"SDL_SCANCODE_SPACE",44);
  lua_registerInt(L,"SDL_SCANCODE_MINUS",45);
  lua_registerInt(L,"SDL_SCANCODE_EQUALS",46);
  lua_registerInt(L,"SDL_SCANCODE_LEFTBRACKET",47);
  lua_registerInt(L,"SDL_SCANCODE_RIGHTBRACKET",48);
  lua_registerInt(L,"SDL_SCANCODE_BACKSLASH",49);
  lua_registerInt(L,"SDL_SCANCODE_NONUSHASH",50);
  lua_registerInt(L,"SDL_SCANCODE_SEMICOLON",51);
  lua_registerInt(L,"SDL_SCANCODE_APOSTROPHE",52);
  lua_registerInt(L,"SDL_SCANCODE_GRAVE",53);
  lua_registerInt(L,"SDL_SCANCODE_COMMA",54);
  lua_registerInt(L,"SDL_SCANCODE_PERIOD",55);
  lua_registerInt(L,"SDL_SCANCODE_SLASH",56);
  lua_registerInt(L,"SDL_SCANCODE_CAPSLOCK",57);
  lua_registerInt(L,"SDL_SCANCODE_F1",58);
  lua_registerInt(L,"SDL_SCANCODE_F2",59);
  lua_registerInt(L,"SDL_SCANCODE_F3",60);
  lua_registerInt(L,"SDL_SCANCODE_F4",61);
  lua_registerInt(L,"SDL_SCANCODE_F5",62);
  lua_registerInt(L,"SDL_SCANCODE_F6",63);
  lua_registerInt(L,"SDL_SCANCODE_F7",64);
  lua_registerInt(L,"SDL_SCANCODE_F8",65);
  lua_registerInt(L,"SDL_SCANCODE_F9",66);
  lua_registerInt(L,"SDL_SCANCODE_F10",67);
  lua_registerInt(L,"SDL_SCANCODE_F11",68);
  lua_registerInt(L,"SDL_SCANCODE_F12",69);
  lua_registerInt(L,"SDL_SCANCODE_PRINTSCREEN",70);
  lua_registerInt(L,"SDL_SCANCODE_SCROLLLOCK",71);
  lua_registerInt(L,"SDL_SCANCODE_PAUSE",72);
  lua_registerInt(L,"SDL_SCANCODE_INSERT",73);
  lua_registerInt(L,"SDL_SCANCODE_HOME",74);
  lua_registerInt(L,"SDL_SCANCODE_PAGEUP",75);
  lua_registerInt(L,"SDL_SCANCODE_DELETE",76);
  lua_registerInt(L,"SDL_SCANCODE_END",77);
  lua_registerInt(L,"SDL_SCANCODE_PAGEDOWN",78);
  lua_registerInt(L,"SDL_SCANCODE_RIGHT",79);
  lua_registerInt(L,"SDL_SCANCODE_LEFT",80);
  lua_registerInt(L,"SDL_SCANCODE_DOWN",81);
  lua_registerInt(L,"SDL_SCANCODE_UP",82);
  lua_registerInt(L,"SDL_SCANCODE_NUMLOCKCLEAR",83);
  lua_registerInt(L,"SDL_SCANCODE_KP_DIVIDE",84);
  lua_registerInt(L,"SDL_SCANCODE_KP_MULTIPLY",85);
  lua_registerInt(L,"SDL_SCANCODE_KP_MINUS",86);
  lua_registerInt(L,"SDL_SCANCODE_KP_PLUS",87);
  lua_registerInt(L,"SDL_SCANCODE_KP_ENTER",88);
  lua_registerInt(L,"SDL_SCANCODE_KP_1",89);
  lua_registerInt(L,"SDL_SCANCODE_KP_2",90);
  lua_registerInt(L,"SDL_SCANCODE_KP_3",91);
  lua_registerInt(L,"SDL_SCANCODE_KP_4",92);
  lua_registerInt(L,"SDL_SCANCODE_KP_5",93);
  lua_registerInt(L,"SDL_SCANCODE_KP_6",94);
  lua_registerInt(L,"SDL_SCANCODE_KP_7",95);
  lua_registerInt(L,"SDL_SCANCODE_KP_8",96);
  lua_registerInt(L,"SDL_SCANCODE_KP_9",97);
  lua_registerInt(L,"SDL_SCANCODE_KP_0",98);
  lua_registerInt(L,"SDL_SCANCODE_KP_PERIOD",99);
  lua_registerInt(L,"SDL_SCANCODE_NONUSBACKSLASH",100);
  lua_registerInt(L,"SDL_SCANCODE_APPLICATION",101);
  lua_registerInt(L,"SDL_SCANCODE_POWER",102);
  lua_registerInt(L,"SDL_SCANCODE_KP_EQUALS",103);
  lua_registerInt(L,"SDL_SCANCODE_F13",104);
  lua_registerInt(L,"SDL_SCANCODE_F14",105);
  lua_registerInt(L,"SDL_SCANCODE_F15",106);
  lua_registerInt(L,"SDL_SCANCODE_F16",107);
  lua_registerInt(L,"SDL_SCANCODE_F17",108);
  lua_registerInt(L,"SDL_SCANCODE_F18",109);
  lua_registerInt(L,"SDL_SCANCODE_F19",110);
  lua_registerInt(L,"SDL_SCANCODE_F20",111);
  lua_registerInt(L,"SDL_SCANCODE_F21",112);
  lua_registerInt(L,"SDL_SCANCODE_F22",113);
  lua_registerInt(L,"SDL_SCANCODE_F23",114);
  lua_registerInt(L,"SDL_SCANCODE_F24",115);
  lua_registerInt(L,"SDL_SCANCODE_EXECUTE",116);
  lua_registerInt(L,"SDL_SCANCODE_HELP",117);
  lua_registerInt(L,"SDL_SCANCODE_MENU",118);
  lua_registerInt(L,"SDL_SCANCODE_SELECT",119);
  lua_registerInt(L,"SDL_SCANCODE_STOP",120);
  lua_registerInt(L,"SDL_SCANCODE_AGAIN",121);
  lua_registerInt(L,"SDL_SCANCODE_UNDO",122);
  lua_registerInt(L,"SDL_SCANCODE_CUT",123);
  lua_registerInt(L,"SDL_SCANCODE_COPY",124);
  lua_registerInt(L,"SDL_SCANCODE_PASTE",125);
  lua_registerInt(L,"SDL_SCANCODE_FIND",126);
  lua_registerInt(L,"SDL_SCANCODE_MUTE",127);
  lua_registerInt(L,"SDL_SCANCODE_VOLUMEUP",128);
  lua_registerInt(L,"SDL_SCANCODE_VOLUMEDOWN",129);
  lua_registerInt(L,"SDL_SCANCODE_KP_COMMA",133);
  lua_registerInt(L,"SDL_SCANCODE_KP_EQUALSAS400",134);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL1",135);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL2",136);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL3",137);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL4",138);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL5",139);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL6",140);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL7",141);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL8",142);
  lua_registerInt(L,"SDL_SCANCODE_INTERNATIONAL9",143);
  lua_registerInt(L,"SDL_SCANCODE_LANG1",144);
  lua_registerInt(L,"SDL_SCANCODE_LANG2",145);
  lua_registerInt(L,"SDL_SCANCODE_LANG3",146);
  lua_registerInt(L,"SDL_SCANCODE_LANG4",147);
  lua_registerInt(L,"SDL_SCANCODE_LANG5",148);
  lua_registerInt(L,"SDL_SCANCODE_LANG6",149);
  lua_registerInt(L,"SDL_SCANCODE_LANG7",150);
  lua_registerInt(L,"SDL_SCANCODE_LANG8",151);
  lua_registerInt(L,"SDL_SCANCODE_LANG9",152);
  lua_registerInt(L,"SDL_SCANCODE_ALTERASE",153);
  lua_registerInt(L,"SDL_SCANCODE_SYSREQ",154);
  lua_registerInt(L,"SDL_SCANCODE_CANCEL",155);
  lua_registerInt(L,"SDL_SCANCODE_CLEAR",156);
  lua_registerInt(L,"SDL_SCANCODE_PRIOR",157);
  lua_registerInt(L,"SDL_SCANCODE_RETURN2",158);
  lua_registerInt(L,"SDL_SCANCODE_SEPARATOR",159);
  lua_registerInt(L,"SDL_SCANCODE_OUT",160);
  lua_registerInt(L,"SDL_SCANCODE_OPER",161);
  lua_registerInt(L,"SDL_SCANCODE_CLEARAGAIN",162);
  lua_registerInt(L,"SDL_SCANCODE_CRSEL",163);
  lua_registerInt(L,"SDL_SCANCODE_EXSEL",164);
  lua_registerInt(L,"SDL_SCANCODE_KP_00",176);
  lua_registerInt(L,"SDL_SCANCODE_KP_000",177);
  lua_registerInt(L,"SDL_SCANCODE_THOUSANDSSEPARATOR",178);
  lua_registerInt(L,"SDL_SCANCODE_DECIMALSEPARATOR",179);
  lua_registerInt(L,"SDL_SCANCODE_CURRENCYUNIT",180);
  lua_registerInt(L,"SDL_SCANCODE_CURRENCYSUBUNIT",181);
  lua_registerInt(L,"SDL_SCANCODE_KP_LEFTPAREN",182);
  lua_registerInt(L,"SDL_SCANCODE_KP_RIGHTPAREN",183);
  lua_registerInt(L,"SDL_SCANCODE_KP_LEFTBRACE",184);
  lua_registerInt(L,"SDL_SCANCODE_KP_RIGHTBRACE",185);
  lua_registerInt(L,"SDL_SCANCODE_KP_TAB",186);
  lua_registerInt(L,"SDL_SCANCODE_KP_BACKSPACE",187);
  lua_registerInt(L,"SDL_SCANCODE_KP_A",188);
  lua_registerInt(L,"SDL_SCANCODE_KP_B",189);
  lua_registerInt(L,"SDL_SCANCODE_KP_C",190);
  lua_registerInt(L,"SDL_SCANCODE_KP_D",191);
  lua_registerInt(L,"SDL_SCANCODE_KP_E",192);
  lua_registerInt(L,"SDL_SCANCODE_KP_F",193);
  lua_registerInt(L,"SDL_SCANCODE_KP_XOR",194);
  lua_registerInt(L,"SDL_SCANCODE_KP_POWER",195);
  lua_registerInt(L,"SDL_SCANCODE_KP_PERCENT",196);
  lua_registerInt(L,"SDL_SCANCODE_KP_LESS",197);
  lua_registerInt(L,"SDL_SCANCODE_KP_GREATER",198);
  lua_registerInt(L,"SDL_SCANCODE_KP_AMPERSAND",199);
  lua_registerInt(L,"SDL_SCANCODE_KP_DBLAMPERSAND",200);
  lua_registerInt(L,"SDL_SCANCODE_KP_VERTICALBAR",201);
  lua_registerInt(L,"SDL_SCANCODE_KP_DBLVERTICALBAR",202);
  lua_registerInt(L,"SDL_SCANCODE_KP_COLON",203);
  lua_registerInt(L,"SDL_SCANCODE_KP_HASH",204);
  lua_registerInt(L,"SDL_SCANCODE_KP_SPACE",205);
  lua_registerInt(L,"SDL_SCANCODE_KP_AT",206);
  lua_registerInt(L,"SDL_SCANCODE_KP_EXCLAM",207);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMSTORE",208);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMRECALL",209);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMCLEAR",210);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMADD",211);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMSUBTRACT",212);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMMULTIPLY",213);
  lua_registerInt(L,"SDL_SCANCODE_KP_MEMDIVIDE",214);
  lua_registerInt(L,"SDL_SCANCODE_KP_PLUSMINUS",215);
  lua_registerInt(L,"SDL_SCANCODE_KP_CLEAR",216);
  lua_registerInt(L,"SDL_SCANCODE_KP_CLEARENTRY",217);
  lua_registerInt(L,"SDL_SCANCODE_KP_BINARY",218);
  lua_registerInt(L,"SDL_SCANCODE_KP_OCTAL",219);
  lua_registerInt(L,"SDL_SCANCODE_KP_DECIMAL",220);
  lua_registerInt(L,"SDL_SCANCODE_KP_HEXADECIMAL",221);
  lua_registerInt(L,"SDL_SCANCODE_LCTRL",224);
  lua_registerInt(L,"SDL_SCANCODE_LSHIFT",225);
  lua_registerInt(L,"SDL_SCANCODE_LALT",226);
  lua_registerInt(L,"SDL_SCANCODE_LGUI",227);
  lua_registerInt(L,"SDL_SCANCODE_RCTRL",228);
  lua_registerInt(L,"SDL_SCANCODE_RSHIFT",229);
  lua_registerInt(L,"SDL_SCANCODE_RALT",230);
  lua_registerInt(L,"SDL_SCANCODE_RGUI",231);
  lua_registerInt(L,"SDL_SCANCODE_MODE",257);
  lua_registerInt(L,"SDL_SCANCODE_AUDIONEXT",258);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOPREV",259);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOSTOP",260);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOPLAY",261);
  lua_registerInt(L,"SDL_SCANCODE_AUDIOMUTE",262);
  lua_registerInt(L,"SDL_SCANCODE_MEDIASELECT",263);
  lua_registerInt(L,"SDL_SCANCODE_WWW",264);
  lua_registerInt(L,"SDL_SCANCODE_MAIL",265);
  lua_registerInt(L,"SDL_SCANCODE_CALCULATOR",266);
  lua_registerInt(L,"SDL_SCANCODE_COMPUTER",267);
  lua_registerInt(L,"SDL_SCANCODE_AC_SEARCH",268);
  lua_registerInt(L,"SDL_SCANCODE_AC_HOME",269);
  lua_registerInt(L,"SDL_SCANCODE_AC_BACK",270);
  lua_registerInt(L,"SDL_SCANCODE_AC_FORWARD",271);
  lua_registerInt(L,"SDL_SCANCODE_AC_STOP",272);
  lua_registerInt(L,"SDL_SCANCODE_AC_REFRESH",273);
  lua_registerInt(L,"SDL_SCANCODE_AC_BOOKMARKS",274);
  lua_registerInt(L,"SDL_SCANCODE_BRIGHTNESSDOWN",275);
  lua_registerInt(L,"SDL_SCANCODE_BRIGHTNESSUP",276);
  lua_registerInt(L,"SDL_SCANCODE_DISPLAYSWITCH",277);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMTOGGLE",278);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMDOWN",279);
  lua_registerInt(L,"SDL_SCANCODE_KBDILLUMUP",280);
  lua_registerInt(L,"SDL_SCANCODE_EJECT",281);
  lua_registerInt(L,"SDL_SCANCODE_SLEEP",282);
  lua_registerInt(L,"SDL_SCANCODE_APP1",283);
  lua_registerInt(L,"SDL_SCANCODE_APP2",284);
  lua_registerInt(L,"SDL_NUM_SCANCODES",512);
  lua_registerInt(L,"DUMMY_ENUM_VALUE",0);
  lua_registerInt(L,"SDL_HINT_DEFAULT",0);
  lua_registerInt(L,"SDL_HINT_NORMAL",1);
  lua_registerInt(L,"SDL_HINT_OVERRIDE",2);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_NONE",0);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_COLOR",1);
  lua_registerInt(L,"SDL_TEXTUREMODULATE_ALPHA",2);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"SDL_LOG_PRIORITY_DEBUG",2);
  lua_registerInt(L,"SDL_LOG_PRIORITY_INFO",3);
  lua_registerInt(L,"SDL_LOG_PRIORITY_WARN",4);
  lua_registerInt(L,"SDL_LOG_PRIORITY_ERROR",5);
  lua_registerInt(L,"SDL_LOG_PRIORITY_CRITICAL",6);
  lua_registerInt(L,"SDL_NUM_LOG_PRIORITIES",7);
  lua_registerInt(L,"SDL_FIRSTEVENT",0);
  lua_registerInt(L,"SDL_QUIT",256);
  lua_registerInt(L,"SDL_APP_TERMINATING",257);
  lua_registerInt(L,"SDL_APP_LOWMEMORY",258);
  lua_registerInt(L,"SDL_APP_WILLENTERBACKGROUND",259);
  lua_registerInt(L,"SDL_APP_DIDENTERBACKGROUND",260);
  lua_registerInt(L,"SDL_APP_WILLENTERFOREGROUND",261);
  lua_registerInt(L,"SDL_APP_DIDENTERFOREGROUND",262);
  lua_registerInt(L,"SDL_WINDOWEVENT",512);
  lua_registerInt(L,"SDL_SYSWMEVENT",513);
  lua_registerInt(L,"SDL_KEYDOWN",768);
  lua_registerInt(L,"SDL_KEYUP",769);
  lua_registerInt(L,"SDL_TEXTEDITING",770);
  lua_registerInt(L,"SDL_TEXTINPUT",771);
  lua_registerInt(L,"SDL_KEYMAPCHANGED",772);
  lua_registerInt(L,"SDL_MOUSEMOTION",1024);
  lua_registerInt(L,"SDL_MOUSEBUTTONDOWN",1025);
  lua_registerInt(L,"SDL_MOUSEBUTTONUP",1026);
  lua_registerInt(L,"SDL_MOUSEWHEEL",1027);
  lua_registerInt(L,"SDL_JOYAXISMOTION",1536);
  lua_registerInt(L,"SDL_JOYBALLMOTION",1537);
  lua_registerInt(L,"SDL_JOYHATMOTION",1538);
  lua_registerInt(L,"SDL_JOYBUTTONDOWN",1539);
  lua_registerInt(L,"SDL_JOYBUTTONUP",1540);
  lua_registerInt(L,"SDL_JOYDEVICEADDED",1541);
  lua_registerInt(L,"SDL_JOYDEVICEREMOVED",1542);
  lua_registerInt(L,"SDL_CONTROLLERAXISMOTION",1616);
  lua_registerInt(L,"SDL_CONTROLLERBUTTONDOWN",1617);
  lua_registerInt(L,"SDL_CONTROLLERBUTTONUP",1618);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEADDED",1619);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEREMOVED",1620);
  lua_registerInt(L,"SDL_CONTROLLERDEVICEREMAPPED",1621);
  lua_registerInt(L,"SDL_FINGERDOWN",1792);
  lua_registerInt(L,"SDL_FINGERUP",1793);
  lua_registerInt(L,"SDL_FINGERMOTION",1794);
  lua_registerInt(L,"SDL_DOLLARGESTURE",2048);
  lua_registerInt(L,"SDL_DOLLARRECORD",2049);
  lua_registerInt(L,"SDL_MULTIGESTURE",2050);
  lua_registerInt(L,"SDL_CLIPBOARDUPDATE",2304);
  lua_registerInt(L,"SDL_DROPFILE",4096);
  lua_registerInt(L,"SDL_DROPTEXT",4097);
  lua_registerInt(L,"SDL_DROPBEGIN",4098);
  lua_registerInt(L,"SDL_DROPCOMPLETE",4099);
  lua_registerInt(L,"SDL_AUDIODEVICEADDED",4352);
  lua_registerInt(L,"SDL_AUDIODEVICEREMOVED",4353);
  lua_registerInt(L,"SDL_RENDER_TARGETS_RESET",8192);
  lua_registerInt(L,"SDL_RENDER_DEVICE_RESET",8193);
  lua_registerInt(L,"SDL_USEREVENT",32768);
  lua_registerInt(L,"SDL_LASTEVENT",65535);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_RENDERER_ACCELERATED",2);
  lua_registerInt(L,"SDL_RENDERER_PRESENTVSYNC",4);
  lua_registerInt(L,"SDL_RENDERER_TARGETTEXTURE",8);
  lua_registerInt(L,"SDL_PIXELFORMAT_UNKNOWN",0);
  lua_registerInt(L,"SDL_PIXELFORMAT_INDEX1LSB",286261504);
  lua_registerInt(L,"SDL_PIXELFORMAT_INDEX1MSB",287310080);
  lua_registerInt(L,"SDL_PIXELFORMAT_INDEX4LSB",303039488);
  lua_registerInt(L,"SDL_PIXELFORMAT_INDEX4MSB",304088064);
  lua_registerInt(L,"SDL_PIXELFORMAT_INDEX8",318769153);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB332",336660481);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB444",353504258);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB555",353570562);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGR555",357764866);
  lua_registerInt(L,"SDL_PIXELFORMAT_ARGB4444",355602434);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGBA4444",356651010);
  lua_registerInt(L,"SDL_PIXELFORMAT_ABGR4444",359796738);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGRA4444",360845314);
  lua_registerInt(L,"SDL_PIXELFORMAT_ARGB1555",355667970);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGBA5551",356782082);
  lua_registerInt(L,"SDL_PIXELFORMAT_ABGR1555",359862274);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGRA5551",360976386);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB565",353701890);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGR565",357896194);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB24",386930691);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGR24",390076419);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGB888",370546692);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGBX8888",371595268);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGR888",374740996);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGRX8888",375789572);
  lua_registerInt(L,"SDL_PIXELFORMAT_ARGB8888",372645892);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGBA8888",373694468);
  lua_registerInt(L,"SDL_PIXELFORMAT_ABGR8888",376840196);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGRA8888",377888772);
  lua_registerInt(L,"SDL_PIXELFORMAT_ARGB2101010",372711428);
  lua_registerInt(L,"SDL_PIXELFORMAT_RGBA32",376840196);
  lua_registerInt(L,"SDL_PIXELFORMAT_ARGB32",377888772);
  lua_registerInt(L,"SDL_PIXELFORMAT_BGRA32",372645892);
  lua_registerInt(L,"SDL_PIXELFORMAT_ABGR32",373694468);
  lua_registerInt(L,"SDL_PIXELFORMAT_YV12",842094169);
  lua_registerInt(L,"SDL_PIXELFORMAT_IYUV",1448433993);
  lua_registerInt(L,"SDL_PIXELFORMAT_YUY2",844715353);
  lua_registerInt(L,"SDL_PIXELFORMAT_UYVY",1498831189);
  lua_registerInt(L,"SDL_PIXELFORMAT_YVYU",1431918169);
  lua_registerInt(L,"SDL_PIXELFORMAT_NV12",842094158);
  lua_registerInt(L,"SDL_PIXELFORMAT_NV21",825382478);
  lua_registerInt(L,"SDL_FALSE",0);
  lua_registerInt(L,"SDL_TRUE",1);
  lua_registerInt(L,"SDL_GL_RED_SIZE",0);
  lua_registerInt(L,"SDL_GL_GREEN_SIZE",1);
  lua_registerInt(L,"SDL_GL_BLUE_SIZE",2);
  lua_registerInt(L,"SDL_GL_ALPHA_SIZE",3);
  lua_registerInt(L,"SDL_GL_BUFFER_SIZE",4);
  lua_registerInt(L,"SDL_GL_DOUBLEBUFFER",5);
  lua_registerInt(L,"SDL_GL_DEPTH_SIZE",6);
  lua_registerInt(L,"SDL_GL_STENCIL_SIZE",7);
  lua_registerInt(L,"SDL_GL_ACCUM_RED_SIZE",8);
  lua_registerInt(L,"SDL_GL_ACCUM_GREEN_SIZE",9);
  lua_registerInt(L,"SDL_GL_ACCUM_BLUE_SIZE",10);
  lua_registerInt(L,"SDL_GL_ACCUM_ALPHA_SIZE",11);
  lua_registerInt(L,"SDL_GL_STEREO",12);
  lua_registerInt(L,"SDL_GL_MULTISAMPLEBUFFERS",13);
  lua_registerInt(L,"SDL_GL_MULTISAMPLESAMPLES",14);
  lua_registerInt(L,"SDL_GL_ACCELERATED_VISUAL",15);
  lua_registerInt(L,"SDL_GL_RETAINED_BACKING",16);
  lua_registerInt(L,"SDL_GL_CONTEXT_MAJOR_VERSION",17);
  lua_registerInt(L,"SDL_GL_CONTEXT_MINOR_VERSION",18);
  lua_registerInt(L,"SDL_GL_CONTEXT_EGL",19);
  lua_registerInt(L,"SDL_GL_CONTEXT_FLAGS",20);
  lua_registerInt(L,"SDL_GL_CONTEXT_PROFILE_MASK",21);
  lua_registerInt(L,"SDL_GL_SHARE_WITH_CURRENT_CONTEXT",22);
  lua_registerInt(L,"SDL_GL_FRAMEBUFFER_SRGB_CAPABLE",23);
  lua_registerInt(L,"SDL_GL_CONTEXT_RELEASE_BEHAVIOR",24);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDL_POWERSTATE_ON_BATTERY",1);
  lua_registerInt(L,"SDL_POWERSTATE_NO_BATTERY",2);
  lua_registerInt(L,"SDL_POWERSTATE_CHARGING",3);
  lua_registerInt(L,"SDL_POWERSTATE_CHARGED",4);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_EMPTY",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_LOW",1);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_MEDIUM",2);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_FULL",3);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_WIRED",4);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_MAX",5);
  lua_registerInt(L,"SDL_POWERSTATE_UNKNOWN",0);
  lua_registerInt(L,"SDL_POWERSTATE_ON_BATTERY",1);
  lua_registerInt(L,"SDL_POWERSTATE_NO_BATTERY",2);
  lua_registerInt(L,"SDL_POWERSTATE_CHARGING",3);
  lua_registerInt(L,"SDL_POWERSTATE_CHARGED",4);
  lua_registerInt(L,"SDLK_UNKNOWN",0);
  lua_registerInt(L,"SDLK_RETURN",13);
  lua_registerInt(L,"SDLK_ESCAPE",27);
  lua_registerInt(L,"SDLK_BACKSPACE",8);
  lua_registerInt(L,"SDLK_TAB",9);
  lua_registerInt(L,"SDLK_SPACE",32);
  lua_registerInt(L,"SDLK_EXCLAIM",33);
  lua_registerInt(L,"SDLK_QUOTEDBL",34);
  lua_registerInt(L,"SDLK_HASH",35);
  lua_registerInt(L,"SDLK_PERCENT",37);
  lua_registerInt(L,"SDLK_DOLLAR",36);
  lua_registerInt(L,"SDLK_AMPERSAND",38);
  lua_registerInt(L,"SDLK_QUOTE",39);
  lua_registerInt(L,"SDLK_LEFTPAREN",40);
  lua_registerInt(L,"SDLK_RIGHTPAREN",41);
  lua_registerInt(L,"SDLK_ASTERISK",42);
  lua_registerInt(L,"SDLK_PLUS",43);
  lua_registerInt(L,"SDLK_COMMA",44);
  lua_registerInt(L,"SDLK_MINUS",45);
  lua_registerInt(L,"SDLK_PERIOD",46);
  lua_registerInt(L,"SDLK_SLASH",47);
  lua_registerInt(L,"SDLK_0",48);
  lua_registerInt(L,"SDLK_1",49);
  lua_registerInt(L,"SDLK_2",50);
  lua_registerInt(L,"SDLK_3",51);
  lua_registerInt(L,"SDLK_4",52);
  lua_registerInt(L,"SDLK_5",53);
  lua_registerInt(L,"SDLK_6",54);
  lua_registerInt(L,"SDLK_7",55);
  lua_registerInt(L,"SDLK_8",56);
  lua_registerInt(L,"SDLK_9",57);
  lua_registerInt(L,"SDLK_COLON",58);
  lua_registerInt(L,"SDLK_SEMICOLON",59);
  lua_registerInt(L,"SDLK_LESS",60);
  lua_registerInt(L,"SDLK_EQUALS",61);
  lua_registerInt(L,"SDLK_GREATER",62);
  lua_registerInt(L,"SDLK_QUESTION",63);
  lua_registerInt(L,"SDLK_AT",64);
  lua_registerInt(L,"SDLK_LEFTBRACKET",91);
  lua_registerInt(L,"SDLK_BACKSLASH",92);
  lua_registerInt(L,"SDLK_RIGHTBRACKET",93);
  lua_registerInt(L,"SDLK_CARET",94);
  lua_registerInt(L,"SDLK_UNDERSCORE",95);
  lua_registerInt(L,"SDLK_BACKQUOTE",96);
  lua_registerInt(L,"SDLK_a",97);
  lua_registerInt(L,"SDLK_b",98);
  lua_registerInt(L,"SDLK_c",99);
  lua_registerInt(L,"SDLK_d",100);
  lua_registerInt(L,"SDLK_e",101);
  lua_registerInt(L,"SDLK_f",102);
  lua_registerInt(L,"SDLK_g",103);
  lua_registerInt(L,"SDLK_h",104);
  lua_registerInt(L,"SDLK_i",105);
  lua_registerInt(L,"SDLK_j",106);
  lua_registerInt(L,"SDLK_k",107);
  lua_registerInt(L,"SDLK_l",108);
  lua_registerInt(L,"SDLK_m",109);
  lua_registerInt(L,"SDLK_n",110);
  lua_registerInt(L,"SDLK_o",111);
  lua_registerInt(L,"SDLK_p",112);
  lua_registerInt(L,"SDLK_q",113);
  lua_registerInt(L,"SDLK_r",114);
  lua_registerInt(L,"SDLK_s",115);
  lua_registerInt(L,"SDLK_t",116);
  lua_registerInt(L,"SDLK_u",117);
  lua_registerInt(L,"SDLK_v",118);
  lua_registerInt(L,"SDLK_w",119);
  lua_registerInt(L,"SDLK_x",120);
  lua_registerInt(L,"SDLK_y",121);
  lua_registerInt(L,"SDLK_z",122);
  lua_registerInt(L,"SDLK_CAPSLOCK",1073741881);
  lua_registerInt(L,"SDLK_F1",1073741882);
  lua_registerInt(L,"SDLK_F2",1073741883);
  lua_registerInt(L,"SDLK_F3",1073741884);
  lua_registerInt(L,"SDLK_F4",1073741885);
  lua_registerInt(L,"SDLK_F5",1073741886);
  lua_registerInt(L,"SDLK_F6",1073741887);
  lua_registerInt(L,"SDLK_F7",1073741888);
  lua_registerInt(L,"SDLK_F8",1073741889);
  lua_registerInt(L,"SDLK_F9",1073741890);
  lua_registerInt(L,"SDLK_F10",1073741891);
  lua_registerInt(L,"SDLK_F11",1073741892);
  lua_registerInt(L,"SDLK_F12",1073741893);
  lua_registerInt(L,"SDLK_PRINTSCREEN",1073741894);
  lua_registerInt(L,"SDLK_SCROLLLOCK",1073741895);
  lua_registerInt(L,"SDLK_PAUSE",1073741896);
  lua_registerInt(L,"SDLK_INSERT",1073741897);
  lua_registerInt(L,"SDLK_HOME",1073741898);
  lua_registerInt(L,"SDLK_PAGEUP",1073741899);
  lua_registerInt(L,"SDLK_DELETE",127);
  lua_registerInt(L,"SDLK_END",1073741901);
  lua_registerInt(L,"SDLK_PAGEDOWN",1073741902);
  lua_registerInt(L,"SDLK_RIGHT",1073741903);
  lua_registerInt(L,"SDLK_LEFT",1073741904);
  lua_registerInt(L,"SDLK_DOWN",1073741905);
  lua_registerInt(L,"SDLK_UP",1073741906);
  lua_registerInt(L,"SDLK_NUMLOCKCLEAR",1073741907);
  lua_registerInt(L,"SDLK_KP_DIVIDE",1073741908);
  lua_registerInt(L,"SDLK_KP_MULTIPLY",1073741909);
  lua_registerInt(L,"SDLK_KP_MINUS",1073741910);
  lua_registerInt(L,"SDLK_KP_PLUS",1073741911);
  lua_registerInt(L,"SDLK_KP_ENTER",1073741912);
  lua_registerInt(L,"SDLK_KP_1",1073741913);
  lua_registerInt(L,"SDLK_KP_2",1073741914);
  lua_registerInt(L,"SDLK_KP_3",1073741915);
  lua_registerInt(L,"SDLK_KP_4",1073741916);
  lua_registerInt(L,"SDLK_KP_5",1073741917);
  lua_registerInt(L,"SDLK_KP_6",1073741918);
  lua_registerInt(L,"SDLK_KP_7",1073741919);
  lua_registerInt(L,"SDLK_KP_8",1073741920);
  lua_registerInt(L,"SDLK_KP_9",1073741921);
  lua_registerInt(L,"SDLK_KP_0",1073741922);
  lua_registerInt(L,"SDLK_KP_PERIOD",1073741923);
  lua_registerInt(L,"SDLK_APPLICATION",1073741925);
  lua_registerInt(L,"SDLK_POWER",1073741926);
  lua_registerInt(L,"SDLK_KP_EQUALS",1073741927);
  lua_registerInt(L,"SDLK_F13",1073741928);
  lua_registerInt(L,"SDLK_F14",1073741929);
  lua_registerInt(L,"SDLK_F15",1073741930);
  lua_registerInt(L,"SDLK_F16",1073741931);
  lua_registerInt(L,"SDLK_F17",1073741932);
  lua_registerInt(L,"SDLK_F18",1073741933);
  lua_registerInt(L,"SDLK_F19",1073741934);
  lua_registerInt(L,"SDLK_F20",1073741935);
  lua_registerInt(L,"SDLK_F21",1073741936);
  lua_registerInt(L,"SDLK_F22",1073741937);
  lua_registerInt(L,"SDLK_F23",1073741938);
  lua_registerInt(L,"SDLK_F24",1073741939);
  lua_registerInt(L,"SDLK_EXECUTE",1073741940);
  lua_registerInt(L,"SDLK_HELP",1073741941);
  lua_registerInt(L,"SDLK_MENU",1073741942);
  lua_registerInt(L,"SDLK_SELECT",1073741943);
  lua_registerInt(L,"SDLK_STOP",1073741944);
  lua_registerInt(L,"SDLK_AGAIN",1073741945);
  lua_registerInt(L,"SDLK_UNDO",1073741946);
  lua_registerInt(L,"SDLK_CUT",1073741947);
  lua_registerInt(L,"SDLK_COPY",1073741948);
  lua_registerInt(L,"SDLK_PASTE",1073741949);
  lua_registerInt(L,"SDLK_FIND",1073741950);
  lua_registerInt(L,"SDLK_MUTE",1073741951);
  lua_registerInt(L,"SDLK_VOLUMEUP",1073741952);
  lua_registerInt(L,"SDLK_VOLUMEDOWN",1073741953);
  lua_registerInt(L,"SDLK_KP_COMMA",1073741957);
  lua_registerInt(L,"SDLK_KP_EQUALSAS400",1073741958);
  lua_registerInt(L,"SDLK_ALTERASE",1073741977);
  lua_registerInt(L,"SDLK_SYSREQ",1073741978);
  lua_registerInt(L,"SDLK_CANCEL",1073741979);
  lua_registerInt(L,"SDLK_CLEAR",1073741980);
  lua_registerInt(L,"SDLK_PRIOR",1073741981);
  lua_registerInt(L,"SDLK_RETURN2",1073741982);
  lua_registerInt(L,"SDLK_SEPARATOR",1073741983);
  lua_registerInt(L,"SDLK_OUT",1073741984);
  lua_registerInt(L,"SDLK_OPER",1073741985);
  lua_registerInt(L,"SDLK_CLEARAGAIN",1073741986);
  lua_registerInt(L,"SDLK_CRSEL",1073741987);
  lua_registerInt(L,"SDLK_EXSEL",1073741988);
  lua_registerInt(L,"SDLK_KP_00",1073742000);
  lua_registerInt(L,"SDLK_KP_000",1073742001);
  lua_registerInt(L,"SDLK_THOUSANDSSEPARATOR",1073742002);
  lua_registerInt(L,"SDLK_DECIMALSEPARATOR",1073742003);
  lua_registerInt(L,"SDLK_CURRENCYUNIT",1073742004);
  lua_registerInt(L,"SDLK_CURRENCYSUBUNIT",1073742005);
  lua_registerInt(L,"SDLK_KP_LEFTPAREN",1073742006);
  lua_registerInt(L,"SDLK_KP_RIGHTPAREN",1073742007);
  lua_registerInt(L,"SDLK_KP_LEFTBRACE",1073742008);
  lua_registerInt(L,"SDLK_KP_RIGHTBRACE",1073742009);
  lua_registerInt(L,"SDLK_KP_TAB",1073742010);
  lua_registerInt(L,"SDLK_KP_BACKSPACE",1073742011);
  lua_registerInt(L,"SDLK_KP_A",1073742012);
  lua_registerInt(L,"SDLK_KP_B",1073742013);
  lua_registerInt(L,"SDLK_KP_C",1073742014);
  lua_registerInt(L,"SDLK_KP_D",1073742015);
  lua_registerInt(L,"SDLK_KP_E",1073742016);
  lua_registerInt(L,"SDLK_KP_F",1073742017);
  lua_registerInt(L,"SDLK_KP_XOR",1073742018);
  lua_registerInt(L,"SDLK_KP_POWER",1073742019);
  lua_registerInt(L,"SDLK_KP_PERCENT",1073742020);
  lua_registerInt(L,"SDLK_KP_LESS",1073742021);
  lua_registerInt(L,"SDLK_KP_GREATER",1073742022);
  lua_registerInt(L,"SDLK_KP_AMPERSAND",1073742023);
  lua_registerInt(L,"SDLK_KP_DBLAMPERSAND",1073742024);
  lua_registerInt(L,"SDLK_KP_VERTICALBAR",1073742025);
  lua_registerInt(L,"SDLK_KP_DBLVERTICALBAR",1073742026);
  lua_registerInt(L,"SDLK_KP_COLON",1073742027);
  lua_registerInt(L,"SDLK_KP_HASH",1073742028);
  lua_registerInt(L,"SDLK_KP_SPACE",1073742029);
  lua_registerInt(L,"SDLK_KP_AT",1073742030);
  lua_registerInt(L,"SDLK_KP_EXCLAM",1073742031);
  lua_registerInt(L,"SDLK_KP_MEMSTORE",1073742032);
  lua_registerInt(L,"SDLK_KP_MEMRECALL",1073742033);
  lua_registerInt(L,"SDLK_KP_MEMCLEAR",1073742034);
  lua_registerInt(L,"SDLK_KP_MEMADD",1073742035);
  lua_registerInt(L,"SDLK_KP_MEMSUBTRACT",1073742036);
  lua_registerInt(L,"SDLK_KP_MEMMULTIPLY",1073742037);
  lua_registerInt(L,"SDLK_KP_MEMDIVIDE",1073742038);
  lua_registerInt(L,"SDLK_KP_PLUSMINUS",1073742039);
  lua_registerInt(L,"SDLK_KP_CLEAR",1073742040);
  lua_registerInt(L,"SDLK_KP_CLEARENTRY",1073742041);
  lua_registerInt(L,"SDLK_KP_BINARY",1073742042);
  lua_registerInt(L,"SDLK_KP_OCTAL",1073742043);
  lua_registerInt(L,"SDLK_KP_DECIMAL",1073742044);
  lua_registerInt(L,"SDLK_KP_HEXADECIMAL",1073742045);
  lua_registerInt(L,"SDLK_LCTRL",1073742048);
  lua_registerInt(L,"SDLK_LSHIFT",1073742049);
  lua_registerInt(L,"SDLK_LALT",1073742050);
  lua_registerInt(L,"SDLK_LGUI",1073742051);
  lua_registerInt(L,"SDLK_RCTRL",1073742052);
  lua_registerInt(L,"SDLK_RSHIFT",1073742053);
  lua_registerInt(L,"SDLK_RALT",1073742054);
  lua_registerInt(L,"SDLK_RGUI",1073742055);
  lua_registerInt(L,"SDLK_MODE",1073742081);
  lua_registerInt(L,"SDLK_AUDIONEXT",1073742082);
  lua_registerInt(L,"SDLK_AUDIOPREV",1073742083);
  lua_registerInt(L,"SDLK_AUDIOSTOP",1073742084);
  lua_registerInt(L,"SDLK_AUDIOPLAY",1073742085);
  lua_registerInt(L,"SDLK_AUDIOMUTE",1073742086);
  lua_registerInt(L,"SDLK_MEDIASELECT",1073742087);
  lua_registerInt(L,"SDLK_WWW",1073742088);
  lua_registerInt(L,"SDLK_MAIL",1073742089);
  lua_registerInt(L,"SDLK_CALCULATOR",1073742090);
  lua_registerInt(L,"SDLK_COMPUTER",1073742091);
  lua_registerInt(L,"SDLK_AC_SEARCH",1073742092);
  lua_registerInt(L,"SDLK_AC_HOME",1073742093);
  lua_registerInt(L,"SDLK_AC_BACK",1073742094);
  lua_registerInt(L,"SDLK_AC_FORWARD",1073742095);
  lua_registerInt(L,"SDLK_AC_STOP",1073742096);
  lua_registerInt(L,"SDLK_AC_REFRESH",1073742097);
  lua_registerInt(L,"SDLK_AC_BOOKMARKS",1073742098);
  lua_registerInt(L,"SDLK_BRIGHTNESSDOWN",1073742099);
  lua_registerInt(L,"SDLK_BRIGHTNESSUP",1073742100);
  lua_registerInt(L,"SDLK_DISPLAYSWITCH",1073742101);
  lua_registerInt(L,"SDLK_KBDILLUMTOGGLE",1073742102);
  lua_registerInt(L,"SDLK_KBDILLUMDOWN",1073742103);
  lua_registerInt(L,"SDLK_KBDILLUMUP",1073742104);
  lua_registerInt(L,"SDLK_EJECT",1073742105);
  lua_registerInt(L,"SDLK_SLEEP",1073742106);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT",1);
  lua_registerInt(L,"SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT",2);
  lua_registerInt(L,"SDL_MESSAGEBOX_ERROR",16);
  lua_registerInt(L,"SDL_MESSAGEBOX_WARNING",32);
  lua_registerInt(L,"SDL_MESSAGEBOX_INFORMATION",64);
  lua_registerInt(L,"SDL_FLIP_NONE",0);
  lua_registerInt(L,"SDL_FLIP_HORIZONTAL",1);
  lua_registerInt(L,"SDL_FLIP_VERTICAL",2);
  lua_registerInt(L,"SDL_ADDEVENT",0);
  lua_registerInt(L,"SDL_PEEKEVENT",1);
  lua_registerInt(L,"SDL_GETEVENT",2);
  lua_registerInt(L,"SDL_ENOMEM",0);
  lua_registerInt(L,"SDL_EFREAD",1);
  lua_registerInt(L,"SDL_EFWRITE",2);
  lua_registerInt(L,"SDL_EFSEEK",3);
  lua_registerInt(L,"SDL_UNSUPPORTED",4);
  lua_registerInt(L,"SDL_LASTERROR",5);
  lua_registerInt(L,"SDL_LOG_PRIORITY_VERBOSE",1);
  lua_registerInt(L,"SDL_LOG_PRIORITY_DEBUG",2);
  lua_registerInt(L,"SDL_LOG_PRIORITY_INFO",3);
  lua_registerInt(L,"SDL_LOG_PRIORITY_WARN",4);
  lua_registerInt(L,"SDL_LOG_PRIORITY_ERROR",5);
  lua_registerInt(L,"SDL_LOG_PRIORITY_CRITICAL",6);
  lua_registerInt(L,"SDL_NUM_LOG_PRIORITIES",7);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_UNKNOWN",-1);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_EMPTY",0);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_LOW",1);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_MEDIUM",2);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_FULL",3);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_WIRED",4);
  lua_registerInt(L,"SDL_JOYSTICK_POWER_MAX",5);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STATIC",0);
  lua_registerInt(L,"SDL_TEXTUREACCESS_STREAMING",1);
  lua_registerInt(L,"SDL_TEXTUREACCESS_TARGET",2);
  lua_registerInt(L,"SDL_RENDERER_SOFTWARE",1);
  lua_registerInt(L,"SDL_RENDERER_ACCELERATED",2);
  lua_registerInt(L,"SDL_RENDERER_PRESENTVSYNC",4);
  lua_registerInt(L,"SDL_RENDERER_TARGETTEXTURE",8);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_ARROW",0);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_IBEAM",1);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_WAIT",2);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_CROSSHAIR",3);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_WAITARROW",4);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENWSE",5);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENESW",6);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZEWE",7);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZENS",8);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_SIZEALL",9);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_NO",10);
  lua_registerInt(L,"SDL_SYSTEM_CURSOR_HAND",11);
  lua_registerInt(L,"SDL_NUM_SYSTEM_CURSORS",12);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_INVALID",-1);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_LEFTX",0);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_LEFTY",1);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_RIGHTX",2);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_RIGHTY",3);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_TRIGGERLEFT",4);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_TRIGGERRIGHT",5);
  lua_registerInt(L,"SDL_CONTROLLER_AXIS_MAX",6);
  lua_registerInt(L,"SDL_ASSERTION_RETRY",0);
  lua_registerInt(L,"SDL_ASSERTION_BREAK",1);
  lua_registerInt(L,"SDL_ASSERTION_ABORT",2);
  lua_registerInt(L,"SDL_ASSERTION_IGNORE",3);
  lua_registerInt(L,"SDL_ASSERTION_ALWAYS_IGNORE",4);
  lua_registerInt(L,"SDL_BLENDMODE_NONE",0);
  lua_registerInt(L,"SDL_BLENDMODE_BLEND",1);
  lua_registerInt(L,"SDL_BLENDMODE_ADD",2);
  lua_registerInt(L,"SDL_BLENDMODE_MOD",4);
  lua_registerInt(L,"_MM_HINT_ET0",7);




//struct 
  lua_getglobal(L,"structs");
  if(lua_isnil(L,-1)){
    lua_newtable(L);
    lua_setglobal(L,"structs");
    lua_getglobal(L,"structs");
  };
  
#ifndef lua_StructBegin
#define lua_setField(name,bpos,bsiz,type,typeName)  \
    lua_createtable(L,4,1);\
    lua_pushinteger(L,bpos);lua_setfield(L,-2,"bpos");\
    lua_pushinteger(L,bsiz);lua_setfield(L,-2,"bsiz");\
    lua_pushstring(L,type);lua_setfield(L,-2,"type");\
    lua_pushstring(L,typeName);lua_setfield(L,-2,"tname");\
    lua_setfield(L,-2,name);

#ifndef lua_StructSize
#define lua_StructSize(siz)   lua_pushinteger(L,siz); lua_setfield(L,-2,"1size");
#endif
#define lua_StructBegin(len)  lua_createtable(L,len+1,4);
#define lua_StructEnd(name)   lua_setfield(L,-2,name);
#endif
  
  lua_StructBegin(8);
    lua_setField("always_ignore",0,32,"integer_type","int");
    lua_setField("trigger_count",32,32,"integer_type","unsigned int");
    lua_setField("condition",64,32,"pointer_type","char");
    lua_setField("filename",96,32,"pointer_type","char");
    lua_setField("linenum",128,32,"integer_type","int");
    lua_setField("function",160,32,"pointer_type","char");
    lua_setField("next",192,32,"pointer_type","SDL_AssertData");
  lua_StructSize(224);

  lua_StructEnd("SDL_AssertData");
  
  lua_StructBegin(12);
    lua_setField("needed",0,32,"integer_type","int");
    lua_setField("src_format",32,16,"integer_type","short unsigned int");
    lua_setField("dst_format",48,16,"integer_type","short unsigned int");
    lua_setField("rate_incr",64,64,"real_type","double");
    lua_setField("buf",128,32,"pointer_type","unsigned char");
    lua_setField("len",160,32,"integer_type","int");
    lua_setField("len_cvt",192,32,"integer_type","int");
    lua_setField("len_mult",224,32,"integer_type","int");
    lua_setField("len_ratio",256,64,"real_type","double");
    lua_setField("filters",320,320,"array_type","SDL_AudioFilter");
    lua_setField("filter_index",640,32,"integer_type","int");
  lua_StructSize(672);

  lua_StructEnd("SDL_AudioCVT");
  
  lua_StructBegin(8);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","unsigned int");
    lua_setField("iscapture",96,8,"integer_type","unsigned char");
    lua_setField("padding1",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
  lua_StructSize(128);

  lua_StructEnd("SDL_AudioDeviceEvent");
  
  lua_StructBegin(10);
    lua_setField("freq",0,32,"integer_type","int");
    lua_setField("format",32,16,"integer_type","short unsigned int");
    lua_setField("channels",48,8,"integer_type","unsigned char");
    lua_setField("silence",56,8,"integer_type","unsigned char");
    lua_setField("samples",64,16,"integer_type","short unsigned int");
    lua_setField("padding",80,16,"integer_type","short unsigned int");
    lua_setField("size",96,32,"integer_type","unsigned int");
    lua_setField("callback",128,32,"pointer_type","SDL_AudioCallback");
    lua_setField("userdata",160,32,"pointer_type","void*");
  lua_StructSize(192);

  lua_StructEnd("SDL_AudioSpec");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_BlitMap");
  
  lua_StructBegin(5);
    lua_setField("r",0,8,"integer_type","unsigned char");
    lua_setField("g",8,8,"integer_type","unsigned char");
    lua_setField("b",16,8,"integer_type","unsigned char");
    lua_setField("a",24,8,"integer_type","unsigned char");
  lua_StructSize(32);

  lua_StructEnd("SDL_Color");
  
  lua_StructBegin(3);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
  lua_StructSize(64);

  lua_StructEnd("SDL_CommonEvent");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("axis",96,8,"integer_type","unsigned char");
    lua_setField("padding1",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
    lua_setField("value",128,16,"integer_type","short int");
    lua_setField("padding4",144,16,"integer_type","short unsigned int");
  lua_StructSize(160);

  lua_StructEnd("SDL_ControllerAxisEvent");
  
  lua_StructBegin(8);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("button",96,8,"integer_type","unsigned char");
    lua_setField("state",104,8,"integer_type","unsigned char");
    lua_setField("padding1",112,8,"integer_type","unsigned char");
    lua_setField("padding2",120,8,"integer_type","unsigned char");
  lua_StructSize(128);

  lua_StructEnd("SDL_ControllerButtonEvent");
  
  lua_StructBegin(4);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
  lua_StructSize(96);

  lua_StructEnd("SDL_ControllerDeviceEvent");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_Cursor");
  
  lua_StructBegin(9);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("touchId",64,64,"integer_type","long long int");
    lua_setField("gestureId",128,64,"integer_type","long long int");
    lua_setField("numFingers",192,32,"integer_type","unsigned int");
    lua_setField("error",224,32,"real_type","float");
    lua_setField("x",256,32,"real_type","float");
    lua_setField("y",288,32,"real_type","float");
  lua_StructSize(320);

  lua_StructEnd("SDL_DollarGestureEvent");
  
  lua_StructBegin(5);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("file",64,32,"pointer_type","char");
    lua_setField("windowID",96,32,"integer_type","unsigned int");
  lua_StructSize(128);

  lua_StructEnd("SDL_DropEvent");
  
  lua_StructBegin(27);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("common",0,64,"record_type","SDL_CommonEvent");
    lua_setField("window",0,192,"record_type","SDL_WindowEvent");
    lua_setField("key",0,256,"record_type","SDL_KeyboardEvent");
    lua_setField("edit",0,416,"record_type","SDL_TextEditingEvent");
    lua_setField("text",0,352,"record_type","SDL_TextInputEvent");
    lua_setField("motion",0,288,"record_type","SDL_MouseMotionEvent");
    lua_setField("button",0,224,"record_type","SDL_MouseButtonEvent");
    lua_setField("wheel",0,224,"record_type","SDL_MouseWheelEvent");
    lua_setField("jaxis",0,160,"record_type","SDL_JoyAxisEvent");
    lua_setField("jball",0,160,"record_type","SDL_JoyBallEvent");
    lua_setField("jhat",0,128,"record_type","SDL_JoyHatEvent");
    lua_setField("jbutton",0,128,"record_type","SDL_JoyButtonEvent");
    lua_setField("jdevice",0,96,"record_type","SDL_JoyDeviceEvent");
    lua_setField("caxis",0,160,"record_type","SDL_ControllerAxisEvent");
    lua_setField("cbutton",0,128,"record_type","SDL_ControllerButtonEvent");
    lua_setField("cdevice",0,96,"record_type","SDL_ControllerDeviceEvent");
    lua_setField("adevice",0,128,"record_type","SDL_AudioDeviceEvent");
    lua_setField("quit",0,64,"record_type","SDL_QuitEvent");
    lua_setField("user",0,192,"record_type","SDL_UserEvent");
    lua_setField("syswm",0,96,"record_type","SDL_SysWMEvent");
    lua_setField("tfinger",0,384,"record_type","SDL_TouchFingerEvent");
    lua_setField("mgesture",0,320,"record_type","SDL_MultiGestureEvent");
    lua_setField("dgesture",0,320,"record_type","SDL_DollarGestureEvent");
    lua_setField("drop",0,128,"record_type","SDL_DropEvent");
    lua_setField("padding",0,448,"array_type","unsigned char");
  lua_StructSize(448);

  lua_StructEnd("SDL_Event");
  
  lua_StructBegin(5);
    lua_setField("id",0,64,"integer_type","long long int");
    lua_setField("x",64,32,"real_type","float");
    lua_setField("y",96,32,"real_type","float");
    lua_setField("pressure",128,32,"real_type","float");
  lua_StructSize(192);

  lua_StructEnd("SDL_Finger");
  
  lua_StructBegin(3);
    lua_setField("bindType",0,32,"enumeral_type","int");
    lua_setField("value",32,64,"union_type","__union");
  lua_StructSize(96);

  lua_StructEnd("SDL_GameControllerButtonBind");
  
  lua_StructBegin(13);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("direction",32,128,"record_type","SDL_HapticDirection");
    lua_setField("length",160,32,"integer_type","unsigned int");
    lua_setField("delay",192,16,"integer_type","short unsigned int");
    lua_setField("button",208,16,"integer_type","short unsigned int");
    lua_setField("interval",224,16,"integer_type","short unsigned int");
    lua_setField("right_sat",240,48,"array_type","short unsigned int");
    lua_setField("left_sat",288,48,"array_type","short unsigned int");
    lua_setField("right_coeff",336,48,"array_type","short int");
    lua_setField("left_coeff",384,48,"array_type","short int");
    lua_setField("deadband",432,48,"array_type","short unsigned int");
    lua_setField("center",480,48,"array_type","short int");
  lua_StructSize(544);

  lua_StructEnd("SDL_HapticCondition");
  
  lua_StructBegin(12);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("direction",32,128,"record_type","SDL_HapticDirection");
    lua_setField("length",160,32,"integer_type","unsigned int");
    lua_setField("delay",192,16,"integer_type","short unsigned int");
    lua_setField("button",208,16,"integer_type","short unsigned int");
    lua_setField("interval",224,16,"integer_type","short unsigned int");
    lua_setField("level",240,16,"integer_type","short int");
    lua_setField("attack_length",256,16,"integer_type","short unsigned int");
    lua_setField("attack_level",272,16,"integer_type","short unsigned int");
    lua_setField("fade_length",288,16,"integer_type","short unsigned int");
    lua_setField("fade_level",304,16,"integer_type","short unsigned int");
  lua_StructSize(320);

  lua_StructEnd("SDL_HapticConstant");
  
  lua_StructBegin(15);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("direction",32,128,"record_type","SDL_HapticDirection");
    lua_setField("length",160,32,"integer_type","unsigned int");
    lua_setField("delay",192,16,"integer_type","short unsigned int");
    lua_setField("button",208,16,"integer_type","short unsigned int");
    lua_setField("interval",224,16,"integer_type","short unsigned int");
    lua_setField("channels",240,8,"integer_type","unsigned char");
    lua_setField("period",256,16,"integer_type","short unsigned int");
    lua_setField("samples",272,16,"integer_type","short unsigned int");
    lua_setField("data",288,32,"pointer_type","short unsigned int");
    lua_setField("attack_length",320,16,"integer_type","short unsigned int");
    lua_setField("attack_level",336,16,"integer_type","short unsigned int");
    lua_setField("fade_length",352,16,"integer_type","short unsigned int");
    lua_setField("fade_level",368,16,"integer_type","short unsigned int");
  lua_StructSize(384);

  lua_StructEnd("SDL_HapticCustom");
  
  lua_StructBegin(3);
    lua_setField("type",0,8,"integer_type","unsigned char");
    lua_setField("dir",32,96,"array_type","int");
  lua_StructSize(128);

  lua_StructEnd("SDL_HapticDirection");
  
  lua_StructBegin(8);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("constant",0,320,"record_type","SDL_HapticConstant");
    lua_setField("periodic",0,384,"record_type","SDL_HapticPeriodic");
    lua_setField("condition",0,544,"record_type","SDL_HapticCondition");
    lua_setField("ramp",0,352,"record_type","SDL_HapticRamp");
    lua_setField("leftright",0,96,"record_type","SDL_HapticLeftRight");
    lua_setField("custom",0,384,"record_type","SDL_HapticCustom");
  lua_StructSize(544);

  lua_StructEnd("SDL_HapticEffect");
  
  lua_StructBegin(5);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("length",32,32,"integer_type","unsigned int");
    lua_setField("large_magnitude",64,16,"integer_type","short unsigned int");
    lua_setField("small_magnitude",80,16,"integer_type","short unsigned int");
  lua_StructSize(96);

  lua_StructEnd("SDL_HapticLeftRight");
  
  lua_StructBegin(15);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("direction",32,128,"record_type","SDL_HapticDirection");
    lua_setField("length",160,32,"integer_type","unsigned int");
    lua_setField("delay",192,16,"integer_type","short unsigned int");
    lua_setField("button",208,16,"integer_type","short unsigned int");
    lua_setField("interval",224,16,"integer_type","short unsigned int");
    lua_setField("period",240,16,"integer_type","short unsigned int");
    lua_setField("magnitude",256,16,"integer_type","short int");
    lua_setField("offset",272,16,"integer_type","short int");
    lua_setField("phase",288,16,"integer_type","short unsigned int");
    lua_setField("attack_length",304,16,"integer_type","short unsigned int");
    lua_setField("attack_level",320,16,"integer_type","short unsigned int");
    lua_setField("fade_length",336,16,"integer_type","short unsigned int");
    lua_setField("fade_level",352,16,"integer_type","short unsigned int");
  lua_StructSize(384);

  lua_StructEnd("SDL_HapticPeriodic");
  
  lua_StructBegin(13);
    lua_setField("type",0,16,"integer_type","short unsigned int");
    lua_setField("direction",32,128,"record_type","SDL_HapticDirection");
    lua_setField("length",160,32,"integer_type","unsigned int");
    lua_setField("delay",192,16,"integer_type","short unsigned int");
    lua_setField("button",208,16,"integer_type","short unsigned int");
    lua_setField("interval",224,16,"integer_type","short unsigned int");
    lua_setField("start",240,16,"integer_type","short int");
    lua_setField("end",256,16,"integer_type","short int");
    lua_setField("attack_length",272,16,"integer_type","short unsigned int");
    lua_setField("attack_level",288,16,"integer_type","short unsigned int");
    lua_setField("fade_length",304,16,"integer_type","short unsigned int");
    lua_setField("fade_level",320,16,"integer_type","short unsigned int");
  lua_StructSize(352);

  lua_StructEnd("SDL_HapticRamp");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("axis",96,8,"integer_type","unsigned char");
    lua_setField("padding1",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
    lua_setField("value",128,16,"integer_type","short int");
    lua_setField("padding4",144,16,"integer_type","short unsigned int");
  lua_StructSize(160);

  lua_StructEnd("SDL_JoyAxisEvent");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("ball",96,8,"integer_type","unsigned char");
    lua_setField("padding1",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
    lua_setField("xrel",128,16,"integer_type","short int");
    lua_setField("yrel",144,16,"integer_type","short int");
  lua_StructSize(160);

  lua_StructEnd("SDL_JoyBallEvent");
  
  lua_StructBegin(8);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("button",96,8,"integer_type","unsigned char");
    lua_setField("state",104,8,"integer_type","unsigned char");
    lua_setField("padding1",112,8,"integer_type","unsigned char");
    lua_setField("padding2",120,8,"integer_type","unsigned char");
  lua_StructSize(128);

  lua_StructEnd("SDL_JoyButtonEvent");
  
  lua_StructBegin(4);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
  lua_StructSize(96);

  lua_StructEnd("SDL_JoyDeviceEvent");
  
  lua_StructBegin(8);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("which",64,32,"integer_type","int");
    lua_setField("hat",96,8,"integer_type","unsigned char");
    lua_setField("value",104,8,"integer_type","unsigned char");
    lua_setField("padding1",112,8,"integer_type","unsigned char");
    lua_setField("padding2",120,8,"integer_type","unsigned char");
  lua_StructSize(128);

  lua_StructEnd("SDL_JoyHatEvent");
  
  lua_StructBegin(9);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("state",96,8,"integer_type","unsigned char");
    lua_setField("repeat",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
    lua_setField("keysym",128,128,"record_type","SDL_Keysym");
  lua_StructSize(256);

  lua_StructEnd("SDL_KeyboardEvent");
  
  lua_StructBegin(5);
    lua_setField("scancode",0,32,"enumeral_type","int");
    lua_setField("sym",32,32,"integer_type","int");
    lua_setField("mod",64,16,"integer_type","short unsigned int");
    lua_setField("unused",96,32,"integer_type","unsigned int");
  lua_StructSize(128);

  lua_StructEnd("SDL_Keysym");
  
  lua_StructBegin(11);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("which",96,32,"integer_type","unsigned int");
    lua_setField("button",128,8,"integer_type","unsigned char");
    lua_setField("state",136,8,"integer_type","unsigned char");
    lua_setField("clicks",144,8,"integer_type","unsigned char");
    lua_setField("padding1",152,8,"integer_type","unsigned char");
    lua_setField("x",160,32,"integer_type","int");
    lua_setField("y",192,32,"integer_type","int");
  lua_StructSize(224);

  lua_StructEnd("SDL_MouseButtonEvent");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("which",96,32,"integer_type","unsigned int");
    lua_setField("state",128,32,"integer_type","unsigned int");
    lua_setField("x",160,32,"integer_type","int");
    lua_setField("y",192,32,"integer_type","int");
    lua_setField("xrel",224,32,"integer_type","int");
    lua_setField("yrel",256,32,"integer_type","int");
  lua_StructSize(288);

  lua_StructEnd("SDL_MouseMotionEvent");
  
  lua_StructBegin(8);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("which",96,32,"integer_type","unsigned int");
    lua_setField("x",128,32,"integer_type","int");
    lua_setField("y",160,32,"integer_type","int");
    lua_setField("direction",192,32,"integer_type","unsigned int");
  lua_StructSize(224);

  lua_StructEnd("SDL_MouseWheelEvent");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("touchId",64,64,"integer_type","long long int");
    lua_setField("dTheta",128,32,"real_type","float");
    lua_setField("dDist",160,32,"real_type","float");
    lua_setField("x",192,32,"real_type","float");
    lua_setField("y",224,32,"real_type","float");
    lua_setField("numFingers",256,16,"integer_type","short unsigned int");
    lua_setField("padding",272,16,"integer_type","short unsigned int");
  lua_StructSize(320);

  lua_StructEnd("SDL_MultiGestureEvent");
  
  lua_StructBegin(3);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
  lua_StructSize(64);

  lua_StructEnd("SDL_OSEvent");
  
  lua_StructBegin(5);
    lua_setField("ncolors",0,32,"integer_type","int");
    lua_setField("colors",32,32,"pointer_type","SDL_Color");
    lua_setField("version",64,32,"integer_type","unsigned int");
    lua_setField("refcount",96,32,"integer_type","int");
  lua_StructSize(128);

  lua_StructEnd("SDL_Palette");
  
  lua_StructBegin(20);
    lua_setField("format",0,32,"integer_type","unsigned int");
    lua_setField("palette",32,32,"pointer_type","SDL_Palette");
    lua_setField("BitsPerPixel",64,8,"integer_type","unsigned char");
    lua_setField("BytesPerPixel",72,8,"integer_type","unsigned char");
    lua_setField("padding",80,16,"array_type","unsigned char");
    lua_setField("Rmask",96,32,"integer_type","unsigned int");
    lua_setField("Gmask",128,32,"integer_type","unsigned int");
    lua_setField("Bmask",160,32,"integer_type","unsigned int");
    lua_setField("Amask",192,32,"integer_type","unsigned int");
    lua_setField("Rloss",224,8,"integer_type","unsigned char");
    lua_setField("Gloss",232,8,"integer_type","unsigned char");
    lua_setField("Bloss",240,8,"integer_type","unsigned char");
    lua_setField("Aloss",248,8,"integer_type","unsigned char");
    lua_setField("Rshift",256,8,"integer_type","unsigned char");
    lua_setField("Gshift",264,8,"integer_type","unsigned char");
    lua_setField("Bshift",272,8,"integer_type","unsigned char");
    lua_setField("Ashift",280,8,"integer_type","unsigned char");
    lua_setField("refcount",288,32,"integer_type","int");
    lua_setField("next",320,32,"pointer_type","SDL_PixelFormat");
  lua_StructSize(352);

  lua_StructEnd("SDL_PixelFormat");
  
  lua_StructBegin(3);
    lua_setField("x",0,32,"integer_type","int");
    lua_setField("y",32,32,"integer_type","int");
  lua_StructSize(64);

  lua_StructEnd("SDL_Point");
  
  lua_StructBegin(3);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
  lua_StructSize(64);

  lua_StructEnd("SDL_QuitEvent");
  
  lua_StructBegin(8);
    lua_setField("size",0,32,"pointer_type","__callback");
    lua_setField("seek",32,32,"pointer_type","__callback");
    lua_setField("read",64,32,"pointer_type","__callback");
    lua_setField("write",96,32,"pointer_type","__callback");
    lua_setField("close",128,32,"pointer_type","__callback");
    lua_setField("type",160,32,"integer_type","unsigned int");
    lua_setField("hidden",192,160,"union_type","__union");
  lua_StructSize(352);

  lua_StructEnd("SDL_RWops");
  
  lua_StructBegin(5);
    lua_setField("x",0,32,"integer_type","int");
    lua_setField("y",32,32,"integer_type","int");
    lua_setField("w",64,32,"integer_type","int");
    lua_setField("h",96,32,"integer_type","int");
  lua_StructSize(128);

  lua_StructEnd("SDL_Rect");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_Renderer");
  
  lua_StructBegin(7);
    lua_setField("name",0,32,"pointer_type","char");
    lua_setField("flags",32,32,"integer_type","unsigned int");
    lua_setField("num_texture_formats",64,32,"integer_type","unsigned int");
    lua_setField("texture_formats",96,512,"array_type","unsigned int");
    lua_setField("max_texture_width",608,32,"integer_type","int");
    lua_setField("max_texture_height",640,32,"integer_type","int");
  lua_StructSize(672);

  lua_StructEnd("SDL_RendererInfo");
  
  lua_StructBegin(13);
    lua_setField("flags",0,32,"integer_type","unsigned int");
    lua_setField("format",32,32,"pointer_type","SDL_PixelFormat");
    lua_setField("w",64,32,"integer_type","int");
    lua_setField("h",96,32,"integer_type","int");
    lua_setField("pitch",128,32,"integer_type","int");
    lua_setField("pixels",160,32,"pointer_type","void*");
    lua_setField("userdata",192,32,"pointer_type","void*");
    lua_setField("locked",224,32,"integer_type","int");
    lua_setField("lock_data",256,32,"pointer_type","void*");
    lua_setField("clip_rect",288,128,"record_type","SDL_Rect");
    lua_setField("map",416,32,"pointer_type","SDL_BlitMap");
    lua_setField("refcount",448,32,"integer_type","int");
  lua_StructSize(480);

  lua_StructEnd("SDL_Surface");
  
  lua_StructBegin(4);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("msg",64,32,"pointer_type","SDL_SysWMmsg");
  lua_StructSize(96);

  lua_StructEnd("SDL_SysWMEvent");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_SysWMmsg");
  
  lua_StructBegin(7);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("text",96,256,"array_type","char");
    lua_setField("start",352,32,"integer_type","int");
    lua_setField("length",384,32,"integer_type","int");
  lua_StructSize(416);

  lua_StructEnd("SDL_TextEditingEvent");
  
  lua_StructBegin(5);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("text",96,256,"array_type","char");
  lua_StructSize(352);

  lua_StructEnd("SDL_TextInputEvent");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_Texture");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_Thread");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("touchId",64,64,"integer_type","long long int");
    lua_setField("fingerId",128,64,"integer_type","long long int");
    lua_setField("x",192,32,"real_type","float");
    lua_setField("y",224,32,"real_type","float");
    lua_setField("dx",256,32,"real_type","float");
    lua_setField("dy",288,32,"real_type","float");
    lua_setField("pressure",320,32,"real_type","float");
  lua_StructSize(384);

  lua_StructEnd("SDL_TouchFingerEvent");
  
  lua_StructBegin(7);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("code",96,32,"integer_type","int");
    lua_setField("data1",128,32,"pointer_type","void*");
    lua_setField("data2",160,32,"pointer_type","void*");
  lua_StructSize(192);

  lua_StructEnd("SDL_UserEvent");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_Window");
  
  lua_StructBegin(10);
    lua_setField("type",0,32,"integer_type","unsigned int");
    lua_setField("timestamp",32,32,"integer_type","unsigned int");
    lua_setField("windowID",64,32,"integer_type","unsigned int");
    lua_setField("event",96,8,"integer_type","unsigned char");
    lua_setField("padding1",104,8,"integer_type","unsigned char");
    lua_setField("padding2",112,8,"integer_type","unsigned char");
    lua_setField("padding3",120,8,"integer_type","unsigned char");
    lua_setField("data1",128,32,"integer_type","int");
    lua_setField("data2",160,32,"integer_type","int");
  lua_StructSize(192);

  lua_StructEnd("SDL_WindowEvent");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_cond");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_mutex");
  
  lua_StructBegin(1);
  

  lua_StructEnd("SDL_semaphore");
  
  lua_StructBegin(4);
    lua_setField("major",0,8,"integer_type","unsigned char");
    lua_setField("minor",8,8,"integer_type","unsigned char");
    lua_setField("patch",16,8,"integer_type","unsigned char");
  lua_StructSize(24);

  lua_StructEnd("SDL_version");
  
  return 0;
};

int luaopen_lSDL(lua_State*L){
  return lSDL_openlibs(L);
};

