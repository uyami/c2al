print(require("drw"));

print(drp,dwp);

local m = dcalloc(0x10,1);
print(m);
local x,y,w,h = drp(m,0,"iiii");
print(x,y,w,h);

x = 22;
y = 33;
w = 44;
h = 55;

--dwp(ptr,offset,format,...)
dwp(m,0,"iii",x,y,w,h);

--drp(ptr,offset,format);
x,y,w,h = drp(m,0,"iiii");
print(x,y,w,h);

--drp(ptr,offset,size[,count=1])
x,y,w,h = drp(m,0,4);  --int[2]
print(x,y,w,h);


x = 66;
y = 77;
w = 88;
h = 99
--dwp(ptr,offset,size,...)
dwp(m,0,4,x,y,w,h);
print(drp(m,0,4,4));

dwpint(m,23,34,45,67);
print(drpint(m,4));

dwpfloat(m,23,34,45,67);
print(drpfloat(m,4));

dwpdouble(m,23,34,45,67);
print(drpdouble(m,4));

dwpchar(m,0x23,0x34,0x45,0x56);
print(string.format("%X",drpint(m,4)));

print(drpchar(m,4));

-----------------------------
--error
--dwpchar(m,0x23,nil,0x45,0x56);
--true

dwpnchar(m,12,nil,23,34);
print(drpchar(m,4));

----------------------------

dfree(m);
