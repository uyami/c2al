print(require("lgtk"));
print(require("drw"));
print(require("dcallback"));
null = dpointer(0);

test = 4;
gtk_init(null,null);
window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
--g_signal_connect(window,"destroy",gtk_main_quit_ptr,dpointer(0));

if(test==1)then
    --[[
    struct calldata{
        lua_State*L;
        int  func_ref;
    }
    callfunc1 = void callfunc(struct calldata*d){ lua_rawgeti(d->L,xxx,d->func_ref);lua_call(d->L,0,9);}
    callfunc2 = void callfunc(void*,struct calldata*d) {...};
    callfunc3 = void callfunc(void*,void*,struct calldata*d) {...};
    callfunc4 = void callfunc(void*,void*,void*,struct calldata*d){...};
    ....
    callfunc8
    end
    ]]
    g_signal_connect(window,"destroy",callfunc2,
    callbackdata(
        function()
            print(11111111);
            gtk_main_quit();
        end)
    );
elseif(test==2)then
    function GetBackCallArgsIs(index)
        --return c function pointer
        local callback = _G["callfunc"..index];
        return callback;
    end

    function lclosure(func)
        --return data
        return callbackdata(func);
    end



    g_signal_connect(window,"destroy",GetBackCallArgsIs(2),
    lclosure(
        function()
            print(22222222);
            gtk_main_quit();
        end)
    );

elseif(test==3)then
    local old = g_signal_connect;
    function g_signal_connect(w,s,func,udata)
        return old(w,s,callfunc2,callbackdata(func));
    end

    g_signal_connect(window,"destroy",
        function()
            print(33333333);
            gtk_main_quit();
        end,0
    );
elseif(test==4)then
    require("callbackfunc");
    if(true)then
        --add gc list
        local oldlcall = lcall;
        cfuncs = {};
        cfuncsmetatable = {
            __gc = function(f)
                print("c functions jit gc",f);
                for k,v in pairs(f)do
                    dfree(v);
                end
            end
        }
        setmetatable(cfuncs,cfuncsmetatable);
        function lcall(f)
            table.insert(cfuncs,f);
            return oldlcall(f);
        end
    end;
    local old = g_signal_connect;
    function g_signal_connect(w,s,func,udata)
        old(w,s,lcall(func),dpointer(0));
    end
end

local image = gtk_image_new_from_file("test.bmp");
gtk_container_add(window,image);
print("window",window);
g_signal_connect(window,"destroy",function(ap)
    print("test == 3 ;g_signal_connect is redefinition");
    print("va_list    = ",ap);
    local window = dpointer(dr(ap,0,4));
    print("(1) window = ",window);
    print("(2) data   = ",dpointer(dr(ap,4,4)));
    --[[
        data = struct{
            lua_State*L;
            int       func_ref;
        }
    ]]
    gtk_main_quit();
end,no_user);


g_signal_connect(window,"motion_notify_event",function(ap)
    local window = dpointer(dr(ap,0,4));
    gtk_window_set_title(window,"hello world!");
end,no_user);


gtk_widget_show_all(window);
gtk_main();

