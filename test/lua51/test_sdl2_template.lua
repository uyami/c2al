package.path = package.path .. ";./../../llib/?.lua";
print(require("lSDL"));
print(require("lSDL_image"));
print(require("lSDL_mixer"));
print(require("lSDL_ttf"));

print(require('dcall'));
require("drw")
require("lglx");

SDL_Init(-1);
posx = 100;
posy = 100;
window = SDL_CreateWindow("test",posx,posy,640,480,SDL_WINDOW_OPENGL+SDL_WINDOW_SHOWN);
renderer = SDL_CreateRenderer(window,2,SDL_RENDERER_TARGETTEXTURE+SDL_RENDERER_ACCELERATED);
context = SDL_GL_CreateContext(window);
event = SDL_calloc(0x100,1);
null = dpointer(0);

-----
oldtick = 0;
frame = 0;
while(true)do
    ctick = SDL_GetTicks();
    if((ctick-oldtick)>1000)then
        oldtick = ctick;
        local s = string.format("fps:%d",frame);
        SDL_SetWindowTitle(window,s);
        frame = 0;
    end
    frame = frame + 1;
    local e = SDL_PollEvent(event);
    typ = dr(event,0,4);
    if(typ==SDL_QUIT)then
        break;
    end
    SDL_RenderPresent(renderer);
end


SDL_free(event);
SDL_GL_DeleteContext(context);
SDL_DestroyRenderer(renderer);
SDL_DestroyWindow(window);
SDL_Quit();
