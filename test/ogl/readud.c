#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int readud(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int offset = (int)luaL_checkinteger(L,2);
  int size = (int)luaL_checkinteger(L,3);
  int ret;
  switch(size){
      case 1:ret = *(unsigned char*)a1; break;
      case 2:ret = *(unsigned short*)a1;break;
      case 4:ret = *(int*)a1;break;
      default:ret = *(unsigned char*)a1; break;
  }
  lua_pushinteger(L,ret);
  return 1;
}

int luaopen_readud(lua_State*L){
  lua_register(L,"readud",readud);
  return 0;
};
