package.path = package.path .. ";./../../llib/?.lua";
print(require("lSDL"));
print(require("lSDL_image"));
print(require("lSDL_mixer"));
print(require("lSDL_ttf"));

print(require('dcall'));
require("drw")
require("lglx");

SDL_Init(-1);
IMG_Init(-1);
posx = 100;
posy = 100;
window = SDL_CreateWindow("test",posx,posy,640,480,SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN);
renderer = SDL_CreateRenderer(window,2,SDL_RENDERER_TARGETTEXTURE|SDL_RENDERER_ACCELERATED);
context = SDL_GL_CreateContext(window);
event = SDL_calloc(0x100,1);
null = dpointer(0);
dstRect = SDL_calloc(4*4,1);
srcRect = SDL_calloc(4*4,1);
function DRect(x,y,w,h)
    if(x)then dw(dstRect,0,4,x);   end
    if(y)then dw(dstRect,4,4,y);   end
    if(w)then dw(dstRect,8,4,w);   end
    if(h)then dw(dstRect,12,4,h);  end
    return dstRect;
end

function SRect(x,y,w,h)
    if(x)then dw(srcRect,0,4,x);   end
    if(y)then dw(srcRect,4,4,y);   end
    if(w)then dw(srcRect,8,4,w);   end
    if(h)then dw(srcRect,12,4,h);  end
    return srcRect;
end



--��������
texture = IMG_LoadTexture(renderer,"test.png");
local w = dpointer(dstRect,8);
local h = dpointer(dstRect,12);
--print(texture,null,null,w,h);
SDL_QueryTexture(texture,null,null,w,h);

-----
oldtick = 0;
frame = 0;
while(true)do
    ctick = SDL_GetTicks();
    if((ctick-oldtick)>1000)then
        oldtick = ctick;
        local s = string.format("fps:%d",frame);
        SDL_SetWindowTitle(window,s);
        frame = 0;
    end
    frame = frame + 1;
    local e = SDL_PollEvent(event);
    typ = dr(event,0,4);
    if(typ==SDL_QUIT)then
        break;
    end
    --SDL_RenderCopy(renderer,texture,null,null);
    --SDL_RenderCopyEx(renderer,texture,null,null,40.0,null,0);
    ----------------��������
    SDL_RenderClear(renderer);
    if(false)then
        --SDL_RenderCopy(renderer,texture,null,null);
        SDL_RenderCopy(renderer,texture,null,dstRect);
    else
        SDL_GL_BindTexture(texture,null,null);
        if(x==nil)then x = 1.0 end;
        --glBegin(GL_TRIANGLES);
        
        local zoom = 1;
        local zoom2 = 1;
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        -- glOrtho(
            -- 0,   --left
            -- 640, --right
            -- 0,   --botton
            -- 480, --top
            -- 0,   --near
            -- 1.0 --far
        -- );
        -- glOrtho(
            -- 0,   --left
            -- 640/zoom2 , --right
            -- 480/zoom2 , --botton
            -- 0,   --top
            -- 0,   --near
            -- 1.0 --far
        -- );
        
        local width = dr(dstRect,8,4);
        local height = dr(dstRect,12,4);
        
        glOrtho(
            0,   --left
            640*(640/width) , --right
            480*(480/height) , --botton
            0,   --top
            0,   --near
            1.0 --far
        );
        
        
        zoom = 1.0;
        glBegin(GL_QUADS);
        
        glTexCoord2f(0.0,0.0);
        glVertex3f(0,0,0);
        
        glTexCoord2f(0.0,1.0);
        glVertex3f(0,480/zoom,0);
        
        glTexCoord2f(1.0,1.0);
        glVertex3f(640/zoom/3,480/zoom,0);
        
        glTexCoord2f(1.0,0.0);
        glVertex3f(640/zoom,0,0);
        
        zoom = 1.0;
        local x = 640/2;
        local y = 480/2
        glTexCoord2f(0.0,0.0);
        glVertex3f(x,y,0);
        
        glTexCoord2f(0.0,1.0);
        glVertex3f(x,y+480/zoom,0);
        
        glTexCoord2f(1.0,1.0);
        glVertex3f(x+640/zoom,y+480/zoom,0);
        
        glTexCoord2f(1.0,0.0);
        glVertex3f(x+640/zoom,y+0,0);
        
        
        glEnd();
        x = x *1.1;
    end
    
    --io.stdin:read();
    
    
    SDL_GL_UnbindTexture(texture);
    SDL_GL_SwapWindow(window);
    
    --SDL_RenderPresent(renderer);
end

SDL_DestroyTexture(texture);
SDL_free(event);
SDL_GL_DeleteContext(context);
SDL_DestroyRenderer(renderer);
SDL_DestroyWindow(window);
SDL_Quit();
