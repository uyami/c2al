package.path = package.path .. ";./../../llib/?.lua";
print(require("lSDL"));
print(require("lSDL_image"));
print(require("lSDL_mixer"));
print(require("lSDL_ttf"));

print(require('dcall'));
require("drw")
require("lglx");

SDL_Init(-1);
IMG_Init(-1);
posx = 100;
posy = 100;
window = SDL_CreateWindow("test",posx,posy,640,480,SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN);
renderer = SDL_CreateRenderer(window,2,SDL_RENDERER_TARGETTEXTURE|SDL_RENDERER_ACCELERATED);
context = SDL_GL_CreateContext(window);
texture = IMG_LoadTexture(renderer,"test.png");
event = SDL_calloc(0x100,1);
dstRect = SDL_calloc(4*4,1);
srcRect = SDL_calloc(4,4,1);
null = dpointer(0);

SDL_QueryTexture(texture,null,null,dpointer(dstRect,8),dpointer(dstRect,12));
local x,y,width,height = drpint(dstRect,4);
print(x,y,width,height);
dwpint(srcRect,x,y,width,height); 

-----
oldtick = 0;
frame = 0;
count = 50000;
while(true)do
    ctick = SDL_GetTicks();
    if((ctick-oldtick)>1000)then
        oldtick = ctick;
        local s = string.format("fps:%d %d",frame,count);
        SDL_SetWindowTitle(window,s);
        if(frame>60+30)then
            count = count + 100;
        elseif(frame>60)then
            count = count + 50;
        elseif(frame<30)then
            count = count >> 1;
        else
            count = count - 2;
        end
        frame = 0;
    end
    x = 20
    y = 20;
    SDL_SetRenderTarget(renderer,texture);
    for k=0,count do
        dwpint(srcRect,x,y,width,height); 
        dwpint(dstRect,x,y,width,height); 
        -- dw(srcRect,0,4,x);
        -- dw(srcRect,4,4,y);
        -- dw(srcRect,8,4,width);
        -- dw(srcRect,12,4,height);
        
        -- dw(dstRect,0,4,x+k*4);
        -- dw(dstRect,4,4,y);
        -- dw(dstRect,8,4,width);
        -- dw(dstRect,12,4,height);
        
        SDL_RenderCopy(renderer,texture,srcRect,dstRect);
    end
    SDL_SetRenderTarget(renderer,null);
    SDL_RenderCopy(renderer,texture,srcRect,dstRect);
    frame = frame + 1;
    local e = SDL_PollEvent(event);
    typ = dr(event,0,4);
    if(typ==SDL_QUIT)then
        break;
    end
    SDL_RenderPresent(renderer);
end

SDL_DestroyTexture(texture);
SDL_free(event);
SDL_GL_DeleteContext(context);
SDL_DestroyRenderer(renderer);
SDL_DestroyWindow(window);
SDL_Quit();
