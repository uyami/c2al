
#include <GL/gl.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

static int lglAccum(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glAccum(a1,a2);
  return 0;
};

static int lglAlphaFunc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glAlphaFunc(a1,a2);
  return 0;
};

static int lglAreTexturesResident(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)glAreTexturesResident(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglArrayElement(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glArrayElement(a1);
  return 0;
};

static int lglBegin(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glBegin(a1);
  return 0;
};

static int lglBindTexture(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glBindTexture(a1,a2);
  return 0;
};

static int lglBitmap(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  void* a7 = 0;
  if(lua_isstring(L,7)){
    a7 = (void*)lua_tostring(L,7);
  }
  else if(lua_islightuserdata(L,7)/*||lua_isuserdata(L,7)*/){
    a7 = (void*)lua_touserdata(L,7);
  }
  else if(lua_isuserdata(L,7)){
    a7 = *(void**)lua_touserdata(L,7);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glBitmap(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lglBlendFunc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glBlendFunc(a1,a2);
  return 0;
};

static int lglCallList(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glCallList(a1);
  return 0;
};

static int lglCallLists(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glCallLists(a1,a2,a3);
  return 0;
};

static int lglClear(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glClear(a1);
  return 0;
};

static int lglClearAccum(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glClearAccum(a1,a2,a3,a4);
  return 0;
};

static int lglClearColor(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glClearColor(a1,a2,a3,a4);
  return 0;
};

static int lglClearDepth(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glClearDepth(a1);
  return 0;
};

static int lglClearIndex(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glClearIndex(a1);
  return 0;
};

static int lglClearStencil(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glClearStencil(a1);
  return 0;
};

static int lglClipPlane(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glClipPlane(a1,a2);
  return 0;
};

static int lglColor3b(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3b(a1,a2,a3);
  return 0;
};

static int lglColor3bv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3bv(a1);
  return 0;
};

static int lglColor3d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glColor3d(a1,a2,a3);
  return 0;
};

static int lglColor3dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3dv(a1);
  return 0;
};

static int lglColor3f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glColor3f(a1,a2,a3);
  return 0;
};

static int lglColor3fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3fv(a1);
  return 0;
};

static int lglColor3i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3i(a1,a2,a3);
  return 0;
};

static int lglColor3iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3iv(a1);
  return 0;
};

static int lglColor3s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3s(a1,a2,a3);
  return 0;
};

static int lglColor3sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3sv(a1);
  return 0;
};

static int lglColor3ub(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3ub(a1,a2,a3);
  return 0;
};

static int lglColor3ubv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3ubv(a1);
  return 0;
};

static int lglColor3ui(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3ui(a1,a2,a3);
  return 0;
};

static int lglColor3uiv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3uiv(a1);
  return 0;
};

static int lglColor3us(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glColor3us(a1,a2,a3);
  return 0;
};

static int lglColor3usv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor3usv(a1);
  return 0;
};

static int lglColor4b(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4b(a1,a2,a3,a4);
  return 0;
};

static int lglColor4bv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4bv(a1);
  return 0;
};

static int lglColor4d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glColor4d(a1,a2,a3,a4);
  return 0;
};

static int lglColor4dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4dv(a1);
  return 0;
};

static int lglColor4f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glColor4f(a1,a2,a3,a4);
  return 0;
};

static int lglColor4fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4fv(a1);
  return 0;
};

static int lglColor4i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4i(a1,a2,a3,a4);
  return 0;
};

static int lglColor4iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4iv(a1);
  return 0;
};

static int lglColor4s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4s(a1,a2,a3,a4);
  return 0;
};

static int lglColor4sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4sv(a1);
  return 0;
};

static int lglColor4ub(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4ub(a1,a2,a3,a4);
  return 0;
};

static int lglColor4ubv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4ubv(a1);
  return 0;
};

static int lglColor4ui(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4ui(a1,a2,a3,a4);
  return 0;
};

static int lglColor4uiv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4uiv(a1);
  return 0;
};

static int lglColor4us(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColor4us(a1,a2,a3,a4);
  return 0;
};

static int lglColor4usv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColor4usv(a1);
  return 0;
};

static int lglColorMask(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glColorMask(a1,a2,a3,a4);
  return 0;
};

static int lglColorMaterial(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glColorMaterial(a1,a2);
  return 0;
};

static int lglColorPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glColorPointer(a1,a2,a3,a4);
  return 0;
};

static int lglCopyPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  glCopyPixels(a1,a2,a3,a4,a5);
  return 0;
};

static int lglCopyTexImage1D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  glCopyTexImage1D(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lglCopyTexImage2D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  glCopyTexImage2D(a1,a2,a3,a4,a5,a6,a7,a8);
  return 0;
};

static int lglCopyTexSubImage1D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  glCopyTexSubImage1D(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglCopyTexSubImage2D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  glCopyTexSubImage2D(a1,a2,a3,a4,a5,a6,a7,a8);
  return 0;
};

static int lglCullFace(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glCullFace(a1);
  return 0;
};

static int lglDeleteLists(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glDeleteLists(a1,a2);
  return 0;
};

static int lglDeleteTextures(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glDeleteTextures(a1,a2);
  return 0;
};

static int lglDepthFunc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glDepthFunc(a1);
  return 0;
};

static int lglDepthMask(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glDepthMask(a1);
  return 0;
};

static int lglDepthRange(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glDepthRange(a1,a2);
  return 0;
};

static int lglDisable(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glDisable(a1);
  return 0;
};

static int lglDisableClientState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glDisableClientState(a1);
  return 0;
};

static int lglDrawArrays(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glDrawArrays(a1,a2,a3);
  return 0;
};

static int lglDrawBuffer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glDrawBuffer(a1);
  return 0;
};

static int lglDrawElements(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glDrawElements(a1,a2,a3,a4);
  return 0;
};

static int lglDrawPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glDrawPixels(a1,a2,a3,a4,a5);
  return 0;
};

static int lglEdgeFlag(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glEdgeFlag(a1);
  return 0;
};

static int lglEdgeFlagPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEdgeFlagPointer(a1,a2);
  return 0;
};

static int lglEdgeFlagv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEdgeFlagv(a1);
  return 0;
};

static int lglEnable(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glEnable(a1);
  return 0;
};

static int lglEnableClientState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glEnableClientState(a1);
  return 0;
};

static int lglEnd(lua_State*L){
  glEnd();
  return 0;
};

static int lglEndList(lua_State*L){
  glEndList();
  return 0;
};

static int lglEvalCoord1d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glEvalCoord1d(a1);
  return 0;
};

static int lglEvalCoord1dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEvalCoord1dv(a1);
  return 0;
};

static int lglEvalCoord1f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glEvalCoord1f(a1);
  return 0;
};

static int lglEvalCoord1fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEvalCoord1fv(a1);
  return 0;
};

static int lglEvalCoord2d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glEvalCoord2d(a1,a2);
  return 0;
};

static int lglEvalCoord2dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEvalCoord2dv(a1);
  return 0;
};

static int lglEvalCoord2f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glEvalCoord2f(a1,a2);
  return 0;
};

static int lglEvalCoord2fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glEvalCoord2fv(a1);
  return 0;
};

static int lglEvalMesh1(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glEvalMesh1(a1,a2,a3);
  return 0;
};

static int lglEvalMesh2(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  glEvalMesh2(a1,a2,a3,a4,a5);
  return 0;
};

static int lglEvalPoint1(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glEvalPoint1(a1);
  return 0;
};

static int lglEvalPoint2(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glEvalPoint2(a1,a2);
  return 0;
};

static int lglFeedbackBuffer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glFeedbackBuffer(a1,a2,a3);
  return 0;
};

static int lglFinish(lua_State*L){
  glFinish();
  return 0;
};

static int lglFlush(lua_State*L){
  glFlush();
  return 0;
};

static int lglFogf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glFogf(a1,a2);
  return 0;
};

static int lglFogfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glFogfv(a1,a2);
  return 0;
};

static int lglFogi(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glFogi(a1,a2);
  return 0;
};

static int lglFogiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glFogiv(a1,a2);
  return 0;
};

static int lglFrontFace(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glFrontFace(a1);
  return 0;
};

static int lglFrustum(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  glFrustum(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglGenLists(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)glGenLists(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglGenTextures(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGenTextures(a1,a2);
  return 0;
};

static int lglGetBooleanv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetBooleanv(a1,a2);
  return 0;
};

static int lglGetClipPlane(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetClipPlane(a1,a2);
  return 0;
};

static int lglGetDoublev(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetDoublev(a1,a2);
  return 0;
};

static int lglGetError(lua_State*L){
  int ret = (int)glGetError();
  lua_pushinteger(L,ret);
  return 1;
};

static int lglGetFloatv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetFloatv(a1,a2);
  return 0;
};

static int lglGetIntegerv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetIntegerv(a1,a2);
  return 0;
};

static int lglGetLightfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetLightfv(a1,a2,a3);
  return 0;
};

static int lglGetLightiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetLightiv(a1,a2,a3);
  return 0;
};

static int lglGetMapdv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetMapdv(a1,a2,a3);
  return 0;
};

static int lglGetMapfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetMapfv(a1,a2,a3);
  return 0;
};

static int lglGetMapiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetMapiv(a1,a2,a3);
  return 0;
};

static int lglGetMaterialfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetMaterialfv(a1,a2,a3);
  return 0;
};

static int lglGetMaterialiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetMaterialiv(a1,a2,a3);
  return 0;
};

static int lglGetPixelMapfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetPixelMapfv(a1,a2);
  return 0;
};

static int lglGetPixelMapuiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetPixelMapuiv(a1,a2);
  return 0;
};

static int lglGetPixelMapusv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetPixelMapusv(a1,a2);
  return 0;
};

static int lglGetPointerv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetPointerv(a1,a2);
  return 0;
};

static int lglGetPolygonStipple(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetPolygonStipple(a1);
  return 0;
};

static int lglGetString(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)glGetString(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lglGetTexEnvfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexEnvfv(a1,a2,a3);
  return 0;
};

static int lglGetTexEnviv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexEnviv(a1,a2,a3);
  return 0;
};

static int lglGetTexGendv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexGendv(a1,a2,a3);
  return 0;
};

static int lglGetTexGenfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexGenfv(a1,a2,a3);
  return 0;
};

static int lglGetTexGeniv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexGeniv(a1,a2,a3);
  return 0;
};

static int lglGetTexImage(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexImage(a1,a2,a3,a4,a5);
  return 0;
};

static int lglGetTexLevelParameterfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexLevelParameterfv(a1,a2,a3,a4);
  return 0;
};

static int lglGetTexLevelParameteriv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexLevelParameteriv(a1,a2,a3,a4);
  return 0;
};

static int lglGetTexParameterfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexParameterfv(a1,a2,a3);
  return 0;
};

static int lglGetTexParameteriv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glGetTexParameteriv(a1,a2,a3);
  return 0;
};

static int lglHint(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glHint(a1,a2);
  return 0;
};

static int lglIndexMask(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glIndexMask(a1);
  return 0;
};

static int lglIndexPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexPointer(a1,a2,a3);
  return 0;
};

static int lglIndexd(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glIndexd(a1);
  return 0;
};

static int lglIndexdv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexdv(a1);
  return 0;
};

static int lglIndexf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glIndexf(a1);
  return 0;
};

static int lglIndexfv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexfv(a1);
  return 0;
};

static int lglIndexi(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glIndexi(a1);
  return 0;
};

static int lglIndexiv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexiv(a1);
  return 0;
};

static int lglIndexs(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glIndexs(a1);
  return 0;
};

static int lglIndexsv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexsv(a1);
  return 0;
};

static int lglIndexub(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glIndexub(a1);
  return 0;
};

static int lglIndexubv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glIndexubv(a1);
  return 0;
};

static int lglInitNames(lua_State*L){
  glInitNames();
  return 0;
};

static int lglInterleavedArrays(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glInterleavedArrays(a1,a2,a3);
  return 0;
};

static int lglIsEnabled(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)glIsEnabled(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglIsList(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)glIsList(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglIsTexture(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)glIsTexture(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglLightModelf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glLightModelf(a1,a2);
  return 0;
};

static int lglLightModelfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLightModelfv(a1,a2);
  return 0;
};

static int lglLightModeli(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glLightModeli(a1,a2);
  return 0;
};

static int lglLightModeliv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLightModeliv(a1,a2);
  return 0;
};

static int lglLightf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glLightf(a1,a2,a3);
  return 0;
};

static int lglLightfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLightfv(a1,a2,a3);
  return 0;
};

static int lglLighti(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glLighti(a1,a2,a3);
  return 0;
};

static int lglLightiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLightiv(a1,a2,a3);
  return 0;
};

static int lglLineStipple(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glLineStipple(a1,a2);
  return 0;
};

static int lglLineWidth(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glLineWidth(a1);
  return 0;
};

static int lglListBase(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glListBase(a1);
  return 0;
};

static int lglLoadIdentity(lua_State*L){
  glLoadIdentity();
  return 0;
};

static int lglLoadMatrixd(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLoadMatrixd(a1);
  return 0;
};

static int lglLoadMatrixf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glLoadMatrixf(a1);
  return 0;
};

static int lglLoadName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glLoadName(a1);
  return 0;
};

static int lglLogicOp(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glLogicOp(a1);
  return 0;
};

static int lglMap1d(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = 0;
  if(lua_isstring(L,6)){
    a6 = (void*)lua_tostring(L,6);
  }
  else if(lua_islightuserdata(L,6)/*||lua_isuserdata(L,6)*/){
    a6 = (void*)lua_touserdata(L,6);
  }
  else if(lua_isuserdata(L,6)){
    a6 = *(void**)lua_touserdata(L,6);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMap1d(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglMap1f(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* a6 = 0;
  if(lua_isstring(L,6)){
    a6 = (void*)lua_tostring(L,6);
  }
  else if(lua_islightuserdata(L,6)/*||lua_isuserdata(L,6)*/){
    a6 = (void*)lua_touserdata(L,6);
  }
  else if(lua_isuserdata(L,6)){
    a6 = *(void**)lua_touserdata(L,6);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMap1f(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglMap2d(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* a10 = 0;
  if(lua_isstring(L,10)){
    a10 = (void*)lua_tostring(L,10);
  }
  else if(lua_islightuserdata(L,10)/*||lua_isuserdata(L,10)*/){
    a10 = (void*)lua_touserdata(L,10);
  }
  else if(lua_isuserdata(L,10)){
    a10 = *(void**)lua_touserdata(L,10);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMap2d(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  return 0;
};

static int lglMap2f(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  double a6 = luaL_checknumber(L,6);
  double a7 = luaL_checknumber(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* a10 = 0;
  if(lua_isstring(L,10)){
    a10 = (void*)lua_tostring(L,10);
  }
  else if(lua_islightuserdata(L,10)/*||lua_isuserdata(L,10)*/){
    a10 = (void*)lua_touserdata(L,10);
  }
  else if(lua_isuserdata(L,10)){
    a10 = *(void**)lua_touserdata(L,10);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMap2f(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10);
  return 0;
};

static int lglMapGrid1d(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glMapGrid1d(a1,a2,a3);
  return 0;
};

static int lglMapGrid1f(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glMapGrid1f(a1,a2,a3);
  return 0;
};

static int lglMapGrid2d(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  glMapGrid2d(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglMapGrid2f(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  glMapGrid2f(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglMaterialf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glMaterialf(a1,a2,a3);
  return 0;
};

static int lglMaterialfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMaterialfv(a1,a2,a3);
  return 0;
};

static int lglMateriali(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glMateriali(a1,a2,a3);
  return 0;
};

static int lglMaterialiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMaterialiv(a1,a2,a3);
  return 0;
};

static int lglMatrixMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glMatrixMode(a1);
  return 0;
};

static int lglMultMatrixd(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMultMatrixd(a1);
  return 0;
};

static int lglMultMatrixf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glMultMatrixf(a1);
  return 0;
};

static int lglNewList(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glNewList(a1,a2);
  return 0;
};

static int lglNormal3b(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glNormal3b(a1,a2,a3);
  return 0;
};

static int lglNormal3bv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormal3bv(a1);
  return 0;
};

static int lglNormal3d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glNormal3d(a1,a2,a3);
  return 0;
};

static int lglNormal3dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormal3dv(a1);
  return 0;
};

static int lglNormal3f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glNormal3f(a1,a2,a3);
  return 0;
};

static int lglNormal3fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormal3fv(a1);
  return 0;
};

static int lglNormal3i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glNormal3i(a1,a2,a3);
  return 0;
};

static int lglNormal3iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormal3iv(a1);
  return 0;
};

static int lglNormal3s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glNormal3s(a1,a2,a3);
  return 0;
};

static int lglNormal3sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormal3sv(a1);
  return 0;
};

static int lglNormalPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glNormalPointer(a1,a2,a3);
  return 0;
};

static int lglOrtho(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  double a5 = luaL_checknumber(L,5);
  double a6 = luaL_checknumber(L,6);
  glOrtho(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lglPassThrough(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glPassThrough(a1);
  return 0;
};

static int lglPixelMapfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glPixelMapfv(a1,a2,a3);
  return 0;
};

static int lglPixelMapuiv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glPixelMapuiv(a1,a2,a3);
  return 0;
};

static int lglPixelMapusv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glPixelMapusv(a1,a2,a3);
  return 0;
};

static int lglPixelStoref(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glPixelStoref(a1,a2);
  return 0;
};

static int lglPixelStorei(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glPixelStorei(a1,a2);
  return 0;
};

static int lglPixelTransferf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  double a2 = luaL_checknumber(L,2);
  glPixelTransferf(a1,a2);
  return 0;
};

static int lglPixelTransferi(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glPixelTransferi(a1,a2);
  return 0;
};

static int lglPixelZoom(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glPixelZoom(a1,a2);
  return 0;
};

static int lglPointSize(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glPointSize(a1);
  return 0;
};

static int lglPolygonMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glPolygonMode(a1,a2);
  return 0;
};

static int lglPolygonOffset(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glPolygonOffset(a1,a2);
  return 0;
};

static int lglPolygonStipple(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glPolygonStipple(a1);
  return 0;
};

static int lglPopAttrib(lua_State*L){
  glPopAttrib();
  return 0;
};

static int lglPopClientAttrib(lua_State*L){
  glPopClientAttrib();
  return 0;
};

static int lglPopMatrix(lua_State*L){
  glPopMatrix();
  return 0;
};

static int lglPopName(lua_State*L){
  glPopName();
  return 0;
};

static int lglPrioritizeTextures(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glPrioritizeTextures(a1,a2,a3);
  return 0;
};

static int lglPushAttrib(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glPushAttrib(a1);
  return 0;
};

static int lglPushClientAttrib(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glPushClientAttrib(a1);
  return 0;
};

static int lglPushMatrix(lua_State*L){
  glPushMatrix();
  return 0;
};

static int lglPushName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glPushName(a1);
  return 0;
};

static int lglRasterPos2d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glRasterPos2d(a1,a2);
  return 0;
};

static int lglRasterPos2dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos2dv(a1);
  return 0;
};

static int lglRasterPos2f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glRasterPos2f(a1,a2);
  return 0;
};

static int lglRasterPos2fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos2fv(a1);
  return 0;
};

static int lglRasterPos2i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glRasterPos2i(a1,a2);
  return 0;
};

static int lglRasterPos2iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos2iv(a1);
  return 0;
};

static int lglRasterPos2s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glRasterPos2s(a1,a2);
  return 0;
};

static int lglRasterPos2sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos2sv(a1);
  return 0;
};

static int lglRasterPos3d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glRasterPos3d(a1,a2,a3);
  return 0;
};

static int lglRasterPos3dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos3dv(a1);
  return 0;
};

static int lglRasterPos3f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glRasterPos3f(a1,a2,a3);
  return 0;
};

static int lglRasterPos3fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos3fv(a1);
  return 0;
};

static int lglRasterPos3i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glRasterPos3i(a1,a2,a3);
  return 0;
};

static int lglRasterPos3iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos3iv(a1);
  return 0;
};

static int lglRasterPos3s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glRasterPos3s(a1,a2,a3);
  return 0;
};

static int lglRasterPos3sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos3sv(a1);
  return 0;
};

static int lglRasterPos4d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRasterPos4d(a1,a2,a3,a4);
  return 0;
};

static int lglRasterPos4dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos4dv(a1);
  return 0;
};

static int lglRasterPos4f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRasterPos4f(a1,a2,a3,a4);
  return 0;
};

static int lglRasterPos4fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos4fv(a1);
  return 0;
};

static int lglRasterPos4i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glRasterPos4i(a1,a2,a3,a4);
  return 0;
};

static int lglRasterPos4iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos4iv(a1);
  return 0;
};

static int lglRasterPos4s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glRasterPos4s(a1,a2,a3,a4);
  return 0;
};

static int lglRasterPos4sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRasterPos4sv(a1);
  return 0;
};

static int lglReadBuffer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glReadBuffer(a1);
  return 0;
};

static int lglReadPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = 0;
  if(lua_isstring(L,7)){
    a7 = (void*)lua_tostring(L,7);
  }
  else if(lua_islightuserdata(L,7)/*||lua_isuserdata(L,7)*/){
    a7 = (void*)lua_touserdata(L,7);
  }
  else if(lua_isuserdata(L,7)){
    a7 = *(void**)lua_touserdata(L,7);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glReadPixels(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lglRectd(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRectd(a1,a2,a3,a4);
  return 0;
};

static int lglRectdv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRectdv(a1,a2);
  return 0;
};

static int lglRectf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRectf(a1,a2,a3,a4);
  return 0;
};

static int lglRectfv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRectfv(a1,a2);
  return 0;
};

static int lglRecti(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glRecti(a1,a2,a3,a4);
  return 0;
};

static int lglRectiv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRectiv(a1,a2);
  return 0;
};

static int lglRects(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glRects(a1,a2,a3,a4);
  return 0;
};

static int lglRectsv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glRectsv(a1,a2);
  return 0;
};

static int lglRenderMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)glRenderMode(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lglRotated(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRotated(a1,a2,a3,a4);
  return 0;
};

static int lglRotatef(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glRotatef(a1,a2,a3,a4);
  return 0;
};

static int lglScaled(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glScaled(a1,a2,a3);
  return 0;
};

static int lglScalef(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glScalef(a1,a2,a3);
  return 0;
};

static int lglScissor(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glScissor(a1,a2,a3,a4);
  return 0;
};

static int lglSelectBuffer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glSelectBuffer(a1,a2);
  return 0;
};

static int lglShadeModel(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glShadeModel(a1);
  return 0;
};

static int lglStencilFunc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glStencilFunc(a1,a2,a3);
  return 0;
};

static int lglStencilMask(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glStencilMask(a1);
  return 0;
};

static int lglStencilOp(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glStencilOp(a1,a2,a3);
  return 0;
};

static int lglTexCoord1d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glTexCoord1d(a1);
  return 0;
};

static int lglTexCoord1dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord1dv(a1);
  return 0;
};

static int lglTexCoord1f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  glTexCoord1f(a1);
  return 0;
};

static int lglTexCoord1fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord1fv(a1);
  return 0;
};

static int lglTexCoord1i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glTexCoord1i(a1);
  return 0;
};

static int lglTexCoord1iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord1iv(a1);
  return 0;
};

static int lglTexCoord1s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  glTexCoord1s(a1);
  return 0;
};

static int lglTexCoord1sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord1sv(a1);
  return 0;
};

static int lglTexCoord2d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glTexCoord2d(a1,a2);
  return 0;
};

static int lglTexCoord2dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord2dv(a1);
  return 0;
};

static int lglTexCoord2f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glTexCoord2f(a1,a2);
  return 0;
};

static int lglTexCoord2fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord2fv(a1);
  return 0;
};

static int lglTexCoord2i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glTexCoord2i(a1,a2);
  return 0;
};

static int lglTexCoord2iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord2iv(a1);
  return 0;
};

static int lglTexCoord2s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glTexCoord2s(a1,a2);
  return 0;
};

static int lglTexCoord2sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord2sv(a1);
  return 0;
};

static int lglTexCoord3d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexCoord3d(a1,a2,a3);
  return 0;
};

static int lglTexCoord3dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord3dv(a1);
  return 0;
};

static int lglTexCoord3f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexCoord3f(a1,a2,a3);
  return 0;
};

static int lglTexCoord3fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord3fv(a1);
  return 0;
};

static int lglTexCoord3i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glTexCoord3i(a1,a2,a3);
  return 0;
};

static int lglTexCoord3iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord3iv(a1);
  return 0;
};

static int lglTexCoord3s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glTexCoord3s(a1,a2,a3);
  return 0;
};

static int lglTexCoord3sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord3sv(a1);
  return 0;
};

static int lglTexCoord4d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glTexCoord4d(a1,a2,a3,a4);
  return 0;
};

static int lglTexCoord4dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord4dv(a1);
  return 0;
};

static int lglTexCoord4f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glTexCoord4f(a1,a2,a3,a4);
  return 0;
};

static int lglTexCoord4fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord4fv(a1);
  return 0;
};

static int lglTexCoord4i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glTexCoord4i(a1,a2,a3,a4);
  return 0;
};

static int lglTexCoord4iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord4iv(a1);
  return 0;
};

static int lglTexCoord4s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glTexCoord4s(a1,a2,a3,a4);
  return 0;
};

static int lglTexCoord4sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoord4sv(a1);
  return 0;
};

static int lglTexCoordPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexCoordPointer(a1,a2,a3,a4);
  return 0;
};

static int lglTexEnvf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexEnvf(a1,a2,a3);
  return 0;
};

static int lglTexEnvfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexEnvfv(a1,a2,a3);
  return 0;
};

static int lglTexEnvi(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glTexEnvi(a1,a2,a3);
  return 0;
};

static int lglTexEnviv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexEnviv(a1,a2,a3);
  return 0;
};

static int lglTexGend(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexGend(a1,a2,a3);
  return 0;
};

static int lglTexGendv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexGendv(a1,a2,a3);
  return 0;
};

static int lglTexGenf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexGenf(a1,a2,a3);
  return 0;
};

static int lglTexGenfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexGenfv(a1,a2,a3);
  return 0;
};

static int lglTexGeni(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glTexGeni(a1,a2,a3);
  return 0;
};

static int lglTexGeniv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexGeniv(a1,a2,a3);
  return 0;
};

static int lglTexImage1D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  void* a8 = 0;
  if(lua_isstring(L,8)){
    a8 = (void*)lua_tostring(L,8);
  }
  else if(lua_islightuserdata(L,8)/*||lua_isuserdata(L,8)*/){
    a8 = (void*)lua_touserdata(L,8);
  }
  else if(lua_isuserdata(L,8)){
    a8 = *(void**)lua_touserdata(L,8);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexImage1D(a1,a2,a3,a4,a5,a6,a7,a8);
  return 0;
};

static int lglTexImage2D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* a9 = 0;
  if(lua_isstring(L,9)){
    a9 = (void*)lua_tostring(L,9);
  }
  else if(lua_islightuserdata(L,9)/*||lua_isuserdata(L,9)*/){
    a9 = (void*)lua_touserdata(L,9);
  }
  else if(lua_isuserdata(L,9)){
    a9 = *(void**)lua_touserdata(L,9);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexImage2D(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  return 0;
};

static int lglTexParameterf(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double a3 = luaL_checknumber(L,3);
  glTexParameterf(a1,a2,a3);
  return 0;
};

static int lglTexParameterfv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexParameterfv(a1,a2,a3);
  return 0;
};

static int lglTexParameteri(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glTexParameteri(a1,a2,a3);
  return 0;
};

static int lglTexParameteriv(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexParameteriv(a1,a2,a3);
  return 0;
};

static int lglTexSubImage1D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = 0;
  if(lua_isstring(L,7)){
    a7 = (void*)lua_tostring(L,7);
  }
  else if(lua_islightuserdata(L,7)/*||lua_isuserdata(L,7)*/){
    a7 = (void*)lua_touserdata(L,7);
  }
  else if(lua_isuserdata(L,7)){
    a7 = *(void**)lua_touserdata(L,7);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexSubImage1D(a1,a2,a3,a4,a5,a6,a7);
  return 0;
};

static int lglTexSubImage2D(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* a9 = 0;
  if(lua_isstring(L,9)){
    a9 = (void*)lua_tostring(L,9);
  }
  else if(lua_islightuserdata(L,9)/*||lua_isuserdata(L,9)*/){
    a9 = (void*)lua_touserdata(L,9);
  }
  else if(lua_isuserdata(L,9)){
    a9 = *(void**)lua_touserdata(L,9);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glTexSubImage2D(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  return 0;
};

static int lglTranslated(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glTranslated(a1,a2,a3);
  return 0;
};

static int lglTranslatef(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glTranslatef(a1,a2,a3);
  return 0;
};

static int lglVertex2d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glVertex2d(a1,a2);
  return 0;
};

static int lglVertex2dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex2dv(a1);
  return 0;
};

static int lglVertex2f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  glVertex2f(a1,a2);
  return 0;
};

static int lglVertex2fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex2fv(a1);
  return 0;
};

static int lglVertex2i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glVertex2i(a1,a2);
  return 0;
};

static int lglVertex2iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex2iv(a1);
  return 0;
};

static int lglVertex2s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  glVertex2s(a1,a2);
  return 0;
};

static int lglVertex2sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex2sv(a1);
  return 0;
};

static int lglVertex3d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glVertex3d(a1,a2,a3);
  return 0;
};

static int lglVertex3dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex3dv(a1);
  return 0;
};

static int lglVertex3f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  glVertex3f(a1,a2,a3);
  return 0;
};

static int lglVertex3fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex3fv(a1);
  return 0;
};

static int lglVertex3i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glVertex3i(a1,a2,a3);
  return 0;
};

static int lglVertex3iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex3iv(a1);
  return 0;
};

static int lglVertex3s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  glVertex3s(a1,a2,a3);
  return 0;
};

static int lglVertex3sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex3sv(a1);
  return 0;
};

static int lglVertex4d(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glVertex4d(a1,a2,a3,a4);
  return 0;
};

static int lglVertex4dv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex4dv(a1);
  return 0;
};

static int lglVertex4f(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  double a4 = luaL_checknumber(L,4);
  glVertex4f(a1,a2,a3,a4);
  return 0;
};

static int lglVertex4fv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex4fv(a1);
  return 0;
};

static int lglVertex4i(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glVertex4i(a1,a2,a3,a4);
  return 0;
};

static int lglVertex4iv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex4iv(a1);
  return 0;
};

static int lglVertex4s(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glVertex4s(a1,a2,a3,a4);
  return 0;
};

static int lglVertex4sv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertex4sv(a1);
  return 0;
};

static int lglVertexPointer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  glVertexPointer(a1,a2,a3,a4);
  return 0;
};

static int lglViewport(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  glViewport(a1,a2,a3,a4);
  return 0;
};

int lgl_openlibs(lua_State*L){
  lua_register(L,"glAccum",lglAccum);
  lua_register(L,"glAlphaFunc",lglAlphaFunc);
  lua_register(L,"glAreTexturesResident",lglAreTexturesResident);
  lua_register(L,"glArrayElement",lglArrayElement);
  lua_register(L,"glBegin",lglBegin);
  lua_register(L,"glBindTexture",lglBindTexture);
  lua_register(L,"glBitmap",lglBitmap);
  lua_register(L,"glBlendFunc",lglBlendFunc);
  lua_register(L,"glCallList",lglCallList);
  lua_register(L,"glCallLists",lglCallLists);
  lua_register(L,"glClear",lglClear);
  lua_register(L,"glClearAccum",lglClearAccum);
  lua_register(L,"glClearColor",lglClearColor);
  lua_register(L,"glClearDepth",lglClearDepth);
  lua_register(L,"glClearIndex",lglClearIndex);
  lua_register(L,"glClearStencil",lglClearStencil);
  lua_register(L,"glClipPlane",lglClipPlane);
  lua_register(L,"glColor3b",lglColor3b);
  lua_register(L,"glColor3bv",lglColor3bv);
  lua_register(L,"glColor3d",lglColor3d);
  lua_register(L,"glColor3dv",lglColor3dv);
  lua_register(L,"glColor3f",lglColor3f);
  lua_register(L,"glColor3fv",lglColor3fv);
  lua_register(L,"glColor3i",lglColor3i);
  lua_register(L,"glColor3iv",lglColor3iv);
  lua_register(L,"glColor3s",lglColor3s);
  lua_register(L,"glColor3sv",lglColor3sv);
  lua_register(L,"glColor3ub",lglColor3ub);
  lua_register(L,"glColor3ubv",lglColor3ubv);
  lua_register(L,"glColor3ui",lglColor3ui);
  lua_register(L,"glColor3uiv",lglColor3uiv);
  lua_register(L,"glColor3us",lglColor3us);
  lua_register(L,"glColor3usv",lglColor3usv);
  lua_register(L,"glColor4b",lglColor4b);
  lua_register(L,"glColor4bv",lglColor4bv);
  lua_register(L,"glColor4d",lglColor4d);
  lua_register(L,"glColor4dv",lglColor4dv);
  lua_register(L,"glColor4f",lglColor4f);
  lua_register(L,"glColor4fv",lglColor4fv);
  lua_register(L,"glColor4i",lglColor4i);
  lua_register(L,"glColor4iv",lglColor4iv);
  lua_register(L,"glColor4s",lglColor4s);
  lua_register(L,"glColor4sv",lglColor4sv);
  lua_register(L,"glColor4ub",lglColor4ub);
  lua_register(L,"glColor4ubv",lglColor4ubv);
  lua_register(L,"glColor4ui",lglColor4ui);
  lua_register(L,"glColor4uiv",lglColor4uiv);
  lua_register(L,"glColor4us",lglColor4us);
  lua_register(L,"glColor4usv",lglColor4usv);
  lua_register(L,"glColorMask",lglColorMask);
  lua_register(L,"glColorMaterial",lglColorMaterial);
  lua_register(L,"glColorPointer",lglColorPointer);
  lua_register(L,"glCopyPixels",lglCopyPixels);
  lua_register(L,"glCopyTexImage1D",lglCopyTexImage1D);
  lua_register(L,"glCopyTexImage2D",lglCopyTexImage2D);
  lua_register(L,"glCopyTexSubImage1D",lglCopyTexSubImage1D);
  lua_register(L,"glCopyTexSubImage2D",lglCopyTexSubImage2D);
  lua_register(L,"glCullFace",lglCullFace);
  lua_register(L,"glDeleteLists",lglDeleteLists);
  lua_register(L,"glDeleteTextures",lglDeleteTextures);
  lua_register(L,"glDepthFunc",lglDepthFunc);
  lua_register(L,"glDepthMask",lglDepthMask);
  lua_register(L,"glDepthRange",lglDepthRange);
  lua_register(L,"glDisable",lglDisable);
  lua_register(L,"glDisableClientState",lglDisableClientState);
  lua_register(L,"glDrawArrays",lglDrawArrays);
  lua_register(L,"glDrawBuffer",lglDrawBuffer);
  lua_register(L,"glDrawElements",lglDrawElements);
  lua_register(L,"glDrawPixels",lglDrawPixels);
  lua_register(L,"glEdgeFlag",lglEdgeFlag);
  lua_register(L,"glEdgeFlagPointer",lglEdgeFlagPointer);
  lua_register(L,"glEdgeFlagv",lglEdgeFlagv);
  lua_register(L,"glEnable",lglEnable);
  lua_register(L,"glEnableClientState",lglEnableClientState);
  lua_register(L,"glEnd",lglEnd);
  lua_register(L,"glEndList",lglEndList);
  lua_register(L,"glEvalCoord1d",lglEvalCoord1d);
  lua_register(L,"glEvalCoord1dv",lglEvalCoord1dv);
  lua_register(L,"glEvalCoord1f",lglEvalCoord1f);
  lua_register(L,"glEvalCoord1fv",lglEvalCoord1fv);
  lua_register(L,"glEvalCoord2d",lglEvalCoord2d);
  lua_register(L,"glEvalCoord2dv",lglEvalCoord2dv);
  lua_register(L,"glEvalCoord2f",lglEvalCoord2f);
  lua_register(L,"glEvalCoord2fv",lglEvalCoord2fv);
  lua_register(L,"glEvalMesh1",lglEvalMesh1);
  lua_register(L,"glEvalMesh2",lglEvalMesh2);
  lua_register(L,"glEvalPoint1",lglEvalPoint1);
  lua_register(L,"glEvalPoint2",lglEvalPoint2);
  lua_register(L,"glFeedbackBuffer",lglFeedbackBuffer);
  lua_register(L,"glFinish",lglFinish);
  lua_register(L,"glFlush",lglFlush);
  lua_register(L,"glFogf",lglFogf);
  lua_register(L,"glFogfv",lglFogfv);
  lua_register(L,"glFogi",lglFogi);
  lua_register(L,"glFogiv",lglFogiv);
  lua_register(L,"glFrontFace",lglFrontFace);
  lua_register(L,"glFrustum",lglFrustum);
  lua_register(L,"glGenLists",lglGenLists);
  lua_register(L,"glGenTextures",lglGenTextures);
  lua_register(L,"glGetBooleanv",lglGetBooleanv);
  lua_register(L,"glGetClipPlane",lglGetClipPlane);
  lua_register(L,"glGetDoublev",lglGetDoublev);
  lua_register(L,"glGetError",lglGetError);
  lua_register(L,"glGetFloatv",lglGetFloatv);
  lua_register(L,"glGetIntegerv",lglGetIntegerv);
  lua_register(L,"glGetLightfv",lglGetLightfv);
  lua_register(L,"glGetLightiv",lglGetLightiv);
  lua_register(L,"glGetMapdv",lglGetMapdv);
  lua_register(L,"glGetMapfv",lglGetMapfv);
  lua_register(L,"glGetMapiv",lglGetMapiv);
  lua_register(L,"glGetMaterialfv",lglGetMaterialfv);
  lua_register(L,"glGetMaterialiv",lglGetMaterialiv);
  lua_register(L,"glGetPixelMapfv",lglGetPixelMapfv);
  lua_register(L,"glGetPixelMapuiv",lglGetPixelMapuiv);
  lua_register(L,"glGetPixelMapusv",lglGetPixelMapusv);
  lua_register(L,"glGetPointerv",lglGetPointerv);
  lua_register(L,"glGetPolygonStipple",lglGetPolygonStipple);
  lua_register(L,"glGetString",lglGetString);
  lua_register(L,"glGetTexEnvfv",lglGetTexEnvfv);
  lua_register(L,"glGetTexEnviv",lglGetTexEnviv);
  lua_register(L,"glGetTexGendv",lglGetTexGendv);
  lua_register(L,"glGetTexGenfv",lglGetTexGenfv);
  lua_register(L,"glGetTexGeniv",lglGetTexGeniv);
  lua_register(L,"glGetTexImage",lglGetTexImage);
  lua_register(L,"glGetTexLevelParameterfv",lglGetTexLevelParameterfv);
  lua_register(L,"glGetTexLevelParameteriv",lglGetTexLevelParameteriv);
  lua_register(L,"glGetTexParameterfv",lglGetTexParameterfv);
  lua_register(L,"glGetTexParameteriv",lglGetTexParameteriv);
  lua_register(L,"glHint",lglHint);
  lua_register(L,"glIndexMask",lglIndexMask);
  lua_register(L,"glIndexPointer",lglIndexPointer);
  lua_register(L,"glIndexd",lglIndexd);
  lua_register(L,"glIndexdv",lglIndexdv);
  lua_register(L,"glIndexf",lglIndexf);
  lua_register(L,"glIndexfv",lglIndexfv);
  lua_register(L,"glIndexi",lglIndexi);
  lua_register(L,"glIndexiv",lglIndexiv);
  lua_register(L,"glIndexs",lglIndexs);
  lua_register(L,"glIndexsv",lglIndexsv);
  lua_register(L,"glIndexub",lglIndexub);
  lua_register(L,"glIndexubv",lglIndexubv);
  lua_register(L,"glInitNames",lglInitNames);
  lua_register(L,"glInterleavedArrays",lglInterleavedArrays);
  lua_register(L,"glIsEnabled",lglIsEnabled);
  lua_register(L,"glIsList",lglIsList);
  lua_register(L,"glIsTexture",lglIsTexture);
  lua_register(L,"glLightModelf",lglLightModelf);
  lua_register(L,"glLightModelfv",lglLightModelfv);
  lua_register(L,"glLightModeli",lglLightModeli);
  lua_register(L,"glLightModeliv",lglLightModeliv);
  lua_register(L,"glLightf",lglLightf);
  lua_register(L,"glLightfv",lglLightfv);
  lua_register(L,"glLighti",lglLighti);
  lua_register(L,"glLightiv",lglLightiv);
  lua_register(L,"glLineStipple",lglLineStipple);
  lua_register(L,"glLineWidth",lglLineWidth);
  lua_register(L,"glListBase",lglListBase);
  lua_register(L,"glLoadIdentity",lglLoadIdentity);
  lua_register(L,"glLoadMatrixd",lglLoadMatrixd);
  lua_register(L,"glLoadMatrixf",lglLoadMatrixf);
  lua_register(L,"glLoadName",lglLoadName);
  lua_register(L,"glLogicOp",lglLogicOp);
  lua_register(L,"glMap1d",lglMap1d);
  lua_register(L,"glMap1f",lglMap1f);
  lua_register(L,"glMap2d",lglMap2d);
  lua_register(L,"glMap2f",lglMap2f);
  lua_register(L,"glMapGrid1d",lglMapGrid1d);
  lua_register(L,"glMapGrid1f",lglMapGrid1f);
  lua_register(L,"glMapGrid2d",lglMapGrid2d);
  lua_register(L,"glMapGrid2f",lglMapGrid2f);
  lua_register(L,"glMaterialf",lglMaterialf);
  lua_register(L,"glMaterialfv",lglMaterialfv);
  lua_register(L,"glMateriali",lglMateriali);
  lua_register(L,"glMaterialiv",lglMaterialiv);
  lua_register(L,"glMatrixMode",lglMatrixMode);
  lua_register(L,"glMultMatrixd",lglMultMatrixd);
  lua_register(L,"glMultMatrixf",lglMultMatrixf);
  lua_register(L,"glNewList",lglNewList);
  lua_register(L,"glNormal3b",lglNormal3b);
  lua_register(L,"glNormal3bv",lglNormal3bv);
  lua_register(L,"glNormal3d",lglNormal3d);
  lua_register(L,"glNormal3dv",lglNormal3dv);
  lua_register(L,"glNormal3f",lglNormal3f);
  lua_register(L,"glNormal3fv",lglNormal3fv);
  lua_register(L,"glNormal3i",lglNormal3i);
  lua_register(L,"glNormal3iv",lglNormal3iv);
  lua_register(L,"glNormal3s",lglNormal3s);
  lua_register(L,"glNormal3sv",lglNormal3sv);
  lua_register(L,"glNormalPointer",lglNormalPointer);
  lua_register(L,"glOrtho",lglOrtho);
  lua_register(L,"glPassThrough",lglPassThrough);
  lua_register(L,"glPixelMapfv",lglPixelMapfv);
  lua_register(L,"glPixelMapuiv",lglPixelMapuiv);
  lua_register(L,"glPixelMapusv",lglPixelMapusv);
  lua_register(L,"glPixelStoref",lglPixelStoref);
  lua_register(L,"glPixelStorei",lglPixelStorei);
  lua_register(L,"glPixelTransferf",lglPixelTransferf);
  lua_register(L,"glPixelTransferi",lglPixelTransferi);
  lua_register(L,"glPixelZoom",lglPixelZoom);
  lua_register(L,"glPointSize",lglPointSize);
  lua_register(L,"glPolygonMode",lglPolygonMode);
  lua_register(L,"glPolygonOffset",lglPolygonOffset);
  lua_register(L,"glPolygonStipple",lglPolygonStipple);
  lua_register(L,"glPopAttrib",lglPopAttrib);
  lua_register(L,"glPopClientAttrib",lglPopClientAttrib);
  lua_register(L,"glPopMatrix",lglPopMatrix);
  lua_register(L,"glPopName",lglPopName);
  lua_register(L,"glPrioritizeTextures",lglPrioritizeTextures);
  lua_register(L,"glPushAttrib",lglPushAttrib);
  lua_register(L,"glPushClientAttrib",lglPushClientAttrib);
  lua_register(L,"glPushMatrix",lglPushMatrix);
  lua_register(L,"glPushName",lglPushName);
  lua_register(L,"glRasterPos2d",lglRasterPos2d);
  lua_register(L,"glRasterPos2dv",lglRasterPos2dv);
  lua_register(L,"glRasterPos2f",lglRasterPos2f);
  lua_register(L,"glRasterPos2fv",lglRasterPos2fv);
  lua_register(L,"glRasterPos2i",lglRasterPos2i);
  lua_register(L,"glRasterPos2iv",lglRasterPos2iv);
  lua_register(L,"glRasterPos2s",lglRasterPos2s);
  lua_register(L,"glRasterPos2sv",lglRasterPos2sv);
  lua_register(L,"glRasterPos3d",lglRasterPos3d);
  lua_register(L,"glRasterPos3dv",lglRasterPos3dv);
  lua_register(L,"glRasterPos3f",lglRasterPos3f);
  lua_register(L,"glRasterPos3fv",lglRasterPos3fv);
  lua_register(L,"glRasterPos3i",lglRasterPos3i);
  lua_register(L,"glRasterPos3iv",lglRasterPos3iv);
  lua_register(L,"glRasterPos3s",lglRasterPos3s);
  lua_register(L,"glRasterPos3sv",lglRasterPos3sv);
  lua_register(L,"glRasterPos4d",lglRasterPos4d);
  lua_register(L,"glRasterPos4dv",lglRasterPos4dv);
  lua_register(L,"glRasterPos4f",lglRasterPos4f);
  lua_register(L,"glRasterPos4fv",lglRasterPos4fv);
  lua_register(L,"glRasterPos4i",lglRasterPos4i);
  lua_register(L,"glRasterPos4iv",lglRasterPos4iv);
  lua_register(L,"glRasterPos4s",lglRasterPos4s);
  lua_register(L,"glRasterPos4sv",lglRasterPos4sv);
  lua_register(L,"glReadBuffer",lglReadBuffer);
  lua_register(L,"glReadPixels",lglReadPixels);
  lua_register(L,"glRectd",lglRectd);
  lua_register(L,"glRectdv",lglRectdv);
  lua_register(L,"glRectf",lglRectf);
  lua_register(L,"glRectfv",lglRectfv);
  lua_register(L,"glRecti",lglRecti);
  lua_register(L,"glRectiv",lglRectiv);
  lua_register(L,"glRects",lglRects);
  lua_register(L,"glRectsv",lglRectsv);
  lua_register(L,"glRenderMode",lglRenderMode);
  lua_register(L,"glRotated",lglRotated);
  lua_register(L,"glRotatef",lglRotatef);
  lua_register(L,"glScaled",lglScaled);
  lua_register(L,"glScalef",lglScalef);
  lua_register(L,"glScissor",lglScissor);
  lua_register(L,"glSelectBuffer",lglSelectBuffer);
  lua_register(L,"glShadeModel",lglShadeModel);
  lua_register(L,"glStencilFunc",lglStencilFunc);
  lua_register(L,"glStencilMask",lglStencilMask);
  lua_register(L,"glStencilOp",lglStencilOp);
  lua_register(L,"glTexCoord1d",lglTexCoord1d);
  lua_register(L,"glTexCoord1dv",lglTexCoord1dv);
  lua_register(L,"glTexCoord1f",lglTexCoord1f);
  lua_register(L,"glTexCoord1fv",lglTexCoord1fv);
  lua_register(L,"glTexCoord1i",lglTexCoord1i);
  lua_register(L,"glTexCoord1iv",lglTexCoord1iv);
  lua_register(L,"glTexCoord1s",lglTexCoord1s);
  lua_register(L,"glTexCoord1sv",lglTexCoord1sv);
  lua_register(L,"glTexCoord2d",lglTexCoord2d);
  lua_register(L,"glTexCoord2dv",lglTexCoord2dv);
  lua_register(L,"glTexCoord2f",lglTexCoord2f);
  lua_register(L,"glTexCoord2fv",lglTexCoord2fv);
  lua_register(L,"glTexCoord2i",lglTexCoord2i);
  lua_register(L,"glTexCoord2iv",lglTexCoord2iv);
  lua_register(L,"glTexCoord2s",lglTexCoord2s);
  lua_register(L,"glTexCoord2sv",lglTexCoord2sv);
  lua_register(L,"glTexCoord3d",lglTexCoord3d);
  lua_register(L,"glTexCoord3dv",lglTexCoord3dv);
  lua_register(L,"glTexCoord3f",lglTexCoord3f);
  lua_register(L,"glTexCoord3fv",lglTexCoord3fv);
  lua_register(L,"glTexCoord3i",lglTexCoord3i);
  lua_register(L,"glTexCoord3iv",lglTexCoord3iv);
  lua_register(L,"glTexCoord3s",lglTexCoord3s);
  lua_register(L,"glTexCoord3sv",lglTexCoord3sv);
  lua_register(L,"glTexCoord4d",lglTexCoord4d);
  lua_register(L,"glTexCoord4dv",lglTexCoord4dv);
  lua_register(L,"glTexCoord4f",lglTexCoord4f);
  lua_register(L,"glTexCoord4fv",lglTexCoord4fv);
  lua_register(L,"glTexCoord4i",lglTexCoord4i);
  lua_register(L,"glTexCoord4iv",lglTexCoord4iv);
  lua_register(L,"glTexCoord4s",lglTexCoord4s);
  lua_register(L,"glTexCoord4sv",lglTexCoord4sv);
  lua_register(L,"glTexCoordPointer",lglTexCoordPointer);
  lua_register(L,"glTexEnvf",lglTexEnvf);
  lua_register(L,"glTexEnvfv",lglTexEnvfv);
  lua_register(L,"glTexEnvi",lglTexEnvi);
  lua_register(L,"glTexEnviv",lglTexEnviv);
  lua_register(L,"glTexGend",lglTexGend);
  lua_register(L,"glTexGendv",lglTexGendv);
  lua_register(L,"glTexGenf",lglTexGenf);
  lua_register(L,"glTexGenfv",lglTexGenfv);
  lua_register(L,"glTexGeni",lglTexGeni);
  lua_register(L,"glTexGeniv",lglTexGeniv);
  lua_register(L,"glTexImage1D",lglTexImage1D);
  lua_register(L,"glTexImage2D",lglTexImage2D);
  lua_register(L,"glTexParameterf",lglTexParameterf);
  lua_register(L,"glTexParameterfv",lglTexParameterfv);
  lua_register(L,"glTexParameteri",lglTexParameteri);
  lua_register(L,"glTexParameteriv",lglTexParameteriv);
  lua_register(L,"glTexSubImage1D",lglTexSubImage1D);
  lua_register(L,"glTexSubImage2D",lglTexSubImage2D);
  lua_register(L,"glTranslated",lglTranslated);
  lua_register(L,"glTranslatef",lglTranslatef);
  lua_register(L,"glVertex2d",lglVertex2d);
  lua_register(L,"glVertex2dv",lglVertex2dv);
  lua_register(L,"glVertex2f",lglVertex2f);
  lua_register(L,"glVertex2fv",lglVertex2fv);
  lua_register(L,"glVertex2i",lglVertex2i);
  lua_register(L,"glVertex2iv",lglVertex2iv);
  lua_register(L,"glVertex2s",lglVertex2s);
  lua_register(L,"glVertex2sv",lglVertex2sv);
  lua_register(L,"glVertex3d",lglVertex3d);
  lua_register(L,"glVertex3dv",lglVertex3dv);
  lua_register(L,"glVertex3f",lglVertex3f);
  lua_register(L,"glVertex3fv",lglVertex3fv);
  lua_register(L,"glVertex3i",lglVertex3i);
  lua_register(L,"glVertex3iv",lglVertex3iv);
  lua_register(L,"glVertex3s",lglVertex3s);
  lua_register(L,"glVertex3sv",lglVertex3sv);
  lua_register(L,"glVertex4d",lglVertex4d);
  lua_register(L,"glVertex4dv",lglVertex4dv);
  lua_register(L,"glVertex4f",lglVertex4f);
  lua_register(L,"glVertex4fv",lglVertex4fv);
  lua_register(L,"glVertex4i",lglVertex4i);
  lua_register(L,"glVertex4iv",lglVertex4iv);
  lua_register(L,"glVertex4s",lglVertex4s);
  lua_register(L,"glVertex4sv",lglVertex4sv);
  lua_register(L,"glVertexPointer",lglVertexPointer);
  lua_register(L,"glViewport",lglViewport);
  return 0;
};

int luaopen_lgl(lua_State*L){
  return lgl_openlibs(L);
};

