
#include <SDL2/SDL.h>
#include <assert.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

static int lSDL_AddEventWatch(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_AddEventWatch(a1,a2);
  return 0;
};

static int lSDL_AddHintCallback(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_AddHintCallback(a1,a2,a3);
  return 0;
};

static int lSDL_AddTimer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_AddTimer(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AllocFormat(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_AllocFormat(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AllocPalette(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_AllocPalette(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AllocRW(lua_State*L){
  void* ret = (void*)SDL_AllocRW();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicAdd(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_AtomicAdd(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicCAS(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_AtomicCAS(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicCASPtr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_AtomicCASPtr(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicGet(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_AtomicGet(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicGetPtr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_AtomicGetPtr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicLock(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_AtomicLock(a1);
  return 0;
};

static int lSDL_AtomicSet(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_AtomicSet(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicSetPtr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_AtomicSetPtr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_AtomicTryLock(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_AtomicTryLock(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AtomicUnlock(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_AtomicUnlock(a1);
  return 0;
};

static int lSDL_AudioInit(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_AudioInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_AudioQuit(lua_State*L){
  SDL_AudioQuit();
  return 0;
};

static int lSDL_BuildAudioCVT(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int ret = (int)SDL_BuildAudioCVT(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CalculateGammaRamp(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_CalculateGammaRamp(a1,a2);
  return 0;
};

static int lSDL_CaptureMouse(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_CaptureMouse(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ClearError(lua_State*L){
  SDL_ClearError();
  return 0;
};

static int lSDL_ClearHints(lua_State*L){
  SDL_ClearHints();
  return 0;
};

static int lSDL_ClearQueuedAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_ClearQueuedAudio(a1);
  return 0;
};

static int lSDL_CloseAudio(lua_State*L){
  SDL_CloseAudio();
  return 0;
};

static int lSDL_CloseAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_CloseAudioDevice(a1);
  return 0;
};

static int lSDL_CondBroadcast(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_CondBroadcast(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondSignal(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_CondSignal(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondWait(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_CondWait(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CondWaitTimeout(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_CondWaitTimeout(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertAudio(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ConvertAudio(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertPixels(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = 0;
  if(lua_isstring(L,7)){
    a7 = (void*)lua_tostring(L,7);
  }
  else if(lua_islightuserdata(L,7)/*||lua_isuserdata(L,7)*/){
    a7 = (void*)lua_touserdata(L,7);
  }
  else if(lua_isuserdata(L,7)){
    a7 = *(void**)lua_touserdata(L,7);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a8 = (int)luaL_checkinteger(L,8);
  int ret = (int)SDL_ConvertPixels(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ConvertSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ConvertSurface(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ConvertSurfaceFormat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ConvertSurfaceFormat(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateColorCursor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_CreateColorCursor(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateCond(lua_State*L){
  void* ret = (void*)SDL_CreateCond();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateCursor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateCursor(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateMutex(lua_State*L){
  void* ret = (void*)SDL_CreateMutex();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurface(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  void* ret = (void*)SDL_CreateRGBSurface(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceFrom(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  int a7 = (int)luaL_checkinteger(L,7);
  int a8 = (int)luaL_checkinteger(L,8);
  int a9 = (int)luaL_checkinteger(L,9);
  void* ret = (void*)SDL_CreateRGBSurfaceFrom(a1,a2,a3,a4,a5,a6,a7,a8,a9);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceWithFormat(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)SDL_CreateRGBSurfaceWithFormat(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRGBSurfaceWithFormatFrom(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateRGBSurfaceWithFormatFrom(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateRenderer(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_CreateRenderer(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSemaphore(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_CreateSemaphore(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSoftwareRenderer(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_CreateSoftwareRenderer(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateSystemCursor(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_CreateSystemCursor(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  void* ret = (void*)SDL_CreateTexture(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateTextureFromSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_CreateTextureFromSurface(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateThread(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_CreateThread(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int a6 = (int)luaL_checkinteger(L,6);
  void* ret = (void*)SDL_CreateWindow(a1,a2,a3,a4,a5,a6);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_CreateWindowAndRenderer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_CreateWindowAndRenderer(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_CreateWindowFrom(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_CreateWindowFrom(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_DXGIGetOutputInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_DXGIGetOutputInfo(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DelEventWatch(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DelEventWatch(a1,a2);
  return 0;
};

static int lSDL_DelHintCallback(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DelHintCallback(a1,a2,a3);
  return 0;
};

static int lSDL_Delay(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_Delay(a1);
  return 0;
};

static int lSDL_DequeueAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_DequeueAudio(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DestroyCond(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroyCond(a1);
  return 0;
};

static int lSDL_DestroyMutex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroyMutex(a1);
  return 0;
};

static int lSDL_DestroyRenderer(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroyRenderer(a1);
  return 0;
};

static int lSDL_DestroySemaphore(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroySemaphore(a1);
  return 0;
};

static int lSDL_DestroyTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroyTexture(a1);
  return 0;
};

static int lSDL_DestroyWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DestroyWindow(a1);
  return 0;
};

static int lSDL_DetachThread(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_DetachThread(a1);
  return 0;
};

static int lSDL_Direct3D9GetAdapterIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Direct3D9GetAdapterIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_DisableScreenSaver(lua_State*L){
  SDL_DisableScreenSaver();
  return 0;
};

static int lSDL_EnableScreenSaver(lua_State*L){
  SDL_EnableScreenSaver();
  return 0;
};

static int lSDL_EnclosePoints(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_EnclosePoints(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Error(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Error(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_EventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_EventState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FillRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_FillRect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FillRects(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_FillRects(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_FilterEvents(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FilterEvents(a1,a2);
  return 0;
};

static int lSDL_FlushEvent(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_FlushEvent(a1);
  return 0;
};

static int lSDL_FlushEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_FlushEvents(a1,a2);
  return 0;
};

static int lSDL_FreeCursor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreeCursor(a1);
  return 0;
};

static int lSDL_FreeFormat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreeFormat(a1);
  return 0;
};

static int lSDL_FreePalette(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreePalette(a1);
  return 0;
};

static int lSDL_FreeRW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreeRW(a1);
  return 0;
};

static int lSDL_FreeSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreeSurface(a1);
  return 0;
};

static int lSDL_FreeWAV(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_FreeWAV(a1);
  return 0;
};

static int lSDL_GL_BindTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_BindTexture(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_CreateContext(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GL_CreateContext(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_DeleteContext(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GL_DeleteContext(a1);
  return 0;
};

static int lSDL_GL_ExtensionSupported(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_ExtensionSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_GetAttribute(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_GetAttribute(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_GetCurrentContext(lua_State*L){
  void* ret = (void*)SDL_GL_GetCurrentContext();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetCurrentWindow(lua_State*L){
  void* ret = (void*)SDL_GL_GetCurrentWindow();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetDrawableSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GL_GetDrawableSize(a1,a2,a3);
  return 0;
};

static int lSDL_GL_GetProcAddress(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GL_GetProcAddress(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GL_GetSwapInterval(lua_State*L){
  int ret = (int)SDL_GL_GetSwapInterval();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_LoadLibrary(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_LoadLibrary(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_MakeCurrent(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_MakeCurrent(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_ResetAttributes(lua_State*L){
  SDL_GL_ResetAttributes();
  return 0;
};

static int lSDL_GL_SetAttribute(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GL_SetAttribute(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_SetSwapInterval(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GL_SetSwapInterval(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_SwapWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GL_SwapWindow(a1);
  return 0;
};

static int lSDL_GL_UnbindTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GL_UnbindTexture(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GL_UnloadLibrary(lua_State*L){
  SDL_GL_UnloadLibrary();
  return 0;
};

static int lSDL_GameControllerAddMapping(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GameControllerAddMapping(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerAddMappingsFromRW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerAddMappingsFromRW(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerClose(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GameControllerClose(a1);
  return 0;
};

static int lSDL_GameControllerEventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GameControllerEventState(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerFromInstanceID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerFromInstanceID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAttached(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GameControllerGetAttached(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAxis(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerGetAxis(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetAxisFromString(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GameControllerGetAxisFromString(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetBindForAxis(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_GameControllerGetBindForAxis(a1,a2);
  printf("unsupported type");  assert(0);};

static int lSDL_GameControllerGetBindForButton(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_GameControllerGetBindForButton(a1,a2);
  printf("unsupported type");  assert(0);};

static int lSDL_GameControllerGetButton(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GameControllerGetButton(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetButtonFromString(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GameControllerGetButtonFromString(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GameControllerGetJoystick(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GameControllerGetJoystick(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetStringForAxis(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerGetStringForAxis(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerGetStringForButton(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerGetStringForButton(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerMapping(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GameControllerMapping(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerMappingForGUID(lua_State*L){
  printf("unsupported type");  assert(0);  void* ret = 0;
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerName(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GameControllerName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerNameForIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerNameForIndex(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GameControllerOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GameControllerUpdate(lua_State*L){
  SDL_GameControllerUpdate();
  return 0;
};

static int lSDL_GetAssertionHandler(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetAssertionHandler(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAssertionReport(lua_State*L){
  void* ret = (void*)SDL_GetAssertionReport();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioDeviceName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_GetAudioDeviceName(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioDeviceStatus(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetAudioDeviceStatus(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetAudioDriver(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetAudioDriver(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetAudioStatus(lua_State*L){
  int ret = (int)SDL_GetAudioStatus();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetBasePath(lua_State*L){
  void* ret = (void*)SDL_GetBasePath();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCPUCacheLineSize(lua_State*L){
  int ret = (int)SDL_GetCPUCacheLineSize();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCPUCount(lua_State*L){
  int ret = (int)SDL_GetCPUCount();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetClipRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetClipRect(a1,a2);
  return 0;
};

static int lSDL_GetClipboardText(lua_State*L){
  void* ret = (void*)SDL_GetClipboardText();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetClosestDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetClosestDisplayMode(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetColorKey(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetColorKey(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCurrentAudioDriver(lua_State*L){
  void* ret = (void*)SDL_GetCurrentAudioDriver();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCurrentDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetCurrentDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetCurrentVideoDriver(lua_State*L){
  void* ret = (void*)SDL_GetCurrentVideoDriver();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetCursor(lua_State*L){
  void* ret = (void*)SDL_GetCursor();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDefaultAssertionHandler(lua_State*L){
  void* ret = (void*)SDL_GetDefaultAssertionHandler();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDefaultCursor(lua_State*L){
  void* ret = (void*)SDL_GetDefaultCursor();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDesktopDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetDesktopDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayBounds(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetDisplayBounds(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayDPI(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetDisplayDPI(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetDisplayMode(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetDisplayName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetDisplayName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetDisplayUsableBounds(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetDisplayUsableBounds(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetError(lua_State*L){
  void* ret = (void*)SDL_GetError();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetEventFilter(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetEventFilter(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetGlobalMouseState(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetGlobalMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetGrabbedWindow(lua_State*L){
  void* ret = (void*)SDL_GetGrabbedWindow();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetHint(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetHint(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetHintBoolean(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_GetHintBoolean(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyFromName(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetKeyFromName(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyFromScancode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetKeyFromScancode(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetKeyName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetKeyName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetKeyboardFocus(lua_State*L){
  void* ret = (void*)SDL_GetKeyboardFocus();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetKeyboardState(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetKeyboardState(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetModState(lua_State*L){
  int ret = (int)SDL_GetModState();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetMouseFocus(lua_State*L){
  void* ret = (void*)SDL_GetMouseFocus();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetMouseState(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumAudioDevices(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumAudioDevices(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumAudioDrivers(lua_State*L){
  int ret = (int)SDL_GetNumAudioDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumDisplayModes(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumDisplayModes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumRenderDrivers(lua_State*L){
  int ret = (int)SDL_GetNumRenderDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumTouchDevices(lua_State*L){
  int ret = (int)SDL_GetNumTouchDevices();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumTouchFingers(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetNumTouchFingers(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumVideoDisplays(lua_State*L){
  int ret = (int)SDL_GetNumVideoDisplays();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetNumVideoDrivers(lua_State*L){
  int ret = (int)SDL_GetNumVideoDrivers();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPerformanceCounter(lua_State*L){
  int ret = (int)SDL_GetPerformanceCounter();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPerformanceFrequency(lua_State*L){
  int ret = (int)SDL_GetPerformanceFrequency();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPixelFormatName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetPixelFormatName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetPlatform(lua_State*L){
  void* ret = (void*)SDL_GetPlatform();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetPowerInfo(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetPowerInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetPrefPath(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetPrefPath(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetQueuedAudioSize(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetQueuedAudioSize(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRGB(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetRGB(a1,a2,a3,a4,a5);
  return 0;
};

static int lSDL_GetRGBA(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a6 = 0;
  if(lua_isstring(L,6)){
    a6 = (void*)lua_tostring(L,6);
  }
  else if(lua_islightuserdata(L,6)/*||lua_isuserdata(L,6)*/){
    a6 = (void*)lua_touserdata(L,6);
  }
  else if(lua_isuserdata(L,6)){
    a6 = *(void**)lua_touserdata(L,6);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetRGBA(a1,a2,a3,a4,a5,a6);
  return 0;
};

static int lSDL_GetRelativeMouseMode(lua_State*L){
  int ret = (int)SDL_GetRelativeMouseMode();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRelativeMouseState(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRelativeMouseState(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDrawBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRenderDrawBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDrawColor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRenderDrawColor(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderDriverInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRenderDriverInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRenderTarget(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetRenderTarget(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRenderer(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetRenderer(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRendererInfo(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRendererInfo(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRendererOutputSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetRendererOutputSize(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetRevision(lua_State*L){
  void* ret = (void*)SDL_GetRevision();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetRevisionNumber(lua_State*L){
  int ret = (int)SDL_GetRevisionNumber();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeFromKey(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetScancodeFromKey(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeFromName(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetScancodeFromName(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetScancodeName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetScancodeName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetSurfaceAlphaMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetSurfaceAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSurfaceBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetSurfaceBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSurfaceColorMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetSurfaceColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetSystemRAM(lua_State*L){
  int ret = (int)SDL_GetSystemRAM();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureAlphaMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetTextureAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetTextureBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTextureColorMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetTextureColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetThreadID(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetThreadID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetThreadName(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetThreadName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetTicks(lua_State*L){
  int ret = (int)SDL_GetTicks();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTouchDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_GetTouchDevice(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetTouchFinger(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_GetTouchFinger(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetVersion(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetVersion(a1);
  return 0;
};

static int lSDL_GetVideoDriver(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetVideoDriver(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowBordersSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowBordersSize(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowBrightness(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double ret = (double)SDL_GetWindowBrightness(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_GetWindowData(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetWindowData(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowDisplayIndex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowDisplayIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowDisplayMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowFlags(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowFlags(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowFromID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_GetWindowFromID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowGammaRamp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowGammaRamp(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowGrab(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowGrab(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowID(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowMaximumSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetWindowMaximumSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowMinimumSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetWindowMinimumSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowOpacity(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowOpacity(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowPixelFormat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_GetWindowPixelFormat(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_GetWindowPosition(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetWindowPosition(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_GetWindowSize(a1,a2,a3);
  return 0;
};

static int lSDL_GetWindowSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetWindowSurface(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_GetWindowTitle(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_GetWindowTitle(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticClose(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_HapticClose(a1);
  return 0;
};

static int lSDL_HapticDestroyEffect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_HapticDestroyEffect(a1,a2);
  return 0;
};

static int lSDL_HapticEffectSupported(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticEffectSupported(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticGetEffectStatus(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticGetEffectStatus(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticIndex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticIndex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticName(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_HapticName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticNewEffect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticNewEffect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumAxes(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticNumAxes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumEffects(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticNumEffects(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticNumEffectsPlaying(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticNumEffectsPlaying(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_HapticOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpenFromJoystick(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_HapticOpenFromJoystick(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpenFromMouse(lua_State*L){
  void* ret = (void*)SDL_HapticOpenFromMouse();
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_HapticOpened(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_HapticOpened(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticPause(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticPause(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticQuery(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticQuery(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleInit(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticRumbleInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumblePlay(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double a2 = luaL_checknumber(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_HapticRumblePlay(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleStop(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticRumbleStop(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRumbleSupported(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticRumbleSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticRunEffect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_HapticRunEffect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticSetAutocenter(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticSetAutocenter(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticSetGain(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticSetGain(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticStopAll(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticStopAll(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticStopEffect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HapticStopEffect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticUnpause(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticUnpause(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HapticUpdateEffect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HapticUpdateEffect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Has3DNow(lua_State*L){
  int ret = (int)SDL_Has3DNow();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAVX(lua_State*L){
  int ret = (int)SDL_HasAVX();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAVX2(lua_State*L){
  int ret = (int)SDL_HasAVX2();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasAltiVec(lua_State*L){
  int ret = (int)SDL_HasAltiVec();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasClipboardText(lua_State*L){
  int ret = (int)SDL_HasClipboardText();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasEvent(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_HasEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_HasEvents(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasIntersection(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_HasIntersection(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasMMX(lua_State*L){
  int ret = (int)SDL_HasMMX();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasRDTSC(lua_State*L){
  int ret = (int)SDL_HasRDTSC();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE(lua_State*L){
  int ret = (int)SDL_HasSSE();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE2(lua_State*L){
  int ret = (int)SDL_HasSSE2();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE3(lua_State*L){
  int ret = (int)SDL_HasSSE3();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE41(lua_State*L){
  int ret = (int)SDL_HasSSE41();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasSSE42(lua_State*L){
  int ret = (int)SDL_HasSSE42();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HasScreenKeyboardSupport(lua_State*L){
  int ret = (int)SDL_HasScreenKeyboardSupport();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_HideWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_HideWindow(a1);
  return 0;
};

static int lSDL_Init(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Init(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_InitSubSystem(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_InitSubSystem(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IntersectRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_IntersectRect(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IntersectRectAndLine(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_IntersectRectAndLine(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsGameController(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_IsGameController(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsScreenKeyboardShown(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_IsScreenKeyboardShown(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsScreenSaverEnabled(lua_State*L){
  int ret = (int)SDL_IsScreenSaverEnabled();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_IsTextInputActive(lua_State*L){
  int ret = (int)SDL_IsTextInputActive();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickClose(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_JoystickClose(a1);
  return 0;
};

static int lSDL_JoystickCurrentPowerLevel(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickCurrentPowerLevel(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickEventState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_JoystickEventState(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickFromInstanceID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickFromInstanceID(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickGetAttached(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickGetAttached(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetAxis(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetAxis(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetBall(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickGetBall(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetButton(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetButton(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickGetDeviceGUID(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_JoystickGetDeviceGUID(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUID(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_JoystickGetGUID(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUIDFromString(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_JoystickGetGUIDFromString(a1);
  printf("unsupported type");  assert(0);};

static int lSDL_JoystickGetGUIDString(lua_State*L){
  printf("unsupported type");  assert(0);  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
/* no support argument type*/
  return 0;
};

static int lSDL_JoystickGetHat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_JoystickGetHat(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickInstanceID(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickInstanceID(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickIsHaptic(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickIsHaptic(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickName(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_JoystickName(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickNameForIndex(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickNameForIndex(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickNumAxes(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickNumAxes(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumBalls(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickNumBalls(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumButtons(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickNumButtons(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickNumHats(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_JoystickNumHats(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_JoystickOpen(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_JoystickOpen(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_JoystickUpdate(lua_State*L){
  SDL_JoystickUpdate();
  return 0;
};

static int lSDL_LoadBMP_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_LoadBMP_RW(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadDollarTemplates(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LoadDollarTemplates(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LoadFunction(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_LoadFunction(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadObject(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_LoadObject(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LoadWAV_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_LoadWAV_RW(a1,a2,a3,a4,a5);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_LockAudio(lua_State*L){
  SDL_LockAudio();
  return 0;
};

static int lSDL_LockAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_LockAudioDevice(a1);
  return 0;
};

static int lSDL_LockMutex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LockSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LockSurface(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LockTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LockTexture(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Log(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_Log(a1);
  return 0;
};

static int lSDL_LogCritical(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogCritical(a1,a2);
  return 0;
};

static int lSDL_LogDebug(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogDebug(a1,a2);
  return 0;
};

static int lSDL_LogError(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogError(a1,a2);
  return 0;
};

static int lSDL_LogGetOutputFunction(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogGetOutputFunction(a1,a2);
  return 0;
};

static int lSDL_LogGetPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_LogGetPriority(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LogInfo(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogInfo(a1,a2);
  return 0;
};

static int lSDL_LogMessage(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogMessage(a1,a2,a3);
  return 0;
};

static int lSDL_LogMessageV(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogMessageV(a1,a2,a3,a4);
  return 0;
};

static int lSDL_LogResetPriorities(lua_State*L){
  SDL_LogResetPriorities();
  return 0;
};

static int lSDL_LogSetAllPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_LogSetAllPriority(a1);
  return 0;
};

static int lSDL_LogSetOutputFunction(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogSetOutputFunction(a1,a2);
  return 0;
};

static int lSDL_LogSetPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_LogSetPriority(a1,a2);
  return 0;
};

static int lSDL_LogVerbose(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogVerbose(a1,a2);
  return 0;
};

static int lSDL_LogWarn(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_LogWarn(a1,a2);
  return 0;
};

static int lSDL_LowerBlit(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LowerBlit(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_LowerBlitScaled(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_LowerBlitScaled(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MapRGB(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_MapRGB(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MapRGBA(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_MapRGBA(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MasksToPixelFormatEnum(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_MasksToPixelFormatEnum(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_MaximizeWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_MaximizeWindow(a1);
  return 0;
};

static int lSDL_MinimizeWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_MinimizeWindow(a1);
  return 0;
};

static int lSDL_MixAudio(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  SDL_MixAudio(a1,a2,a3,a4);
  return 0;
};

static int lSDL_MixAudioFormat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  SDL_MixAudioFormat(a1,a2,a3,a4,a5);
  return 0;
};

static int lSDL_MouseIsHaptic(lua_State*L){
  int ret = (int)SDL_MouseIsHaptic();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_NumHaptics(lua_State*L){
  int ret = (int)SDL_NumHaptics();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_NumJoysticks(lua_State*L){
  int ret = (int)SDL_NumJoysticks();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_OpenAudio(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_OpenAudio(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_OpenAudioDevice(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_OpenAudioDevice(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PauseAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_PauseAudio(a1);
  return 0;
};

static int lSDL_PauseAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_PauseAudioDevice(a1,a2);
  return 0;
};

static int lSDL_PeepEvents(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_PeepEvents(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PixelFormatEnumToMasks(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a6 = 0;
  if(lua_isstring(L,6)){
    a6 = (void*)lua_tostring(L,6);
  }
  else if(lua_islightuserdata(L,6)/*||lua_isuserdata(L,6)*/){
    a6 = (void*)lua_touserdata(L,6);
  }
  else if(lua_isuserdata(L,6)){
    a6 = *(void**)lua_touserdata(L,6);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_PixelFormatEnumToMasks(a1,a2,a3,a4,a5,a6);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PointInRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_PointInRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PollEvent(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_PollEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_PumpEvents(lua_State*L){
  SDL_PumpEvents();
  return 0;
};

static int lSDL_PushEvent(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_PushEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_QueryTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_QueryTexture(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_QueueAudio(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_QueueAudio(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Quit(lua_State*L){
  SDL_Quit();
  return 0;
};

static int lSDL_QuitSubSystem(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_QuitSubSystem(a1);
  return 0;
};

static int lSDL_RWFromConstMem(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromConstMem(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromFP(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromFP(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromFile(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_RWFromFile(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RWFromMem(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_RWFromMem(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RaiseWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RaiseWindow(a1);
  return 0;
};

static int lSDL_ReadBE16(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadBE16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadBE32(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadBE32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadBE64(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadBE64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE16(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadLE16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE32(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadLE32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadLE64(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadLE64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReadU8(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ReadU8(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RecordGesture(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RecordGesture(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RectEmpty(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RectEmpty(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RectEquals(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RectEquals(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RegisterApp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RegisterApp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RegisterEvents(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RegisterEvents(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RemoveTimer(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_RemoveTimer(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderClear(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderClear(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderCopy(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderCopy(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderCopyEx(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double a5 = luaL_checknumber(L,5);
  void* a6 = 0;
  if(lua_isstring(L,6)){
    a6 = (void*)lua_tostring(L,6);
  }
  else if(lua_islightuserdata(L,6)/*||lua_isuserdata(L,6)*/){
    a6 = (void*)lua_touserdata(L,6);
  }
  else if(lua_isuserdata(L,6)){
    a6 = *(void**)lua_touserdata(L,6);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a7 = (int)luaL_checkinteger(L,7);
  int ret = (int)SDL_RenderCopyEx(a1,a2,a3,a4,a5,a6,a7);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawLine(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_RenderDrawLine(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawLines(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawLines(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawPoint(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawPoint(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawPoints(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawPoints(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderDrawRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderDrawRects(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderDrawRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderFillRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderFillRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderFillRects(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderFillRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderGetClipRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RenderGetClipRect(a1,a2);
  return 0;
};

static int lSDL_RenderGetD3D9Device(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_RenderGetD3D9Device(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_RenderGetIntegerScale(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderGetIntegerScale(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderGetLogicalSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RenderGetLogicalSize(a1,a2,a3);
  return 0;
};

static int lSDL_RenderGetScale(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RenderGetScale(a1,a2,a3);
  return 0;
};

static int lSDL_RenderGetViewport(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RenderGetViewport(a1,a2);
  return 0;
};

static int lSDL_RenderIsClipEnabled(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderIsClipEnabled(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderPresent(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RenderPresent(a1);
  return 0;
};

static int lSDL_RenderReadPixels(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_RenderReadPixels(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetClipRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderSetClipRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetIntegerScale(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_RenderSetIntegerScale(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetLogicalSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_RenderSetLogicalSize(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetScale(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double a2 = luaL_checknumber(L,2);
  double a3 = luaL_checknumber(L,3);
  int ret = (int)SDL_RenderSetScale(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderSetViewport(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderSetViewport(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_RenderTargetSupported(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_RenderTargetSupported(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ReportAssertion(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_ReportAssertion(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ResetAssertionReport(lua_State*L){
  SDL_ResetAssertionReport();
  return 0;
};

static int lSDL_RestoreWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_RestoreWindow(a1);
  return 0;
};

static int lSDL_SaveAllDollarTemplates(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SaveAllDollarTemplates(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SaveBMP_RW(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SaveBMP_RW(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SaveDollarTemplate(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SaveDollarTemplate(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemPost(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SemPost(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemTryWait(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SemTryWait(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemValue(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SemValue(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemWait(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SemWait(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SemWaitTimeout(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SemWaitTimeout(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetAssertionHandler(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetAssertionHandler(a1,a2);
  return 0;
};

static int lSDL_SetClipRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetClipRect(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetClipboardText(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetClipboardText(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetColorKey(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SetColorKey(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetCursor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetCursor(a1);
  return 0;
};

static int lSDL_SetError(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetError(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetEventFilter(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetEventFilter(a1,a2);
  return 0;
};

static int lSDL_SetHint(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetHint(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetHintWithPriority(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_SetHintWithPriority(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetMainReady(lua_State*L){
  SDL_SetMainReady();
  return 0;
};

static int lSDL_SetModState(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_SetModState(a1);
  return 0;
};

static int lSDL_SetPaletteColors(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetPaletteColors(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetPixelFormatPalette(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetPixelFormatPalette(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRelativeMouseMode(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_SetRelativeMouseMode(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderDrawBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetRenderDrawBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderDrawColor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int a5 = (int)luaL_checkinteger(L,5);
  int ret = (int)SDL_SetRenderDrawColor(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetRenderTarget(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetRenderTarget(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceAlphaMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceColorMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetSurfaceColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfacePalette(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetSurfacePalette(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetSurfaceRLE(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetSurfaceRLE(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextInputRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetTextInputRect(a1);
  return 0;
};

static int lSDL_SetTextureAlphaMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetTextureAlphaMod(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextureBlendMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetTextureBlendMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetTextureColorMod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_SetTextureColorMod(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetThreadPriority(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_SetThreadPriority(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowBordered(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowBordered(a1,a2);
  return 0;
};

static int lSDL_SetWindowBrightness(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double a2 = luaL_checknumber(L,2);
  int ret = (int)SDL_SetWindowBrightness(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowData(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_SetWindowData(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_SetWindowDisplayMode(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetWindowDisplayMode(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowFullscreen(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_SetWindowFullscreen(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowGammaRamp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetWindowGammaRamp(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowGrab(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowGrab(a1,a2);
  return 0;
};

static int lSDL_SetWindowHitTest(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetWindowHitTest(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowIcon(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetWindowIcon(a1,a2);
  return 0;
};

static int lSDL_SetWindowInputFocus(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetWindowInputFocus(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowMaximumSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowMaximumSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowMinimumSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowMinimumSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowModalFor(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SetWindowModalFor(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowOpacity(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double a2 = luaL_checknumber(L,2);
  int ret = (int)SDL_SetWindowOpacity(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SetWindowPosition(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowPosition(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowResizable(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  SDL_SetWindowResizable(a1,a2);
  return 0;
};

static int lSDL_SetWindowSize(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_SetWindowSize(a1,a2,a3);
  return 0;
};

static int lSDL_SetWindowTitle(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetWindowTitle(a1,a2);
  return 0;
};

static int lSDL_SetWindowsMessageHook(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_SetWindowsMessageHook(a1,a2);
  return 0;
};

static int lSDL_ShowCursor(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_ShowCursor(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowMessageBox(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ShowMessageBox(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowSimpleMessageBox(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_ShowSimpleMessageBox(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ShowWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_ShowWindow(a1);
  return 0;
};

static int lSDL_SoftStretch(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_SoftStretch(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_StartTextInput(lua_State*L){
  SDL_StartTextInput();
  return 0;
};

static int lSDL_StopTextInput(lua_State*L){
  SDL_StopTextInput();
  return 0;
};

static int lSDL_Swap16(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap16(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Swap32(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap32(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_Swap64(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_Swap64(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_SwapFloat(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_SwapFloat(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_TLSCreate(lua_State*L){
  int ret = (int)SDL_TLSCreate();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_TLSGet(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_TLSGet(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_TLSSet(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_TLSSet(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_ThreadID(lua_State*L){
  int ret = (int)SDL_ThreadID();
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_TryLockMutex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_TryLockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UnionRect(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_UnionRect(a1,a2,a3);
  return 0;
};

static int lSDL_UnloadObject(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_UnloadObject(a1);
  return 0;
};

static int lSDL_UnlockAudio(lua_State*L){
  SDL_UnlockAudio();
  return 0;
};

static int lSDL_UnlockAudioDevice(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  SDL_UnlockAudioDevice(a1);
  return 0;
};

static int lSDL_UnlockMutex(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_UnlockMutex(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UnlockSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_UnlockSurface(a1);
  return 0;
};

static int lSDL_UnlockTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_UnlockTexture(a1);
  return 0;
};

static int lSDL_UnregisterApp(lua_State*L){
  SDL_UnregisterApp();
  return 0;
};

static int lSDL_UpdateTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a4 = (int)luaL_checkinteger(L,4);
  int ret = (int)SDL_UpdateTexture(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateWindowSurface(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_UpdateWindowSurface(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateWindowSurfaceRects(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_UpdateWindowSurfaceRects(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpdateYUVTexture(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a4 = (int)luaL_checkinteger(L,4);
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a6 = (int)luaL_checkinteger(L,6);
  void* a7 = 0;
  if(lua_isstring(L,7)){
    a7 = (void*)lua_tostring(L,7);
  }
  else if(lua_islightuserdata(L,7)/*||lua_isuserdata(L,7)*/){
    a7 = (void*)lua_touserdata(L,7);
  }
  else if(lua_isuserdata(L,7)){
    a7 = *(void**)lua_touserdata(L,7);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a8 = (int)luaL_checkinteger(L,8);
  int ret = (int)SDL_UpdateYUVTexture(a1,a2,a3,a4,a5,a6,a7,a8);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpperBlit(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_UpperBlit(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_UpperBlitScaled(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_UpperBlitScaled(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_VideoInit(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_VideoInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_VideoQuit(lua_State*L){
  SDL_VideoQuit();
  return 0;
};

static int lSDL_WaitEvent(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_WaitEvent(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WaitEventTimeout(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WaitEventTimeout(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WaitThread(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_WaitThread(a1,a2);
  return 0;
};

static int lSDL_WarpMouseGlobal(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WarpMouseGlobal(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WarpMouseInWindow(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_WarpMouseInWindow(a1,a2,a3);
  return 0;
};

static int lSDL_WasInit(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_WasInit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE16(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE16(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE32(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE32(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteBE64(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteBE64(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE16(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE16(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE32(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE32(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteLE64(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteLE64(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_WriteU8(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int ret = (int)SDL_WriteU8(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_abs(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_abs(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_acos(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_acos(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_asin(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_asin(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atan(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_atan(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atan2(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_atan2(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atof(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double ret = (double)SDL_atof(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_atoi(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_atoi(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_calloc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_calloc(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ceil(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_ceil(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_copysign(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_copysign(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_cos(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_cos(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_cosf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_cosf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_fabs(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_fabs(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_floor(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_floor(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_free(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_free(a1);
  return 0;
};

static int lSDL_getenv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_getenv(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_iconv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a5 = 0;
  if(lua_isstring(L,5)){
    a5 = (void*)lua_tostring(L,5);
  }
  else if(lua_islightuserdata(L,5)/*||lua_isuserdata(L,5)*/){
    a5 = (void*)lua_touserdata(L,5);
  }
  else if(lua_isuserdata(L,5)){
    a5 = *(void**)lua_touserdata(L,5);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_iconv(a1,a2,a3,a4,a5);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_iconv_close(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_iconv_close(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_iconv_open(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_iconv_open(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_iconv_string(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a4 = (int)luaL_checkinteger(L,4);
  void* ret = (void*)SDL_iconv_string(a1,a2,a3,a4);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_isdigit(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_isdigit(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_isspace(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_isspace(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_itoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_itoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_lltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_lltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_log(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_log(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_ltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_malloc(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* ret = (void*)SDL_malloc(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memcmp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_memcmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_memcpy(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memcpy(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memcpy4(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memcpy4(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memmove(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memmove(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memset(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_memset(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_memset4(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  SDL_memset4(a1,a2,a3);
  return 0;
};

static int lSDL_pow(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double a2 = luaL_checknumber(L,2);
  double ret = (double)SDL_pow(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_qsort(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  int a3 = (int)luaL_checkinteger(L,3);
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  SDL_qsort(a1,a2,a3,a4);
  return 0;
};

static int lSDL_realloc(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_realloc(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_scalbn(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  int a2 = (int)luaL_checkinteger(L,2);
  double ret = (double)SDL_scalbn(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_setenv(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_setenv(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_sin(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sin(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sinf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sinf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_snprintf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_snprintf(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_sqrt(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sqrt(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sqrtf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_sqrtf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_sscanf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_sscanf(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strcasecmp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_strcasecmp(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strchr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_strchr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strcmp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_strcmp(a1,a2);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strdup(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_strdup(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strlcat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strlcat(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlcpy(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strlcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlen(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_strlen(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strlwr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_strlwr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strncasecmp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strncasecmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strncmp(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strncmp(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strrchr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* ret = (void*)SDL_strrchr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strrev(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_strrev(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strstr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_strstr(a1,a2);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_strtod(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  double ret = (double)SDL_strtod(a1,a2);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_strtol(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtol(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoll(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoll(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoul(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoul(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strtoull(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_strtoull(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_strupr(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* ret = (void*)SDL_strupr(a1);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_tan(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_tan(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_tanf(lua_State*L){
  double a1 = luaL_checknumber(L,1);
  double ret = (double)SDL_tanf(a1);
  lua_pushnumber(L,ret);
  return 1;
};

static int lSDL_tolower(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_tolower(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_toupper(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  int ret = (int)SDL_toupper(a1);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_uitoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_uitoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ulltoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ulltoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_ultoa(lua_State*L){
  int a1 = (int)luaL_checkinteger(L,1);
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  void* ret = (void*)SDL_ultoa(a1,a2,a3);
  lua_pushlightuserdata(L,ret);
  return 1;
};

static int lSDL_utf8strlcpy(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_utf8strlcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_vsnprintf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a2 = (int)luaL_checkinteger(L,2);
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a4 = 0;
  if(lua_isstring(L,4)){
    a4 = (void*)lua_tostring(L,4);
  }
  else if(lua_islightuserdata(L,4)/*||lua_isuserdata(L,4)*/){
    a4 = (void*)lua_touserdata(L,4);
  }
  else if(lua_isuserdata(L,4)){
    a4 = *(void**)lua_touserdata(L,4);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_vsnprintf(a1,a2,a3,a4);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_vsscanf(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a3 = 0;
  if(lua_isstring(L,3)){
    a3 = (void*)lua_tostring(L,3);
  }
  else if(lua_islightuserdata(L,3)/*||lua_isuserdata(L,3)*/){
    a3 = (void*)lua_touserdata(L,3);
  }
  else if(lua_isuserdata(L,3)){
    a3 = *(void**)lua_touserdata(L,3);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_vsscanf(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslcat(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_wcslcat(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslcpy(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  void* a2 = 0;
  if(lua_isstring(L,2)){
    a2 = (void*)lua_tostring(L,2);
  }
  else if(lua_islightuserdata(L,2)/*||lua_isuserdata(L,2)*/){
    a2 = (void*)lua_touserdata(L,2);
  }
  else if(lua_isuserdata(L,2)){
    a2 = *(void**)lua_touserdata(L,2);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int a3 = (int)luaL_checkinteger(L,3);
  int ret = (int)SDL_wcslcpy(a1,a2,a3);
  lua_pushinteger(L,ret);
  return 1;
};

static int lSDL_wcslen(lua_State*L){
  void* a1 = 0;
  if(lua_isstring(L,1)){
    a1 = (void*)lua_tostring(L,1);
  }
  else if(lua_islightuserdata(L,1)/*||lua_isuserdata(L,1)*/){
    a1 = (void*)lua_touserdata(L,1);
  }
  else if(lua_isuserdata(L,1)){
    a1 = *(void**)lua_touserdata(L,1);
  }
  else{
    lua_pushliteral(L,"type error:pointer_type");
    lua_error(L);
  }
  int ret = (int)SDL_wcslen(a1);
  lua_pushinteger(L,ret);
  return 1;
};

int lSDL_openlibs(lua_State*L){
  lua_register(L,"SDL_AddEventWatch",lSDL_AddEventWatch);
  lua_register(L,"SDL_AddHintCallback",lSDL_AddHintCallback);
  lua_register(L,"SDL_AddTimer",lSDL_AddTimer);
  lua_register(L,"SDL_AllocFormat",lSDL_AllocFormat);
  lua_register(L,"SDL_AllocPalette",lSDL_AllocPalette);
  lua_register(L,"SDL_AllocRW",lSDL_AllocRW);
  lua_register(L,"SDL_AtomicAdd",lSDL_AtomicAdd);
  lua_register(L,"SDL_AtomicCAS",lSDL_AtomicCAS);
  lua_register(L,"SDL_AtomicCASPtr",lSDL_AtomicCASPtr);
  lua_register(L,"SDL_AtomicGet",lSDL_AtomicGet);
  lua_register(L,"SDL_AtomicGetPtr",lSDL_AtomicGetPtr);
  lua_register(L,"SDL_AtomicLock",lSDL_AtomicLock);
  lua_register(L,"SDL_AtomicSet",lSDL_AtomicSet);
  lua_register(L,"SDL_AtomicSetPtr",lSDL_AtomicSetPtr);
  lua_register(L,"SDL_AtomicTryLock",lSDL_AtomicTryLock);
  lua_register(L,"SDL_AtomicUnlock",lSDL_AtomicUnlock);
  lua_register(L,"SDL_AudioInit",lSDL_AudioInit);
  lua_register(L,"SDL_AudioQuit",lSDL_AudioQuit);
  lua_register(L,"SDL_BuildAudioCVT",lSDL_BuildAudioCVT);
  lua_register(L,"SDL_CalculateGammaRamp",lSDL_CalculateGammaRamp);
  lua_register(L,"SDL_CaptureMouse",lSDL_CaptureMouse);
  lua_register(L,"SDL_ClearError",lSDL_ClearError);
  lua_register(L,"SDL_ClearHints",lSDL_ClearHints);
  lua_register(L,"SDL_ClearQueuedAudio",lSDL_ClearQueuedAudio);
  lua_register(L,"SDL_CloseAudio",lSDL_CloseAudio);
  lua_register(L,"SDL_CloseAudioDevice",lSDL_CloseAudioDevice);
  lua_register(L,"SDL_CondBroadcast",lSDL_CondBroadcast);
  lua_register(L,"SDL_CondSignal",lSDL_CondSignal);
  lua_register(L,"SDL_CondWait",lSDL_CondWait);
  lua_register(L,"SDL_CondWaitTimeout",lSDL_CondWaitTimeout);
  lua_register(L,"SDL_ConvertAudio",lSDL_ConvertAudio);
  lua_register(L,"SDL_ConvertPixels",lSDL_ConvertPixels);
  lua_register(L,"SDL_ConvertSurface",lSDL_ConvertSurface);
  lua_register(L,"SDL_ConvertSurfaceFormat",lSDL_ConvertSurfaceFormat);
  lua_register(L,"SDL_CreateColorCursor",lSDL_CreateColorCursor);
  lua_register(L,"SDL_CreateCond",lSDL_CreateCond);
  lua_register(L,"SDL_CreateCursor",lSDL_CreateCursor);
  lua_register(L,"SDL_CreateMutex",lSDL_CreateMutex);
  lua_register(L,"SDL_CreateRGBSurface",lSDL_CreateRGBSurface);
  lua_register(L,"SDL_CreateRGBSurfaceFrom",lSDL_CreateRGBSurfaceFrom);
  lua_register(L,"SDL_CreateRGBSurfaceWithFormat",lSDL_CreateRGBSurfaceWithFormat);
  lua_register(L,"SDL_CreateRGBSurfaceWithFormatFrom",lSDL_CreateRGBSurfaceWithFormatFrom);
  lua_register(L,"SDL_CreateRenderer",lSDL_CreateRenderer);
  lua_register(L,"SDL_CreateSemaphore",lSDL_CreateSemaphore);
  lua_register(L,"SDL_CreateSoftwareRenderer",lSDL_CreateSoftwareRenderer);
  lua_register(L,"SDL_CreateSystemCursor",lSDL_CreateSystemCursor);
  lua_register(L,"SDL_CreateTexture",lSDL_CreateTexture);
  lua_register(L,"SDL_CreateTextureFromSurface",lSDL_CreateTextureFromSurface);
  lua_register(L,"SDL_CreateThread",lSDL_CreateThread);
  lua_register(L,"SDL_CreateWindow",lSDL_CreateWindow);
  lua_register(L,"SDL_CreateWindowAndRenderer",lSDL_CreateWindowAndRenderer);
  lua_register(L,"SDL_CreateWindowFrom",lSDL_CreateWindowFrom);
  lua_register(L,"SDL_DXGIGetOutputInfo",lSDL_DXGIGetOutputInfo);
  lua_register(L,"SDL_DelEventWatch",lSDL_DelEventWatch);
  lua_register(L,"SDL_DelHintCallback",lSDL_DelHintCallback);
  lua_register(L,"SDL_Delay",lSDL_Delay);
  lua_register(L,"SDL_DequeueAudio",lSDL_DequeueAudio);
  lua_register(L,"SDL_DestroyCond",lSDL_DestroyCond);
  lua_register(L,"SDL_DestroyMutex",lSDL_DestroyMutex);
  lua_register(L,"SDL_DestroyRenderer",lSDL_DestroyRenderer);
  lua_register(L,"SDL_DestroySemaphore",lSDL_DestroySemaphore);
  lua_register(L,"SDL_DestroyTexture",lSDL_DestroyTexture);
  lua_register(L,"SDL_DestroyWindow",lSDL_DestroyWindow);
  lua_register(L,"SDL_DetachThread",lSDL_DetachThread);
  lua_register(L,"SDL_Direct3D9GetAdapterIndex",lSDL_Direct3D9GetAdapterIndex);
  lua_register(L,"SDL_DisableScreenSaver",lSDL_DisableScreenSaver);
  lua_register(L,"SDL_EnableScreenSaver",lSDL_EnableScreenSaver);
  lua_register(L,"SDL_EnclosePoints",lSDL_EnclosePoints);
  lua_register(L,"SDL_Error",lSDL_Error);
  lua_register(L,"SDL_EventState",lSDL_EventState);
  lua_register(L,"SDL_FillRect",lSDL_FillRect);
  lua_register(L,"SDL_FillRects",lSDL_FillRects);
  lua_register(L,"SDL_FilterEvents",lSDL_FilterEvents);
  lua_register(L,"SDL_FlushEvent",lSDL_FlushEvent);
  lua_register(L,"SDL_FlushEvents",lSDL_FlushEvents);
  lua_register(L,"SDL_FreeCursor",lSDL_FreeCursor);
  lua_register(L,"SDL_FreeFormat",lSDL_FreeFormat);
  lua_register(L,"SDL_FreePalette",lSDL_FreePalette);
  lua_register(L,"SDL_FreeRW",lSDL_FreeRW);
  lua_register(L,"SDL_FreeSurface",lSDL_FreeSurface);
  lua_register(L,"SDL_FreeWAV",lSDL_FreeWAV);
  lua_register(L,"SDL_GL_BindTexture",lSDL_GL_BindTexture);
  lua_register(L,"SDL_GL_CreateContext",lSDL_GL_CreateContext);
  lua_register(L,"SDL_GL_DeleteContext",lSDL_GL_DeleteContext);
  lua_register(L,"SDL_GL_ExtensionSupported",lSDL_GL_ExtensionSupported);
  lua_register(L,"SDL_GL_GetAttribute",lSDL_GL_GetAttribute);
  lua_register(L,"SDL_GL_GetCurrentContext",lSDL_GL_GetCurrentContext);
  lua_register(L,"SDL_GL_GetCurrentWindow",lSDL_GL_GetCurrentWindow);
  lua_register(L,"SDL_GL_GetDrawableSize",lSDL_GL_GetDrawableSize);
  lua_register(L,"SDL_GL_GetProcAddress",lSDL_GL_GetProcAddress);
  lua_register(L,"SDL_GL_GetSwapInterval",lSDL_GL_GetSwapInterval);
  lua_register(L,"SDL_GL_LoadLibrary",lSDL_GL_LoadLibrary);
  lua_register(L,"SDL_GL_MakeCurrent",lSDL_GL_MakeCurrent);
  lua_register(L,"SDL_GL_ResetAttributes",lSDL_GL_ResetAttributes);
  lua_register(L,"SDL_GL_SetAttribute",lSDL_GL_SetAttribute);
  lua_register(L,"SDL_GL_SetSwapInterval",lSDL_GL_SetSwapInterval);
  lua_register(L,"SDL_GL_SwapWindow",lSDL_GL_SwapWindow);
  lua_register(L,"SDL_GL_UnbindTexture",lSDL_GL_UnbindTexture);
  lua_register(L,"SDL_GL_UnloadLibrary",lSDL_GL_UnloadLibrary);
  lua_register(L,"SDL_GameControllerAddMapping",lSDL_GameControllerAddMapping);
  lua_register(L,"SDL_GameControllerAddMappingsFromRW",lSDL_GameControllerAddMappingsFromRW);
  lua_register(L,"SDL_GameControllerClose",lSDL_GameControllerClose);
  lua_register(L,"SDL_GameControllerEventState",lSDL_GameControllerEventState);
  lua_register(L,"SDL_GameControllerFromInstanceID",lSDL_GameControllerFromInstanceID);
  lua_register(L,"SDL_GameControllerGetAttached",lSDL_GameControllerGetAttached);
  lua_register(L,"SDL_GameControllerGetAxis",lSDL_GameControllerGetAxis);
  lua_register(L,"SDL_GameControllerGetAxisFromString",lSDL_GameControllerGetAxisFromString);
  lua_register(L,"SDL_GameControllerGetBindForAxis",lSDL_GameControllerGetBindForAxis);
  lua_register(L,"SDL_GameControllerGetBindForButton",lSDL_GameControllerGetBindForButton);
  lua_register(L,"SDL_GameControllerGetButton",lSDL_GameControllerGetButton);
  lua_register(L,"SDL_GameControllerGetButtonFromString",lSDL_GameControllerGetButtonFromString);
  lua_register(L,"SDL_GameControllerGetJoystick",lSDL_GameControllerGetJoystick);
  lua_register(L,"SDL_GameControllerGetStringForAxis",lSDL_GameControllerGetStringForAxis);
  lua_register(L,"SDL_GameControllerGetStringForButton",lSDL_GameControllerGetStringForButton);
  lua_register(L,"SDL_GameControllerMapping",lSDL_GameControllerMapping);
  lua_register(L,"SDL_GameControllerMappingForGUID",lSDL_GameControllerMappingForGUID);
  lua_register(L,"SDL_GameControllerName",lSDL_GameControllerName);
  lua_register(L,"SDL_GameControllerNameForIndex",lSDL_GameControllerNameForIndex);
  lua_register(L,"SDL_GameControllerOpen",lSDL_GameControllerOpen);
  lua_register(L,"SDL_GameControllerUpdate",lSDL_GameControllerUpdate);
  lua_register(L,"SDL_GetAssertionHandler",lSDL_GetAssertionHandler);
  lua_register(L,"SDL_GetAssertionReport",lSDL_GetAssertionReport);
  lua_register(L,"SDL_GetAudioDeviceName",lSDL_GetAudioDeviceName);
  lua_register(L,"SDL_GetAudioDeviceStatus",lSDL_GetAudioDeviceStatus);
  lua_register(L,"SDL_GetAudioDriver",lSDL_GetAudioDriver);
  lua_register(L,"SDL_GetAudioStatus",lSDL_GetAudioStatus);
  lua_register(L,"SDL_GetBasePath",lSDL_GetBasePath);
  lua_register(L,"SDL_GetCPUCacheLineSize",lSDL_GetCPUCacheLineSize);
  lua_register(L,"SDL_GetCPUCount",lSDL_GetCPUCount);
  lua_register(L,"SDL_GetClipRect",lSDL_GetClipRect);
  lua_register(L,"SDL_GetClipboardText",lSDL_GetClipboardText);
  lua_register(L,"SDL_GetClosestDisplayMode",lSDL_GetClosestDisplayMode);
  lua_register(L,"SDL_GetColorKey",lSDL_GetColorKey);
  lua_register(L,"SDL_GetCurrentAudioDriver",lSDL_GetCurrentAudioDriver);
  lua_register(L,"SDL_GetCurrentDisplayMode",lSDL_GetCurrentDisplayMode);
  lua_register(L,"SDL_GetCurrentVideoDriver",lSDL_GetCurrentVideoDriver);
  lua_register(L,"SDL_GetCursor",lSDL_GetCursor);
  lua_register(L,"SDL_GetDefaultAssertionHandler",lSDL_GetDefaultAssertionHandler);
  lua_register(L,"SDL_GetDefaultCursor",lSDL_GetDefaultCursor);
  lua_register(L,"SDL_GetDesktopDisplayMode",lSDL_GetDesktopDisplayMode);
  lua_register(L,"SDL_GetDisplayBounds",lSDL_GetDisplayBounds);
  lua_register(L,"SDL_GetDisplayDPI",lSDL_GetDisplayDPI);
  lua_register(L,"SDL_GetDisplayMode",lSDL_GetDisplayMode);
  lua_register(L,"SDL_GetDisplayName",lSDL_GetDisplayName);
  lua_register(L,"SDL_GetDisplayUsableBounds",lSDL_GetDisplayUsableBounds);
  lua_register(L,"SDL_GetError",lSDL_GetError);
  lua_register(L,"SDL_GetEventFilter",lSDL_GetEventFilter);
  lua_register(L,"SDL_GetGlobalMouseState",lSDL_GetGlobalMouseState);
  lua_register(L,"SDL_GetGrabbedWindow",lSDL_GetGrabbedWindow);
  lua_register(L,"SDL_GetHint",lSDL_GetHint);
  lua_register(L,"SDL_GetHintBoolean",lSDL_GetHintBoolean);
  lua_register(L,"SDL_GetKeyFromName",lSDL_GetKeyFromName);
  lua_register(L,"SDL_GetKeyFromScancode",lSDL_GetKeyFromScancode);
  lua_register(L,"SDL_GetKeyName",lSDL_GetKeyName);
  lua_register(L,"SDL_GetKeyboardFocus",lSDL_GetKeyboardFocus);
  lua_register(L,"SDL_GetKeyboardState",lSDL_GetKeyboardState);
  lua_register(L,"SDL_GetModState",lSDL_GetModState);
  lua_register(L,"SDL_GetMouseFocus",lSDL_GetMouseFocus);
  lua_register(L,"SDL_GetMouseState",lSDL_GetMouseState);
  lua_register(L,"SDL_GetNumAudioDevices",lSDL_GetNumAudioDevices);
  lua_register(L,"SDL_GetNumAudioDrivers",lSDL_GetNumAudioDrivers);
  lua_register(L,"SDL_GetNumDisplayModes",lSDL_GetNumDisplayModes);
  lua_register(L,"SDL_GetNumRenderDrivers",lSDL_GetNumRenderDrivers);
  lua_register(L,"SDL_GetNumTouchDevices",lSDL_GetNumTouchDevices);
  lua_register(L,"SDL_GetNumTouchFingers",lSDL_GetNumTouchFingers);
  lua_register(L,"SDL_GetNumVideoDisplays",lSDL_GetNumVideoDisplays);
  lua_register(L,"SDL_GetNumVideoDrivers",lSDL_GetNumVideoDrivers);
  lua_register(L,"SDL_GetPerformanceCounter",lSDL_GetPerformanceCounter);
  lua_register(L,"SDL_GetPerformanceFrequency",lSDL_GetPerformanceFrequency);
  lua_register(L,"SDL_GetPixelFormatName",lSDL_GetPixelFormatName);
  lua_register(L,"SDL_GetPlatform",lSDL_GetPlatform);
  lua_register(L,"SDL_GetPowerInfo",lSDL_GetPowerInfo);
  lua_register(L,"SDL_GetPrefPath",lSDL_GetPrefPath);
  lua_register(L,"SDL_GetQueuedAudioSize",lSDL_GetQueuedAudioSize);
  lua_register(L,"SDL_GetRGB",lSDL_GetRGB);
  lua_register(L,"SDL_GetRGBA",lSDL_GetRGBA);
  lua_register(L,"SDL_GetRelativeMouseMode",lSDL_GetRelativeMouseMode);
  lua_register(L,"SDL_GetRelativeMouseState",lSDL_GetRelativeMouseState);
  lua_register(L,"SDL_GetRenderDrawBlendMode",lSDL_GetRenderDrawBlendMode);
  lua_register(L,"SDL_GetRenderDrawColor",lSDL_GetRenderDrawColor);
  lua_register(L,"SDL_GetRenderDriverInfo",lSDL_GetRenderDriverInfo);
  lua_register(L,"SDL_GetRenderTarget",lSDL_GetRenderTarget);
  lua_register(L,"SDL_GetRenderer",lSDL_GetRenderer);
  lua_register(L,"SDL_GetRendererInfo",lSDL_GetRendererInfo);
  lua_register(L,"SDL_GetRendererOutputSize",lSDL_GetRendererOutputSize);
  lua_register(L,"SDL_GetRevision",lSDL_GetRevision);
  lua_register(L,"SDL_GetRevisionNumber",lSDL_GetRevisionNumber);
  lua_register(L,"SDL_GetScancodeFromKey",lSDL_GetScancodeFromKey);
  lua_register(L,"SDL_GetScancodeFromName",lSDL_GetScancodeFromName);
  lua_register(L,"SDL_GetScancodeName",lSDL_GetScancodeName);
  lua_register(L,"SDL_GetSurfaceAlphaMod",lSDL_GetSurfaceAlphaMod);
  lua_register(L,"SDL_GetSurfaceBlendMode",lSDL_GetSurfaceBlendMode);
  lua_register(L,"SDL_GetSurfaceColorMod",lSDL_GetSurfaceColorMod);
  lua_register(L,"SDL_GetSystemRAM",lSDL_GetSystemRAM);
  lua_register(L,"SDL_GetTextureAlphaMod",lSDL_GetTextureAlphaMod);
  lua_register(L,"SDL_GetTextureBlendMode",lSDL_GetTextureBlendMode);
  lua_register(L,"SDL_GetTextureColorMod",lSDL_GetTextureColorMod);
  lua_register(L,"SDL_GetThreadID",lSDL_GetThreadID);
  lua_register(L,"SDL_GetThreadName",lSDL_GetThreadName);
  lua_register(L,"SDL_GetTicks",lSDL_GetTicks);
  lua_register(L,"SDL_GetTouchDevice",lSDL_GetTouchDevice);
  lua_register(L,"SDL_GetTouchFinger",lSDL_GetTouchFinger);
  lua_register(L,"SDL_GetVersion",lSDL_GetVersion);
  lua_register(L,"SDL_GetVideoDriver",lSDL_GetVideoDriver);
  lua_register(L,"SDL_GetWindowBordersSize",lSDL_GetWindowBordersSize);
  lua_register(L,"SDL_GetWindowBrightness",lSDL_GetWindowBrightness);
  lua_register(L,"SDL_GetWindowData",lSDL_GetWindowData);
  lua_register(L,"SDL_GetWindowDisplayIndex",lSDL_GetWindowDisplayIndex);
  lua_register(L,"SDL_GetWindowDisplayMode",lSDL_GetWindowDisplayMode);
  lua_register(L,"SDL_GetWindowFlags",lSDL_GetWindowFlags);
  lua_register(L,"SDL_GetWindowFromID",lSDL_GetWindowFromID);
  lua_register(L,"SDL_GetWindowGammaRamp",lSDL_GetWindowGammaRamp);
  lua_register(L,"SDL_GetWindowGrab",lSDL_GetWindowGrab);
  lua_register(L,"SDL_GetWindowID",lSDL_GetWindowID);
  lua_register(L,"SDL_GetWindowMaximumSize",lSDL_GetWindowMaximumSize);
  lua_register(L,"SDL_GetWindowMinimumSize",lSDL_GetWindowMinimumSize);
  lua_register(L,"SDL_GetWindowOpacity",lSDL_GetWindowOpacity);
  lua_register(L,"SDL_GetWindowPixelFormat",lSDL_GetWindowPixelFormat);
  lua_register(L,"SDL_GetWindowPosition",lSDL_GetWindowPosition);
  lua_register(L,"SDL_GetWindowSize",lSDL_GetWindowSize);
  lua_register(L,"SDL_GetWindowSurface",lSDL_GetWindowSurface);
  lua_register(L,"SDL_GetWindowTitle",lSDL_GetWindowTitle);
  lua_register(L,"SDL_HapticClose",lSDL_HapticClose);
  lua_register(L,"SDL_HapticDestroyEffect",lSDL_HapticDestroyEffect);
  lua_register(L,"SDL_HapticEffectSupported",lSDL_HapticEffectSupported);
  lua_register(L,"SDL_HapticGetEffectStatus",lSDL_HapticGetEffectStatus);
  lua_register(L,"SDL_HapticIndex",lSDL_HapticIndex);
  lua_register(L,"SDL_HapticName",lSDL_HapticName);
  lua_register(L,"SDL_HapticNewEffect",lSDL_HapticNewEffect);
  lua_register(L,"SDL_HapticNumAxes",lSDL_HapticNumAxes);
  lua_register(L,"SDL_HapticNumEffects",lSDL_HapticNumEffects);
  lua_register(L,"SDL_HapticNumEffectsPlaying",lSDL_HapticNumEffectsPlaying);
  lua_register(L,"SDL_HapticOpen",lSDL_HapticOpen);
  lua_register(L,"SDL_HapticOpenFromJoystick",lSDL_HapticOpenFromJoystick);
  lua_register(L,"SDL_HapticOpenFromMouse",lSDL_HapticOpenFromMouse);
  lua_register(L,"SDL_HapticOpened",lSDL_HapticOpened);
  lua_register(L,"SDL_HapticPause",lSDL_HapticPause);
  lua_register(L,"SDL_HapticQuery",lSDL_HapticQuery);
  lua_register(L,"SDL_HapticRumbleInit",lSDL_HapticRumbleInit);
  lua_register(L,"SDL_HapticRumblePlay",lSDL_HapticRumblePlay);
  lua_register(L,"SDL_HapticRumbleStop",lSDL_HapticRumbleStop);
  lua_register(L,"SDL_HapticRumbleSupported",lSDL_HapticRumbleSupported);
  lua_register(L,"SDL_HapticRunEffect",lSDL_HapticRunEffect);
  lua_register(L,"SDL_HapticSetAutocenter",lSDL_HapticSetAutocenter);
  lua_register(L,"SDL_HapticSetGain",lSDL_HapticSetGain);
  lua_register(L,"SDL_HapticStopAll",lSDL_HapticStopAll);
  lua_register(L,"SDL_HapticStopEffect",lSDL_HapticStopEffect);
  lua_register(L,"SDL_HapticUnpause",lSDL_HapticUnpause);
  lua_register(L,"SDL_HapticUpdateEffect",lSDL_HapticUpdateEffect);
  lua_register(L,"SDL_Has3DNow",lSDL_Has3DNow);
  lua_register(L,"SDL_HasAVX",lSDL_HasAVX);
  lua_register(L,"SDL_HasAVX2",lSDL_HasAVX2);
  lua_register(L,"SDL_HasAltiVec",lSDL_HasAltiVec);
  lua_register(L,"SDL_HasClipboardText",lSDL_HasClipboardText);
  lua_register(L,"SDL_HasEvent",lSDL_HasEvent);
  lua_register(L,"SDL_HasEvents",lSDL_HasEvents);
  lua_register(L,"SDL_HasIntersection",lSDL_HasIntersection);
  lua_register(L,"SDL_HasMMX",lSDL_HasMMX);
  lua_register(L,"SDL_HasRDTSC",lSDL_HasRDTSC);
  lua_register(L,"SDL_HasSSE",lSDL_HasSSE);
  lua_register(L,"SDL_HasSSE2",lSDL_HasSSE2);
  lua_register(L,"SDL_HasSSE3",lSDL_HasSSE3);
  lua_register(L,"SDL_HasSSE41",lSDL_HasSSE41);
  lua_register(L,"SDL_HasSSE42",lSDL_HasSSE42);
  lua_register(L,"SDL_HasScreenKeyboardSupport",lSDL_HasScreenKeyboardSupport);
  lua_register(L,"SDL_HideWindow",lSDL_HideWindow);
  lua_register(L,"SDL_Init",lSDL_Init);
  lua_register(L,"SDL_InitSubSystem",lSDL_InitSubSystem);
  lua_register(L,"SDL_IntersectRect",lSDL_IntersectRect);
  lua_register(L,"SDL_IntersectRectAndLine",lSDL_IntersectRectAndLine);
  lua_register(L,"SDL_IsGameController",lSDL_IsGameController);
  lua_register(L,"SDL_IsScreenKeyboardShown",lSDL_IsScreenKeyboardShown);
  lua_register(L,"SDL_IsScreenSaverEnabled",lSDL_IsScreenSaverEnabled);
  lua_register(L,"SDL_IsTextInputActive",lSDL_IsTextInputActive);
  lua_register(L,"SDL_JoystickClose",lSDL_JoystickClose);
  lua_register(L,"SDL_JoystickCurrentPowerLevel",lSDL_JoystickCurrentPowerLevel);
  lua_register(L,"SDL_JoystickEventState",lSDL_JoystickEventState);
  lua_register(L,"SDL_JoystickFromInstanceID",lSDL_JoystickFromInstanceID);
  lua_register(L,"SDL_JoystickGetAttached",lSDL_JoystickGetAttached);
  lua_register(L,"SDL_JoystickGetAxis",lSDL_JoystickGetAxis);
  lua_register(L,"SDL_JoystickGetBall",lSDL_JoystickGetBall);
  lua_register(L,"SDL_JoystickGetButton",lSDL_JoystickGetButton);
  lua_register(L,"SDL_JoystickGetDeviceGUID",lSDL_JoystickGetDeviceGUID);
  lua_register(L,"SDL_JoystickGetGUID",lSDL_JoystickGetGUID);
  lua_register(L,"SDL_JoystickGetGUIDFromString",lSDL_JoystickGetGUIDFromString);
  lua_register(L,"SDL_JoystickGetGUIDString",lSDL_JoystickGetGUIDString);
  lua_register(L,"SDL_JoystickGetHat",lSDL_JoystickGetHat);
  lua_register(L,"SDL_JoystickInstanceID",lSDL_JoystickInstanceID);
  lua_register(L,"SDL_JoystickIsHaptic",lSDL_JoystickIsHaptic);
  lua_register(L,"SDL_JoystickName",lSDL_JoystickName);
  lua_register(L,"SDL_JoystickNameForIndex",lSDL_JoystickNameForIndex);
  lua_register(L,"SDL_JoystickNumAxes",lSDL_JoystickNumAxes);
  lua_register(L,"SDL_JoystickNumBalls",lSDL_JoystickNumBalls);
  lua_register(L,"SDL_JoystickNumButtons",lSDL_JoystickNumButtons);
  lua_register(L,"SDL_JoystickNumHats",lSDL_JoystickNumHats);
  lua_register(L,"SDL_JoystickOpen",lSDL_JoystickOpen);
  lua_register(L,"SDL_JoystickUpdate",lSDL_JoystickUpdate);
  lua_register(L,"SDL_LoadBMP_RW",lSDL_LoadBMP_RW);
  lua_register(L,"SDL_LoadDollarTemplates",lSDL_LoadDollarTemplates);
  lua_register(L,"SDL_LoadFunction",lSDL_LoadFunction);
  lua_register(L,"SDL_LoadObject",lSDL_LoadObject);
  lua_register(L,"SDL_LoadWAV_RW",lSDL_LoadWAV_RW);
  lua_register(L,"SDL_LockAudio",lSDL_LockAudio);
  lua_register(L,"SDL_LockAudioDevice",lSDL_LockAudioDevice);
  lua_register(L,"SDL_LockMutex",lSDL_LockMutex);
  lua_register(L,"SDL_LockSurface",lSDL_LockSurface);
  lua_register(L,"SDL_LockTexture",lSDL_LockTexture);
  lua_register(L,"SDL_Log",lSDL_Log);
  lua_register(L,"SDL_LogCritical",lSDL_LogCritical);
  lua_register(L,"SDL_LogDebug",lSDL_LogDebug);
  lua_register(L,"SDL_LogError",lSDL_LogError);
  lua_register(L,"SDL_LogGetOutputFunction",lSDL_LogGetOutputFunction);
  lua_register(L,"SDL_LogGetPriority",lSDL_LogGetPriority);
  lua_register(L,"SDL_LogInfo",lSDL_LogInfo);
  lua_register(L,"SDL_LogMessage",lSDL_LogMessage);
  lua_register(L,"SDL_LogMessageV",lSDL_LogMessageV);
  lua_register(L,"SDL_LogResetPriorities",lSDL_LogResetPriorities);
  lua_register(L,"SDL_LogSetAllPriority",lSDL_LogSetAllPriority);
  lua_register(L,"SDL_LogSetOutputFunction",lSDL_LogSetOutputFunction);
  lua_register(L,"SDL_LogSetPriority",lSDL_LogSetPriority);
  lua_register(L,"SDL_LogVerbose",lSDL_LogVerbose);
  lua_register(L,"SDL_LogWarn",lSDL_LogWarn);
  lua_register(L,"SDL_LowerBlit",lSDL_LowerBlit);
  lua_register(L,"SDL_LowerBlitScaled",lSDL_LowerBlitScaled);
  lua_register(L,"SDL_MapRGB",lSDL_MapRGB);
  lua_register(L,"SDL_MapRGBA",lSDL_MapRGBA);
  lua_register(L,"SDL_MasksToPixelFormatEnum",lSDL_MasksToPixelFormatEnum);
  lua_register(L,"SDL_MaximizeWindow",lSDL_MaximizeWindow);
  lua_register(L,"SDL_MinimizeWindow",lSDL_MinimizeWindow);
  lua_register(L,"SDL_MixAudio",lSDL_MixAudio);
  lua_register(L,"SDL_MixAudioFormat",lSDL_MixAudioFormat);
  lua_register(L,"SDL_MouseIsHaptic",lSDL_MouseIsHaptic);
  lua_register(L,"SDL_NumHaptics",lSDL_NumHaptics);
  lua_register(L,"SDL_NumJoysticks",lSDL_NumJoysticks);
  lua_register(L,"SDL_OpenAudio",lSDL_OpenAudio);
  lua_register(L,"SDL_OpenAudioDevice",lSDL_OpenAudioDevice);
  lua_register(L,"SDL_PauseAudio",lSDL_PauseAudio);
  lua_register(L,"SDL_PauseAudioDevice",lSDL_PauseAudioDevice);
  lua_register(L,"SDL_PeepEvents",lSDL_PeepEvents);
  lua_register(L,"SDL_PixelFormatEnumToMasks",lSDL_PixelFormatEnumToMasks);
  lua_register(L,"SDL_PointInRect",lSDL_PointInRect);
  lua_register(L,"SDL_PollEvent",lSDL_PollEvent);
  lua_register(L,"SDL_PumpEvents",lSDL_PumpEvents);
  lua_register(L,"SDL_PushEvent",lSDL_PushEvent);
  lua_register(L,"SDL_QueryTexture",lSDL_QueryTexture);
  lua_register(L,"SDL_QueueAudio",lSDL_QueueAudio);
  lua_register(L,"SDL_Quit",lSDL_Quit);
  lua_register(L,"SDL_QuitSubSystem",lSDL_QuitSubSystem);
  lua_register(L,"SDL_RWFromConstMem",lSDL_RWFromConstMem);
  lua_register(L,"SDL_RWFromFP",lSDL_RWFromFP);
  lua_register(L,"SDL_RWFromFile",lSDL_RWFromFile);
  lua_register(L,"SDL_RWFromMem",lSDL_RWFromMem);
  lua_register(L,"SDL_RaiseWindow",lSDL_RaiseWindow);
  lua_register(L,"SDL_ReadBE16",lSDL_ReadBE16);
  lua_register(L,"SDL_ReadBE32",lSDL_ReadBE32);
  lua_register(L,"SDL_ReadBE64",lSDL_ReadBE64);
  lua_register(L,"SDL_ReadLE16",lSDL_ReadLE16);
  lua_register(L,"SDL_ReadLE32",lSDL_ReadLE32);
  lua_register(L,"SDL_ReadLE64",lSDL_ReadLE64);
  lua_register(L,"SDL_ReadU8",lSDL_ReadU8);
  lua_register(L,"SDL_RecordGesture",lSDL_RecordGesture);
  lua_register(L,"SDL_RectEmpty",lSDL_RectEmpty);
  lua_register(L,"SDL_RectEquals",lSDL_RectEquals);
  lua_register(L,"SDL_RegisterApp",lSDL_RegisterApp);
  lua_register(L,"SDL_RegisterEvents",lSDL_RegisterEvents);
  lua_register(L,"SDL_RemoveTimer",lSDL_RemoveTimer);
  lua_register(L,"SDL_RenderClear",lSDL_RenderClear);
  lua_register(L,"SDL_RenderCopy",lSDL_RenderCopy);
  lua_register(L,"SDL_RenderCopyEx",lSDL_RenderCopyEx);
  lua_register(L,"SDL_RenderDrawLine",lSDL_RenderDrawLine);
  lua_register(L,"SDL_RenderDrawLines",lSDL_RenderDrawLines);
  lua_register(L,"SDL_RenderDrawPoint",lSDL_RenderDrawPoint);
  lua_register(L,"SDL_RenderDrawPoints",lSDL_RenderDrawPoints);
  lua_register(L,"SDL_RenderDrawRect",lSDL_RenderDrawRect);
  lua_register(L,"SDL_RenderDrawRects",lSDL_RenderDrawRects);
  lua_register(L,"SDL_RenderFillRect",lSDL_RenderFillRect);
  lua_register(L,"SDL_RenderFillRects",lSDL_RenderFillRects);
  lua_register(L,"SDL_RenderGetClipRect",lSDL_RenderGetClipRect);
  lua_register(L,"SDL_RenderGetD3D9Device",lSDL_RenderGetD3D9Device);
  lua_register(L,"SDL_RenderGetIntegerScale",lSDL_RenderGetIntegerScale);
  lua_register(L,"SDL_RenderGetLogicalSize",lSDL_RenderGetLogicalSize);
  lua_register(L,"SDL_RenderGetScale",lSDL_RenderGetScale);
  lua_register(L,"SDL_RenderGetViewport",lSDL_RenderGetViewport);
  lua_register(L,"SDL_RenderIsClipEnabled",lSDL_RenderIsClipEnabled);
  lua_register(L,"SDL_RenderPresent",lSDL_RenderPresent);
  lua_register(L,"SDL_RenderReadPixels",lSDL_RenderReadPixels);
  lua_register(L,"SDL_RenderSetClipRect",lSDL_RenderSetClipRect);
  lua_register(L,"SDL_RenderSetIntegerScale",lSDL_RenderSetIntegerScale);
  lua_register(L,"SDL_RenderSetLogicalSize",lSDL_RenderSetLogicalSize);
  lua_register(L,"SDL_RenderSetScale",lSDL_RenderSetScale);
  lua_register(L,"SDL_RenderSetViewport",lSDL_RenderSetViewport);
  lua_register(L,"SDL_RenderTargetSupported",lSDL_RenderTargetSupported);
  lua_register(L,"SDL_ReportAssertion",lSDL_ReportAssertion);
  lua_register(L,"SDL_ResetAssertionReport",lSDL_ResetAssertionReport);
  lua_register(L,"SDL_RestoreWindow",lSDL_RestoreWindow);
  lua_register(L,"SDL_SaveAllDollarTemplates",lSDL_SaveAllDollarTemplates);
  lua_register(L,"SDL_SaveBMP_RW",lSDL_SaveBMP_RW);
  lua_register(L,"SDL_SaveDollarTemplate",lSDL_SaveDollarTemplate);
  lua_register(L,"SDL_SemPost",lSDL_SemPost);
  lua_register(L,"SDL_SemTryWait",lSDL_SemTryWait);
  lua_register(L,"SDL_SemValue",lSDL_SemValue);
  lua_register(L,"SDL_SemWait",lSDL_SemWait);
  lua_register(L,"SDL_SemWaitTimeout",lSDL_SemWaitTimeout);
  lua_register(L,"SDL_SetAssertionHandler",lSDL_SetAssertionHandler);
  lua_register(L,"SDL_SetClipRect",lSDL_SetClipRect);
  lua_register(L,"SDL_SetClipboardText",lSDL_SetClipboardText);
  lua_register(L,"SDL_SetColorKey",lSDL_SetColorKey);
  lua_register(L,"SDL_SetCursor",lSDL_SetCursor);
  lua_register(L,"SDL_SetError",lSDL_SetError);
  lua_register(L,"SDL_SetEventFilter",lSDL_SetEventFilter);
  lua_register(L,"SDL_SetHint",lSDL_SetHint);
  lua_register(L,"SDL_SetHintWithPriority",lSDL_SetHintWithPriority);
  lua_register(L,"SDL_SetMainReady",lSDL_SetMainReady);
  lua_register(L,"SDL_SetModState",lSDL_SetModState);
  lua_register(L,"SDL_SetPaletteColors",lSDL_SetPaletteColors);
  lua_register(L,"SDL_SetPixelFormatPalette",lSDL_SetPixelFormatPalette);
  lua_register(L,"SDL_SetRelativeMouseMode",lSDL_SetRelativeMouseMode);
  lua_register(L,"SDL_SetRenderDrawBlendMode",lSDL_SetRenderDrawBlendMode);
  lua_register(L,"SDL_SetRenderDrawColor",lSDL_SetRenderDrawColor);
  lua_register(L,"SDL_SetRenderTarget",lSDL_SetRenderTarget);
  lua_register(L,"SDL_SetSurfaceAlphaMod",lSDL_SetSurfaceAlphaMod);
  lua_register(L,"SDL_SetSurfaceBlendMode",lSDL_SetSurfaceBlendMode);
  lua_register(L,"SDL_SetSurfaceColorMod",lSDL_SetSurfaceColorMod);
  lua_register(L,"SDL_SetSurfacePalette",lSDL_SetSurfacePalette);
  lua_register(L,"SDL_SetSurfaceRLE",lSDL_SetSurfaceRLE);
  lua_register(L,"SDL_SetTextInputRect",lSDL_SetTextInputRect);
  lua_register(L,"SDL_SetTextureAlphaMod",lSDL_SetTextureAlphaMod);
  lua_register(L,"SDL_SetTextureBlendMode",lSDL_SetTextureBlendMode);
  lua_register(L,"SDL_SetTextureColorMod",lSDL_SetTextureColorMod);
  lua_register(L,"SDL_SetThreadPriority",lSDL_SetThreadPriority);
  lua_register(L,"SDL_SetWindowBordered",lSDL_SetWindowBordered);
  lua_register(L,"SDL_SetWindowBrightness",lSDL_SetWindowBrightness);
  lua_register(L,"SDL_SetWindowData",lSDL_SetWindowData);
  lua_register(L,"SDL_SetWindowDisplayMode",lSDL_SetWindowDisplayMode);
  lua_register(L,"SDL_SetWindowFullscreen",lSDL_SetWindowFullscreen);
  lua_register(L,"SDL_SetWindowGammaRamp",lSDL_SetWindowGammaRamp);
  lua_register(L,"SDL_SetWindowGrab",lSDL_SetWindowGrab);
  lua_register(L,"SDL_SetWindowHitTest",lSDL_SetWindowHitTest);
  lua_register(L,"SDL_SetWindowIcon",lSDL_SetWindowIcon);
  lua_register(L,"SDL_SetWindowInputFocus",lSDL_SetWindowInputFocus);
  lua_register(L,"SDL_SetWindowMaximumSize",lSDL_SetWindowMaximumSize);
  lua_register(L,"SDL_SetWindowMinimumSize",lSDL_SetWindowMinimumSize);
  lua_register(L,"SDL_SetWindowModalFor",lSDL_SetWindowModalFor);
  lua_register(L,"SDL_SetWindowOpacity",lSDL_SetWindowOpacity);
  lua_register(L,"SDL_SetWindowPosition",lSDL_SetWindowPosition);
  lua_register(L,"SDL_SetWindowResizable",lSDL_SetWindowResizable);
  lua_register(L,"SDL_SetWindowSize",lSDL_SetWindowSize);
  lua_register(L,"SDL_SetWindowTitle",lSDL_SetWindowTitle);
  lua_register(L,"SDL_SetWindowsMessageHook",lSDL_SetWindowsMessageHook);
  lua_register(L,"SDL_ShowCursor",lSDL_ShowCursor);
  lua_register(L,"SDL_ShowMessageBox",lSDL_ShowMessageBox);
  lua_register(L,"SDL_ShowSimpleMessageBox",lSDL_ShowSimpleMessageBox);
  lua_register(L,"SDL_ShowWindow",lSDL_ShowWindow);
  lua_register(L,"SDL_SoftStretch",lSDL_SoftStretch);
  lua_register(L,"SDL_StartTextInput",lSDL_StartTextInput);
  lua_register(L,"SDL_StopTextInput",lSDL_StopTextInput);
  lua_register(L,"SDL_Swap16",lSDL_Swap16);
  lua_register(L,"SDL_Swap32",lSDL_Swap32);
  lua_register(L,"SDL_Swap64",lSDL_Swap64);
  lua_register(L,"SDL_SwapFloat",lSDL_SwapFloat);
  lua_register(L,"SDL_TLSCreate",lSDL_TLSCreate);
  lua_register(L,"SDL_TLSGet",lSDL_TLSGet);
  lua_register(L,"SDL_TLSSet",lSDL_TLSSet);
  lua_register(L,"SDL_ThreadID",lSDL_ThreadID);
  lua_register(L,"SDL_TryLockMutex",lSDL_TryLockMutex);
  lua_register(L,"SDL_UnionRect",lSDL_UnionRect);
  lua_register(L,"SDL_UnloadObject",lSDL_UnloadObject);
  lua_register(L,"SDL_UnlockAudio",lSDL_UnlockAudio);
  lua_register(L,"SDL_UnlockAudioDevice",lSDL_UnlockAudioDevice);
  lua_register(L,"SDL_UnlockMutex",lSDL_UnlockMutex);
  lua_register(L,"SDL_UnlockSurface",lSDL_UnlockSurface);
  lua_register(L,"SDL_UnlockTexture",lSDL_UnlockTexture);
  lua_register(L,"SDL_UnregisterApp",lSDL_UnregisterApp);
  lua_register(L,"SDL_UpdateTexture",lSDL_UpdateTexture);
  lua_register(L,"SDL_UpdateWindowSurface",lSDL_UpdateWindowSurface);
  lua_register(L,"SDL_UpdateWindowSurfaceRects",lSDL_UpdateWindowSurfaceRects);
  lua_register(L,"SDL_UpdateYUVTexture",lSDL_UpdateYUVTexture);
  lua_register(L,"SDL_UpperBlit",lSDL_UpperBlit);
  lua_register(L,"SDL_UpperBlitScaled",lSDL_UpperBlitScaled);
  lua_register(L,"SDL_VideoInit",lSDL_VideoInit);
  lua_register(L,"SDL_VideoQuit",lSDL_VideoQuit);
  lua_register(L,"SDL_WaitEvent",lSDL_WaitEvent);
  lua_register(L,"SDL_WaitEventTimeout",lSDL_WaitEventTimeout);
  lua_register(L,"SDL_WaitThread",lSDL_WaitThread);
  lua_register(L,"SDL_WarpMouseGlobal",lSDL_WarpMouseGlobal);
  lua_register(L,"SDL_WarpMouseInWindow",lSDL_WarpMouseInWindow);
  lua_register(L,"SDL_WasInit",lSDL_WasInit);
  lua_register(L,"SDL_WriteBE16",lSDL_WriteBE16);
  lua_register(L,"SDL_WriteBE32",lSDL_WriteBE32);
  lua_register(L,"SDL_WriteBE64",lSDL_WriteBE64);
  lua_register(L,"SDL_WriteLE16",lSDL_WriteLE16);
  lua_register(L,"SDL_WriteLE32",lSDL_WriteLE32);
  lua_register(L,"SDL_WriteLE64",lSDL_WriteLE64);
  lua_register(L,"SDL_WriteU8",lSDL_WriteU8);
  lua_register(L,"SDL_abs",lSDL_abs);
  lua_register(L,"SDL_acos",lSDL_acos);
  lua_register(L,"SDL_asin",lSDL_asin);
  lua_register(L,"SDL_atan",lSDL_atan);
  lua_register(L,"SDL_atan2",lSDL_atan2);
  lua_register(L,"SDL_atof",lSDL_atof);
  lua_register(L,"SDL_atoi",lSDL_atoi);
  lua_register(L,"SDL_calloc",lSDL_calloc);
  lua_register(L,"SDL_ceil",lSDL_ceil);
  lua_register(L,"SDL_copysign",lSDL_copysign);
  lua_register(L,"SDL_cos",lSDL_cos);
  lua_register(L,"SDL_cosf",lSDL_cosf);
  lua_register(L,"SDL_fabs",lSDL_fabs);
  lua_register(L,"SDL_floor",lSDL_floor);
  lua_register(L,"SDL_free",lSDL_free);
  lua_register(L,"SDL_getenv",lSDL_getenv);
  lua_register(L,"SDL_iconv",lSDL_iconv);
  lua_register(L,"SDL_iconv_close",lSDL_iconv_close);
  lua_register(L,"SDL_iconv_open",lSDL_iconv_open);
  lua_register(L,"SDL_iconv_string",lSDL_iconv_string);
  lua_register(L,"SDL_isdigit",lSDL_isdigit);
  lua_register(L,"SDL_isspace",lSDL_isspace);
  lua_register(L,"SDL_itoa",lSDL_itoa);
  lua_register(L,"SDL_lltoa",lSDL_lltoa);
  lua_register(L,"SDL_log",lSDL_log);
  lua_register(L,"SDL_ltoa",lSDL_ltoa);
  lua_register(L,"SDL_malloc",lSDL_malloc);
  lua_register(L,"SDL_memcmp",lSDL_memcmp);
  lua_register(L,"SDL_memcpy",lSDL_memcpy);
  lua_register(L,"SDL_memcpy4",lSDL_memcpy4);
  lua_register(L,"SDL_memmove",lSDL_memmove);
  lua_register(L,"SDL_memset",lSDL_memset);
  lua_register(L,"SDL_memset4",lSDL_memset4);
  lua_register(L,"SDL_pow",lSDL_pow);
  lua_register(L,"SDL_qsort",lSDL_qsort);
  lua_register(L,"SDL_realloc",lSDL_realloc);
  lua_register(L,"SDL_scalbn",lSDL_scalbn);
  lua_register(L,"SDL_setenv",lSDL_setenv);
  lua_register(L,"SDL_sin",lSDL_sin);
  lua_register(L,"SDL_sinf",lSDL_sinf);
  lua_register(L,"SDL_snprintf",lSDL_snprintf);
  lua_register(L,"SDL_sqrt",lSDL_sqrt);
  lua_register(L,"SDL_sqrtf",lSDL_sqrtf);
  lua_register(L,"SDL_sscanf",lSDL_sscanf);
  lua_register(L,"SDL_strcasecmp",lSDL_strcasecmp);
  lua_register(L,"SDL_strchr",lSDL_strchr);
  lua_register(L,"SDL_strcmp",lSDL_strcmp);
  lua_register(L,"SDL_strdup",lSDL_strdup);
  lua_register(L,"SDL_strlcat",lSDL_strlcat);
  lua_register(L,"SDL_strlcpy",lSDL_strlcpy);
  lua_register(L,"SDL_strlen",lSDL_strlen);
  lua_register(L,"SDL_strlwr",lSDL_strlwr);
  lua_register(L,"SDL_strncasecmp",lSDL_strncasecmp);
  lua_register(L,"SDL_strncmp",lSDL_strncmp);
  lua_register(L,"SDL_strrchr",lSDL_strrchr);
  lua_register(L,"SDL_strrev",lSDL_strrev);
  lua_register(L,"SDL_strstr",lSDL_strstr);
  lua_register(L,"SDL_strtod",lSDL_strtod);
  lua_register(L,"SDL_strtol",lSDL_strtol);
  lua_register(L,"SDL_strtoll",lSDL_strtoll);
  lua_register(L,"SDL_strtoul",lSDL_strtoul);
  lua_register(L,"SDL_strtoull",lSDL_strtoull);
  lua_register(L,"SDL_strupr",lSDL_strupr);
  lua_register(L,"SDL_tan",lSDL_tan);
  lua_register(L,"SDL_tanf",lSDL_tanf);
  lua_register(L,"SDL_tolower",lSDL_tolower);
  lua_register(L,"SDL_toupper",lSDL_toupper);
  lua_register(L,"SDL_uitoa",lSDL_uitoa);
  lua_register(L,"SDL_ulltoa",lSDL_ulltoa);
  lua_register(L,"SDL_ultoa",lSDL_ultoa);
  lua_register(L,"SDL_utf8strlcpy",lSDL_utf8strlcpy);
  lua_register(L,"SDL_vsnprintf",lSDL_vsnprintf);
  lua_register(L,"SDL_vsscanf",lSDL_vsscanf);
  lua_register(L,"SDL_wcslcat",lSDL_wcslcat);
  lua_register(L,"SDL_wcslcpy",lSDL_wcslcpy);
  lua_register(L,"SDL_wcslen",lSDL_wcslen);
  return 0;
};

int luaopen_lSDL(lua_State*L){
  return lSDL_openlibs(L);
};

