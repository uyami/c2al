local baseH5 = [[
    <!--DOVTYPE html-->
    <html>
    <head>
     </head>
    <body>
    <div id='top' style="white-space: pre">
%s
    </div>
        </body>
    </html>
]]


local out = io.tmpfile();
local line = 1;
for v in io.lines(arg[1])do
    line = line + 1;
    local s = v:gsub('<','&lt;');
    s = s:gsub('>','&gt;');
    local div;
    if(v:sub(1,1)=='@')then
        local len = v:find(' ');
        div = string.format([[<div id="_%s">]],v:sub(2,len-1));
        out:write(div);
    end
    local offset = 2;
    local begin = 1;
    while(true)do
        offset = s:find('@',offset);
        if(offset)then
            local len = s:find(' ',offset);
            local href = '<a href="#_%s">' .. s:sub(offset,len-1) .. "</a>";
            out:write(s:sub(begin,offset-1));
            out:write(string.format(href,s:sub(offset+1,len-1)));
            begin = len+1;
            offset = len;
        else
            break;
        end
    end
    if(div)then
        out:write('</div>');
    end
    out:write(s:sub(begin));
    out:write('\n')
end

out:seek('set');
local text = out:read('*a');
out = io.open(arg[2],'w+');
out:write(string.format(baseH5,text));


